﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
[System.Serializable]
public class Heal : Value {    
    public Heal(float RawHeal, bool Crit, int applyerID,int targetID, CR Type,bool TraceBack) {
        this.Amount = HealHPCalculation(RawHeal);
        this.Crit = Crit;
        this.applyerID = applyerID;
        this.targetID = targetID;
        this.Type = Type;
        this.TraceBack = TraceBack;
    }

    private float HealHPCalculation(float RawHeal) {
        return Mathf.Ceil(RawHeal);
    }
}

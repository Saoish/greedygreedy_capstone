﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
[System.Serializable]
public abstract class Value {
    public float Amount = 0;
    public bool Crit = false;
    public int applyerID = 0;
    public int targetID = 0;
    public CR Type;
    public bool TraceBack = true;
}

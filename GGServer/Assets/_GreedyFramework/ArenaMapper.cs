﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
[System.Serializable]
public static class ArenaMapper {
    public static Dictionary<SceneID, Dictionary<Side, Vector2>> _mapper = new Dictionary<SceneID, Dictionary<Side, Vector2>>() {
        { SceneID.RootOfEvil, new Dictionary<Side, Vector2> {{Side.Blue,new Vector2(-2.8f,-1.6f)},{Side.Red,new Vector2(3,0)}}},
        { SceneID.MysteriousWell, new Dictionary<Side, Vector2> {{Side.Blue,new Vector2(-4.339f, -0.608f) },{Side.Red,new Vector2(5.595f, -0.608f) }}}
    }; 
    

    public static Vector2 GetPos(SceneID SID,Side Side) {        
        return _mapper[SID][Side];
    }

}

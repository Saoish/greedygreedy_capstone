﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;

using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text.RegularExpressions;

namespace Networking.Data {    
    public static class NetworkIDChecker {
        public static int Dynamic_Start = -1000;
        public static int Static_Start = -1;
        public static int Player_Start = 1;
        public static bool IsDynamic(int NetworkID) {
            return NetworkID <= Dynamic_Start;
        }
        public static bool IsStatic(int NetworkID) {
            return NetworkID <= Static_Start && NetworkID > Dynamic_Start;
        }
        public static bool IsPlayer(int NetwokrID) {
            return NetwokrID >= Player_Start;
        }
    }

    public static class Protocols {
        //Server Protocols
        public static string RegisterUser = "RegisterUser";
        public static string UserLogin = "UserLogin";
        public static string RequestGraveState = "RequestGraveState";
        public static string CreateCharacter = "CreateCharacter";
        public static string DeleteCharacter = "DeleteCharacter";
        public static string SubscribeIdentityAndInstance = "SubscribeIdentityAndInstance";
        public static string SaveLogOffState = "SaveLogOffState";
        public static string UpdateLastLogableSC = "UpdateLastLogableSC";

        public static string AddExp = "AddExp";
        public static string AddtoInventoryAction = "AddtoInventoryAction";
        public static string RemoveFromInventoryAction = "RemoveFromInventoryAction";
        public static string SetActiveSkillAction = "SetActiveSkillAction";
        public static string Switch_I2S_Action = "Switch_I2S_Action";
        public static string Switch_S2I_Action = "Switch_S2I_Action";
        public static string SellAction = "SellAction";
        public static string BuyAction = "BuyAction";
        public static string SwitchControlAnalog = "SwitchControlAnalog";

        public static string Queue = "Queue";
        public static string Drop = "Drop";
        public static string AcceptMatchMakingEntrance = "AcceptMatchMakingEntrance";
        public static string DenyMatchMakingEntrance = "DenyMatchMakingEntrance";
        public static string CreateSummonRequest = "CreateSummonRequest";
        public static string AcceptSummonRequest = "AcceptSummonRequest";        

        public static string LoadInstanceState = "LoadInstanceState";
        public static string AddLoadedClient = "AddLoadedClient";
        public static string AbandInstanceAndForwardScene = "AbandInstanceAndForwardScene";        
        public static string ReturnOwnInstance = "ReturnOwnInstance";

        public static string UpdateCurrHealth = "UpdateCurrHealth";
        public static string UpdateCurrStats = "UpdateCurrStats";
        public static string UpdateToAliveState = "UpdateToAliveState";
        public static string UpdateToDeadState = "UpdateToDeadState";
        public static string ReloadInstanceState = "ReloadInstanceState";
        public static string UpdateInstanceCurretScene = "UpdateInstanceCurretScene";
        public static string UpdateCurrHealthWithCoordinate = "UpdateCurrHealthWithCoordinate";
        public static string SyncInstanceActiveMonsterCoordinates = "SyncInstanceActiveMonsterCoordinates";

       
        //Client Protocols
        public static string Ping = "Ping";       
        public static string Identify = "Identify";
        public static string CreateUsername = "CreateUsername";
        public static string PopUpNotify = "PopUpNotify";
        public static string TopNotify = "TopNotify";        
        public static string DisconnectByServer = "DisconnectByServer";                
        public static string LoadUserData = "LoadUserData";
        public static string LoadGraveState = "LoadGraveState";
        
        public static string LoadSceneWithSync = "LoadSceneWithSync";        
        public static string SyncLoadedScene = "SyncLoadedScene";        

        public static string PopQueueEntrance = "PopQueueEntrance";
        public static string SummonRequest = "SummonRequest";        

        public static string LoadPlayerIdentities = "LoadPlayerIdentities";
        public static string LogOffPlayer = "PlayerLogOff";
        public static string SetDominateState = "SetDominateState";
        public static string PresetMPSCThenUploadStates = "PresetMPSCThenUploadStates";

        public static string UploadStates = "UploadStates";
        public static string LoadJoiningStates = "LoadJoiningStates";
        public static string AddPlayer = "AddPlayer";

        /*
        Duplicated protocols
        */        
        public static string LevelUp = "LevelUp";
        public static string EquipAction = "EquipAction";
        public static string UnEquipAction = "UnEquipAction";
        public static string LvlUpSkillAction = "LvlUpSkillAction";
        public static string Respec = "Respec";

        public static string UpdateObjectMoveState = "UpdateObjectMoveState";
        public static string UpdateObjectAttackState = "UpdateObjectAttackState";
        public static string UpdateObjectDirection = "UpdateObjectDirection";
        public static string UpdateObjectPosition = "UpdateObjectPosition";
        public static string UpdateObjctDeadPosition = "UpdateObjctDeadPosition";

        public static string ObjectOnHealthGain = "ObjectOnHealthGain";
        public static string ObjectOnHealthLoss = "ObjectOnHealthLoss";
        public static string ObjectOnDeath = "ObjectOnDeath";
        public static string ReviveUponHelp = "ReviveUponHelp";

        public static string ObjectStartCasting = "ObjectStartCasting";
        public static string ObjectInterruptCasting = "ObjectInterruptCasting";

        public static string ObjectActiveSkill = "ObjectActiveSkill";
        public static string ObjectDeactiveSkill = "ObjectDeactiveSkill";
        public static string ObjectAddForce = "ObjectAddForce";
        public static string ObjectAddEffect = "ObjectAddEffect";
        public static string Launch = "Launch";

        public static string ObjectUpdateTarget = "ObjectUpdateTarget";

        public static string AddGrave = "AddGrave";
        public static string RemoveGrave = "RemoveGrave";

        public static string Infusion = "Infusion";
        public static string Defusion = "Defusion";        

        public static string ListeningForPacakage = "ListeningForPacakage";
    }

    [System.Serializable]
    public class BuyActionData {
        public int InventorySlot;
        public Equipment E;
        public BuyActionData(int InventorySlot,Equipment E) {
            this.InventorySlot = InventorySlot;
            this.E = E;
        }
    }

    [System.Serializable]
    public class InventoryStashSwitchingData {
        public int InventorySlot;
        public int StashSlot;
        public InventoryStashSwitchingData(int InventorySlot, int StashSlot) {
            this.InventorySlot = InventorySlot;
            this.StashSlot = StashSlot;
        }
    }

    [System.Serializable]    
    public class UpdateCurrHealthWithCoordinateData {
        public int NetworkID;
        public float CurrHealth;
        public Coordinate CD;
        public UpdateCurrHealthWithCoordinateData(int NetworkID, float CurrHealth, Vector2 Position) {
            this.NetworkID = NetworkID;
            this.CurrHealth = CurrHealth;
            this.CD = new Coordinate(Position);
        }        
    }        

    [System.Serializable]
    public class ID_Coordinates {
        public int NetworkID;
        public Coordinate CD;
        public ID_Coordinates(int NetworkID, Vector2 Position){
            this.NetworkID = NetworkID;
            this.CD = new Coordinate(Position);
        }            
    }

    [System.Serializable]
    public class SyncInstanceActiveMonsterCoordinatesData {
        public List<ID_Coordinates> ID_CDs;
        public SyncInstanceActiveMonsterCoordinatesData() {
            ID_CDs = new List<ID_Coordinates>();
        }
        public void Add(ID_Coordinates CD) {
            ID_CDs.Add(CD);
        }
        public int Count {
            get { return ID_CDs.Count; }
        }        
    }

    [System.Serializable]
    public class SpawnMonsterData {
        public int AttemptKey;
        public int Identity;
        public int SpawnIndex;
        public int SpawnLvl;                              
        public SpawnMonsterData(int AttemptKey,int Identity, int SpawnIndex,int SpawnLvl) {            
            this.AttemptKey = AttemptKey;
            this.Identity = Identity;
            this.SpawnIndex = SpawnIndex;
            this.SpawnLvl = SpawnLvl; 
        }
    }

    [System.Serializable]
    public class FusionData {
        public int PlayerNetworkID;
        public int PortalIdentity;
        public FusionData(int PlayerNetworkID, int PortalIdentity) {
            this.PlayerNetworkID = PlayerNetworkID;
            this.PortalIdentity = PortalIdentity;
        }
    }

    [System.Serializable]
    public class State {
        public Stats CurrStats;
        public bool Alive;
        public SceneCoordinate SC;
        public List<bool> InventoryVieweds;

        public State(Stats CurrStats, bool Alive, SceneCoordinate SC, List<bool> InventoryVieweds) {//These are all shallow copy
            this.CurrStats = CurrStats;
            this.Alive = Alive;
            this.SC = SC;
            this.InventoryVieweds = InventoryVieweds;
        }
    }

    [System.Serializable]
    public class UpdateToDeadStateData {
        public int TargetNetworkID;
        public SceneCoordinate RespawnSC;
        public UpdateToDeadStateData(int TargetNetworkID, SceneCoordinate RespawnSC) {
            this.TargetNetworkID = TargetNetworkID;
            this.RespawnSC = RespawnSC;           
        }
    }

    [System.Serializable]
    public class UpdateToAliveStateData {
        public int TargetNetworkID;
        public Stats CurrStats;
        public SceneCoordinate SC;
        
        public UpdateToAliveStateData(int TargetNetworkID, Stats CurrStats, SceneCoordinate SC) {
            this.TargetNetworkID = TargetNetworkID;
            this.CurrStats = CurrStats;
            this.SC = SC;
        }
    }    

    [System.Serializable]
    public class UpdateTargetData {
        public int TargetNetworkID;
        public int SetTargetNetworkID;
        public UpdateTargetData(int TargetNetworkID, int SetTargetNetworkID) {
            this.TargetNetworkID = TargetNetworkID;
            this.SetTargetNetworkID = SetTargetNetworkID;
        }
    }

    [System.Serializable]
    public class SummonRequestData {
        public LinkerKey LK;
        public string RequesterName;
        public SummonRequestData(LinkerKey LK, string RequesterName) {
            this.LK = LK;
            this.RequesterName = RequesterName;
        }
    }

    [System.Serializable]
    public class LinkerKey {
        public int RequesterID;
        public int TargetID;
        public LinkerKey(int RequesterID, int TargetID) {
            this.RequesterID = RequesterID;
            this.TargetID = TargetID;
        }

        public override int GetHashCode() {
            return RequesterID.GetHashCode() + TargetID.GetHashCode();
        }

        public override bool Equals(object obj) {
            var otheroj = obj as LinkerKey;
            if (otheroj == null)
                return false;
            else
                return RequesterID == otheroj.RequesterID && TargetID == otheroj.TargetID;
        }
    }

    [System.Serializable]
    public class UpdateCurrHealthData {
        public int TargetNetworkID;
        public float CurrHealth;
        public UpdateCurrHealthData(int TargetNetworkID, float CurrHealth) {
            this.TargetNetworkID = TargetNetworkID;
            this.CurrHealth = CurrHealth;
        }
    }

    [System.Serializable]
    public class UpdateCurrStatsData {        
        public int TargetNetworkID;
        public Stats CurrStats; 
        public UpdateCurrStatsData(int TargetNetworkID, Stats CurrStats) {            
            this.TargetNetworkID = TargetNetworkID;
            this.CurrStats = CurrStats;
        }
    }

    [System.Serializable]
    public class AddEffectData {
        public CR Type;
        public int ApplyerNetworkID;
        public int TargetNetworkID;
        public float Duration;
        public string[] ExtraParamaters;
        public AddEffectData(CR Type, int ApplyerNetworkID,int TargetNetworkID,float Duration, string[] ExtraParamaters) {
            this.Type = Type;
            this.ApplyerNetworkID = ApplyerNetworkID;
            this.TargetNetworkID = TargetNetworkID;
            this.Duration = Duration;
            this.ExtraParamaters = ExtraParamaters;
        }
        public AddEffectData(AddEffectData Copy) {
            this.Type = Copy.Type;
            this.ApplyerNetworkID = Copy.ApplyerNetworkID;
            this.TargetNetworkID = Copy.TargetNetworkID;
            this.Duration = Copy.Duration;
            this.ExtraParamaters = Copy.ExtraParamaters;            
        }

    }

    [System.Serializable]
    public class AddForceData {
        public int TargetNetworkID;
        public Vector2 StartPosition;
        public Vector2 Force;
        public GreedyForceMode ForceMode;        
        public AddForceData(int TargetNetworkID, Vector2 StartPosition, Vector2 Force, GreedyForceMode ForceMode) {            
            this.TargetNetworkID = TargetNetworkID;
            this.StartPosition = StartPosition;
            this.Force = Force;            
            this.ForceMode = ForceMode;
        }
    }

    [System.Serializable]
    public class SkillDeactivationData {
        public int TargetNetworkID;
        public CR Type;
        public float TimeOut;
        public SkillDeactivationData(int TargetNetworkID, CR Type, float TimeOut) {
            this.TargetNetworkID = TargetNetworkID;
            this.Type = Type;
            this.TimeOut = TimeOut;
        }
    }

    [System.Serializable]
    public class SkillActivationData {
        public int TargetNetworkID;
        public CR Type;
        public float[] Paramaters;
        public SkillActivationData(int TargetNetworkID, CR Type, float[] Paramaters) {
            this.TargetNetworkID = TargetNetworkID;
            this.Type = Type;
            this.Paramaters = Paramaters;
        }
    }

    [System.Serializable]
    public class StartCastingData {
        public int TargetNetworkID;
        public Vector2 TargetPosition;        
        public StartCastingData(int TargetNetworkID, Vector2 TargetPosition) {
            this.TargetNetworkID = TargetNetworkID;
            this.TargetPosition = TargetPosition;
        }
    }

    [System.Serializable]
    public class SetActiveSkillActionData {
        public int Slot;
        public CR Type;
        public SetActiveSkillActionData(int Slot, CR Type) {            
            this.Slot = Slot;
            this.Type = Type;
        }
    }

    [System.Serializable]
    public class LvlUpSkillData {
        public int TargetNetworkID;
        public int SkillIndex;
        public LvlUpSkillData(int TargetNetworkID, int SkillIndex) {
            this.TargetNetworkID = TargetNetworkID;
            this.SkillIndex = SkillIndex;
        }
    }

    [System.Serializable]
    public class TopNotifyData {
        public string message;
        public Color color;
        public float period;
        public TopNotifyData(string message, Color color, float period) {
            this.message = message;
            this.color = color;
            this.period = period;
        }
    }

    [System.Serializable]
    public class PositionData {
        public int TargetNetworkID;
        public Vector2 Position;
        public int Direction;
        public PositionData(int TargetNetworkID, Vector2 Position, int Direction) {
            this.TargetNetworkID = TargetNetworkID;
            this.Position = Position;
            this.Direction = Direction;
        }
    }

    [System.Serializable]
    public class DirectionData {
        public int TargetNetworkID;
        public int Direction;
        public DirectionData(int TargetNetworkID, int Direction) {
            this.TargetNetworkID = TargetNetworkID;
            this.Direction = Direction;
        }
    }

    [System.Serializable]
    public class AttackData {
        public int TargetNetworkID;
        public bool Attacking;
        public int Direction;
        public AttackData(int TargetNetworkID, bool Attacking, int Direction) {
            this.TargetNetworkID = TargetNetworkID;
            this.Attacking = Attacking;
            this.Direction = Direction;
        }
    }

    [System.Serializable]
    public class MovementData {
        public int TargetNetworkID;
        public bool Moving;
        public int Direction;
        public MovementData(int TargetNetworkID, bool Moving, int Direction) {
            this.TargetNetworkID = TargetNetworkID;
            this.Moving = Moving;
            this.Direction = Direction;
        }
    }

    [System.Serializable]
    public class UnEquipActionData {
        public int TargetNetworkID;
        public int Slot;
        public UnEquipActionData(int TargetNetworkID, int Slot) {
            this.TargetNetworkID = TargetNetworkID;
            this.Slot = Slot;
        }
    }

    [System.Serializable]
    public class EquipActionData {
        public int TargetNetworkID;
        public Equipment E;
        public EquipActionData(int TargetNetworkID, Equipment E) {
            this.TargetNetworkID = TargetNetworkID;
            this.E = E;
        }
    }


    [System.Serializable]
    public class RemoveFromInventoryData {
        public int Slot;
        public RemoveFromInventoryData(int Slot) {
            this.Slot = Slot;
        }
    }

    [System.Serializable]
    public class AddToInventoryData {
        public int Slot;
        public Equipment E;
        public AddToInventoryData(int Slot, Equipment E) {
            this.Slot = Slot;
            this.E = E;
        }
    }

    [System.Serializable]
    public class CreationData {
        public int SlotIndex;
        public RGB SkinColor;
        public string Name;
        public CLASS Class;
        public CreationData(int SlotIndex, RGB SkinColor, string Name, CLASS Class) {
            this.SlotIndex = SlotIndex;
            this.SkinColor = SkinColor;
            this.Name = Name;
            this.Class = Class;
        }
    }

    [System.Serializable]
    public class RGB {
        public float R;
        public float G;
        public float B;
        public RGB() { R = G = B = 1; }
        public RGB(float R, float G, float B) {
            this.R = R;
            this.G = G;
            this.B = B;
        }
    }

    [System.Serializable]
    public class Coordinate {
        public float x;
        public float y;
        public Coordinate(float x, float y) {
            this.x = x;
            this.y = y;
        }
        public Coordinate(Vector2 v) {
            this.x = v.x;
            this.y = v.y;
        }
        public Coordinate(Coordinate Copy) {
            this.x = Copy.x;
            this.y = Copy.y;
        }
        public Vector2 ToVector {
            get { return new Vector2(this.x, this.y); }
            set { this.x = value.x; this.y = value.y; }
        }
    }

    [System.Serializable]
    public class SceneCoordinate {
        public SceneID SID;        
        public Coordinate Coordinate;//Server cant serilize vector2
        public SceneCoordinate(SceneID SID, Vector2 Vector) {            
            this.SID = SID;            
            this.Coordinate = new Coordinate(Vector.x, Vector.y);            
        }
        public SceneCoordinate(SceneCoordinate Copy) {
            this.SID = Copy.SID;
            this.Coordinate = new Coordinate(Copy.Coordinate);
        }
    }


    [System.Serializable]
    public class Decipher {
        public string protocol;
        public string content;
        public int tail;

        public Decipher(string protocol, string content, int tail) {
            this.protocol = protocol;
            this.content = content;
            this.tail = tail;
        }
        public static Decipher None {
            get { return new Decipher(string.Empty, string.Empty, -1); }
        }
    }

    [System.Serializable]
    public class Package {        
        public byte[] buffer;
        public int size;
        public int length;
        public Decipher decipher;

        public Package(int length, Decipher decipher) {
            this.length = length;
            this.decipher = decipher;
            size = 0;            
        }
        public void Initialize() {
            buffer = new byte[length];
        }
    }

    public static class Serializer {
        public static byte[] Seal<T>(string protocol, T instance) {
            string integrated;
            if (typeof(T) == typeof(string) || typeof(T) == typeof(int) || typeof(T) == typeof(bool))
                integrated = protocol + "/" + instance;
            else
                integrated = protocol + "/" + JsonUtility.ToJson(instance);
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream()) {
                bf.Serialize(ms, integrated);
                return ms.ToArray();
            }
        }

        public static byte[] Seal(string protocol) {
            string integrated = protocol + "/";
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream()) {
                bf.Serialize(ms, integrated);
                return ms.ToArray();
            }
        }

        public static Decipher UnSeal(int tail, byte[] binary_data) {
            Stream stream = new MemoryStream(binary_data);
            BinaryFormatter f = new BinaryFormatter();
            string msg = f.Deserialize(stream).ToString();
            string[] msg_array = Regex.Split(msg, "/");
            return new Decipher(msg_array[0], msg_array[1], tail);
        }
        

        public static byte[] Serialize<T>(T instace) {
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream()) {
                bf.Serialize(ms, instace);
                return ms.ToArray();
            }
        }

        public static T DeSerialize<T>(byte[] arrBytes) {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            T instance = (T)binForm.Deserialize(memStream);
            return instance;
        }

        public static int SizeInBytes<T>(T instance) {
            string msg = JsonUtility.ToJson(instance);            
            return System.Text.ASCIIEncoding.Unicode.GetByteCount(msg);
        }


        //public static byte[] ObjectToByteArray(object obj) {
        //    if (obj == null)
        //        return null;
        //    BinaryFormatter bf = new BinaryFormatter();
        //    using (MemoryStream ms = new MemoryStream()) {
        //        bf.Serialize(ms, obj);
        //        return ms.ToArray();
        //    }
        //}

        //public static T ByteArrayToObject<T>(byte[] arrBytes) {
        //    MemoryStream memStream = new MemoryStream();
        //    BinaryFormatter binForm = new BinaryFormatter();
        //    memStream.Write(arrBytes, 0, arrBytes.Length);
        //    memStream.Seek(0, SeekOrigin.Begin);
        //    T obj = (T)binForm.Deserialize(memStream);
        //    return obj;
        //}
    }
}

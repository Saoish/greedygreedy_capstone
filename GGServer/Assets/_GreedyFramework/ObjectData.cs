﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using System;
using Networking.Data;


[System.Serializable]
public class ObjectData{//Always spawned with max stats, doent care about RespawnSC
    [HideInInspector]
    public Stats CurrStats = null;
    [HideInInspector]
    public OID OID;
    [HideInInspector]
    public SceneCoordinate SC;
    [HideInInspector]
    public bool Alive = true;
    [HideInInspector]
    public SceneCoordinate RespawnSC;
    [HideInInspector]
    public List<AddEffectData> OnGoingEffects = new List<AddEffectData>();
    [HideInInspector]
    public int Target;//Used for non player

    public int lvl = 1;    

    public ObjectData() {        
    }

    public ObjectData(ObjectData Copy) {
        this.CurrStats = new Stats(Copy.CurrStats);
        this.OID = Copy.OID;
        this.SC = new SceneCoordinate(Copy.SC);
        this.Alive = Copy.Alive;
        this.RespawnSC = Copy.RespawnSC;
        this.OnGoingEffects = new List<AddEffectData>(Copy.OnGoingEffects);//Always a copy, cant be reference, otherwise it will override defualt world state
        this.Target = Copy.Target;

        this.lvl = Copy.lvl;        
    }

    public ObjectData(OID OID,SceneCoordinate SC,int lvl) {        
        this.OID = OID;
        this.SC = SC;
        this.lvl = lvl;
        this.CurrStats = null/*new Stats(MaxStatsWithGrowth)*/;
    }
}

[System.Serializable]
public class StaticObjectData : ObjectData {//Most likely been used on world bosses and NPCs
    public SNID SNID;    
    public float SpawnCD;        
    public StaticObjectData(SNID SNID,float SpawnCD,SceneCoordinate RespawnSC){//For NPC only
        this.SNID = SNID;
        this.OID = OID.NPC;
        this.RespawnSC = RespawnSC;
        this.SC = new SceneCoordinate(RespawnSC);        
    }

    public StaticObjectData(SNID SNID,float SpawnCD,SceneCoordinate RespawnSC, int StaticLvl) {//For Monster Only
        this.SNID = SNID;
        this.OID = OID.EnemyMonster;
        this.RespawnSC = RespawnSC;
        this.SC = new SceneCoordinate(RespawnSC);
        this.lvl = StaticLvl;
    }

    public StaticObjectData(StaticObjectData Copy) : base(Copy) {
        this.SNID = Copy.SNID;
        this.SpawnCD = Copy.SpawnCD;
    }
}

[System.Serializable]
public class PlayerData : ObjectData {
    public int SlotIndex;
    public RGB SkinColor;
    public string Name;
    public CLASS Class;
    public int exp;   
    public int SkillPoints;
    public Stats BaseStats;    
    public List<int> SkillTreelvls;
    public List<CR> ActiveSlotData;
    public List<Equipment> Equipments;
    public List<Equipment> Inventory;
    public List<bool> InventoryViewSigns;
    

    //Setting
    //public List<CastSyncAxis> ActionSlotSyncAxisMapping;

    public PlayerData(PlayerData PD) :base(PD) {        
        this.SlotIndex = PD.SlotIndex;
        this.SkinColor = PD.SkinColor;
        this.Name = PD.Name;
        this.Class = PD.Class;
        this.exp = PD.exp;        
        this.SkillPoints = PD.SkillPoints;
        this.BaseStats = new Stats(PD.BaseStats);
        this.SkillTreelvls = new List<int>(PD.SkillTreelvls);
        this.ActiveSlotData = new List<CR>(PD.ActiveSlotData);
        this.Equipments = new List<Equipment>(PD.Equipments);
        this.Inventory = new List<Equipment>(PD.Inventory);
        this.InventoryViewSigns = new List<bool>(PD.InventoryViewSigns);
    }

    public PlayerData(int pre_lvl = 1) {
        this.CurrStats = null;
        this.OID = OID.Main;
        this.SC = new SceneCoordinate(SceneID.Tyneham, new Vector2(0.16f,4f));
        this.Alive = true;
        this.RespawnSC = new SceneCoordinate(SceneID.Tyneham, new Vector2(0.16f, 4f));

        SlotIndex = 0;
        SkinColor = new RGB();
        Name = "";
        Class = CLASS.Warrior;
        lvl = pre_lvl;        
        exp = 0;        
        SkillPoints = pre_lvl;
        BaseStats = new Stats(Stats.InitStatsType.OBJECT);
        SkillTreelvls = new List<int>();
        for(int i = 0; i < Patch.SkillTreeSize; i++) {
            SkillTreelvls.Add(0);
        }        
        ActiveSlotData = new List<CR>();
        for (int i = 0; i < Patch.SkillSlots; i++) {
            ActiveSlotData.Add(CR.None);
        }        
        Equipments = new List<Equipment>();
        for (int i = 0; i < Enum.GetValues(typeof(EQUIPTYPE)).Length; i++) {
            Equipments.Add(new Equipment());
        }        
        Inventory = new List<Equipment>();
        for (int i = 0; i < Patch.InventoryCapacity; i++) {
            Inventory.Add(new Equipment());
        }
        InventoryViewSigns = new List<bool>();
        for(int i = 0; i < Patch.InventoryCapacity; i++) {
            InventoryViewSigns.Add(false);
        }       
    }

    public void ResetSkillTreeData() {
        SkillTreelvls = new List<int>();
        for (int i = 0; i < Patch.SkillTreeSize; i++) {
            SkillTreelvls.Add(0);
        }
    }

    public void ResetActionSlotData() {
        ActiveSlotData = new List<CR>();
        for (int i = 0; i < Patch.SkillSlots; i++) {
            ActiveSlotData.Add(CR.None);
        }
    }
}

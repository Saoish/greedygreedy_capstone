﻿using UnityEngine;
using System.Collections;

public class Log : MonoBehaviour {
    string myLog;
    Queue myLogQueue = new Queue();
    private Vector2 scrollViewVector = Vector2.zero;
    private float vScrollbarValue;

    void OnEnable() {        
        Application.logMessageReceivedThreaded += HandleLog;        
    }

    void OnDisable() {
        Application.logMessageReceivedThreaded -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type) {
        myLog = logString;
        string newString = "\n [" + type + "] : " + myLog;
        myLogQueue.Enqueue(newString);
        if (type == LogType.Exception) {
            newString = "\n" + stackTrace;
            myLogQueue.Enqueue(newString);
        }
        myLog = string.Empty;
        foreach (string mylog in myLogQueue) {
            myLog += mylog;
        }
    }

    void OnGUI() {        
        scrollViewVector = GUILayout.BeginScrollView(scrollViewVector, GUILayout.MaxWidth(Screen.width), GUILayout.ExpandHeight(false));
        GUI.skin.label.fontSize = 50;
        GUILayout.Label(myLog, GUILayout.ExpandHeight(true));
        GUILayout.EndScrollView();
    }
}
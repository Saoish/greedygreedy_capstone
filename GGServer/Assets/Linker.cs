﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;


public static class Linker {
    public static Dictionary<LinkerKey, Link> _Links = new Dictionary<LinkerKey, Link>();    
         

    public static void EstablishSummonLink(LinkerKey LK) {        
        _Links[LK] = new SummonLink(LK);                    
    }

    public static void Accept(LinkerKey LK) {//Summonlink only
        ((SummonLink)_Links[LK]).Accept();
    }    

    public static void Remove(LinkerKey LK) {
        if (_Links.ContainsKey(LK))
            _Links.Remove(LK);        
    }
}

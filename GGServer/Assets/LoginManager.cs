﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Networking.Data;
using GreedyNameSpace;

[System.Serializable]
public class Identity {
    public string Username;
    public int SlotIndex = -1;//Unauthorized slotindex
    public SceneCoordinate LastLogableSC = null;    
    public Identity(string Username){
        this.Username = Username;                
    }
    public void CacheLastLogableSC(SceneCoordinate LastLogableSC) {
        this.LastLogableSC = LastLogableSC;
    }
}

public static class LoginManager {
    public static Dictionary<int, Identity> LoginClients = new Dictionary<int, Identity>();    

    public static void Broadcast<T>(string Protocol, T instance, int Channel = 0) {//To everyone who logined
        foreach (var c in LoginClients) {
            Server.Send(c.Key, Protocol, instance, Channel);
        }
    }

    public static void Add(int ClientID, string Username) {
        LoginClients.Add(ClientID, new Identity(Username));
    }

    public static void Remove(int ClientID) {
        if (LoginClients.ContainsKey(ClientID)) {
            Identity ID = LoginClients[ClientID];
            if (ID.SlotIndex != -1) {//DC
                if (ID.LastLogableSC != null)
                    DataBaseManager.GetLoginedPlayerData(ClientID).SC = ID.LastLogableSC;
            }
            LoginClients.Remove(ClientID);
        }
    }

    public static bool Logined(string Username) {
        foreach(var i in LoginClients) {//Bad
            if (i.Value.Username == Username)
                return true;
        }
        return false;
    }   
    
    public static string GetUsername(int ClientID) {
        return LoginClients[ClientID].Username;
    }

    public static int GetSlotIndex(int ClientID) {
        return LoginClients[ClientID].SlotIndex;
    }

    public static Identity GetIdentity(int ClientID) {
        return LoginClients[ClientID];
    }

    public static void UpdateLastLogableSC(int ClientID,SceneCoordinate LastLogableSC) {
        LoginClients[ClientID].LastLogableSC = LastLogableSC;
    }
}

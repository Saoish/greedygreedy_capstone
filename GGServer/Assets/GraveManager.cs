﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;
public static class GraveManager{//These grave information cross out all instance
    public static Dictionary<int ,GraveData> Collections = new Dictionary<int, GraveData>();

    public static void Add(GraveData GD) {
        Remove(GD.OwnerNetworkID);//Remove before adding, one client can only have one grave active at a time
        Collections[GD.OwnerNetworkID] = GD;        
        LoginManager.Broadcast(Protocols.AddGrave, GD, Server.TCP);
    }

    public static void Remove(int ClientID) {
        if (Collections.ContainsKey(ClientID)) {
            LoginManager.Broadcast(Protocols.RemoveGrave, ClientID, Server.TCP);
            Collections.Remove(ClientID);                        
        }
    }

    public static GraveData Get(int ClientID) {
        return Collections[ClientID];
    }

    public static void CreateSummonRequest(int RequesterID,int TargetID) {
        Remove(RequesterID);
        MatchMaker.Remove(RequesterID);
        if (Collections.ContainsKey(TargetID)) {
            Linker.EstablishSummonLink(new LinkerKey(RequesterID,TargetID));
        }
        else {                            
            Server.Send(RequesterID,Protocols.TopNotify, new TopNotifyData("Summoned ghost is lost...", MyColor.Yellow, 3f),Server.TCP);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Networking.Data;
using GreedyNameSpace;

public static class MatchMaker{        
    public static Dictionary<int, MatchMaking> QueuingClients = new Dictionary<int, MatchMaking>();

    public static List<SoloQueue_1v1> SoloQueue_1v1s = new List<SoloQueue_1v1>();
    public static List<SoloQueue_2v2> SoloQueue_2v2s = new List<SoloQueue_2v2>();

    static void QueUpForSoloQueue_1v1(int ClientID) {
        if (SoloQueue_1v1s.Count > 0) {
            int i = 0;
            do {
                if (!SoloQueue_1v1s[i].Full) {//Rate matching
                    SoloQueue_1v1s[i].Join(ClientID);
                    //Server.Send(ClientID, Protocols.TopNotify, new TopNotifyData("You have been queue up for Arena 1 v 1.", MyColor.Yellow, 3f), Server.TCP);
                    return;
                }
                else {
                    i++;
                }
            } while (i < SoloQueue_1v1s.Count);
        }
        SoloQueue_1v1s.Add(new SoloQueue_1v1(ClientID));
        //Server.Send(ClientID, Protocols.TopNotify, new TopNotifyData("You have been queue up for Arena 1 v 1.", MyColor.Yellow, 3f), Server.TCP);
    }

    static void QueUpForSoloQueue_2v2(int ClientID) {
        if (SoloQueue_2v2s.Count > 0) {
            int i = 0;
            do {
                if (!SoloQueue_2v2s[i].Full) {//Rate matching
                    SoloQueue_2v2s[i].Join(ClientID);                    
                    return;
                }
                else {
                    i++;
                }
            } while (i < SoloQueue_2v2s.Count);
        }
        SoloQueue_2v2s.Add(new SoloQueue_2v2(ClientID));        
    }

    public static void Queue(int ClientID, Queuemode QM) {
        if (QueuingClients.ContainsKey(ClientID)) {
            QueuingClients[ClientID].Remove(ClientID);
        }
        switch (QM) {
            case Queuemode.SoloQueue_1v1:
                QueUpForSoloQueue_1v1(ClientID);
                break;
            case Queuemode.SoloQueue_2v2:
                QueUpForSoloQueue_2v2(ClientID);
                break;
            default:
                Debug.Log("Unregistered Queuemode Error:::");
                break;
        }               
    }


    public static void Remove(int ClientID) {
        if (QueuingClients.ContainsKey(ClientID))
            QueuingClients[ClientID].Remove(ClientID);        
    }

    public static void AcceptEntrance(int ClientID) {
        if (QueuingClients.ContainsKey(ClientID))
            QueuingClients[ClientID].AcceptEntrance(ClientID);
    }

    public static void DenyEntrance(int ClientID) {
        if (QueuingClients.ContainsKey(ClientID))
            QueuingClients[ClientID].DenyEntrance(ClientID);
    }
}

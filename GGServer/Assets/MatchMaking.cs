﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Networking.Data;
using GreedyNameSpace;
[System.Serializable]
public class PlayerMatchState {
    public Side Side;    
    public bool Accepted;
    public PlayerMatchState(Side Side) {
        this.Side = Side;    
        Accepted = false;
    }
}

[System.Serializable]
public abstract class MatchMaking{
    //public abstract void Remove(int ClientID);
    //public abstract void AcceptEntrance(int ClientID);
    //public abstract void DenyEntrance(int ClientID);
    //public abstract void Join(int ClientID);













    protected int Rating;//Not yet used    

    protected Coroutine AcceptCheckingCO = null;
    protected Dictionary<int, PlayerMatchState> PlayersInMatch;

    //protected Side Occupied;
    protected SceneID Map;

    public bool Full { get { return PlayersInMatch.Count == Cap; } }
    public abstract int Cap { get; }

    //Must implement

    public void AcceptEntrance(int ClientID) {
        PlayersInMatch[ClientID].Accepted = true;
        if (AllAccepted) {            
            SendLoadWithSyncProtocolAndPrepareInstance();
        }
    }

    public void DenyEntrance(int ClientID) {
        Remove(ClientID);
    }

    public void Remove(int ClientID) {
        PlayersInMatch.Remove(ClientID);
        MatchMaker.QueuingClients.Remove(ClientID);
        if (PlayersInMatch.Count == 0) {
            if(this is SoloQueue_1v1)
                MatchMaker.SoloQueue_1v1s.Remove((SoloQueue_1v1)this);
            else if(this is SoloQueue_2v2)
                MatchMaker.SoloQueue_2v2s.Remove((SoloQueue_2v2)this);
        }
        //else {
        //    foreach (var p in PlayersInMatch) {
        //        Occupied = p.Value.Side;
        //    }
        //}
    }

    public void Join(int ClientID) {
        MatchMaker.QueuingClients[ClientID] = this;
        //switch (Occupied) {
        //    case Side.Blue:
        //        PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Red));
        //        break;
        //    case Side.Red:
        //        PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Blue));
        //        break;
        //}
        int Blues = 0;
        int Reds = 0;
        foreach (var P in PlayersInMatch) {
            if (P.Value.Side == Side.Blue)
                Blues++;
            else
                Reds++;
        }
        if (Blues > Reds)
            PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Red));
        else
            PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Blue));

        if (Full)
            SendEntrance();
    }

    private void SendEntrance() {
        if (AcceptCheckingCO != null)
            Server.instance.StopCoroutine(AcceptCheckingCO);
        foreach (int ClientID in PlayersInMatch.Keys) {
            Server.SendProtocol(ClientID, Protocols.PopQueueEntrance, Server.TCP);
        }
        AcceptCheckingCO = Server.instance.StartCoroutine(StartEntranceCountdownAndKick(10f));
    }
    public IEnumerator StartEntranceCountdownAndKick(float Countdown) {
        List<int> Keys = new List<int>(PlayersInMatch.Keys);
        foreach (int client_id in Keys) {
            if (PlayersInMatch.ContainsKey(client_id))
                PlayersInMatch[client_id].Accepted = false;//Defulat all accepted state
        }
        yield return new WaitForSeconds(Countdown);
        List<int> LeftKeys = new List<int>(PlayersInMatch.Keys);
        foreach (int client_id in LeftKeys) {//For someone quit the game while queue is poped
            if (PlayersInMatch[client_id].Accepted) {
                PlayersInMatch[client_id].Accepted = false;//Defulat all accepted state        
                Server.Send(client_id, Protocols.TopNotify, new TopNotifyData("There is a player denied the entrance, please wait for another queue...", MyColor.Yellow, 3f), Server.TCP);
            }
            else {
                Remove(client_id);
            }
        }
        AcceptCheckingCO = null;
    }

    private void SendLoadWithSyncProtocolAndPrepareInstance() {
        Instance InstanceToJoin = null;
        foreach (int ClientID in PlayersInMatch.Keys) {
            Server.Send(ClientID, Protocols.LoadSceneWithSync, Map, Server.TCP);
            InstanceManager.Remove(ClientID);
            if (InstanceToJoin == null)
                InstanceToJoin = InstanceManager.CreateAndReturnCreatedInstanceWithPreset(ClientID, new SceneCoordinate(Map, ArenaMapper.GetPos(Map, PlayersInMatch[ClientID].Side)));
            else {
                PlayerData ToJoinData = new PlayerData(DataBaseManager.GetLoginedPlayerData(ClientID));
                ToJoinData.SC = new SceneCoordinate(Map, ArenaMapper.GetPos(Map, PlayersInMatch[ClientID].Side));
                if (PlayersInMatch[ClientID].Side != PlayersInMatch[InstanceToJoin.Dominator].Side) {
                    InstanceManager.JoinAsEnemy(ClientID, ToJoinData, false, InstanceToJoin.Dominator);
                }
                else {
                    InstanceManager.JoinAsFriend(ClientID, ToJoinData, false, InstanceToJoin.Dominator);
                }
            }
        }
        Server.instance.StartCoroutine(RepeatChecking(InstanceToJoin));
    }

    IEnumerator RepeatChecking(Instance InstanceToJoin) {
        do {
            yield return new WaitForSeconds(1f);
        }
        while (InstanceToJoin.Fetching);
        do {
            yield return new WaitForSeconds(1f);
        } while (InstanceToJoin.UnderJoining);
        SendSyncWithLoadedSceneProtocolAndDiscardSelf();
    }


    private bool AllAccepted {
        get {
            if (PlayersInMatch.Count < Cap)
                return false;
            else {
                foreach (var p in PlayersInMatch)
                    if (!p.Value.Accepted)
                        return false;
                return true;
            }
        }
    }

    private void SendSyncWithLoadedSceneProtocolAndDiscardSelf() {
        foreach (var p in PlayersInMatch) {
            Server.SendProtocol(p.Key, Protocols.SyncLoadedScene, Server.TCP);
            MatchMaker.QueuingClients.Remove(p.Key);
        }
        PlayersInMatch.Clear();
        if(this is SoloQueue_1v1)
            MatchMaker.SoloQueue_1v1s.Remove((SoloQueue_1v1)this);
        else if (this is SoloQueue_2v2)
            MatchMaker.SoloQueue_2v2s.Remove((SoloQueue_2v2)this);
    }
}


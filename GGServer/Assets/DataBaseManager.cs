﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using GreedyNameSpace;


public static class DataBaseManager{    
    static public Dictionary<string, UserData> DataBase = new Dictionary<string, UserData>();

    public static void Load() {
        foreach (FileInfo f in new DirectoryInfo("Data").GetFiles()) {
            string username = f.Name.Substring(0, f.Name.Length - 4);
            StreamReader LoadStream = new StreamReader("Data/" + f.Name);
            string json = LoadStream.ReadToEnd();
            DataBase.Add(username, JsonUtility.FromJson<UserData>(json));
            LoadStream.Close();
        }        
    }

    public static void CreateUserProfile(string username) {//Create user on database and disconnect registration
        if(username == "1" || username == "2" || username == "3" || username == "4" )//presets
            DataBase.Add(username, new UserData(Patch.LvlCap));
        else
            DataBase.Add(username, new UserData(1));
        SaveUserProfile(username);
    }

    public static void SaveDataBase() {
        foreach(var user in DataBase) {
            SaveUserProfile(user.Key);
        }
    }

    private static void SaveUserProfile(string username) {
        StreamWriter SaveStream = new StreamWriter("Data/" + username + ".txt");
        string json = JsonUtility.ToJson(DataBase[username]);
        SaveStream.Write(json);
        SaveStream.Close();
    }

    public static bool HasUser(string username) {
        return DataBase.ContainsKey(username);
    }             

    public static PlayerData GetLoginedPlayerData(int ClientID) {//Warning: Required user are already subscribe to login manager        
        return DataBase[LoginManager.GetUsername(ClientID)].PlayerDatas[LoginManager.GetSlotIndex(ClientID)];
    }
    
    public static UserData GetLoginedUserData(int ClientID) {
        return DataBase[LoginManager.GetUsername(ClientID)];
    }
    
    public static void LevelUp(int ClientID) {
        PlayerData PD = GetLoginedPlayerData(ClientID);
        PD.lvl++;
        PD.exp = 0;
        PD.SkillPoints++;
        PD.BaseStats.Grow(Patch.GetGrowth(PD.Class));
        PD.CurrStats.Grow(Patch.GetGrowth(PD.Class));
    }
}

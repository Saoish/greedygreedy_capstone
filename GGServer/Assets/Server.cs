﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using GreedyNameSpace;
using Networking.Data;

public class Server : MonoBehaviour {
    Dictionary<string, Action<Decipher>> _Protocols = new Dictionary<string, Action<Decipher>>();
    void SetUp_Protocols() {
        //Unique Section
        _Protocols[Protocols.RegisterUser] = RegisterUser;
        _Protocols[Protocols.UserLogin] = UserLogin;
        _Protocols[Protocols.RequestGraveState] = RequestGraveState;
        _Protocols[Protocols.CreateCharacter] = CreateCharacter;
        _Protocols[Protocols.DeleteCharacter] = DeleteCharacter;
        _Protocols[Protocols.SubscribeIdentityAndInstance] = SubscribeIdentityAndInstance;
        _Protocols[Protocols.SaveLogOffState] = SaveLogOffState;
        _Protocols[Protocols.UpdateLastLogableSC] = UpdateLastLogableSC;

        _Protocols[Protocols.AddExp] = AddExp;
        _Protocols[Protocols.AddtoInventoryAction] = AddtoInventoryAction;
        _Protocols[Protocols.RemoveFromInventoryAction] = RemoveFromInventoryAction;
        _Protocols[Protocols.SetActiveSkillAction] = SetActiveSkillAction;
        _Protocols[Protocols.Switch_I2S_Action] = Switch_I2S_Action;
        _Protocols[Protocols.Switch_S2I_Action] = Switch_S2I_Action;
        _Protocols[Protocols.SellAction] = SellAction;
        _Protocols[Protocols.BuyAction] = BuyAction;
        _Protocols[Protocols.SwitchControlAnalog] = SwitchControlAnalog;

        _Protocols[Protocols.Queue] = Queue;
        _Protocols[Protocols.Drop] = Drop;
        _Protocols[Protocols.AcceptMatchMakingEntrance] = AcceptMatchMakingEntrance;
        _Protocols[Protocols.DenyMatchMakingEntrance] = DenyMatchMakingEntrance;
        _Protocols[Protocols.CreateSummonRequest] = CreateSummonRequest;
        _Protocols[Protocols.AcceptSummonRequest] = AcceptSummonRequest;

        _Protocols[Protocols.LoadInstanceState] = LoadInstanceState;
        _Protocols[Protocols.AddLoadedClient] = AddLoadedClient;
        _Protocols[Protocols.AbandInstanceAndForwardScene] = AbandInstanceAndForwardScene;
        _Protocols[Protocols.ReturnOwnInstance] = ReturnOwnInstance;

        _Protocols[Protocols.UpdateCurrHealth] = UpdateCurrHealth;
        _Protocols[Protocols.UpdateCurrStats] = UpdateCurrStats;
        _Protocols[Protocols.UpdateToAliveState] = UpdateToAliveState;
        _Protocols[Protocols.UpdateToDeadState] = UpdateToDeadState;
        _Protocols[Protocols.ReloadInstanceState] = ReloadInstanceState;
        _Protocols[Protocols.UpdateInstanceCurretScene] = UpdateInstanceCurretScene;
        _Protocols[Protocols.UpdateCurrHealthWithCoordinate] = UpdateCurrHealthWithCoordinate;
        _Protocols[Protocols.SyncInstanceActiveMonsterCoordinates] = SyncInstanceActiveMonsterCoordinates;

        //Duplicate Section
        _Protocols[Protocols.LevelUp] = LevelUp;
        _Protocols[Protocols.EquipAction] = EquipAction;
        _Protocols[Protocols.UnEquipAction] = UnEquipAction;
        _Protocols[Protocols.LvlUpSkillAction] = LvlUpSkillAction;
        _Protocols[Protocols.Respec] = Respec;

        _Protocols[Protocols.UpdateObjectMoveState] = UpdateObjectMoveState;
        _Protocols[Protocols.UpdateObjectAttackState] = UpdateObjectAttackState;
        _Protocols[Protocols.UpdateObjectDirection] = UpdateObjectDirection;
        _Protocols[Protocols.UpdateObjectPosition] = UpdateObjectPosition;
        _Protocols[Protocols.UpdateObjctDeadPosition] = UpdateObjctDeadPosition;

        _Protocols[Protocols.ObjectOnHealthGain] = ObjectOnHealthGain;
        _Protocols[Protocols.ObjectOnHealthLoss] = ObjectOnHealthLoss;
        _Protocols[Protocols.ObjectOnDeath] = ObjectOnDeath;
        _Protocols[Protocols.ReviveUponHelp] = ReviveUponHelp;

        _Protocols[Protocols.ObjectStartCasting] = ObjectStartCasting;
        _Protocols[Protocols.ObjectInterruptCasting] = ObjectInterruptCasting;

        _Protocols[Protocols.ObjectActiveSkill] = ObjectActiveSkill;
        _Protocols[Protocols.ObjectDeactiveSkill] = ObjectDeactiveSkill;
        _Protocols[Protocols.ObjectAddForce] = ObjectAddForce;        
        _Protocols[Protocols.ObjectAddEffect] = ObjectAddEffect;
        _Protocols[Protocols.Launch] = Launch;

        _Protocols[Protocols.ObjectUpdateTarget] = ObjectUpdateTarget;

        _Protocols[Protocols.AddGrave] = AddGrave;
        _Protocols[Protocols.RemoveGrave] = RemoveGrave;

        _Protocols[Protocols.Infusion] = Infusion;
        _Protocols[Protocols.Defusion] = Defusion;        

        _Protocols[Protocols.ListeningForPacakage] = ListeningForPacakage;
    }

    static float PingInterval = 5f;
    static float PingCounter = 0f;

    public static int TCP;
    public static int UDP;
    public static int PackageChannel;
    static int ServerID;

    static int maxConections = 100;//Server capacity
    static string ip = string.Empty;
    static int port = 34758;

    static Dictionary<int, Package> Packages = new Dictionary<int, Package>();

    public static Server instance;
    //---The following part for LAN only, should be deleted when it goes public
    //int BroadcastKey = 2222;
    //int BroadcastVersion = 22;
    //int BroadcastSubVersion = 33;
    //void SetUpCredentials() {
    //    byte error;
    //    NetworkTransport.SetBroadcastCredentials(HostID, BroadcastKey, BroadcastVersion, BroadcastSubVersion, out error);
    //}
    //void StartBroadcastingIP() {
    //    byte error;
    //    byte[] scream_buffer = Serializer.Serialize(ip);
    //    NetworkTransport.StartBroadcastDiscovery(HostID, port, BroadcastKey, BroadcastVersion, BroadcastSubVersion, scream_buffer, scream_buffer.Length, 1000, out error);
    //}

    //---The following part for LAN only, should be deleted when it goes public
    //public static Server instance;
    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            SetUp_Protocols();            
            DataBaseManager.Load();
            instance = this;
            DontDestroyOnLoad(this);
        }
    }

    private void OnApplicationQuit() {
        DataBaseManager.SaveDataBase();
    }
    
    void Start() {   
        Camera.main.aspect = 1920f / 1080f;
        NetworkTransport.Init();
        ConnectionConfig config = new ConnectionConfig();
        TCP = config.AddChannel(QosType.Reliable);
        UDP = config.AddChannel(QosType.Unreliable);
        PackageChannel = config.AddChannel(QosType.ReliableSequenced);
        HostTopology topology = new HostTopology(config, maxConections);
        ServerID = NetworkTransport.AddHost(topology, port);//Host       
        //NetworkTransport.SetPacketStat         
        ip = Network.player.ipAddress;
        Debug.Log("Server Initialized on ip: " + ip + " with port: " + port);
        //SetupLANCredentials();
        //StartBroadcastingIP();
    }

    // Update is called once per frame
    void Update() {
        Process();
        Pinging();
    }

    void Pinging() {
        if (PingCounter >= PingInterval) {
            foreach (int clientID in LoginManager.LoginClients.Keys) {
                Send(clientID, Protocols.Ping, NetworkTransport.GetNetworkTimestamp(), UDP);
            }            
            PingCounter = 0;            
        } else {
            PingCounter += Time.deltaTime;
        }
    }

    void Process() {
        int rec_hostID;
        int rec_connectionID;
        int rec_channelID;
        byte[] buffer = new byte[1024];
        int buffer_length;
        byte error;

        NetworkEventType networkEvent = NetworkEventType.DataEvent;
        do {
            networkEvent = NetworkTransport.Receive(out rec_hostID, out rec_connectionID, out rec_channelID, buffer, 1024, out buffer_length, out error);
            switch (networkEvent) {
                case NetworkEventType.ConnectEvent:// Server received disconnect event           
                    Debug.Log("Client " + rec_connectionID + ": Connected.");
                    break;
                case NetworkEventType.DataEvent:// Server received data                        
                    if (rec_channelID != PackageChannel) {
                        Decipher d = Serializer.UnSeal(rec_connectionID, buffer);
                        _Protocols[d.protocol](d);
                    }
                    else {
                        AppendBuffer(rec_connectionID, buffer, buffer_length);
                    }
                    break;
                case NetworkEventType.DisconnectEvent:// Client received disconnect                         
                    LogOffUser(rec_connectionID);
                    Debug.Log("Client " + rec_connectionID + ": Disconnected.");
                    break;
                    //case NetworkEventType.BroadcastEvent:
                    //    NetworkTransport.GetBroadcastConnectionMessage(rec_hostID, buffer, 1024, out buffer_length, out error);                        
                    //    break;
            }
        } while (networkEvent != NetworkEventType.Nothing);
    }

    private T Get<T>(string content) {        
        return JsonUtility.FromJson<T>(content);
    }

    private void AppendBuffer(int rec_connectionID, byte[] buffer, int dataSize) {
        System.Buffer.BlockCopy(buffer, 0, Packages[rec_connectionID].buffer, Packages[rec_connectionID].size, dataSize);
        Packages[rec_connectionID].size += dataSize;
        if (Packages[rec_connectionID].size == Packages[rec_connectionID].length) {
            _Protocols[Packages[rec_connectionID].decipher.protocol](Packages[rec_connectionID].decipher);
        }
    }

    static void _Send(int clientID, byte[] binary_data, int channel = 0) {//Default by TCP channel
        byte error;
        if (binary_data.Length > 1024) {
            Debug.Log("Not sent, this package contain size of MTU.");
        }
        NetworkTransport.Send(ServerID, clientID, channel, binary_data, binary_data.Length, out error);
    }


    public static void SendPacakage<T>(int clientID, T instance, Decipher decipher) {
        byte[] buffer = Serializer.Serialize(instance);
        byte[] listen_event = Serializer.Seal(Protocols.ListeningForPacakage, new Package(buffer.Length, decipher));
        _Send(clientID, listen_event);
        int round = Mathf.FloorToInt(buffer.Length / 1024);
        for (int i = 0; i < round; i++) {
            byte[] transmit = new byte[1024];
            System.Buffer.BlockCopy(buffer, 1024 * i, transmit, 0, 1024);
            _Send(clientID, transmit, PackageChannel);
        }
        int remain = buffer.Length - 1024 * round;
        if (remain > 0) {
            byte[] transmit = new byte[remain];
            System.Buffer.BlockCopy(buffer, 1024 * round, transmit, 0, remain);
            _Send(clientID, transmit, PackageChannel);
        }
    }


    void DisconnectClient(int clientID, string msg = "") {
        byte[] disconnectevent = Serializer.Seal(Protocols.DisconnectByServer, msg);
        _Send(clientID, disconnectevent);
        LogOffUser(clientID);
    }

    public static void SendProtocol(int ClientID, string Protocol, int Channel) {//TCP only
        byte[] data = Serializer.Seal(Protocol);
        _Send(ClientID, data, Channel);
    }

    public static void Send<T>(int ClientID, string Protocol, T instance, int Channel) {
        byte[] data = Serializer.Seal(Protocol, instance);
        _Send(ClientID, data, Channel);
    }


    void LogOffUser(int ClientID) {        
        MatchMaker.Remove(ClientID);
        InstanceManager.Remove(ClientID);
        GraveManager.Remove(ClientID);        
        LoginManager.Remove(ClientID);        
    }

    //Server Protocols
    private void RegisterUser(Decipher decipher) {
        if (DataBaseManager.HasUser(decipher.content)) {
            Send(decipher.tail, Protocols.PopUpNotify, "Username is already been registered, please pick another one.", Server.TCP);
        } else {
            DataBaseManager.CreateUserProfile(decipher.content);
            Send(decipher.tail, Protocols.CreateUsername, decipher.content, Server.TCP);
        }        
        DisconnectClient(decipher.tail);
    }

    private void UserLogin(Decipher decipher) {
        if (!DataBaseManager.HasUser(decipher.content)) {
            Send(decipher.tail, Protocols.PopUpNotify, "Can not find user data on server, if you don't have one, please use the Register Button to register one.", Server.TCP);
            DisconnectClient(decipher.tail);
        } else if (LoginManager.Logined(decipher.content)) {
            DisconnectClient(decipher.tail, "This account has already login.");
        } else {
            LoginManager.Add(decipher.tail, decipher.content);
            Send(decipher.tail, Protocols.Identify, decipher.tail, Server.TCP);
            DataBaseManager.DataBase[decipher.content].ResetToDefaultWorldStates();//For now
            SendPacakage(decipher.tail, DataBaseManager.DataBase[decipher.content], new Decipher(Protocols.LoadUserData, null, decipher.tail));
        }
    }

    private void RequestGraveState(Decipher decipher) {
        SendPacakage(decipher.tail, GraveManager.Collections, new Decipher(Protocols.LoadGraveState, null, decipher.tail));
    }

    private void CreateCharacter(Decipher decipher) {
        CreationData temp = JsonUtility.FromJson<CreationData>(decipher.content);
        string Username = LoginManager.GetUsername(decipher.tail);
        if (Username == "1" || Username == "2" || Username == "3" || Username == "4")//presets
            DataBaseManager.DataBase[Username].PlayerDatas[temp.SlotIndex] = new PlayerData(Patch.LvlCap);
        DataBaseManager.DataBase[Username].PlayerDatas[temp.SlotIndex].SlotIndex = temp.SlotIndex;
        DataBaseManager.DataBase[Username].PlayerDatas[temp.SlotIndex].SkinColor = temp.SkinColor;
        DataBaseManager.DataBase[Username].PlayerDatas[temp.SlotIndex].Name = temp.Name;
        DataBaseManager.DataBase[Username].PlayerDatas[temp.SlotIndex].Class = temp.Class;
        for (int i = 0; i < DataBaseManager.DataBase[Username].PlayerDatas[temp.SlotIndex].lvl; i++) {
            DataBaseManager.DataBase[Username].PlayerDatas[temp.SlotIndex].BaseStats.Grow(Patch.GetGrowth(temp.Class));
        }
        SendPacakage(decipher.tail, DataBaseManager.DataBase[Username], new Decipher(Protocols.LoadUserData, null, decipher.tail));
    }

    private void DeleteCharacter(Decipher decipher) {
        int Slot = int.Parse(decipher.content);
        string Username = LoginManager.GetUsername(decipher.tail);
        DataBaseManager.DataBase[Username].PlayerDatas[Slot] = new PlayerData();
        SendPacakage(decipher.tail, DataBaseManager.DataBase[Username], new Decipher(Protocols.LoadUserData, null, decipher.tail));
    }

    private void SubscribeIdentityAndInstance(Decipher decipher) {
        LoginManager.GetIdentity(decipher.tail).SlotIndex = int.Parse(decipher.content);        
        StartCoroutine(CreatingInstanceProgress(decipher.tail));
    } private IEnumerator CreatingInstanceProgress(int ClientID) {
        InstanceManager.Create(ClientID);
        while (InstanceManager.IsFetching(ClientID)) {
            yield return null;
        }
        SendProtocol(ClientID, Protocols.SyncLoadedScene, TCP);
    }

    private void SaveLogOffState(Decipher decipher) {        
        MatchMaker.Remove(decipher.tail);
        InstanceManager.Remove(decipher.tail);
        GraveManager.Remove(decipher.tail);

        if (LoginManager.GetIdentity(decipher.tail).SlotIndex != -1) {
            State temp = JsonUtility.FromJson<State>(decipher.content);
            DataBaseManager.GetLoginedPlayerData(decipher.tail).CurrStats = temp.CurrStats;
            DataBaseManager.GetLoginedPlayerData(decipher.tail).SC = temp.SC;
            DataBaseManager.GetLoginedPlayerData(decipher.tail).Alive = temp.Alive;
            DataBaseManager.GetLoginedPlayerData(decipher.tail).InventoryViewSigns = temp.InventoryVieweds;
            LoginManager.GetIdentity(decipher.tail).SlotIndex = -1;
        }  
    }

    private void UpdateLastLogableSC(Decipher decipher) {
        SceneCoordinate temp = Get<SceneCoordinate>(decipher.content);
        LoginManager.UpdateLastLogableSC(decipher.tail, temp);
    }

    private void AddExp(Decipher decipher) {
        DataBaseManager.GetLoginedPlayerData(decipher.tail).exp += int.Parse(decipher.content);
    }

    private void AddtoInventoryAction(Decipher decipher) {
        AddToInventoryData temp = JsonUtility.FromJson<AddToInventoryData>(decipher.content);
        Identity user = LoginManager.GetIdentity(decipher.tail);
        DataBaseManager.DataBase[user.Username].PlayerDatas[user.SlotIndex].Inventory[temp.Slot] = temp.E;
    }

    private void RemoveFromInventoryAction(Decipher decipher) {
        RemoveFromInventoryData temp = JsonUtility.FromJson<RemoveFromInventoryData>(decipher.content);
        Identity user = LoginManager.GetIdentity(decipher.tail);
        DataBaseManager.DataBase[user.Username].PlayerDatas[user.SlotIndex].Inventory[temp.Slot] = new Equipment();
    }

    private void SetActiveSkillAction(Decipher decipher) {
        SetActiveSkillActionData temp = JsonUtility.FromJson<SetActiveSkillActionData>(decipher.content);
        Identity user = LoginManager.GetIdentity(decipher.tail);
        DataBaseManager.DataBase[user.Username].PlayerDatas[user.SlotIndex].ActiveSlotData[temp.Slot] = temp.Type;
    }

    private void Switch_I2S_Action(Decipher decipher) {
        InventoryStashSwitchingData temp = Get<InventoryStashSwitchingData>(decipher.content);
        Equipment ToStash = DataBaseManager.GetLoginedPlayerData(decipher.tail).Inventory[temp.InventorySlot];
        DataBaseManager.GetLoginedUserData(decipher.tail).Stash[temp.StashSlot] = ToStash;
        DataBaseManager.GetLoginedPlayerData(decipher.tail).Inventory[temp.InventorySlot] = new Equipment();
    }

    private void Switch_S2I_Action(Decipher decipher) {
        InventoryStashSwitchingData temp = Get<InventoryStashSwitchingData>(decipher.content);
        Equipment ToInventory = DataBaseManager.GetLoginedUserData(decipher.tail).Stash[temp.StashSlot];
        DataBaseManager.GetLoginedPlayerData(decipher.tail).Inventory[temp.InventorySlot] = ToInventory;
        DataBaseManager.GetLoginedUserData(decipher.tail).Stash[temp.StashSlot] = new Equipment();
    }

    private void SellAction(Decipher decipher) {
        int InventorySlot = int.Parse(decipher.content);
        DataBaseManager.GetLoginedUserData(decipher.tail).Souls += DataBaseManager.GetLoginedPlayerData(decipher.tail).Inventory[InventorySlot].Value;
        DataBaseManager.GetLoginedPlayerData(decipher.tail).Inventory[InventorySlot] = new Equipment();
    }

    private void BuyAction(Decipher decipher) {
        BuyActionData temp = Get<BuyActionData>(decipher.content);
        DataBaseManager.GetLoginedUserData(decipher.tail).Souls -= temp.E.Value;
        temp.E.Value /= Patch.HeartlessRate;
        DataBaseManager.GetLoginedPlayerData(decipher.tail).Inventory[temp.InventorySlot] = temp.E;
    }
    
    private void SwitchControlAnalog(Decipher decipher) {
        int Slot = int.Parse(decipher.content);
        int PlayerSlot = DataBaseManager.GetLoginedPlayerData(decipher.tail).SlotIndex;
        DataBaseManager.GetLoginedUserData(decipher.tail).AxisMappings[PlayerSlot].Switch(Slot);
        //if (DataBaseManager.GetLoginedUserData(decipher.tail).AxisMappings[PlayerSlot].Switch(Slot);
        //    DataBaseManager.GetLoginedPlayerData(decipher.tail).ActionSlotSyncAxisMapping[Slot] = CastSyncAxis.Right;
        //else
        //    DataBaseManager.GetLoginedPlayerData(decipher.tail).ActionSlotSyncAxisMapping[Slot] = CastSyncAxis.Left;
    }

    private void Queue(Decipher decipher) {
        GraveManager.Remove(decipher.tail);
        MatchMaker.Queue(decipher.tail, Get<Queuemode>(decipher.content));
    }

    private void Drop(Decipher decipher) {
        MatchMaker.Remove(decipher.tail);
    }

    private void AcceptMatchMakingEntrance(Decipher decipher) {
        MatchMaker.AcceptEntrance(decipher.tail);
    }

    private void DenyMatchMakingEntrance(Decipher decipher) {
        MatchMaker.DenyEntrance(decipher.tail);
    }

    private void CreateSummonRequest(Decipher decipher) {
        GraveManager.CreateSummonRequest(decipher.tail, int.Parse(decipher.content));
    }

    private void AcceptSummonRequest(Decipher decipher) {
        Linker.Accept(Get<LinkerKey>(decipher.content));
    }

    private void LoadInstanceState(Decipher decipher) {
        Dictionary<int, ObjectData> temp = Serializer.DeSerialize<Dictionary<int, ObjectData>>(Packages[decipher.tail].buffer);
        InstanceManager.LoadStates(decipher.tail, temp);
        Packages.Remove(decipher.tail);
    }

    private void AddLoadedClient(Decipher decipher) {
        InstanceManager.AddLoadedClient(decipher.tail);
    }

    private void AbandInstanceAndForwardScene(Decipher decipher) {
        InstanceManager.Remove(decipher.tail, true);
        Instance New_Instance = InstanceManager.CreateAndReturnInstance(decipher.tail);
        StartCoroutine(CheckReadyAndForwardPrevScene(New_Instance, decipher.tail));
    } private IEnumerator CheckReadyAndForwardPrevScene(Instance InstanceToCheck, int ClientID) {
        while (InstanceToCheck.Fetching)
            yield return null;
        SendProtocol(ClientID, Protocols.SyncLoadedScene, TCP);
    }

    private void ReturnOwnInstance(Decipher decipher) {
        InstanceManager.Remove(decipher.tail);
        Instance New_Instance = InstanceManager.CreateAndReturnInstance(decipher.tail);
        StartCoroutine(CheckReadyAndForwardPrevScene(New_Instance, decipher.tail));
    }

    private void UpdateCurrHealth(Decipher decipher) {
        UpdateCurrHealthData temp = Get<UpdateCurrHealthData>(decipher.content);
        InstanceManager.UpdateCurrHealth(decipher.tail, temp.TargetNetworkID, temp.CurrHealth);
        if (temp.TargetNetworkID > 0) {
            DataBaseManager.GetLoginedPlayerData(temp.TargetNetworkID).CurrStats.Set(STATSTYPE.HEALTH, temp.CurrHealth);
        }
    }

    private void UpdateCurrStats(Decipher decipher) {
        UpdateCurrStatsData temp = Get<UpdateCurrStatsData>(decipher.content);
        InstanceManager.UpdateCurrStats(decipher.tail, temp.TargetNetworkID, temp.CurrStats);
        if (temp.TargetNetworkID > 0) {//Player
            DataBaseManager.GetLoginedPlayerData(temp.TargetNetworkID).CurrStats = temp.CurrStats;
        }
    }

    private void UpdateToAliveState(Decipher decipher) {
        UpdateToAliveStateData temp = Get<UpdateToAliveStateData>(decipher.content);
        InstanceManager.UpdateToAliveState(decipher.tail, temp.TargetNetworkID, temp.CurrStats, temp.SC);
        if (temp.TargetNetworkID > 0) {//Player
            DataBaseManager.GetLoginedPlayerData(temp.TargetNetworkID).Alive = true;
            DataBaseManager.GetLoginedPlayerData(temp.TargetNetworkID).CurrStats = temp.CurrStats;
            DataBaseManager.GetLoginedPlayerData(temp.TargetNetworkID).SC = temp.SC;            
        }
    }

    private void UpdateToDeadState(Decipher decipher) {
        UpdateToDeadStateData temp = Get<UpdateToDeadStateData>(decipher.content);
        InstanceManager.UpdateToDeadState(decipher.tail, temp.TargetNetworkID, temp.RespawnSC);
        if (temp.TargetNetworkID > 0) {//Player
            GraveManager.Remove(temp.TargetNetworkID);
            DataBaseManager.GetLoginedPlayerData(temp.TargetNetworkID).Alive = false;
            DataBaseManager.GetLoginedPlayerData(temp.TargetNetworkID).CurrStats.Health = 0f;
            DataBaseManager.GetLoginedPlayerData(temp.TargetNetworkID).RespawnSC = temp.RespawnSC;
        }
    }    

    private void ReloadInstanceState(Decipher decipher) {
        InstanceManager.ReloadInstanceState(decipher.tail);
    }

    private void UpdateInstanceCurretScene(Decipher decipher) {
        InstanceManager.UpdateInstanceCurretScene(decipher.tail, Get<SceneID>(decipher.content));
    }

    private void UpdateCurrHealthWithCoordinate(Decipher decipher) {//This protocol is used for detect roll back and update instance coordinate of monster                
        UpdateCurrHealthWithCoordinateData temp = Get<UpdateCurrHealthWithCoordinateData>(decipher.content);
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.UpdateObjectPosition, new PositionData(temp.NetworkID, temp.CD.ToVector,0/*Direction doesnt matter here*/), UDP);
        if (!NetworkIDChecker.IsDynamic(temp.NetworkID)) {//Worldstate monster
            InstanceManager.UpdateCurrHealth(decipher.tail, temp.NetworkID, temp.CurrHealth);
            InstanceManager.UpdatePosition(decipher.tail, temp.NetworkID, temp.CD.ToVector);            
        }        
    }

    private void SyncInstanceActiveMonsterCoordinates(Decipher decipher) {
        SyncInstanceActiveMonsterCoordinatesData temp = Serializer.DeSerialize<SyncInstanceActiveMonsterCoordinatesData>(Packages[decipher.tail].buffer);
        InstanceManager.SyncInstanceActiveMonsterPositions(decipher.tail, temp);        
    }

    //Duplicated Actions
    private void LevelUp(Decipher decipher) {
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.LevelUp, decipher.tail, TCP);
        InstanceManager.LevelUp(decipher.tail);
        DataBaseManager.LevelUp(decipher.tail);
    }

    private void EquipAction(Decipher decipher) {
        EquipActionData temp = JsonUtility.FromJson<EquipActionData>(decipher.content);
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.EquipAction, decipher.content);
        InstanceManager.GetInstancePlayerData(decipher.tail).Equipments[(int)temp.E.EquipType] = temp.E;
        DataBaseManager.GetLoginedPlayerData(decipher.tail).Equipments[(int)temp.E.EquipType] = temp.E;
    }

    private void UnEquipAction(Decipher decipher) {
        UnEquipActionData temp = JsonUtility.FromJson<UnEquipActionData>(decipher.content);
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.UnEquipAction, decipher.content);
        InstanceManager.GetInstancePlayerData(decipher.tail).Equipments[temp.Slot] = new Equipment();
        DataBaseManager.GetLoginedPlayerData(decipher.tail).Equipments[temp.Slot] = new Equipment();
    }

    private void LvlUpSkillAction(Decipher decipher) {
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.LvlUpSkillAction, decipher.content);
        LvlUpSkillData temp = JsonUtility.FromJson<LvlUpSkillData>(decipher.content);
        InstanceManager.GetInstancePlayerData(decipher.tail).SkillTreelvls[temp.SkillIndex]++;
        InstanceManager.GetInstancePlayerData(decipher.tail).SkillPoints--;
        DataBaseManager.GetLoginedPlayerData(decipher.tail).SkillTreelvls[temp.SkillIndex]++;
        DataBaseManager.GetLoginedPlayerData(decipher.tail).SkillPoints--;
    }

    private void Respec(Decipher decipher) {
        InstanceManager.ExclusivelyMulticastProtocol(decipher.tail, Protocols.Respec,TCP);
        InstanceManager.GetInstancePlayerData(decipher.tail).ResetSkillTreeData();
        InstanceManager.GetInstancePlayerData(decipher.tail).SkillPoints = InstanceManager.GetInstancePlayerData(decipher.tail).lvl;
        InstanceManager.GetInstancePlayerData(decipher.tail).ResetActionSlotData();
        DataBaseManager.GetLoginedPlayerData(decipher.tail).ResetSkillTreeData();
        DataBaseManager.GetLoginedPlayerData(decipher.tail).SkillPoints = DataBaseManager.GetLoginedPlayerData(decipher.tail).lvl;
        DataBaseManager.GetLoginedPlayerData(decipher.tail).ResetActionSlotData();
        DataBaseManager.GetLoginedUserData(decipher.tail).Souls -= DataBaseManager.GetLoginedPlayerData(decipher.tail).lvl * Patch.RespecCostIncRate;
    }

    private void UpdateObjectMoveState(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.UpdateObjectMoveState, decipher.content, UDP);
    }

    private void UpdateObjectAttackState(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.UpdateObjectAttackState, decipher.content, UDP);
    }

    private void UpdateObjectDirection(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.UpdateObjectDirection, decipher.content, UDP);
    }

    private void UpdateObjectPosition(Decipher decipher) {
        if (InstanceManager.Get(decipher.tail) != null) {
            InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.UpdateObjectPosition, decipher.content, UDP);
            PositionData temp = Get<PositionData>(decipher.content);
            InstanceManager.UpdatePosition(decipher.tail, temp.TargetNetworkID, temp.Position);
            if (temp.TargetNetworkID > 0)//Player
                DataBaseManager.GetLoginedPlayerData(decipher.tail).SC.Coordinate.ToVector = temp.Position;
        }
    }

    private void UpdateObjctDeadPosition(Decipher decipher) {//Simillar to above except it's using TCP and it hard sync by ignoring force
        if (InstanceManager.Get(decipher.tail) != null) {
            InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.UpdateObjctDeadPosition, decipher.content, TCP);
            PositionData temp = Get<PositionData>(decipher.content);
            InstanceManager.UpdatePosition(decipher.tail, temp.TargetNetworkID, temp.Position);
            if (temp.TargetNetworkID > 0)//Player
                DataBaseManager.GetLoginedPlayerData(decipher.tail).SC.Coordinate.ToVector = temp.Position;
        }
    }

    private void ObjectOnHealthGain(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.ObjectOnHealthGain, decipher.content, TCP);
    }

    private void ObjectOnHealthLoss(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.ObjectOnHealthLoss, decipher.content, TCP);
    }

    private void ObjectOnDeath(Decipher decipher) {
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.ObjectOnDeath, decipher.content, TCP);
    }

    private void ReviveUponHelp(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.ReviveUponHelp, decipher.content, TCP);
    }

    private void ObjectStartCasting(Decipher decipher) {
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.ObjectStartCasting, decipher.content, TCP);
    }

    private void ObjectInterruptCasting(Decipher decipher) {
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.ObjectInterruptCasting, decipher.content, TCP);
    }

    private void ObjectActiveSkill(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.ObjectActiveSkill, decipher.content, TCP);
    }

    private void ObjectDeactiveSkill(Decipher decipher) {
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.ObjectDeactiveSkill, decipher.content, TCP);
    }

    private void ObjectAddForce(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.ObjectAddForce, decipher.content, TCP);
    }

    private void ObjectAddEffect(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.ObjectAddEffect, decipher.content, TCP);
        if (SceneChecker.TargetInArenas(InstanceManager.GetCurrentScene(decipher.tail)))
            return;
        AddEffectData temp = Get<AddEffectData>(decipher.content);
        if (!NetworkIDChecker.IsDynamic(temp.TargetNetworkID))            
            InstanceManager.AddOnGoingEffect(decipher.tail, Get<AddEffectData>(decipher.content));
    }

    private void Launch(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.Launch, decipher.content,UDP);
    }

    private void ObjectUpdateTarget(Decipher decipher) {//Only the dominator will raise this event
        InstanceManager.ExclusivelyMulticast(decipher.tail, Protocols.ObjectUpdateTarget, decipher.content, TCP);
        UpdateTargetData temp = Get<UpdateTargetData>(decipher.content);
        if (NetworkIDChecker.IsDynamic(temp.TargetNetworkID))
            return;
        InstanceManager.UpdateTarget(decipher.tail, temp.TargetNetworkID, temp.SetTargetNetworkID);
    }

    private void AddGrave(Decipher decipher) {
        MatchMaker.Remove(decipher.tail);
        GraveManager.Add(Get<GraveData>(decipher.content));
    }

    private void RemoveGrave(Decipher decipher) {
        GraveManager.Remove(decipher.tail);
    }

    private void Infusion(Decipher decipher) {
        MatchMaker.Remove(decipher.tail);
        GraveManager.Remove(decipher.tail);
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.Infusion, decipher.content, TCP);
    }

    private void Defusion(Decipher decipher) {
        InstanceManager.InclusivelyMulticast(decipher.tail, Protocols.Defusion, decipher.content, TCP);
    }







    private void ListeningForPacakage(Decipher decipher) {
        Packages[decipher.tail] = JsonUtility.FromJson<Package>(decipher.content);
        Packages[decipher.tail].Initialize();
    }
}


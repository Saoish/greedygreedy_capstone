﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Networking.Data;
using System;
using GreedyNameSpace;

public class SoloQueue_1v1 : MatchMaking {
    public override int Cap {get { return 2; }}
    public SoloQueue_1v1(int ClientID) {//Unique Constructor        
        PlayersInMatch = new Dictionary<int, PlayerMatchState>();
        if (UnityEngine.Random.Range(0, 2) == 0)
            PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Blue));
        else
            PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Red));        
        Map = SceneChecker.Arenas[UnityEngine.Random.Range(0, SceneChecker.Arenas.Count)];
        MatchMaker.QueuingClients.Add(ClientID, this);
    }


    //public int Rating;//Not yet used    

    //Coroutine AcceptCheckingCO = null;
    //Dictionary<int, PlayerMatchState> PlayersInMatch;    


    //private Side Occupied;    
    //public SceneID Map;

    ////Must implement
    //public override void Remove(int ClientID) {
    //    PlayersInMatch.Remove(ClientID);
    //    MatchMaker.QueuingClients.Remove(ClientID);        
    //    if (PlayersInMatch.Count == 0) {
    //        MatchMaker.SoloQueue_1v1s.Remove(this);
    //    } else {
    //        foreach (var p in PlayersInMatch) {                
    //            Occupied = p.Value.Side;
    //        }
    //    }        
    //}    

    //public override void AcceptEntrance(int ClientID) {        
    //    PlayersInMatch[ClientID].Accepted = true;
    //    if (AllAccepted) {
    //        //Server.instance.StopCoroutine(AccpetCheckingCO);
    //        SendLoadWithSyncProtocolAndPrepareInstance();
    //    }
    //}

    //public override void DenyEntrance(int ClientID) {              
    //    Remove(ClientID);        
    //}

    //public SoloQueue_1v1(int ClientID) {//Unique Constructor        
    //    PlayersInMatch = new Dictionary<int, PlayerMatchState>();
    //    if (UnityEngine.Random.Range(0, 2) == 0)
    //        Occupied = Side.Blue;
    //    else
    //        Occupied = Side.Red;
    //    PlayersInMatch.Add(ClientID, new PlayerMatchState(Occupied));
    //    Map = SceneChecker.Arenas[UnityEngine.Random.Range(0, SceneChecker.Arenas.Count)];
    //    MatchMaker.QueuingClients.Add(ClientID, this);
    //}

    //public override bool Full {
    //    get {return PlayersInMatch.Count == 2;}
    //}

    //public void Join(int ClientID) {
    //    MatchMaker.QueuingClients[ClientID] =  this;
    //    switch (Occupied) {
    //        case Side.Blue:
    //            PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Red));
    //            break;
    //        case Side.Red:
    //            PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Blue));
    //            break;
    //    }        
    //    if(Full)
    //        SendEntrance();        
    //}

    //private void SendEntrance() {
    //    if (AcceptCheckingCO != null)
    //        Server.instance.StopCoroutine(AcceptCheckingCO);        
    //    foreach (int ClientID in PlayersInMatch.Keys) {            
    //        Server.SendProtocol(ClientID, Protocols.PopQueueEntrance, Server.TCP);
    //    }
    //    AcceptCheckingCO = Server.instance.StartCoroutine(StartEntranceCountdownAndKick(10f));
    //}
    //public IEnumerator StartEntranceCountdownAndKick(float Countdown) {
    //    List<int> Keys = new List<int>(PlayersInMatch.Keys);
    //    foreach (int client_id in Keys) {
    //        if(PlayersInMatch.ContainsKey(client_id))
    //            PlayersInMatch[client_id].Accepted = false;//Defulat all accepted state
    //    }
    //    yield return new WaitForSeconds(Countdown);
    //    List<int> LeftKeys = new List<int>(PlayersInMatch.Keys);
    //    foreach (int client_id in LeftKeys) {//For someone quit the game while queue is poped
    //        if (PlayersInMatch[client_id].Accepted) {
    //            PlayersInMatch[client_id].Accepted = false;//Defulat all accepted state        
    //            Server.Send(client_id, Protocols.TopNotify, new TopNotifyData("There is a player denied the entrance, please wait for another queue...", MyColor.Yellow, 3f), Server.TCP);
    //        }
    //        else {
    //            Remove(client_id);
    //        }
    //    }
    //    AcceptCheckingCO = null;
    //}

    //private void SendLoadWithSyncProtocolAndPrepareInstance() {
    //    Instance InstanceToJoin = null;
    //    foreach (int ClientID in PlayersInMatch.Keys) {            
    //        Server.Send(ClientID, Protocols.LoadSceneWithSync,Map, Server.TCP);            
    //        InstanceManager.Remove(ClientID);
    //        if (InstanceToJoin == null)
    //            InstanceToJoin = InstanceManager.CreateAndReturnCreatedInstanceWithPreset(ClientID,new SceneCoordinate(Map, Mapper.GetPos(Map, (int)PlayersInMatch[ClientID].Side)));
    //        else {
    //            PlayerData ToJoinData = new PlayerData(DataBaseManager.GetLoginedPlayerData(ClientID));
    //            ToJoinData.SC = new SceneCoordinate(Map, Mapper.GetPos(Map, (int)PlayersInMatch[ClientID].Side));
    //            if (PlayersInMatch[ClientID].Side != PlayersInMatch[InstanceToJoin.Dominator].Side) {
    //                InstanceManager.JoinAsEnemy(ClientID,ToJoinData,false, InstanceToJoin.Dominator);
    //            }
    //            else {
    //                InstanceManager.JoinAsFriend(ClientID, ToJoinData, false, InstanceToJoin.Dominator);
    //            }                                
    //        }
    //    }
    //    Server.instance.StartCoroutine(RepeatChecking(InstanceToJoin));
    //}

    //IEnumerator RepeatChecking(Instance InstanceToJoin) {
    //    do {            
    //        yield return new WaitForSeconds(1f);
    //    }
    //    while (InstanceToJoin.Fetching);
    //    do {
    //        yield return new WaitForSeconds(1f);
    //    } while (InstanceToJoin.UnderJoining);
    //    SendSyncWithLoadedSceneProtocolAndDiscardSelf();
    //}


    //private bool AllAccepted {
    //    get {
    //        if (PlayersInMatch.Count < 2)
    //            return false;
    //        else {
    //            foreach (var p in PlayersInMatch)
    //                if (!p.Value.Accepted)
    //                    return false;
    //            return true;
    //        }
    //    }
    //}

    //private void SendSyncWithLoadedSceneProtocolAndDiscardSelf() {        
    //    foreach (var p in PlayersInMatch) {
    //        Server.SendProtocol(p.Key, Protocols.SyncLoadedScene, Server.TCP);
    //        MatchMaker.QueuingClients.Remove(p.Key);
    //    }
    //    PlayersInMatch.Clear();
    //    MatchMaker.SoloQueue_1v1s.Remove(this);        
    //}
}

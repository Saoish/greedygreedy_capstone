﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;

public static class InstanceManager{
    public static Dictionary<int, Instance> ClientsInInstance = new Dictionary<int, Instance>();

    public static Instance Get(int ClientID) {      
        if(ClientsInInstance.ContainsKey(ClientID))
            return ClientsInInstance[ClientID];
        return null;
    }

    public static void LoadStates(int ClientID,Dictionary<int,ObjectData> States) {
        ClientsInInstance[ClientID].LoadStates(States);
    }

    public static bool IsFetching(int ClientID) {
        return ClientsInInstance[ClientID].Fetching;
    }

    public static void Create(int ClientID) {//Create by adding a client to it
        ClientsInInstance[ClientID] = new Instance(ClientID);                
    }

    public static SceneID GetCurrentScene(int ClientID) {
        return ClientsInInstance[ClientID].CurrentScene;
    }

    public static void AddOnGoingEffect(int ClientID,AddEffectData AED) {
        ClientsInInstance[ClientID].AddOnGoingEffect(AED);
    }

    public static PlayerData GetInstancePlayerData(int ClientID) {
        return ClientsInInstance[ClientID].GetInstancePlayerData(ClientID);
    }

    public static Instance CreateAndReturnInstance(int ClientID) {
        ClientsInInstance[ClientID] = new Instance(ClientID);
        return ClientsInInstance[ClientID];
    }

    public static Instance CreateAndReturnCreatedInstanceWithPreset(int ClientID, SceneCoordinate SC) {
        ClientsInInstance[ClientID] =  new Instance(ClientID,SC);
        return ClientsInInstance[ClientID];
    }

    public static void AddLoadedClient(int ClientID) {
        ClientsInInstance[ClientID].AddLoadedClient();
    }    

    public static void JoinAsFriend(int ClientID, PlayerData PD, bool ActiveOnceJoined, int TargetIDInInstance) {
        Get(TargetIDInInstance).JoinAsFriend(ClientID, PD, ActiveOnceJoined);
    }

    public static void JoinAsEnemy(int ClientID, PlayerData PD, bool ActiveOnceJoined, int TargetIDInInstance) {
        Get(TargetIDInInstance).JoinAsEnemy(ClientID, PD, ActiveOnceJoined);
    }

    public static void Remove(int ClientID, bool Abanding = false) {
        if (ClientsInInstance.ContainsKey(ClientID)) {
            ClientsInInstance[ClientID].Remove(ClientID);
        }
    }

    public static void LevelUp(int ClientID) {
        ClientsInInstance[ClientID].LevelUp(ClientID);
    }

    public static void UpdateTarget(int ClientID, int TargetID, int SetTargetID) {
        ClientsInInstance[ClientID].UpdateTarget(TargetID, SetTargetID);
    }

    public static void UpdateCurrHealth(int ClientID, int TargetID, float CurrHealth) {
        ClientsInInstance[ClientID].UpdateCurrHealth(TargetID, CurrHealth);
    }

    public static void UpdateCurrStats(int ClientID,int TargetID, Stats CurrStats) {
        ClientsInInstance[ClientID].UpdateCurrStats(TargetID, CurrStats);        
    }

    public static void UpdatePosition(int ClientID, int TargetID, Vector2 Position) {
        ClientsInInstance[ClientID].UpdatePosition(TargetID, Position);
    }

    public static void UpdateToAliveState(int ClientID, int TargetID, Stats CurrStats, SceneCoordinate SC) {
        ClientsInInstance[ClientID].UpdateToAliveState(TargetID,CurrStats, SC);
    }

    public static void UpdateToDeadState(int ClientID, int TargetID, SceneCoordinate RespawnSC) {
        ClientsInInstance[ClientID].UpdateToDeadState(TargetID, RespawnSC);
    }

    public static void ReloadInstanceState(int ClientID) {
        ClientsInInstance[ClientID].ReloadInstanceState();
    }

    public static void UpdateInstanceCurretScene(int ClientID, SceneID SID) {
        ClientsInInstance[ClientID].CurrentScene = SID;
    }

    public static void SyncInstanceActiveMonsterPositions(int ClientID, SyncInstanceActiveMonsterCoordinatesData SIAMCs) {
        if (ClientsInInstance.ContainsKey(ClientID))
            ClientsInInstance[ClientID].SyncInstanceActiveMonsterPositions(SIAMCs);
    }

    public static void ExclusivelyMulticast<T>(int ClientID, string Protocol, T instance, int Channel = 0) {
        //if(ClientsInInstance.ContainsKey(ClientID))
            ClientsInInstance[ClientID].ExclusivelyMulticast(ClientID, Protocol, instance, Channel);
    }

    public static void InclusivelyMulticast<T>(int ClientID, string Protocol, T instance, int Channel = 0) {
        //if (ClientsInInstance.ContainsKey(ClientID))
            ClientsInInstance[ClientID].InclusivelyMulticast(Protocol, instance, Channel);
    }

    public static void ExclusivelyMulticastProtocol(int ClientID, string Protocol, int Channel = 0) {
            ClientsInInstance[ClientID].ExclusivelyMulticastProtocol(ClientID, Protocol, Channel);
    }

}

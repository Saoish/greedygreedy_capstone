﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;

public class SummonLink:Link {//A one time one move stuff        
    Coroutine LinkingCO = null;    

    public SummonLink(LinkerKey LK) {
        this.LK = LK;        
        Server.Send(LK.TargetID, Protocols.SummonRequest,new SummonRequestData(LK, DataBaseManager.GetLoginedPlayerData(LK.RequesterID).Name), Server.TCP);
        LinkingCO = Server.instance.StartCoroutine(ExpiringCountdown(10f));        
    }

    public void Accept() {        
        Server.instance.StopCoroutine(LinkingCO);
        Linker.Remove(LK);
        if (InstanceManager.Get(LK.RequesterID)!=null) {
            Server.Send(LK.TargetID, Protocols.LoadSceneWithSync, GraveManager.Get(LK.TargetID).SC.SID, Server.TCP);
            PlayerData TargetPD = DataBaseManager.GetLoginedPlayerData(LK.TargetID);            
            TargetPD.SC = GraveManager.Get(LK.TargetID).SC;
            GraveManager.Remove(LK.TargetID);            
            InstanceManager.JoinAsFriend(LK.TargetID, TargetPD, true, LK.RequesterID);
        }
        else {
            Server.Send(LK.TargetID, Protocols.TopNotify, new TopNotifyData("The summoner's world has been destroyed...", MyColor.Yellow, 3f), Server.TCP);
        }
    }

    private IEnumerator ExpiringCountdown(float Countdown) {
        yield return new WaitForSeconds(Countdown);//Link expied once it reaches end
        Server.Send(LK.RequesterID, Protocols.TopNotify, new TopNotifyData("Summoned ghost is lost...", MyColor.Yellow, 3f), Server.TCP);
        Linker.Remove(LK);
    }
}

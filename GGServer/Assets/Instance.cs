﻿using UnityEngine;
using System.Collections;

using Networking.Data;
using System.Collections.Generic;
using GreedyNameSpace;

public class Instance{
    public SceneID CurrentScene = SceneID.Loading;//Initialize with an invalid Scene for Raven            
    public int Dominator;
    public Dictionary<int, ObjectData> States = null;
    public bool Fetching;

    public List<int> Clients;
    private List<int> JQ;                      

    private int LoadedClients;

    Coroutine ReloadingTracker = null;    
    
    public Instance(int ClientID) {
        this.Clients = new List<int>();
        this.JQ = new List<int>();
        this.LoadedClients = 0;        
        Fetching = true;        
        Clients.Add(ClientID);
        Dominator = ClientID;        
        Server.SendProtocol(ClientID, Protocols.UploadStates, Server.TCP);
    }    

    public Instance(int ClientID, SceneCoordinate SC) {//With Preset 
        this.CurrentScene = SC.SID;
        this.Clients = new List<int>();
        this.JQ = new List<int>();
        this.LoadedClients = 0;
        Fetching = true;        
        Clients.Add(ClientID);
        Dominator = ClientID;
        Server.Send(ClientID, Protocols.PresetMPSCThenUploadStates, SC, Server.TCP);        
    }

    public void AddOnGoingEffect(AddEffectData AED) {
        if (AED.Duration > 0) {
            //Debug.Log(AED.TargetNetworkID);
            foreach (var effect in States[AED.TargetNetworkID].OnGoingEffects) {
                if (AED.Type == effect.Type) {
                    effect.Duration = AED.Duration;
                    effect.ExtraParamaters = AED.ExtraParamaters;
                    return;
                }
            }
            States[AED.TargetNetworkID].OnGoingEffects.Add(AED);            
            //Debug.Log(DataBaseManager.GetLoginedPlayerData(AED.TargetNetworkID).OnGoingEffects.Count);
        }
        Server.instance.StartCoroutine(OnGoingEffectTracking(AED));        
    }private IEnumerator OnGoingEffectTracking(AddEffectData AED) {
        while (AED.Duration > 0) {
            if (!States.ContainsKey(AED.TargetNetworkID)||Size == 0 || !States[AED.TargetNetworkID].Alive)
                yield break;
            AED.Duration -= Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        States[AED.TargetNetworkID].OnGoingEffects.Remove(AED);
    }

    public void JoinAsFriend(int ClientID, PlayerData PD, bool ActiveOnceJoined) {
        Server.instance.StartCoroutine(StartJoiningProcess(ClientID, PD, ActiveOnceJoined,OID.FriendlyPlayer));
    }public void JoinAsEnemy(int ClientID, PlayerData PD, bool ActiveOnceJoined) {
        Server.instance.StartCoroutine(StartJoiningProcess(ClientID, PD, ActiveOnceJoined, OID.EnemyPlayer));
    }IEnumerator StartJoiningProcess(int ClientID, PlayerData PD, bool ActiveOnceJoined, OID OID) {
        Server.Send(ClientID, Protocols.SetDominateState, false, Server.TCP);
        while (Fetching)
            yield return null;
        JQ.Add(ClientID);
        while (JQ.Count > 0 && JQ[0] != ClientID) {
            yield return null;
        }
        RegisterNewClient(ClientID);
        PlayerData PD_InInstance = new PlayerData(PD);
        PD_InInstance.OID = OID;
        States[ClientID] = PD_InInstance;
        Server.SendPacakage(ClientID, GenerateRelativeMainStates(ClientID), new Decipher(Protocols.LoadJoiningStates, ActiveOnceJoined.ToString(), ClientID));
        foreach (var c in Clients) {
            if (c != ClientID)
                Server.SendPacakage(c, GenerateAddPlayerStatesData(c, ClientID), new Decipher(Protocols.AddPlayer, null, ClientID));
        }
    }


    public PlayerData GetInstancePlayerData(int ClientID) {
        return (PlayerData)States[ClientID];
    }

    public bool UnderJoining {
        get {            
            return JQ.Count != 0;
        }
    }

    public bool IsAllLoaded {
        get {
            return LoadedClients == Size;
        }
    }

    public void AddLoadedClient() {
        LoadedClients++;
        if (IsAllLoaded) {
            JQ.RemoveAt(0);
        }
    }

    public void LoadStates(Dictionary<int, ObjectData> States) {
        this.States = States;
        if(CurrentScene == SceneID.Loading)//Only loads if it's not been presetted
            this.CurrentScene = States[Dominator].SC.SID;
        Fetching = false;
    }

    public void LevelUp(int ClientID) {
        PlayerData PD = (PlayerData)States[ClientID];
        PD.lvl++;
        PD.exp = 0;
        PD.SkillPoints++;
        PD.BaseStats.Grow(Patch.GetGrowth(PD.Class));
        PD.CurrStats.Grow(Patch.GetGrowth(PD.Class));        
    }

    public void UpdateTarget(int TargetID,int SetTargetID) {
        States[TargetID].Target = SetTargetID;
    }

    public void UpdateCurrHealth(int TargetID, float CurrHealth) {
        if (States != null && States.ContainsKey(TargetID)) {
            States[TargetID].CurrStats.Set(STATSTYPE.HEALTH, CurrHealth);
        }
    }

    public void UpdateCurrStats(int TargetID, Stats CurrStats) {
        if (States != null && States.ContainsKey(TargetID))
            States[TargetID].CurrStats = CurrStats;
    }
    
    public void UpdatePosition(int TargetID, Vector2 Positon) {
        if(States!=null && States.ContainsKey(TargetID))
            States[TargetID].SC.Coordinate.ToVector = Positon;
    } 

    public void UpdateToAliveState(int TargetID,Stats CurrStats,SceneCoordinate SC) {
        if (States != null && States.ContainsKey(TargetID)) {
            States[TargetID].Alive = true;
            States[TargetID].CurrStats = CurrStats;
            States[TargetID].SC = SC;
        }
    }

    public void UpdateToDeadState(int TargetID,SceneCoordinate RespawnSC) {
        if (States != null && States.ContainsKey(TargetID)) {
            States[TargetID].CurrStats.Health = 0f;
            States[TargetID].Alive = false;            
            States[TargetID].OnGoingEffects.Clear();
            States[TargetID].RespawnSC = RespawnSC;
        }
    }

    public void ReloadInstanceState() {//Meanwhile, no joining and invading, and ignore extra Reseting request
        if(ReloadingTracker==null)
            ReloadingTracker = Server.instance.StartCoroutine(SendingUploadStatesRequestIn(3f));
    }private IEnumerator SendingUploadStatesRequestIn(float time) {
        Fetching = true;
        yield return new WaitForSeconds(time);
        Server.SendProtocol(Dominator, Protocols.UploadStates, Server.TCP);
        ReloadingTracker = null;
    }

    public void SyncInstanceActiveMonsterPositions(SyncInstanceActiveMonsterCoordinatesData SIAMCD) {
        foreach (var ID_CD in SIAMCD.ID_CDs) {
            States[ID_CD.NetworkID].SC.Coordinate = ID_CD.CD;
        }
    }

    public void Remove(int ClientID, bool Abanding = false) {
        Clients.Remove(ClientID);
        States.Remove(ClientID);
        if (Size == 0) {            
            InstanceManager.ClientsInInstance.Remove(ClientID);
            if (ReloadingTracker != null)
                Server.instance.StopCoroutine(ReloadingTracker);
        }
        else {
            if (!Abanding) {
                if (JQ.Contains(ClientID))
                    JQ.Remove(ClientID);
                if (ClientID == Dominator) {                                            
                    PickDominator();
                    if (Fetching) {
                        if(ReloadingTracker==null)
                            Server.SendProtocol(ClientID, Protocols.UploadStates, Server.TCP);
                    }
                    else {
                        ResetStateIdentity();
                        Server.Send(Dominator, Protocols.SetDominateState, true, Server.TCP);
                    }
                }
                ExclusivelyMulticast(ClientID, Protocols.LogOffPlayer, ClientID, Server.TCP);
            }
        }
    }

    public int Size {
        get { return Clients.Count; }
    }

    public void ExclusivelyMulticastProtocol(int ExcludedClient, string Protocol, int Channel = 0) {//TCP by default
        foreach (var Client in Clients) {
            if (Client != ExcludedClient)
                Server.SendProtocol(Client, Protocol, Channel);
        }
    }

    public void ExclusivelyMulticast<T>(int ExcludedClient, string Protocol, T instance, int Channel = 0) {//TCP by default
        foreach (var Client in Clients) {
            if (Client != ExcludedClient)
                Server.Send(Client, Protocol, instance, Channel);
        }
    }

    public void InclusivelyMulticast<T>(string Protocol, T instance, int Channel = 0) {
        foreach (var Client in Clients) {
            Server.Send(Client, Protocol, instance, Channel);
        }
    }

    private void RegisterNewClient(int ClientID) {
        LoadedClients = 0;
        Clients.Add(ClientID);        
        InstanceManager.ClientsInInstance[ClientID] = this;        
    }

    private void PickDominator() {
        Dominator = Clients[0];
    }

    private void ResetStateIdentity() {
        Fetching = true;
        if (States[Dominator].OID == OID.EnemyPlayer) {
            States[Dominator].OID = OID.Main;
            foreach (var state in States) {
                if (state.Value.OID == OID.FriendlyMonster)
                    state.Value.OID = OID.EnemyMonster;
                else if (state.Value.OID == OID.FriendlyPlayer)
                    state.Value.OID = OID.EnemyPlayer;
                else if (state.Value.OID == OID.EnemyPlayer)
                    state.Value.OID = OID.FriendlyPlayer;
            }
        }
        else {
            States[Dominator].OID = OID.Main;
        }
        Fetching = false;
    }

    private PlayerData GenerateAddPlayerStatesData(int Joined_ClientID, int Joining_ClientID) {        
        if (States[Joined_ClientID].OID == OID.EnemyPlayer) {
            PlayerData Filtered = new PlayerData((PlayerData)States[Joining_ClientID]);
            if (States[Joining_ClientID].OID == OID.EnemyPlayer)
                Filtered.OID = OID.FriendlyPlayer;
            else
                Filtered.OID = OID.EnemyPlayer;
            return Filtered;
        }
        else {            
            return (PlayerData)States[Joining_ClientID];
        }        
    }

    private Dictionary<int,ObjectData> GenerateRelativeMainStates(int Joining_ClientID) {        
        Dictionary<int, ObjectData> FilteredState = new Dictionary<int, ObjectData>();
        foreach (var state in States) {         
            if (state.Value is PlayerData){//Players
                FilteredState[state.Key] = new PlayerData((PlayerData)state.Value);
                if (States[Joining_ClientID].OID == OID.EnemyPlayer) {
                    if (state.Value.OID == OID.EnemyPlayer) {
                        if (state.Key != Joining_ClientID)
                            FilteredState[state.Key].OID = OID.FriendlyPlayer;
                        else
                            FilteredState[state.Key].OID = OID.Main;
                    }
                    else {
                        FilteredState[state.Key].OID = OID.EnemyPlayer;
                    }
                }
                else {
                    if(state.Value.OID == OID.Main) {
                        FilteredState[state.Key].OID = OID.FriendlyPlayer;
                    }
                    else if(state.Key == Joining_ClientID) {
                        FilteredState[state.Key].OID = OID.Main;
                    }
                }
            }
            else if (state.Value is StaticObjectData) {//Worldstate               
                FilteredState[state.Key] = new StaticObjectData((StaticObjectData)state.Value);
            }
            else {//Dungeon state, Necormancer's friendly monster identity is controller locally                
                FilteredState[state.Key] = new ObjectData(state.Value);
            }       
        }
        //Debug.Log(FilteredState[Joining_ClientID] == DataBaseManager.GetLoginedPlayerData(Joining_ClientID));
        return FilteredState;
    }

    //~Instance() {        
    //    Debug.Log("Got destroyed");
    //}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
using System;

public class SoloQueue_2v2 : MatchMaking {
    public override int Cap { get { return 4; } }    
    public SoloQueue_2v2(int ClientID) {//Unique Constructor        
        PlayersInMatch = new Dictionary<int, PlayerMatchState>();
        if (UnityEngine.Random.Range(0, 2) == 0)
            PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Blue));
        else
            PlayersInMatch.Add(ClientID, new PlayerMatchState(Side.Red));        
        Map = SceneChecker.Arenas[UnityEngine.Random.Range(0, SceneChecker.Arenas.Count)];
        MatchMaker.QueuingClients.Add(ClientID, this);
    }
}

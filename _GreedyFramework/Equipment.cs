﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;

[System.Serializable]
public class Equipment {
    public RARITY Rarity;//0 common and so on

    public string Name;
    public CLASS Class;//For non-trinket equipment only
    public EQUIPTYPE EquipType;

    public int Itemlvl = 1;
    public int LvlReq;
    public EQUIPSET Set;
    public int Reforged = 0; //NumofTime been reforged             
    public Stats Stats;
    public int Value = 0;

    public Equipment() {
        Name = "";
        Stats = new Stats(Stats.InitStatsType.EQUIP);
    }

    public Equipment(Equipment Copy) {
        this.Rarity = Copy.Rarity;
        this.Name = Copy.Name;
        this.Class = Copy.Class;        
        this.EquipType = Copy.EquipType;
        this.Itemlvl = Copy.Itemlvl;
        this.LvlReq = Copy.LvlReq;
        this.Set = Copy.Set;
        this.Reforged = Copy.Reforged;
        this.Stats = new Stats(Copy.Stats);
        this.Value = Copy.Value;
    }

    public bool isNull {
        get { return Name == ""; }
    }

}

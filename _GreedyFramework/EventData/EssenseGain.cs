﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
[System.Serializable]
public class EssenseGain : Value {
    public EssenseGain(float RawEssense,bool Crit, int applyerID,int targetID, CR Type,bool TraceBack) {
        this.Amount = EssenseGainCalculation(RawEssense);
        this.Crit = Crit;
        this.applyerID = applyerID;
        this.targetID = targetID;
        this.Type = Type;
        this.TraceBack = TraceBack;
    }

    private float EssenseGainCalculation(float RawEssenseGain) {
        return Mathf.Ceil(RawEssenseGain);
    }
}

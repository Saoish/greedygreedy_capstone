﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
[System.Serializable]
public class ProjectileData{
    public string Name;    
    public Vector2 LaunchPosition;
    public Vector2 LaunchDirection;
    public float Size;
    public Damage dmg;        
    public int HomingTargetID;        
    
    public ProjectileData(string Name, Vector2 LaunchPosition,Vector2 LaunchDirection,float Size,Damage dmg, int HomingTargetID) {
        this.Name = Name;
        this.LaunchPosition = LaunchPosition;
        this.LaunchDirection = LaunchDirection;
        this.Size = Size;
        this.dmg = dmg;                
        this.HomingTargetID = HomingTargetID;
    }
}

﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
[System.Serializable]
public class Damage : Value {
    public float TargetDefense;
    public float Penetration;
    public Damage(float RawDamage, float TargetDefense, float Penetration, bool Crit, int applyerID, int targetID, CR Type, bool TraceBack = true) {//Default with traceback
        this.Amount = DirectDamageCalculation(RawDamage, TargetDefense, Penetration);
        this.Crit = Crit;
        this.applyerID = applyerID;
        this.targetID = targetID;
        this.Type = Type;
        this.TraceBack = TraceBack;
    }

    public Damage(float RawDamage, float Penetration,bool Crit, int applyerID, CR Type, bool TraceBack = true) {//Must post cal the actual damage
        this.Amount = RawDamage;
        this.Penetration = Penetration;
        this.Crit = Crit;
        this.applyerID = applyerID;
        this.Type = Type;
        this.TraceBack = TraceBack;
    }public void PostCalculate (int targetID, float TargetDefense){
        this.targetID = targetID;
        this.TargetDefense = TargetDefense;
        float PenetratedPortion = TargetDefense - Penetration;
        if (PenetratedPortion < 0) {
            PenetratedPortion = 0;
        }
        float reduced_dmg = Amount * PenetratedPortion / 100;        
        Amount = Amount - reduced_dmg >= 1 ? Mathf.Ceil(Amount - reduced_dmg) : 1;        
    }    

    private float DirectDamageCalculation(float RawDamage, float TargetDefense, float Penetration) {
        this.TargetDefense = TargetDefense;
        this.Penetration = Penetration;
        float PenetratedPortion = TargetDefense - Penetration;
        if (PenetratedPortion < 0) {
            PenetratedPortion = 0;
        }
        float reduced_dmg = RawDamage * PenetratedPortion / 100;
        return RawDamage - reduced_dmg >= 1 ? Mathf.Ceil(RawDamage - reduced_dmg) : 1;
    }
}

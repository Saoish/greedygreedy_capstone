﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
using System.Collections.Generic;
using UnityEditor;
[CustomEditor(typeof(Monster), true)]
public class MonsterInspector : Editor {
    Monster EC;
    public static bool FoldMaxStats = false;
    public static bool FoldBaseGrowth = false;
    void OnEnable() {
        EC = (Monster)target;
    }

    public override void OnInspectorGUI() {
        if (EC.MaxStats.IsNull)
            EC.MaxStats = new Stats(Stats.InitStatsType.OBJECT);  
        base.OnInspectorGUI();

        FoldMaxStats = EditorGUILayout.Foldout(FoldMaxStats, "MaxStats");        
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.Space();
        if (FoldMaxStats) {
            EditorGUILayout.BeginVertical();
            for (int i = 0; i < EC.MaxStats.stats.Length; i++) {
                EC.MaxStats.stats[i] = EditorGUILayout.FloatField(((STATSTYPE)i).ToString(), EC.MaxStats.stats[i]);
            }
            EditorGUILayout.EndVertical();
        }

        if (!(EC is NPC)) {
            if (EC.Growth.IsNull)
                EC.Growth = new Stats(Stats.InitStatsType.EQUIP);
            EditorGUILayout.EndHorizontal();
            FoldBaseGrowth = EditorGUILayout.Foldout(FoldBaseGrowth, "GrowthStats");
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space();
            if (FoldBaseGrowth) {
                EditorGUILayout.BeginVertical();
                for (int i = 0; i < EC.Growth.stats.Length; i++) {
                    EC.Growth.stats[i] = EditorGUILayout.FloatField(((STATSTYPE)i).ToString(), EC.Growth.stats[i]);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }

        if (GUI.changed) {
            EditorUtility.SetDirty(EC);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using GreedyNameSpace;
using Visual;
[CustomEditor(typeof(EquipmentController))]
public class EquipmentControllerInspector : Editor {
    EquipmentController EC;

    void OnEnable() {
        EC = (EquipmentController)target;
    }

    public override void OnInspectorGUI() {
        if (EC.E == null)
            EC.E = new Equipment();
        EC.E.Name = EditorGUILayout.TextField("Name", EC.E.Name);
        EC.E.Class = (CLASS)EditorGUILayout.EnumPopup("Class", EC.E.Class);
        EC.E.EquipType = (EQUIPTYPE)EditorGUILayout.EnumPopup("Type", EC.E.EquipType);
        EC.E.Set = (EQUIPSET)EditorGUILayout.EnumPopup("Set", EC.E.Set);
        EC.GetComponent<Description>().Info = EditorGUILayout.TextField("Description", EC.GetComponent<Description>().Info);
        if (GUILayout.Button("Assign Render Mode")) {
            try {
                EC.ELRM = (Layer.EquipmentLayerRenderMode)System.Enum.Parse(typeof(Layer.EquipmentLayerRenderMode), EC.E.EquipType.ToString());
            }
            catch {
                Debug.Log("Can't find ELRM for : " + EC.E.Name + ", please manually assign it. ");
            }
        }
        base.OnInspectorGUI();
        if (GUILayout.Button("Preset Fields")) {
            if (EC.E.EquipType == EQUIPTYPE.Trinket)
                Debug.Log("No preset for "+EC.E.Name);
            else if (EC.E.EquipType == EQUIPTYPE.Weapon) {
                PresetSolidFields PF = ((GameObject)Resources.Load("PresetFields/" + EC.E.Class.ToString() + "/" + EC.GetComponent<WeaponController>().Type.ToString())).GetComponent<PresetSolidFields>();
                PresetSpecialFields SF = ((GameObject)Resources.Load("PresetFields/" + EC.E.Class.ToString() + "/" + EC.GetComponent<WeaponController>().Type.ToString())).GetComponent<PresetSpecialFields>();
                EC.SolidFields = PF.SolidFields;
                EC.SpecialFields = SF.SpecialFields;
            }
            else {
                PresetSolidFields PF = ((GameObject)Resources.Load("PresetFields/" + EC.E.Class.ToString() + "/" + EC.E.EquipType.ToString())).GetComponent<PresetSolidFields>();
                PresetSpecialFields SF = ((GameObject)Resources.Load("PresetFields/" + EC.E.Class.ToString() + "/" + EC.E.EquipType.ToString())).GetComponent<PresetSpecialFields>();
                EC.SolidFields = PF.SolidFields;
                EC.SpecialFields = SF.SpecialFields;
            }
        }
        if(GUILayout.Button("Apply Presets For All")) {
            EquipmentController[] ECs = Resources.LoadAll<EquipmentController>("EquipmentPrefabs");
            foreach(var ec in ECs) {
                if (ec.E.EquipType == EQUIPTYPE.Trinket)
                    Debug.Log("No preset for "+ec.E.Name);
                else if (ec.E.EquipType == EQUIPTYPE.Weapon) {
                    PresetSolidFields PF = ((GameObject)Resources.Load("PresetFields/" + ec.E.Class.ToString() + "/" + ec.GetComponent<WeaponController>().Type.ToString())).GetComponent<PresetSolidFields>();
                    PresetSpecialFields SF = ((GameObject)Resources.Load("PresetFields/" + ec.E.Class.ToString() + "/" + ec.GetComponent<WeaponController>().Type.ToString())).GetComponent<PresetSpecialFields>();
                    ec.SolidFields = PF.SolidFields;
                    ec.SpecialFields = SF.SpecialFields;
                }
                else {
                    PresetSolidFields PF = ((GameObject)Resources.Load("PresetFields/" + ec.E.Class.ToString() + "/" + ec.E.EquipType.ToString())).GetComponent<PresetSolidFields>();
                    PresetSpecialFields SF = ((GameObject)Resources.Load("PresetFields/" + ec.E.Class.ToString() + "/" + ec.E.EquipType.ToString())).GetComponent<PresetSpecialFields>();
                    ec.SolidFields = PF.SolidFields;
                    ec.SpecialFields = SF.SpecialFields;
                }
            }
        }
        if (GUI.changed)
            EditorUtility.SetDirty(target);
    }
}

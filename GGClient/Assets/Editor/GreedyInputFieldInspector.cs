﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(GreedyInputField), true)]
public class GreedyInputFieldInspector : Editor {
    GreedyInputField GIF;

    private void OnEnable() {
        GIF = (GreedyInputField)target;
    }

    public override void OnInspectorGUI() {
        //GIF.CG = GIF.transform.parent.GetComponent<CanvasGroup>();//For most cases
        GIF.CG = EditorGUILayout.ObjectField("Canvas Group", GIF.CG, typeof(CanvasGroup), true) as CanvasGroup;
        GIF.Type = (GreedyInputField._Type)EditorGUILayout.EnumPopup("Type", GIF.Type);
        base.OnInspectorGUI();
    }

}

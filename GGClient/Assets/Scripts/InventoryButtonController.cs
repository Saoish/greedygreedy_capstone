﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GreedyNameSpace;
using UnityEngine.EventSystems;

public class InventoryButtonController : MonoBehaviour, ISelectHandler {
    public GameObject InventoryViewSign;
    public InventorySubMenu ISM;
    GameObject EquipmentIcon;    
    [HideInInspector]
    public EquipmentInfo EI = null;
    [HideInInspector]
    public int Slot = -999;        
    [HideInInspector]
    public Equipment E = null;    

    private void Awake() {        
        ISM = (Instantiate(ISM.gameObject, transform) as GameObject).GetComponent<InventorySubMenu>();
        ISM.transform.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        ISM.transform.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;      
                  
        InventoryViewSign = Instantiate(InventoryViewSign, transform) as GameObject;
        InventoryViewSign.transform.localScale = new Vector2(100, 100);
        InventoryViewSign.transform.localPosition = new Vector2(30,30);
    }

    public void OnSelect(BaseEventData eventData) {
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position, GameManager.SFX_Volume);
        if (!EI) {
            EI = transform.parent.parent.GetComponentInChildren<EquipmentInfo>(true);
        }
        EI.Show(E, EquipmentInfo.Mode.Inventory);
        CacheManager.MP_Data.InventoryViewSigns[Slot] = false;
        InventoryViewSign.SetActive(false);
    }    

    void OnEnable() {      
        Slot = int.Parse(gameObject.name);        
        //EI = transform.parent.parent.Find("EquipmentInfo").GetComponent<EquipmentInfo>();
        UpdateSlot();
                        
    }

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        //UpdateSlot();
        //UpdateInfo();
    }

    public void OnClickActiveSubMenu() {
        if(!CacheManager.MP.GetInventoryItem(Slot).isNull)
            ISM.TurnOn();
    }

    public void OnClickSell() {
        if (!CacheManager.MP.GetInventoryItem(Slot).isNull) {
            VendorInteractionContent VIC = GetComponentInParent<VendorInteractionContent>();
            int VendorSlot = VIC.FirstAvailableVendorSlot;
            if (VendorSlot < Patch.InventoryCapacity) {//Full
                Equipment ToVendor = new Equipment(CacheManager.MP.GetInventoryItem(Slot));
                ToVendor.Value *= Patch.HeartlessRate;                
                VIC.Goods[VendorSlot] = ToVendor;
                transform.parent.parent.Find("VendorButtons/" + VendorSlot).GetComponent<VendorButtonController>().UpdateSlot();
            }
            CacheManager.MP.Sell(Slot);
            UpdateSlot();
            EI.Show(E, EquipmentInfo.Mode.Inventory);
        }
    }

    public void OnClieckStash() {        
        if (!CacheManager.MP.GetInventoryItem(Slot).isNull) {
            int Page = GetComponentInParent<StashInteractionContent>().StashPage;
            int StashSlot = CacheManager.MP.GetFirstAvailableStashSlot(Page);
            if (StashSlot != (Page + 1) * Patch.InventoryCapacity) {
                CacheManager.MP.Switch_I2S(Slot, StashSlot);
                transform.parent.parent.Find("StashButtons/" + (StashSlot - Page * Patch.InventoryCapacity)).GetComponent<StashButtonController>().UpdateSlot();
                UpdateSlot();
                EI.Show(E, EquipmentInfo.Mode.Inventory);
            }
            else {
                RedNotification.Push(RedNotification.Type.STASH_FULL);
            }                
        }
    }
       
    public void OnClickEquip() {
        if (!CacheManager.MP.GetInventoryItem(Slot).isNull) {
            if (CacheManager.MP.Compatible(E)) {
                if (!CacheManager.MP.GetEquippedItem(E.EquipType).isNull) {//Has Equipped Item
                    Equipment TakeOff = CacheManager.MP.GetEquippedItem(E.EquipType);
                    CacheManager.MP.UnEquip(TakeOff.EquipType);
                    CacheManager.MP.Equip(E);                    
                    transform.parent.parent.Find("EquippedSlotButtons/" + (int)E.EquipType).GetComponent<EquippedButtonController>().UpdateSlot();
                    CacheManager.MP.RemoveFromInventory(Slot);
                    CacheManager.MP.AddToInventory(Slot, TakeOff);
                    UpdateSlot();
                    EI.Show(E, EquipmentInfo.Mode.Inventory);
                } else {//No Equipped Item
                    CacheManager.MP.Equip(E);
                    transform.parent.parent.Find("EquippedSlotButtons/" + (int)E.EquipType).GetComponent<EquippedButtonController>().UpdateSlot();
                    CacheManager.MP.RemoveFromInventory(Slot);                    
                    UpdateSlot();
                    EI.Show(E, EquipmentInfo.Mode.Inventory);
                }
            } else {
                RedNotification.Push(RedNotification.Type.CANT_EQUIP);
            }
        }
    }

    void UpdateInfo() {
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == gameObject) {
            EI.Show(E, EquipmentInfo.Mode.Inventory);
        }
    }

    public void UpdateSlot() {        
        E = CacheManager.MP.GetInventoryItem(Slot);
        if (!E.isNull) {
            if (EquipmentIcon != null)
                DestroyObject(EquipmentIcon);
            EquipmentIcon = EquipmentController.ObtainInventoryIcon(E, transform);
        } else {
            DestroyObject(EquipmentIcon);
            EquipmentIcon = null;
        }
        if (CacheManager.MP_Data.InventoryViewSigns[Slot])
            InventoryViewSign.SetActive(true);        
    }
}
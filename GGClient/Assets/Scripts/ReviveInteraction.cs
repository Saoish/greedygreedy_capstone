﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public class ReviveInteraction : Interaction {
    public Player PC;

    List<Player> Revivers = new List<Player>();

    protected override void Start() {
        base.Start();
        Priority = 999;//Save life
    }

    public void Enable() {        
        gameObject.SetActive(true);
        Revivers.Clear();
    }

    public void Disable() {        
        StopAllCoroutines();        
        gameObject.SetActive(false);
        Revivers.Clear();
    }

    public override bool IsTriggered() {//No Trigger
        return false;
    }

    public override void Interact() {
    }

    void OnDestroy() {
        if (CacheManager.MP.MPI.InteractTarget == this) {
            try {
                InteractionNotification.TurnOff();
            }
            catch {
                Debug.Log("Wiped");
            }
        }
    }
    private void OnDisable() {
        if (CacheManager.MP.MPI.InteractTarget == this)
            InteractionNotification.TurnOff();
        Revivers.Clear();
    }

    private void FixedUpdate() {
        if (Revivers.Count > 0) {
            int i = 0;
            do {
                if (!Revivers[i] || !Revivers[i].Alive) {
                    Revivers.RemoveAt(i);                    
                    if (Revivers.Count == 0) {                        
                        PC.IC.DisableReviveBar();
                    }
                }
                else
                    i++;
            }
            while (i < Revivers.Count);
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if(collider.gameObject.layer == CollisionLayer.Friendly || collider.gameObject.layer == CollisionLayer.Enemy) {
            ObjectController Enterer = collider.GetComponent<ObjectController>();
            if (!(collider.GetComponent<ObjectController>() is Player))
                return;            
            else {
                Player P = (Player)Enterer;
                if (PC.OID == OID.EnemyPlayer && P.OID != OID.EnemyPlayer)
                    return;                
                else if (PC.OID!=OID.EnemyPlayer && P.OID == OID.EnemyPlayer)
                    return;
                else {
                    Revivers.Add(P);
                    if (!PC.IC.ReviveBar.active)
                        PC.IC.EnableReviveBar();
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider) {
        if (collider.gameObject.layer == CollisionLayer.Friendly || collider.gameObject.layer == CollisionLayer.Enemy) {
            ObjectController Enterer = collider.GetComponent<ObjectController>();
            if (!(collider.GetComponent<ObjectController>() is Player))
                return;
            else if (Revivers.Contains((Player)Enterer)) {
                Revivers.Remove((Player)Enterer);
                if (Revivers.Count == 0)
                    PC.IC.DisableReviveBar(); 
            }
        }
    }


    public override void SetIndication() {
        if (CacheManager.MP.MPI.InteractTarget == this) {
            if (PC.OID == OID.FriendlyPlayer) {
                InteractionNotification.SetIndication(MyText.Colofied("Stay on it to revive", MyText.lime), null);
                InteractionNotification.TurnOn();
            }
        }
    }
}

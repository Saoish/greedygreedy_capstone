﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnvironmentController : MonoBehaviour {

    public bool destroyable;
    public GameObject DieVFX;
    public AudioClip DieSFX;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //IgnoreCollisionUpdate();         
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (!destroyable  || collider.tag == Tag.EnemyMonster || collider.tag == Tag.FriendlyMonster || collider.tag == Tag.FriendlyPlayer || collider.tag == Tag.EnemyPlayer)
            return;
        string cached_name = DieVFX.name;
        DieVFX = Instantiate(DieVFX, transform.position, transform.rotation) as GameObject;
        DieVFX.name = cached_name;
        Destroy(DieVFX, DieVFX.transform.GetComponent<ParticleSystem>().duration);
        Destroy(gameObject);
        if (DieSFX != null)
            AudioSource.PlayClipAtPoint(DieSFX, transform.position, GameManager.SFX_Volume);
    }

    //still become transparent when someshit crashed on this solid environment
    //void OnTriggerStay2D(Collider2D collider)
    //{
    //    if (collider.tag == Tag.EnemyMonster || collider.tag == Tag.FriendlyMonster|| collider.tag == Tag.FriendlyPlayer || collider.tag == Tag.EnemyPlayer)
    //    {
    //        SpriteRenderer environment = transform.GetComponent<SpriteRenderer>();
    //        Color col = environment.color;
    //        col.a = 0.7f;
    //        environment.color = col;
    //        return;
    //    }
    //}

    //void OnTriggerExit2D(Collider2D collider)
    //{
    //    SpriteRenderer environment = transform.GetComponent<SpriteRenderer>();
    //    Color col = environment.color;
    //    col.a = 1.0f;
    //    environment.color = col;
    //}

}

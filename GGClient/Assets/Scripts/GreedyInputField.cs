﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GreedyNameSpace;


public class GreedyInputField : InputField{
    public enum _Type {
        Username = 16,
        CharacterName = 12,
        GraveStoneNote = 258
    }
    [HideInInspector]
    public CanvasGroup CG;
    [HideInInspector]
    public _Type Type;
    [HideInInspector]
    public bool AutoPoped = false;
    [HideInInspector]
    public bool ManualPoped = false;

    protected override void Start() {
        base.Start();
        if((int)Type == 0) {
            Debug.Log(gameObject + " is missing input type.");
            return;
        }
        else if(CG == null) {
            Debug.Log(gameObject + "'s Canvas Group is not assigned.");
            return;
        }
        placeholder = transform.Find("Placeholder").GetComponent<Graphic>();
        textComponent = transform.Find("Text").GetComponent<Text>();
        characterLimit = (int)Type;
        caretWidth = 5;        
        ColorBlock cb = colors;
        cb.normalColor = MyColor.LightGrey;
        cb.highlightedColor = MyColor.White;
        cb.disabledColor = MyColor.Grey;
        colors = cb;                
    }    

    public override void OnUpdateSelected(BaseEventData eventData) {        
        if (GameManager.UseOnScreenKeyboard) {
            if (!GreedyKeyboard.IsActive && !AutoPoped) {
                GreedyKeyboard.SlideIn(this);
                AutoPoped = true;
            }
        }
        else
            base.OnUpdateSelected(eventData);        
    }

    public override void OnDeselect(BaseEventData eventData) {
        base.OnDeselect(eventData);
        if (GameManager.UseOnScreenKeyboard) {
            if (GreedyKeyboard.instance) {
                if (!GreedyKeyboard.instance.CG.interactable) {                    
                    AutoPoped = false;
                }
            }
        }
    }    

    public override void OnSubmit(BaseEventData eventData) {
        if (GameManager.UseOnScreenKeyboard && !GreedyKeyboard.IsActive) {
            GreedyKeyboard.SlideIn(this);
            ManualPoped = true;
            return;
        }
        base.OnSubmit(eventData);
    }    
}

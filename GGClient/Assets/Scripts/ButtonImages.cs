﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ButtonImages {

    //public static Sprite X = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Collections/ButtonImages/DS4_4");
    //public static Sprite D_Down = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Collections/ButtonImages/DS4_17");

    //public static Sprite X = Resources.Load<Sprite>("ButtonImages/DS4_4");

    static Sprite[] DS4_Images = Resources.LoadAll<Sprite>("ButtonImages/DS4");
    static Sprite[] XBOXONE_Images = Resources.LoadAll<Sprite>("ButtonImages/XBOXONE");


    //Button names are based on DS4, Keyboard cases are not yet implemented
    public static Sprite X {                        
        get {            
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[2];
            else
                return DS4_Images[2];
        }            
    }            

    public static Sprite Triangle {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[5];
            else
                return DS4_Images[5];
        }
    }

    public static Sprite O {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[3];
            else
                return DS4_Images[3];
        }
    }

    public static Sprite Sqauare {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[4];
            else
                return DS4_Images[4];
        }
    }

    public static Sprite L1 {
        get {            
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[8];
            else
                return DS4_Images[8];
        }
    }
    public static Sprite R1 {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[9];
            else
                return DS4_Images[9];
        }
    }
    public static Sprite L2 {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[6];
            else
                return DS4_Images[6];
        }
    }
    public static Sprite R2 {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[7];
            else
                return DS4_Images[7];
        }
    }

    public static Sprite D_Down {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[11];
            else
                return DS4_Images[11];
        }
    }    

    public static Sprite L_Analog_Stick {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[31];
            else
                return DS4_Images[31];
        }
    }

    public static Sprite R_Analog_Stick {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[23];
            else
                return DS4_Images[23];
        }
    }
    
    public static Sprite Start {
        get {
            if (ControllerManager.CT == ControllerManager.ControllerType.Xbox)
                return XBOXONE_Images[1];
            else
                return DS4_Images[1];
        }
    }         
}

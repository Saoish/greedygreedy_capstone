﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using GreedyNameSpace;


public static class UsernameManager {
    public static string Username;
    //public static UserData UserData = new UserData();


    //public static void LoadUserData(UserData userdata) {
    //    UserData = userdata;        
    //}

    //public static void LoadPlayerData(PlayerData PlayerData) {
    //    UserData.PlayerDatas[PlayerData.SlotIndex] = PlayerData;        
    //}

    public static void LoadUserName() {
        try {
            StreamReader LoadStream = new StreamReader("username.txt");
            Username = LoadStream.ReadToEnd();
            LoadStream.Close();
        }
        catch {
            Username = null;
        }
        //else {
        //    StreamReader LoadStream = new StreamReader("username.txt");
        //    Username = LoadStream.ReadToEnd();
        //    LoadStream.Close();
        //}
    }

    public static void SaveUsername(string username) {
        StreamWriter SaveStream = new StreamWriter("username.txt");
        SaveStream.Write(username);
        SaveStream.Close();
        LoadUserName();
    }
}

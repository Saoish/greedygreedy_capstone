﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TopNotification : MonoBehaviour {
    static Text message;
    static Animator Anim;

    static GameObject Self;

    static Queue<float> track = new Queue<float>();
    
    void Awake() {
        Anim = GetComponent<Animator>();
        message = transform.Find("Message").GetComponent<Text>();
        Self = gameObject;
    }

    public static void Push(string message, Color color, float period) {
        if (!Self)
            return;
        track.Enqueue(period);
        TopNotification.message.color = color;
        TopNotification.message.text = message;
        GameManager.instance.StartCoroutine(Transition(period));
    }

    public static string Message {
        get { return message.text; }
        set { message.text = value; }
    }   

    static IEnumerator Transition(float period) {
        Anim.SetBool("Off", false);
        Anim.SetBool("On",true);
        yield return new WaitForSeconds(period);
        track.Dequeue();
        if (Self != null && track.Count == 0) {
            Anim.SetBool("Off", true);
            Anim.SetBool("On", false);
        }
    }


}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GreedyNameSpace;
using InControl;
using System.Collections.Generic;
using Networking.Data;

public class ActiveSkillButtonController : MonoBehaviour {
    MainPlayer MPC;
    private int Slot = -999;

    //[HideInInspector]
    private ActiveSkill AS;    

    private PlayerAction Key;

    private Image IconImage;
    private Image CD_Mask;
    private GameObject Red_Mask_OJ;
    private Transform BG;

    private List<ActiveSkillButtonController> OtherASBCs;


    bool Assigning = false;

    public AudioClip selected;

    public Image CD_Mask_Image;
    public Image Root_Holder_Image;

    public void OnSelect() {
        AudioSource.PlayClipAtPoint(selected, transform.position, GameManager.SFX_Volume);
    }

    void Awake() {
        OtherASBCs = new List<ActiveSkillButtonController>();
    }

    // Use this for initialization
    void Start() {
        Slot = int.Parse(gameObject.name);
        IconImage = GetComponent<Image>();
        CD_Mask = transform.Find("CD_Mask").GetComponent<Image>();
        Red_Mask_OJ = transform.Find("Red_Mask").gameObject;
        BG = transform.parent;
        GetComponent<Button>().onClick.AddListener(OnClickActive);

        //MPC = BG.parent.parent.parent.GetComponent<MainPlayerUI>().MPC;
        MPC = CacheManager.MP;

        MapKey();
        FetchSkill();        

        foreach (ActiveSkillButtonController ASBC in transform.parent.parent.GetComponentsInChildren<ActiveSkillButtonController>()) {
            if (ASBC != GetComponent<ActiveSkillButtonController>()) {
                OtherASBCs.Add(ASBC);
            } 
        }
        MapColor();
    }

    void MapColor() {
        CD_Mask_Image.color = Root_Holder_Image.color = MPC.MPUI.ResourceOrbSR.color;
        transform.parent.GetComponent<Image>().color = MyColor.Green;
    }
	
	// Update is called once per frame
	void Update () {
        if (AS) {
            UpdateCDMask();
            UpdateRedMask();
        }
        if (!ControllerManager.SyncActions && !Assigning) {
            GetComponent<Button>().interactable = false;
            if (AS && AS.OC.Casting) {                
                AS.Interrupt(true);
            }
            return;
        } else if (Assigning) {
            AssigningUpdate();
        } else if(ControllerManager.SyncActions){
            CombatUpdate();
        }
    }

    public static void FactoryReset() {
        foreach (var ASBC in FindObjectsOfType<ActiveSkillButtonController>()) {
            ASBC.FetchSkill();
            ASBC.CD_Mask.fillAmount = 0;
        }

    }

    public void FetchSkill() {                
        AS = MPC.GetActiveSlotSkill(Slot);        
        LoadSkillIcon();
    }

    public void DiscardSkill() {
        MPC.SetActiveSkillAt(Slot, null);
        FetchSkill();
    }


    void OnClickAssign(SkillButton skill_button) {
        ActiveSkill active_skill = (ActiveSkill)skill_button.Skill;
        if(AS!=null && AS.RealTime_CD != 0) {
            RedNotification.Push(RedNotification.Type.ON_CD);
            return;
        }
        foreach (ActiveSkillButtonController ASBC in OtherASBCs) {
            if (ASBC.AS != null && ASBC.AS.Name == active_skill.Name) {
                if (ASBC.AS.RealTime_CD != 0) {
                    RedNotification.Push(RedNotification.Type.ON_CD);
                    return;
                }
                ASBC.DiscardSkill();
            }
        }
        MPC.SetActiveSkillAt(Slot, active_skill);
        FetchSkill();
        DisableActiveSlotsAssigning();
        skill_button.SkillSubMenu.TurnOff();
    }

    void DisableActiveSlotsAssigning() {
        DisableAssigning();
        foreach (ActiveSkillButtonController ASBC in OtherASBCs) {
            ASBC.DisableAssigning();            
        }
    }

    public void EnableAssigning(SkillButton skill_button) {
        Assigning = true;
        transform.parent.GetComponent<Animator>().SetBool("Blinking", true);
        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(delegate { OnClickAssign(skill_button); });
        Navigation auto = new Navigation();
        auto.mode = Navigation.Mode.Automatic;
        transform.GetComponent<Button>().navigation = auto;
    }

    public void DisableAssigning() {
        CacheManager.MP.MPUI.ActionBar.Find("AssignToolTip").gameObject.SetActive(false);
        Assigning = false;
        transform.parent.GetComponent<Animator>().SetBool("Blinking", false);
        GetComponent<Button>().interactable = false;
        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(OnClickActive);
        Navigation none = new Navigation();
        none.mode = Navigation.Mode.None;
        transform.GetComponent<Button>().navigation = none;
    }


    void MapKey() {
        switch (Slot) {
            case 0:
                Key = ControllerManager.Actions.Skill_0;
                break;
            case 1:
                Key = ControllerManager.Actions.Skill_1;
                break;
            case 2:
                Key = ControllerManager.Actions.Skill_2;
                break;
            case 3:
                Key = ControllerManager.Actions.Skill_3;
                break;
        }
    }

    void OnClickActive() {
        if (!AS || ((Player)AS.OC).Infusing) {
            return;
        }        
        if (AS.Ready()) {
            InterruptOthersLocally();
            switch (AS.active_type) {
                case ActiveSkill.ActiveType.Instant:
                    AS.Active();
                    break;
                case ActiveSkill.ActiveType.Charge:
                    AS.StartCasting(Slot);
                    break;
            }            
        }
    }

    void UpdateRedMask() {
        if (MPC.GetCurrStats(STATSTYPE.ESSENCE) - AS.EssenceCost < 0) {
            Red_Mask_OJ.SetActive(true);
        } else
            Red_Mask_OJ.SetActive(false);
    }

    void UpdateCDMask() {
        if (AS.RealTime_CD - Time.deltaTime <= 0 && AS.RealTime_CD != 0) {            
            BG.GetComponent<Animator>().Play("bg_blink");
        }
        CD_Mask.fillAmount = AS.GetCDPortion();
    }

    void LoadSkillIcon() {
        if (!AS) {
            IconImage.sprite = null;
            Color ImageIconColor = IconImage.color;
            ImageIconColor.a = 0;
            IconImage.color = ImageIconColor;
        }
        else {
            IconImage.sprite = Resources.Load<Sprite>("SkillIcons/"+AS.Name);
            Color ImageIconColor = IconImage.color;
            ImageIconColor.a = 255;
            IconImage.color = ImageIconColor;
        }
    }

    void AssigningUpdate() {
        GetComponent<Button>().interactable = true;
        var pointer = new PointerEventData(EventSystem.current);
        if (Key.WasPressed) {
            ExecuteEvents.Execute(this.gameObject, pointer, ExecuteEvents.submitHandler);
        }
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == gameObject)
            transform.parent.GetComponent<Image>().color = MyColor.Green;
        else
            transform.parent.GetComponent<Image>().color = MPC.MPUI.ResourceOrbSR.color;
    }

    void CombatUpdate() {
        if (!MPC.Alive)
            return;
        GetComponent<Button>().interactable = true;
        if (AS == null)
            return;
        var pointer = new PointerEventData(EventSystem.current);
        if (Key.WasPressed)
            ExecuteEvents.Execute(this.gameObject, pointer, ExecuteEvents.submitHandler);
        if (Key.WasReleased && AS.OC.Casting) {
            if (AS.active_type == ActiveSkill.ActiveType.Charge && AS.Ready() && AS.Holding) {
                AS.Active();
            }
        }
        if(ControllerManager.Actions.Cancel.WasPressed && AS.Holding) {            
            AS.Interrupt(true);
        }
        if (!CacheManager.MP.Alive) {            
            AS.Interrupt(false);
        }
    }

    void InterruptOthersLocally() {        
        foreach(ActiveSkillButtonController ASBC in OtherASBCs) {
            if(ASBC.AS!=null)
                ASBC.AS.Interrupt(false);
        }
    }

    public static void InterruptAll() {
        if (CacheManager.MP.Casting) {
            Client.Send(Protocols.ObjectInterruptCasting, Client.ClientID, Client.UDP);
        }
        foreach (var ASBC in FindObjectsOfType<ActiveSkillButtonController>()) {
            if (ASBC.AS != null)
                ASBC.AS.Interrupt(false);
        }
    }
}

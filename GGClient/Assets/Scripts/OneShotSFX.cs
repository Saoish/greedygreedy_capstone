﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotSFX : MonoBehaviour {
    public AudioClip SFX;

	// Use this for initialization
	void Start () {
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GreedyNameSpace;

public class Path_L : MonoBehaviour {
    MainPlayer MPC;

    public Text PathTitle;

    int[] PathContainSkills = new int[] { 0, 1, 2, 3, 4, 5, 6, 7,8 };
	// Use this for initialization
	void Start () {
        MPC = transform.parent.GetComponent<Tab_1>().MPC;
        UpdatePathInfo();
        SetPathSpiritColor();
    }

    void SetPathSpiritColor() {
        foreach (var particle in GetComponentsInChildren<ParticleSystem>(true)) {
            particle.startColor = new Color(PathTitle.color.r, PathTitle.color.g, PathTitle.color.b, particle.startColor.a);
        }
    }
	
	// Update is called once per frame
	void Update () {
        UpdatePathInfo();
    }

    int GetPathTotal() {
        int total = 0;
        foreach (int skillindex in PathContainSkills) {
            total += MPC.GetSkilllvlByIndex(skillindex);
        }
        return total;
    }

    void UpdatePathInfo() {
        if (MPC.Class == CLASS.Warrior) {
            PathTitle.color = MyColor.Orange;
            if (GetPathTotal() != 0)
                PathTitle.text = SkillPath.Berserker.ToString()+" (" + GetPathTotal() + ")";
            else
                PathTitle.text = SkillPath.Berserker.ToString();
        }else if(MPC.Class == CLASS.Mage) {
            PathTitle.color = MyColor.Cyan;
            if (GetPathTotal() != 0)
                PathTitle.text = SkillPath.Dominance.ToString() + " (" + GetPathTotal() + ")";
            else
                PathTitle.text = SkillPath.Dominance.ToString();
        }
    }

}

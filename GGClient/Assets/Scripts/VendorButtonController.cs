﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using UnityEngine.EventSystems;
public class VendorButtonController : MonoBehaviour, ISelectHandler {

    public VendorInteractionContent VIC;
    
    GameObject EquipmentIcon;
    [HideInInspector]
    public EquipmentInfo EI = null;
    [HideInInspector]
    public int Slot = -999;
    [HideInInspector]
    public Equipment E = null;
    

    private void Awake() {

    }

    public void OnSelect(BaseEventData eventData) {
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position, GameManager.SFX_Volume);
        if (!EI) {
            EI = transform.parent.parent.GetComponentInChildren<EquipmentInfo>(true);
        }
        EI.Show(E, EquipmentInfo.Mode.Inventory);
    }

    public void OnClickBuy() {
        if (!VIC.Goods[Slot].isNull) {
            if (CacheManager.UserData.Souls < VIC.Goods[Slot].Value) {
                RedNotification.Push(RedNotification.Type.NOT_ENOUGH_SOULS);
            }
            else {
                int InventorySlot = CacheManager.MP.FirstAvailbleInventorySlot;
                if (InventorySlot==Patch.InventoryCapacity-1) {
                    RedNotification.Push(RedNotification.Type.INVENTORY_FULL);
                }
                else {                                                                                
                    CacheManager.MP.Buy(InventorySlot,VIC.Goods[Slot]);                    
                    VIC.Goods[Slot] = new Equipment();
                    transform.parent.parent.Find("InventoryButtons/" + InventorySlot).GetComponent<InventoryButtonController>().UpdateSlot();
                    UpdateSlot();
                    EI.Show(E, EquipmentInfo.Mode.Inventory);
                }
            }
        }
    }

    void OnEnable() {
        Slot = int.Parse(gameObject.name);        
        UpdateSlot();

    }

    // Use this for initialization
    void Start() {
    }


    void UpdateInfo() {
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == gameObject) {
            EI.Show(E, EquipmentInfo.Mode.Inventory);
        }
    }

    public void UpdateSlot() {
        E = VIC.Goods[Slot];
        if (!E.isNull) {
            if (EquipmentIcon != null)
                DestroyObject(EquipmentIcon);
            EquipmentIcon = EquipmentController.ObtainInventoryIcon(E, transform);
        }
        else {
            DestroyObject(EquipmentIcon);
            EquipmentIcon = null;
        }
    }
}

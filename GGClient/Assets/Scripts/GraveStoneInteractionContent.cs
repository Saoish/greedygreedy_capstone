﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GreedyNameSpace;
using Networking.Data;
public class GraveStoneInteractionContent : InteractionContent {
    GraveData GD = null;
    public Text Class;
    public Text Lvl;
    public Text Message;
    public Text PlayerName;
    public Button Summon;
    public Button Discard;
    public Button Cancel;

    private GameObject CachedButtonOJ;

    private Coroutine SummonCO = null;
    static float SummonCD = 30f;

    private bool IsOn = false;

    void OnDestroy() {        
        if (IsOn) {
            if (PopUpNotification.GameObject && PopUpNotification.IsActive)
                PopUpNotification.AbortWithNo();
            ControllerManager.SyncActions = true;
            if (SummonCO != null)
                StopCoroutine(SummonCO);
        }
    }

    protected override void _Interrupt() {        
        CachedButtonOJ = EventSystem.current.currentSelectedGameObject;
        SyncActions = false;
        GetComponent<CanvasGroup>().interactable = false;
    }

    protected override void _Resume() {        
        try {
            SyncActions = true;
            GetComponent<CanvasGroup>().interactable = true;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(CachedButtonOJ);
        }
        catch {
            Debug.Log("Gravestone already is destroyed");
        }
    }

    void Update() {
        if (SyncActions && ControllerManager.Actions.Cancel.WasPressed) {
            if (gameObject.active)
                TurnOff();
        }
    }

    private void ConfirmSummon() {        
        TurnOff();
        Client.Send(Protocols.CreateSummonRequest, GD.OwnerNetworkID, Client.TCP);
        CacheManager.MP.MPI.DigGoInCD();
        MainPlayer.DropQueue();
        SummonCO = GameManager.instance.StartCoroutine(StartSummonCD());
    }IEnumerator StartSummonCD() {
        yield return new WaitForSeconds(SummonCD);
        SummonCO = null;
    }

    public void OnClickSummon() {
        if (SummonCO!=null) {
            RedNotification.Push(RedNotification.Type.SUMMON_SPAM);
        }
        else {
            if (MainPlayer.InQueue) {
                PopUpNotification.Push("Your queue will be dropped, do you want to procceed?", PopUpNotification.Type.Select);
                PopUpNotification.HereWeGo(ConfirmSummon);
            }
            else if (MainPlayer.HasGravestone) {
                PopUpNotification.Push("Your Gravestone will be discarded, do you want to procceed?", PopUpNotification.Type.Select);
                PopUpNotification.HereWeGo(ConfirmSummon);
            }
            else
                ConfirmSummon();
        }
    }

    public void OnClickCancel() {
        if (gameObject.active)
            TurnOff();
    }

    void ConfirmDiscard() {
        //SyncActions = true;
        TurnOff();
        Client.Send(Protocols.RemoveGrave, Client.ClientID, Client.TCP);
    }

    public void OnClickDiscard() {        
        PopUpNotification.Push("Are you sure?", PopUpNotification.Type.Select);
        PopUpNotification.HereWeGo(ConfirmDiscard);
    }

    public override void TurnOn() {
        ControllerManager.SyncActions = false;
        transform.position = CacheManager.MP.Position/* + new Vector2(0.5f,0)*/;
        if (GD == null) {
            GD = transform.parent.parent.GetComponent<GraveStoneController>().GD;
        }
        if (GD.OwnerNetworkID != Client.ClientID) {
            Class.text = GD.Class.ToString();
            Lvl.text = "Lvl " + GD.Lvl;
            if (GD.Message != String.Empty)
                Message.text = "\"" + GD.Message + "\"";
            else
                Message.text = "\"" + "Sadly, I did not even get a chance to leave my last words." + "\"";
            PlayerName.text = GD.Name;
            gameObject.SetActive(true);
            Discard.gameObject.SetActive(false);
            if (Client.Dominating) {
                Summon.gameObject.SetActive(true);
                SelectWithDelay(Summon.gameObject);
            }
            else {
                Summon.gameObject.SetActive(false);
                SelectWithDelay(Cancel.gameObject);
            }
        }
        else {
            Class.text = "Yourself";
            Lvl.text = "Lvl " + GD.Lvl;
            if (GD.Message != String.Empty)
                Message.text = "\"" + GD.Message + "\"";
            else
                Message.text = "\"" + "Sadly, I did not even get a chance to leave my last words." + "\"";
            PlayerName.text = GD.Name;
            gameObject.SetActive(true);
            Summon.gameObject.SetActive(false);
            Discard.gameObject.SetActive(true);
            SelectWithDelay(Discard.gameObject);
        }
        IsOn = true;
    }

    public override void TurnOff() {        
        gameObject.SetActive(false);
        SyncActions = true;
        GetComponent<CanvasGroup>().interactable = true;
        ControllerManager.SyncActions = true;
        IsOn = false;
    }
}

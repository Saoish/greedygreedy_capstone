﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;

public abstract class NPCInteraction : Interaction {
    public InteractionContent IC;
    public override bool IsTriggered() {
        return ControllerManager.Actions.Submit.WasPressed;
    }

    public override void Interact() {        
        IC.TurnOn();
    }

    //public override void SetIndication() {
    //    InteractionNotification.SetIndication("Talk", ButtonImages.X);
    //    InteractionNotification.TurnOn();        
    //}
}

﻿using UnityEngine;
using System.Collections;
using Networking.Data;
using System.IO;
using GreedyNameSpace;
public class LocalSetting {    
    public float SFX_Volume;
    public bool ShowNames;
    public bool ShowFloatingGuide;
    public bool ControllerEnable;
    public bool UseOnScreenKeyboard;

    public LocalSetting() {
        SFX_Volume = 1f;
        ShowNames = true;
        ShowFloatingGuide = true;
        ControllerEnable = true;
        UseOnScreenKeyboard = true;
    }
}

public class GameManager : MonoBehaviour {
    private static string LocalSettingFilePath = "settings.txt";
    private static LocalSetting LS;

    public static float SFX_Volume {
        get { return LS.SFX_Volume; }
        set { LS.SFX_Volume = value; }
    }

    public static bool ShowNames {
        get { return LS.ShowNames; }
        set { LS.ShowNames = value; }
    }

    public static bool ShowFloatingGuide {
        get { return LS.ShowFloatingGuide; }
        set { LS.ShowFloatingGuide = value; }
    }

    public static bool ControllerEnable {
        get { return LS.ControllerEnable; }
        set { LS.ControllerEnable = value; }
    }

    public static bool UseOnScreenKeyboard {
        get { return LS.UseOnScreenKeyboard; }
        set { LS.UseOnScreenKeyboard = value; }
    }

    public static GameManager instance;          

    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        } else {
            LoadLocalSetting();
            CollisionLayer.SetUpCollisionLayers();
            instance = this;
            DontDestroyOnLoad(this);
        }
    }

    void OnApplicationQuit() {        
        if (Client.Connected) {            
            if (SceneChecker.TargetInDungeons(Scene.Current_SID)) {
                Client.Send(Protocols.SaveLogOffState, new State(CacheManager.MP_Data.CurrStats, CacheManager.MP_Data.Alive, CacheManager.PrevState.SC, CacheManager.MP_Data.InventoryViewSigns), Client.TCP);
            }
            else if (!SceneChecker.TargetInUnLogables(Scene.Current_SID)) {
                Client.Send(Protocols.SaveLogOffState, new State(CacheManager.MP_Data.CurrStats, CacheManager.MP_Data.Alive, CacheManager.MP_Data.SC, CacheManager.MP_Data.InventoryViewSigns), Client.TCP);
            }
            else {
                if (CacheManager.PrevState != null) {
                    Client.Send(Protocols.SaveLogOffState, CacheManager.PrevState, Client.TCP);
                }
            }
        }
        Client.ShutDown();
        SaveLocalSetting();
    }

    void SaveLocalSetting() {        
        string json = JsonUtility.ToJson(LS);
        StreamWriter SaveStream = new StreamWriter(LocalSettingFilePath);
        SaveStream.Write(json);
        SaveStream.Close();
    }

    void LoadLocalSetting() {        
        if (!File.Exists(LocalSettingFilePath)) {
            LS = new LocalSetting();
        }
        else {            
            StreamReader LoadStream = new StreamReader(LocalSettingFilePath);
            string json = LoadStream.ReadToEnd();                        
            LS = JsonUtility.FromJson<LocalSetting>(json);
            if(LS == null)
                LS = new LocalSetting();
        }
    }

    void Update() {        
        if (ControllerManager.SyncActions && ControllerManager.Actions.ToggleName.WasPressed) {
            ShowNames = !ShowNames;
        }        
    }    

    public static void Quit() {        
        Application.Quit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
public class RespecInteraction : NPCInteraction {    
    public override void Interact() {
        if (MainPlayer.Self.CurrentlyHasEffect)
            RedNotification.Push(RedNotification.Type.EDIT_DURING_EFFECTS);
        else
            StartCoroutine(DelayPush());
    }
    IEnumerator DelayPush() {
        yield return new WaitForSeconds(0.1f);
        PopUpNotification.Push("Do you want to spent " + MyText.Colofied((MainPlayer.Level * Patch.RespecCostIncRate).ToString("n0"), "blue") + " to reset your skill tree?", PopUpNotification.Type.Select);
        PopUpNotification.HereWeGo(SubmitRespec);
    }

    void SubmitRespec() {        
        if (CacheManager.UserData.Souls >= MainPlayer.Level * Patch.RespecCostIncRate) {
            MainPlayer.Self.Respec();
            AudioSource.PlayClipAtPoint(ActionSFX.Respec, transform.position, GameManager.SFX_Volume);
        }
        else {
            RedNotification.Push(RedNotification.Type.NOT_ENOUGH_SOULS);
        }        
    }

    public override void SetIndication() {
        InteractionNotification.SetIndication("Prey", ButtonImages.X);
        InteractionNotification.TurnOn();
    }               
    
                         
}

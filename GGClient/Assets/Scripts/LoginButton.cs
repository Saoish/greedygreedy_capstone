﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

using Networking.Data;

public class LoginButton : MonoBehaviour {
    public UsernameFectcher UF;

    private void OnEnable() {
        UF.Load();
        StartCoroutine(SelectWithDelay());
    }
    IEnumerator SelectWithDelay() {
        yield return new WaitForSeconds(0.1f);
        if (!PopUpNotification.IsActive) {
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(gameObject);
        }else {
            while (PopUpNotification.IsActive)
                yield return null;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(gameObject);
        }
    }

    public void Login() {
        if(Client.ConnectMode == Client.Mode.Local)
            FindObjectOfType<IPFetcher>().UpdateIP();
        if (Client.ip == string.Empty) {
            PopUpNotification.Push("Please enter IP.", PopUpNotification.Type.Confirm);            
        }
        else if (/*UsernameManager.Username == null*/UF.Username_Input.text == string.Empty) {
            //PopUpNotification.Push("You have not yet registered.", PopUpNotification.Type.Confirm);
            PopUpNotification.Push("Please enter the username you want to login with.", PopUpNotification.Type.Confirm);
        }        
        else {
            PopUpNotification.Push("Waiting for server...", PopUpNotification.Type.Pending);                      
            Client.Connect();
            StartCoroutine(CheckConnectionAndSendRequest(0.5f));
            UF.Save();
        }
    }

    private IEnumerator CheckConnectionAndSendRequest(float time) {
        yield return new WaitForSeconds(time);
        if (!Client.Connected) {
            PopUpNotification.Push("No connection to server.", PopUpNotification.Type.Confirm);
        } else {
            Client.Send(Protocols.UserLogin, UsernameManager.Username, Client.TCP);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public static class Tag {
    public static string EnemyMonster = "EnemyMonster";
    public static string FriendlyMonster = "FriendlyMonster";         
    public static string EnemyPlayer = "EnemyPlayer";
    public static string FriendlyPlayer = "FriendlyPlayer";
    public static string PhantomCharge = "PhantomCharge";
    public static string SpiritualFollower = "SpiritualFollower";
}
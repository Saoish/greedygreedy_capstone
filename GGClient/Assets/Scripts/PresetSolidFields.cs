﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class PresetSolidFields :MonoBehaviour{
    public List<StatsRangeField> SolidFields;    
}

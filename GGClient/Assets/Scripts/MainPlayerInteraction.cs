﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPlayerInteraction : MonoBehaviour {
    [HideInInspector]
    public Interaction InteractTarget = null;    
    public MainPlayerUI MPUI;

        
    public bool Diggable = true;
    public bool Infusable = true;
    private float DigCD = 11f;
    private float InfusionCD = 3f;
    private Coroutine DigTracker = null;
    private Coroutine InfusionTracker = null;

    public void DigGoInCD() {
        if (DigTracker != null)
            StopCoroutine(DigTracker);
        DigTracker =  StartCoroutine(StartDigCoolDown());
    }private IEnumerator StartDigCoolDown() {
        Diggable = false;
        yield return new WaitForSeconds(DigCD);
        Diggable = true;
    }

    public void InfuseGoInCD() {
        if (InfusionTracker != null)
            StopCoroutine(InfusionTracker);
        InfusionTracker = StartCoroutine(StartInfusionCoolDown());
    }private IEnumerator StartInfusionCoolDown() {
        Infusable = false;
        yield return new WaitForSeconds(InfusionCD);
        Infusable = true;
    }

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        InteractionUpdate();
    }

    void InteractionUpdate() {
        if (InteractTarget != null && ControllerManager.SyncActions) {
            InteractTarget.SetIndication();
            if (InteractTarget.IsTriggered()) {
                InteractTarget.Interact();
            }
            else if (InteractTarget.AskedForCancel()) {
                InteractTarget.Cancel();
            }
            else if (!InteractTarget.gameObject.active) {
                InteractTarget = null;
                InteractionNotification.TurnOff();
            }
        }
        else {            
            InteractionNotification.TurnOff();
        }
    }

    void OnTriggerStay2D(Collider2D collider) {        
        if (collider.gameObject.layer == CollisionLayer.Interaction) {
            if(InteractTarget==null)
                InteractTarget = collider.GetComponent<Interaction>();
            else if (collider.GetComponent<Interaction>().Priority>= InteractTarget.Priority)
                InteractTarget = collider.GetComponent<Interaction>();
        }
    }

    void OnTriggerExit2D(Collider2D collider) {
        if (collider.gameObject.layer == CollisionLayer.Interaction && InteractTarget != null) {
            InteractTarget.Disengage();
        }
    }
}

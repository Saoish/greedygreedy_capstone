﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolTipShower : MonoBehaviour {    



    public static void TurnAllOn() {
        foreach (var tts in Resources.FindObjectsOfTypeAll<ToolTipShower>()) {//Very dangerous, for temp use only
            tts.gameObject.SetActive(true);
        }
    }

    public static void TurnAllOff() {
        foreach (var tts in Resources.FindObjectsOfTypeAll<ToolTipShower>()) {//Very dangerous, for temp use only
            tts.gameObject.SetActive(false);
        }
    }

    private void Start() {
        if (GameManager.ShowFloatingGuide)
            gameObject.SetActive(true);
        else
            gameObject.SetActive(false);
    }        
}

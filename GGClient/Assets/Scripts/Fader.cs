﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fader : MonoBehaviour {

    public Animator Anim;    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {		
	}

    public void FadeIn() {
        Anim.Play("FadeIn");
    }

    public void FadeOut() {
        Anim.Play("FadeOut");
    }    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GreedyNameSpace;

public class NetworkDebugger : MonoBehaviour {
    public Text Ping;
    public Text Dominating;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!Client.Connected)
            return;
        string Color;
        if (Client.ping < 100) {
            Color = MyText.green;
        }else if(Client.ping < 150) {
            Color = MyText.yellow;
        }
        else {
            Color = MyText.red;
        }
        Ping.text = "Ping: "+MyText.Colofied(Client.ping,Color) + "ms";

        if (Client.Dominating) {
            Dominating.text = MyText.Colofied("Dominator", MyText.megenta);
        }
        else {
            Dominating.text = MyText.Colofied("Slave", MyText.grey);
        }
	}
}

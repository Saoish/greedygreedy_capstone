﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class SkillSubMenu : InteractionContent {

    GameObject LvlUpBtn_OJ;
    GameObject AssignBtn_OJ;
    private GameObject CachedButtonOJ;
    void Awake() {
        LvlUpBtn_OJ = transform.Find("Level Up").gameObject;
        AssignBtn_OJ = transform.Find("Assign").gameObject;
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if ( SyncActions && ControllerManager.Actions.Cancel.WasPressed) {
            transform.parent.GetComponent<SkillButton>().DisableActiveSlotsAssigning();
            TurnOff();
        }
    }

    public void OnSelect() {
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position, GameManager.SFX_Volume);
    }

    public void OnClickLevelUp() {
        transform.parent.GetComponent<SkillButton>().LvlUp();
    }

    public void OnClickAssign() {
        transform.parent.GetComponent<SkillButton>().Assign();
    }

    public bool IsOn() {
        return gameObject.active;
    }

    public override void TurnOn() {
        //transform.parent.GetComponent<SkillButton>().MPUI.SyncActions = false;
        //transform.parent.GetComponent<SkillButton>().CSC.SyncActions = false;
        transform.GetComponentInParent<CharacterSheetController>().UnSyncCSCActions();
        gameObject.SetActive(true);
        DisableAllSkillButtonNavigation();
        if (transform.parent.GetComponent<SkillButton>().Skill.GetType().IsSubclassOf(typeof(ActiveSkill))) {
            AssignBtn_OJ.SetActive(true);
        }
        LvlUpBtn_OJ.SetActive(true);
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(LvlUpBtn_OJ);
    }

    public override void TurnOff() {
        EnableAllSkillButtonNavigation();
        EnableSubmenuNavigation();
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(transform.parent.gameObject);
        gameObject.SetActive(false);
        transform.parent.GetComponent<SkillButton>().RestoreCSCMPUIControl();
        GameManager.instance.StartCoroutine(ResumeCSCSyncWithDelay());
    }private IEnumerator ResumeCSCSyncWithDelay() {
        yield return new WaitForSeconds(0.1f);
        transform.GetComponentInParent<CharacterSheetController>().ResumeCSCSyncActions();
        EventSystem.current.SetSelectedGameObject(transform.parent.gameObject);
    }

    protected override void _Interrupt() {
        transform.parent.GetComponent<SkillButton>().DisableActiveSlotsAssigning();
        CachedButtonOJ = LvlUpBtn_OJ;
        SyncActions = false;
        GetComponent<CanvasGroup>().interactable = false;
    }

    protected override void _Resume() {        
        SyncActions = true;
        EnableSubmenuNavigation();
        GetComponent<CanvasGroup>().interactable = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(CachedButtonOJ);
    }

    public void DisableAllSkillButtonNavigation() {
        Navigation none = new Navigation();
        none.mode = Navigation.Mode.None;
        foreach (SkillButton SB in transform.parent.parent.parent.parent.GetComponentsInChildren<SkillButton>())
            SB.transform.GetComponent<Button>().navigation = none;
    }

    public void EnableAllSkillButtonNavigation() {
        Navigation auto = new Navigation();
        auto.mode = Navigation.Mode.Automatic;
        foreach (SkillButton SB in transform.parent.parent.parent.parent.GetComponentsInChildren<SkillButton>())
            SB.transform.GetComponent<Button>().navigation = auto;
    }
    
    public void DisableSubmenuNavigation() {
        Navigation none = new Navigation();
        none.mode = Navigation.Mode.None;
        LvlUpBtn_OJ.transform.GetComponent<Button>().navigation = none;
        AssignBtn_OJ.transform.GetComponent<Button>().navigation = none;
    }

    public void EnableSubmenuNavigation() {
        Navigation auto = new Navigation();
        auto.mode = Navigation.Mode.Automatic;
        LvlUpBtn_OJ.transform.GetComponent<Button>().navigation = auto;
        AssignBtn_OJ.transform.GetComponent<Button>().navigation = auto;
    }
}

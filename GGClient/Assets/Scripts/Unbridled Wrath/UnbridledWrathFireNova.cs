﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;

public class UnbridledWrathFireNova : MonoBehaviour {
    public AudioClip SFX;
    public CircleCollider2D SelfCollider;
    ObjectController OC;
    float DamageScale;
    private Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    public void Explode(Vector2 Position, ObjectController OC, float DamageScale) {
        UnbridledWrathFireNova UWFN = Instantiate((Resources.Load("SkillPrefabs/Tools/" + gameObject.name) as GameObject), Position, Quaternion.identity).GetComponent<UnbridledWrathFireNova>();
        UWFN.OC = OC;
        UWFN.DamageScale = DamageScale;
    }

    // Use this for initialization
    void Start() {        
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
        StartCoroutine(RunEffect());
        Destroy(gameObject, 2f);
    }

    IEnumerator RunEffect() {
        yield return new WaitForSeconds(0.1f);
        SelfCollider.enabled = false;        
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.transform.GetComponent<ObjectController>();
        Damage dmg = CombatGenerator.GenerateDirectDamage(OC, target, DamageScale, CR.UnbridledWrath);
        OC.ON_DMG_DEAL += DealDamage;
        OC.ON_DMG_DEAL(dmg);
        OC.ON_DMG_DEAL -= DealDamage;
        HittedStack.Push(collider);
    }


    private void DealDamage(Damage dmg) {
        if (Client.Connected)
            NetworkDealDamage(dmg);
        else
            LocalDealDamage(dmg);
    }

    private void NetworkDealDamage(Damage dmg) {
        if (OC is MainPlayer) {            
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
        }
    }

    private void LocalDealDamage(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);        
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
    }
}

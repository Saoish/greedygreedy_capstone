﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
using System;

public class UnbridledWrath : ActiveSkill {
    public override CR Type { get { return CR.UnbridledWrath; } }
    float Duration = 10f;
    float DamageScale;   

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        UnbridledWrathlvl UWL = (UnbridledWrathlvl)AllLvls[Index];
        return "\nInstantly gain Unbridled Wrath Debuff for " + Duration + " secs: You lose " + UnbridledWrathDebuff.MaxHealthTickPercentage + "% of your max health every " + UnbridledWrathDebuff.TickInterval + " sec and release a Fire Nova to deal " + MyText.Colofied(DamageScale + "% damage", ScaleHighlight) + " to enemies around you."
            + "\n\nCost: " + MyText.Colofied(UWL.EssenceCost + " Essence", ScaleHighlight) + "\nCD: " + MyText.Colofied(UWL.CD + " secs", ScaleHighlight);
    }


    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        UnbridledWrathlvl UWL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                UWL = GetComponent<UnbridledWrath1>();
                break;
            case 2:
                UWL = GetComponent<UnbridledWrath2>();
                break;
            case 3:
                UWL = GetComponent<UnbridledWrath3>();
                break;
            case 4:
                UWL = GetComponent<UnbridledWrath4>();
                break;
            case 5:
                UWL = GetComponent<UnbridledWrath5>();
                break;
        }
        CD = UWL.CD;
        EssenceCost = UWL.EssenceCost;
        DamageScale = UWL.DamageScale;        
        GenerateDescription();
    }

    void DealFireNovaDamage(Damage dmg) {
        if (Client.Connected)
            NetworkDealFireNovaDamage(dmg);
        else
            LocalDealFireNovaDamage(dmg);
    }
    
    void NetworkDealFireNovaDamage(Damage dmg) {
        if(OC is MainPlayer)
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
    }
    void LocalDealFireNovaDamage(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
    }

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        if(OC is MainPlayer) {
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.UnbridledWrathDebuff, OC.NetworkID, OC.NetworkID, Duration, CombatGenerator.GenerateEffectParameters(DamageScale)), Client.TCP);
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
            RealTime_CD = CD;
        }
    }

    void ActiveNetwork() {
        HandyNetwork.SendSkillActivation(OC, Type, null);        
    }

    void ActiveLocal() {
        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
        CacheManager.Effects[CR.UnbridledWrathDebuff].Apply(OC, OC, Duration, CombatGenerator.GenerateEffectParameters(DamageScale));        
        RealTime_CD = CD;
    }           
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendorInteraction : NPCInteraction {    
    public EquipmentInfo EI;

    protected override void Start() {
        base.Start();
        ((VendorInteractionContent)IC).PreStock();
        EI = (Instantiate(EI.gameObject, IC.transform) as GameObject).GetComponent<EquipmentInfo>();
        EI.transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        EI.transform.GetComponent<RectTransform>().localScale = new Vector2(0.0015f,0.0015f);
    }

    public override void SetIndication() {
        InteractionNotification.SetIndication("Shop", ButtonImages.X);
        InteractionNotification.TurnOn();
    }
}

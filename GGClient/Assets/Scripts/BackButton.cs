﻿using UnityEngine;
using System.Collections;


public class BackButton : MonoBehaviour {

    public void GoBack() {
        if(Scene.Current_SInt - 1 <=0)
            Client.ManuallyDisconnectSelf();
        Scene.Load(Scene.Current_SInt - 1);
    }

}

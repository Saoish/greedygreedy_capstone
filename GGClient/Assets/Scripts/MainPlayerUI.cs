﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GreedyNameSpace;

public class MainPlayerUI : MonoBehaviour {
    public MainPlayer MPC;   
    public Transform HealthMask;
    public Transform ManaMask;
    public Transform ExpMask;
    public Transform ActionBar;
    public SpriteRenderer ResourceOrbSR;


    public MenuController MC;
    public CharacterSheetController CSC;

    GameObject ES;

    [HideInInspector]
    public bool SyncActions = true;

    void Start() {
        UpdatEssenceOrbColor();
    }
	
    void Update() {
        PopUpUIUpdate();
        UpdateHealthManaBar();
        UpdateExpBar();
    }

    private void UpdatEssenceOrbColor() {
        switch (MPC.Class) {
            case CLASS.Warrior:
                ResourceOrbSR.color = MyColor.Orange;
                break;
            case CLASS.Mage:
                ResourceOrbSR.color = MyColor.Blue;
                break;
        }
    }

    private void PopUpUIUpdate() {
        if (!SyncActions)
            return;
        if (ControllerManager.SyncActions && CacheManager.MP.Alive) {
            if (ControllerManager.Actions.Inventory.WasPressed) {
                CSC.CachedTabIndex = 0;
                CSC.TurnOn();
            }
            else if (ControllerManager.Actions.SkillTree.WasPressed) {
                CSC.CachedTabIndex = 1;
                CSC.TurnOn();
            }
            else if (ControllerManager.Actions.Menu.WasPressed) {
                MC.TurnOn();
            }
        }
        //if (ControllerManager.Actions.Cancel.WasPressed) {
        //    CSC.TurnOff();
        //    MC.TurnOff();
        //}
    }

    public void UpdateHealthManaBar() {
        if(MPC.GetCurrStats(STATSTYPE.HEALTH)/MPC.GetMaxStats(STATSTYPE.HEALTH) >=0)
            HealthMask.transform.localScale = new Vector2(1, MPC.GetCurrStats(STATSTYPE.HEALTH) / MPC.GetMaxStats(STATSTYPE.HEALTH));
        else
            HealthMask.transform.localScale = new Vector2(1, 0);
        if (MPC.GetCurrStats(STATSTYPE.ESSENCE) / MPC.GetMaxStats(STATSTYPE.ESSENCE) >= 0)
            ManaMask.transform.localScale = new Vector2( 1, MPC.GetCurrStats(STATSTYPE.ESSENCE) / MPC.GetMaxStats(STATSTYPE.ESSENCE));
        else
            ManaMask.transform.localScale = new Vector2(1,0);
    }

    public void UpdateExpBar() {
        ExpMask.GetComponent<Image>().fillAmount = ((float)MPC.exp / (float)MPC.NextLevelExp);
    }


    public void UpdateEquippedSlot(EQUIPTYPE slot) {
        CSC.Tab_0.transform.Find("EquippedSlotButtons/" + (int)slot).GetComponent<EquippedButtonController>().UpdateSlot();
    }

    public void UpdateInventorySlot(int slot) {
        CSC.Tab_0.transform.Find("InventoryButtons/" + slot).GetComponent<InventoryButtonController>().UpdateSlot();
    }


    public void Close() {
        CSC.TurnOff();
        MC.TurnOff();
    }
}

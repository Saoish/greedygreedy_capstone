﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public class BloodSpring : Interaction {

    bool Drinkable = true;

    public override bool IsTriggered() {
        return ControllerManager.Actions.Submit.WasPressed;
    }

    public override void Interact() {
        if (Drinkable)
            HealPlayer();
        else
            RedNotification.Push(RedNotification.Type.CANT_DO_NOW);
    }

    public override void SetIndication() {
        if (Drinkable) {
            InteractionNotification.SetIndication(MyText.Colofied("Drink some blood", MyText.orange), ButtonImages.X);
            InteractionNotification.TurnOn();
        }
        else
            InteractionNotification.TurnOff();
    }

    void HealPlayer() {
        if (Client.Connected)
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.HealingBuff, 0, Client.ClientID, 5f, CombatGenerator.GenerateEffectParameters(CacheManager.MP.MaxHealth * 0.05f)), Client.TCP);
        else
            CacheManager.Effects[CR.HealingBuff].Apply(null, CacheManager.MP, 5f, CombatGenerator.GenerateEffectParameters(CacheManager.MP.MaxHealth * 0.05f));
        StartCoroutine(DrinkableGoInCD());
    } private IEnumerator DrinkableGoInCD() {
        Drinkable = false;
        yield return new WaitForSeconds(10f);
        Drinkable = true;
    }
}

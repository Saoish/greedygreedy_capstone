﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;
using Networking.Data;

public class NPC : Monster {
    //public new StaticObjectData Data;

    public SNID SNID;//For local testing only
    public Interaction Interaction;    

    public override ObjectController Instantiate(int NetworkID, bool Reset) {
        if (Reset) {
            Data.Target = 0;
            Data.Alive = true;
            Data.SC = new SceneCoordinate(Data.RespawnSC);
            Data.CurrStats = new Stats(MaxStats);
            Data.OnGoingEffects.Clear();
        }
        else if (!Data.Alive)
            return null;
        GameObject OJ = Resources.Load("NPCPrefabs/"+GetName()) as GameObject;
        OJ = Instantiate(OJ, Data.SC.Coordinate.ToVector, Quaternion.identity) as GameObject;
        OJ.GetComponent<NPC>().Reset = Reset;
        OJ.GetComponent<NPC>().Data = Data;
        OJ.name = GetName();        
        OJ.GetComponent<NPC>().NetworkID = NetworkID;
        return OJ.GetComponent<NPC>();
    }

    void LocalInstantiate() {        
        StaticObjectData testing_data = new StaticObjectData(SNID, 10f, new SceneCoordinate(Scene.Current_SID, transform.position));
        testing_data.CurrStats = new Stats(MaxStats);
        this.Data = testing_data;        
        this.NetworkID = (int)SNID;
        CacheManager.AddNPC(SNID,testing_data);
        CacheManager.Objects[(int)SNID].OC = this;
    }

    protected override void Awake() {
        if (!Client.Connected)
            LocalInstantiate();
        base.Awake();
    }

    //public override OID OID() {
    //    return GreedyNameSpace.OID.NPC;
    //}
}

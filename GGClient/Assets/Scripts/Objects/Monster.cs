﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;
using System;

public class Monster : ObjectController {
    [HideInInspector]
    public Stats Growth;

    public string Name;

    public bool LootDrop = true;

    public int exp;

    //public int lvl;
    //public int lvl {
    //    get { return Data.lvl; }
    //    set { lvl = value; }
    //}

    private Animator BaseModelAnim;    

    [HideInInspector]
    public AIController AI;

    public Stats GetScaledMaxStats(int lvl) {
        Stats Scaled = new Stats(MaxStats);
        if (lvl > 1) {
            for (int i = 0; i < Patch.NumOfStatsFields; i++) {//Max is Patch.lvlCap - 1 times
                if (Growth.stats[i] > 0) {
                    Scaled.stats[i] += Growth.stats[i] * (lvl - 1);
                }
            }
        }
        return Scaled;
    }

    public Monster Spawn(int NetworkID) {//Dont call it on NPC
        GameObject MonsterOJ = Resources.Load("MonsterPrefabs/"+GetName()) as GameObject;
        MonsterOJ = Instantiate(MonsterOJ, Data.SC.Coordinate.ToVector, Quaternion.identity) as GameObject;
        MonsterOJ.GetComponent<Monster>().Data = Data;        
        switch (OID) {
            case GreedyNameSpace.OID.FriendlyMonster:
                MonsterOJ.layer = CollisionLayer.Friendly;
                MonsterOJ.tag = Tag.FriendlyMonster;                
                break;
            case GreedyNameSpace.OID.EnemyMonster:
                MonsterOJ.layer = CollisionLayer.Enemy;
                MonsterOJ.tag = Tag.EnemyMonster;
                break;
        }
        MonsterOJ.name = GetName();
        MonsterOJ.GetComponent<Monster>().NetworkID = NetworkID; /*CacheManager.SubscribeDynamicMonster(MonsterOJ.GetComponent<Monster>(), Data);*/                
        return MonsterOJ.GetComponent<Monster>();
    }

    public override ObjectController Instantiate(int NetworkID,bool Reset) {                
        if (Reset) {
            Data.Target = 0;
            Data.Alive = true;
            Data.SC = new SceneCoordinate(Data.RespawnSC);
            Data.CurrStats = new Stats(GetScaledMaxStats(Data.lvl));
            Data.OnGoingEffects.Clear();
        }
        else if (!Data.Alive)
            return null;
        GameObject OJ = Resources.Load("MonsterPrefabs/" + GetName()) as GameObject;
        OJ = Instantiate(OJ, Data.SC.Coordinate.ToVector, Quaternion.identity) as GameObject;
        OJ.GetComponent<Monster>().Reset = Reset;
        OJ.GetComponent<Monster>().Data = Data;     
        switch (OID) {
            case OID.FriendlyMonster:
                OJ.layer = CollisionLayer.Friendly;
                OJ.tag = Tag.FriendlyMonster;
                break;
            case OID.EnemyMonster:
                OJ.layer = CollisionLayer.Enemy;
                OJ.tag = Tag.EnemyMonster;
                break;
        }
        OJ.name = GetName();
        OJ.GetComponent<Monster>().NetworkID = NetworkID;        
        return OJ.GetComponent<Monster>();
    }    

    protected override void Awake() {
        base.Awake();
        //VisualHolder.GetComponent<SpriteRenderer>().sortingLayerName = SortingLayer.Object;
    }

    // Use this for initialization
    protected override void Start() {        
        base.Start();
        InitMaxStats();
        InitCurrStats();
        ApplyMapEvents();        
        BaseModelAnim = OZC.transform.Find("BaseModel").GetComponent<Animator>();                
        ApplyOnGoingEffects();        
        AI = GetComponent<AIController>();
        AI.Target = CacheManager.GetOC(Data.Target);
        LetsRoll();
        InstantiatedCheck();                
    }
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
        ControlUpdate();
        AnimUpdate();        
    }

    protected override void MoveFixedUpdate() {
        //if (Moving) {
        //    ProcessContacts();
        //    Vector2 MoveVector = AI.MoveVector;
        //    foreach (var Contacted_OJ in Contacts) {
        //        Vector2 ContactDirection = Vector3.Normalize(Position - Contacted_OJ.Position);
        //        if (MoveVector.x > 0 && ContactDirection.x < 0 || MoveVector.x < 0 && ContactDirection.x > 0) {
        //            MoveVector.x = 0;
        //        }
        //        if (MoveVector.y > 0 && ContactDirection.y < 0 || MoveVector.y < 0 && ContactDirection.y > 0) {
        //            MoveVector.y = 0;
        //        }
        //        if (MoveVector == Vector2.zero)
        //            break;
        //    }
        //    rb.MovePosition(rb.position + MoveVector * (CurrMoveSpeed / 100) * Time.fixedDeltaTime);            
        //    SC.Coordinate.ToVector = Position;
        //}

        if (Moving) {
            Vector2 MoveVector = AI.MoveVector;
            foreach (var Contacted_OJ in Contacts) {
                Vector2 ContactDirection = Vector3.Normalize(Position - (Vector2)Contacted_OJ.transform.position);
                if (MoveVector.x > 0 && ContactDirection.x < 0 || MoveVector.x < 0 && ContactDirection.x > 0) {
                    MoveVector.x = 0;
                }
                if (MoveVector.y > 0 && ContactDirection.y < 0 || MoveVector.y < 0 && ContactDirection.y > 0) {
                    MoveVector.y = 0;
                }
                if (MoveVector == Vector2.zero)
                    break;
            }
            rb.MovePosition(rb.position + MoveVector * (CurrMoveSpeed / 100) * Time.fixedDeltaTime);
            SC.Coordinate.ToVector = Position;
        }
    }

    void ControlUpdate() {
        if (Stunned || !Alive) {
            Attacking = false;
            Moving = false;
        } else if (Casting) {
            Attacking = false;
            Moving = false;
            Direction = AI.Direction;
        } else if (Immobilized) {
            Moving = false;
            Attacking = AI.Attacking;            
            Direction = AI.Direction;
        }
        else {
            Attacking = AI.Attacking;
            if (HasForce) {
                Moving = false;
            } else {
                Moving = AI.Moving;
            }
            Direction = AI.Direction;
        }
    }

    //Initialization
    void InitMaxStats() {
        if (this is NPC)
            return;
        MaxStats = GetScaledMaxStats(lvl);
    }        

    void InitCurrStats() {        
        if (CurrStats.IsNull) {
            CurrStats = new Stats(MaxStats);            
        }
        else {
            CurrStats = Data.CurrStats;            
        }        
    }

    //----------public
    private void NetworkDeductHealth(Damage dmg) {        
        IC.PopUpText(dmg);
        if (dmg.Crit) {
            VisualHolderAnim.Play("crit");
        }        
        if (CurrHealth - dmg.Amount <= 0 && Alive) {//Checking alive is critical for non-duplicated die event
            if (dmg.applyerID == Client.ClientID) {//Main authority to update        
                Client.Send(Protocols.ObjectOnDeath, dmg, Client.TCP);
                ON_DEATH_UPDATE += Die;
                ON_DEATH_UPDATE(dmg);
                ON_DEATH_UPDATE -= Die;
                if (SceneChecker.TargetInWorldStates(Scene.Current_SID)) {
                    Client.Send(Protocols.UpdateToDeadState, new UpdateToDeadStateData(NetworkID,Data.RespawnSC), Client.TCP);
                }
            }
            else {
                CurrHealth = 0;
            }
        }
        else {
            CurrStats.Dec(STATSTYPE.HEALTH, dmg.Amount);
            if(/*SceneChecker.TargetInWorldStates(Scene.Current_SID) && */dmg.applyerID == Client.ClientID) {//Main authority to update, and server determines if this data should be thrown into the worldstate of instance
                Client.Send(Protocols.UpdateCurrHealthWithCoordinate, new UpdateCurrHealthWithCoordinateData(NetworkID, CurrHealth,Position), Client.TCP);
            }
        }
    }

    private void LocalDeductHealth(Damage dmg) {
        IC.PopUpText(dmg);
        if (dmg.Crit) {
            VisualHolderAnim.Play("crit");
        }
        if (CurrHealth - dmg.Amount <= 0 && Alive) {
            ON_DEATH_UPDATE += Die;
            ON_DEATH_UPDATE(dmg);
            ON_DEATH_UPDATE -= Die;
        }
        else {
            CurrStats.Dec(STATSTYPE.HEALTH, dmg.Amount);
        }
    }

    //Combat
    override public void DeductHealth(Damage dmg) {
        if (Client.Connected) {            
            NetworkDeductHealth(dmg);
        }
        else {            
            LocalDeductHealth(dmg);
        }
    }
    public override void HealHP(Heal heal_hp) {//Not yet tested
        base.HealHP(heal_hp);
        if(SceneChecker.TargetInWorldStates(Scene.Current_SID) && Client.Connected) {
            Client.Send(Protocols.UpdateCurrHealth, new UpdateCurrHealthData(NetworkID,CurrHealth), Client.TCP);
        }
    }

    public override void Die(Damage dmg) {
        base.Die(dmg);
        SpawnEXP();
        if (LootDrop) {
            if (GetComponent<LootSpawner>())
                GetComponent<LootSpawner>().SpawnLoots(lvl);
        }
        if(DieVFX)
            ActiveOutsideVFXPartical(DieVFX);        
        Destroy(gameObject,1f);
    }

    //Enemy Anim
    void AnimUpdate() {
        if (!Alive)
            return;
        if (InAttackingState) {
            BaseModelAnim.speed = GetAttackAnimSpeed();
        } else {
            BaseModelAnim.speed = GetMovementAnimSpeed();
        }
        if (Attacking) {
            InAttackingState = true;
            BaseModelAnim.SetBool("IsAttacking", true);
            BaseModelAnim.SetInteger("Direction", Direction);
            BaseModelAnim.SetBool("IsMoving", false);
        }
        else if (Moving && !Attacking) {
            BaseModelAnim.SetBool("IsMoving", true);
            BaseModelAnim.SetInteger("Direction", Direction);
            BaseModelAnim.SetBool("IsAttacking", false);
        }
        else {
            BaseModelAnim.SetBool("IsMoving", false);
            BaseModelAnim.SetBool("IsAttacking", false);
        }
    }   

    protected void SpawnEXP() {
        //if (GameObject.Find("MainPlayer") != null) {
        //    GameObject.Find("MainPlayer").GetComponent<MainPlayer>().AddEXP(exp);
        //}      
        if (CacheManager.MP.Alive) {
            if (lvl == 1)
                CacheManager.MP.AddEXP(exp);
            else
                CacheManager.MP.AddEXP(exp + (exp * (lvl-1) / 10));
        }
    }

    public override string GetName() {
        return Name;
    }

    //public override OID OID() {
    //    return GreedyNameSpace.OID.EnemyMonster;
    //}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using GreedyNameSpace;
using Networking.Data;

public class OtherPlayer : Player {
    [HideInInspector]
    public Vector2 NetworkPosition;
    [HideInInspector]    
    Vector2 PredictDirection;

    static Vector2 DumpPosition = new Vector2(-999f, -999f);
    static float TouchSensity = 0.03f;

    public override ObjectController Instantiate(int NetworkID,bool Reset) {        
        if (!Alive && Reset) {
            if (Scene.Current_SID != Data.RespawnSC.SID)
                Data.RespawnSC = CacheManager.Map.GetCloestestRespawnSC(Data.SC.Coordinate.ToVector);
            Data.SC = new SceneCoordinate(Data.RespawnSC);
            Data.OnGoingEffects.Clear();
        }
        GameObject OJ = Resources.Load("PlayerPrefabs/OtherPlayer") as GameObject;
        OJ = Instantiate(OJ, Data.SC.Coordinate.ToVector, Quaternion.identity) as GameObject;        
        OJ.GetComponent<OtherPlayer>().Reset = Reset;
        OJ.GetComponent<OtherPlayer>().Data = Data;

        OJ.GetComponent<OtherPlayer>().NetworkPosition = Data.SC.Coordinate.ToVector;

        switch (OID) {
            case OID.FriendlyPlayer:
                OJ.layer = CollisionLayer.Friendly;
                OJ.tag = Tag.FriendlyPlayer;                
                break;
            case OID.EnemyPlayer:
                OJ.layer = CollisionLayer.Enemy;
                OJ.tag = Tag.EnemyPlayer;                
                break;
        }
        OJ.name = GetName();
        OJ.GetComponent<OtherPlayer>().NetworkID = NetworkID;
        return OJ.GetComponent<OtherPlayer>();
    }

    public void AssignNetworkPosition(PositionData PD) {            
        NetworkPosition = PD.Position;
        Direction = PD.Direction;        
    }
    public void AssignNetworkPosition(Vector2 N_Position) {
        NetworkPosition = N_Position;
    }
    //void MoveUpdate() {
    //    if (Moving || Casting) {
    //        if (NetworkPosition != DumpPosition) {
    //            Position = Vector2.MoveTowards(Position, NetworkPosition, Time.deltaTime * CurrMoveSpeed / 100f);
    //            SC.Coordinate.ToVector = Position;
    //        }
    //    }
    //}

    protected override void MoveFixedUpdate() {
        if (Moving || Casting) {
            if (NetworkPosition != DumpPosition) {
                //Position = Vector2.Lerp(Position, NetworkPosition, Time.fixedDeltaTime * 10f);
                Position = Vector2.MoveTowards(Position, NetworkPosition, Time.deltaTime * CurrMoveSpeed / 100f);
                //Vector2 MoveVector = PredictDirection;
                //foreach (var Contacted_OJ in Contacts) {
                //    Vector2 ContactDirection = Vector3.Normalize(Position - (Vector2)Contacted_OJ.transform.position);
                //    if (MoveVector.x > 0 && ContactDirection.x < 0 || MoveVector.x < 0 && ContactDirection.x > 0) {
                //        MoveVector.x = 0;
                //    }
                //    if (MoveVector.y > 0 && ContactDirection.y < 0 || MoveVector.y < 0 && ContactDirection.y > 0) {
                //        MoveVector.y = 0;
                //    }
                //    if (MoveVector == Vector2.zero)
                //        break;
                //}
                //transform.position = (rb.position + MoveVector * (CurrMoveSpeed / 100) * Time.fixedDeltaTime);
                SC.Coordinate.ToVector = Position;
            }
        }
    }

    protected override void ControlUpdate() {
        if (Stunned || !Alive || Casting || Infusing) {
            Attacking = false;
            Moving = false;
        }
        else if (Immobilized || HasForce || Position == NetworkPosition || Lerping) {
            Moving = false;
            NetworkPosition = DumpPosition;//Expecing new position
        }
        else if (NetworkPosition != DumpPosition && Position != NetworkPosition) {
            Moving = true;
            //PredictDirection = Vector3.Normalize(NetworkPosition - Position);
            //if (Vector2.Distance(NetworkPosition, Position) >= TouchSensity) {//There is a case that it get pushed
            //    if (!Attacking) {
            //        if (Mathf.Abs((float)Math.Round(PredictDirection.x, 1)) >= Mathf.Abs((float)Math.Round(PredictDirection.y, 1))) {
            //            if (PredictDirection.x < 0)
            //                Direction = 1;
            //            else if (PredictDirection.x > 0)
            //                Direction = 2;
            //        }
            //        else {
            //            if (PredictDirection.y < 0)
            //                Direction = 0;
            //            else if (PredictDirection.y > 0)
            //                Direction = 3;
            //        }
            //    }
            //}
        }
    }

    override public void DeductHealth(Damage dmg) {
        if (InfusedPortal)
            InfusedPortal.Defuse(this);
        IC.PopUpText(dmg);
        if (dmg.Crit) {
            VisualHolderAnim.Play("crit");
        }
        if (CurrHealth - dmg.Amount < 0)//Death Check by main authority
            CurrHealth = 0;
        else
            CurrHealth -= dmg.Amount;
    }

    public override void Die(Damage dmg) {
        base.Die(dmg);
    }
}

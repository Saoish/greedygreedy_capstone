﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using GreedyNameSpace;
using Networking.Data;
using Visual;

public abstract class ObjectController : MonoBehaviour {
    [HideInInspector]
    public bool Lerping = false;//Currently only checked by otherplayer

    [HideInInspector]
    public ObjectData Data;

    //Networking Stuff
    [HideInInspector]
    public int NetworkID = 0;//0 is unauthorizied

    [HideInInspector]
    public Rigidbody2D rb;

    public GameObject DieVFX;
    public AudioClip DieSFX;

    [HideInInspector]
    public bool AITargetable = false;

    [HideInInspector]
    public int Direction = 0;
    [HideInInspector]
    public bool Casting = false;
    [HideInInspector]
    public bool Stunned = false;
    [HideInInspector]
    public bool Immobilized = false;

    public bool Alive {
        get { return Data.Alive; }
        set { Data.Alive = value; }
    }

    [HideInInspector]
    public bool Moving = false;
    [HideInInspector]
    public bool Attacking = false;


    [HideInInspector]
    public bool InAttackingState = false;//This var is for local attack sync, nothing to do with network

    protected List<Skill> CachedTreeSkills;

    public Dictionary<CR, Skill> Skills;
    public Dictionary<CR, Effect> Effects;

    //public List<ObjectController> Contacts;

    public delegate ProjectileData on_generate_pd(ProjectileData pd);
    public delegate void on_dmg_deal(Damage dmg);
    public delegate void on_health_loss(Damage dmg);
    public delegate void on_health_gain(Heal heal);
    public delegate void on_essense_loss(EssenseLoss essense_cost);
    public delegate void on_essense_gain(EssenseGain essense_gain);
    public delegate void on_dealth_update(Damage dmg);

    public on_generate_pd ON_GENERATE_PD;
    public on_dmg_deal ON_DMG_DEAL;
    public on_health_loss ON_HEALTH_LOSS;
    public on_health_gain ON_HEALTH_GAIN;
    public on_essense_loss ON_ESSENSE_LOSS;
    public on_essense_gain ON_ESSENSE_GAIN;
    public on_dealth_update ON_DEATH_UPDATE;

    public float movement_animation_interval = 1f;
    public float attack_animation_interval = 1f;

    private Transform VFXTransform;
    [HideInInspector]
    public Object_Z_Controller OZC;
    [HideInInspector]
    public Transform VisualHolder;
    protected Animator VisualHolderAnim;

    protected Transform MeleeAttackColliderTransform;
    protected Transform SkillsTransform;
    protected Transform EffectsTransform;
    [HideInInspector]
    public CircleCollider2D ObjectCollider;
    [HideInInspector]
    public IndicationController IC;
    public float IC_yaxis;

    [HideInInspector]
    public Stats MaxStats;
    public Stats CurrStats {
        get { return Data.CurrStats; }
        set { Data.CurrStats = value; }
    }
    public OID OID {
        get { return Data.OID; }
        set { Data.OID = value; }
    }
    public SceneCoordinate SC {
        get { return Data.SC; }
        set { Data.SC = value; }
    }

    public int lvl {
        get { return Data.lvl; }
        set { lvl = value; }
    }    

    [HideInInspector]
    public bool Reset = false;
    public abstract ObjectController Instantiate(int NetworkID, bool Reset);


    virtual protected void Awake() {
        //gameObject.layer = CollisionLayer.KillingGround;
        IC = Instantiate(Resources.Load<GameObject>("UIPrefabs/Indication Board"), transform).GetComponent<IndicationController>();
        rb = transform.GetComponent<Rigidbody2D>();
        MeleeAttackColliderTransform = transform.Find("MeleeAttackCollider");        
        VisualHolder = transform.Find("VisualHolder");
        OZC = VisualHolder.GetComponentInChildren<Object_Z_Controller>();
        VFXTransform = OZC.transform.Find("VFX");
        VFXTransform.localPosition = new Vector3(0, 0, Layer.Effect_Z);
        VisualHolderAnim = VisualHolder.GetComponent<Animator>();
        SkillsTransform = transform.Find("Skills");
        EffectsTransform = transform.Find("Effects");
        ObjectCollider = GetComponent<CircleCollider2D>();
        Skills = new Dictionary<CR, Skill>();
        Effects = new Dictionary<CR, Effect>();        
    }

    protected void LetsRoll() {
        ObjectCollider.enabled = true;
    }

    virtual protected void Start() {

    }

    virtual protected void Update() {
        //foreach (var Contact in KillingGroundCollidings) {
        //    if (Contact != ObjectCollider && !HasForce) {                
        //        Contact.GetComponent<ObjectController>().NormalizePhysics();
        //    }
        //}
    }


    virtual protected void FixedUpdate() {
        MoveFixedUpdate();
        foreach (var Contact in Contacts) {
            if (Contact != ObjectCollider) {
                Contact.GetComponent<ObjectController>().NormalizeDrag();
            }
        }
    }

    protected Collider2D[] Contacts {
        get {
            int layer = (1 << CollisionLayer.Friendly | 1 << CollisionLayer.Enemy);
            return Physics2D.OverlapCircleAll(Position, ObjectCollider.radius, layer);
        }
    }

    //virtual protected void OnCollisionEnter2D(Collision2D collision) {
    //    if (collision.collider.tag == Tag.EnemyMonster || collision.collider.tag == Tag.FriendlyPlayer || collision.collider.tag == Tag.EnemyPlayer) {
    //        ObjectController OC = collision.collider.transform.GetComponent<ObjectController>();
    //        OC.ZerolizeForce();
    //        Contacts.Add(OC);
    //    }
    //}
    //virtual protected void OnCollisionExit2D(Collision2D collision) {
    //    if (collision.collider.tag == Tag.EnemyMonster || collision.collider.tag == Tag.FriendlyPlayer || collision.collider.tag == Tag.EnemyPlayer) {
    //        //collision.collider.transform.GetComponent<ObjectController>().NormalizeMass();
    //        Contacts.Remove(collision.collider.transform.GetComponent<ObjectController>());
    //    }
    //}
    //protected void ProcessContacts() {
    //    if (Contacts.Count > 0) {
    //        int i = 0;
    //        do {
    //            if (!Contacts[i] || !Contacts[i].Alive)
    //                Contacts.RemoveAt(i);
    //            else
    //                i++;
    //        }
    //        while (i < Contacts.Count);
    //    }
    //}

    protected abstract void MoveFixedUpdate();

    //Instantiated Check
    protected void InstantiatedCheck() {
        CacheManager.Instantiateds++;
    }

    //Map Events
    protected void ApplyMapEvents() {//Child decided when to call it
        if (CacheManager.Map) {            
            CacheManager.Map.ApplyEvents(this);
        }
    }    

    //Transform
    public Transform Skills_T() {
        return SkillsTransform;
    }

    public Transform Effects_T() {
        return EffectsTransform;
    }

    //public 
    //public Transform VisualHolderTransform{
    //    get { return VisualHolder; }
    //}

    public MeleeAttackCollider Melee_AC{
        get { return MeleeAttackColliderTransform.GetComponent<MeleeAttackCollider>(); }
    }

    public void SwapMeleeAttackCollider(Transform MeleeAttackCollider) {
        Destroy(this.MeleeAttackColliderTransform.gameObject);
        this.MeleeAttackColliderTransform = null;
        MeleeAttackCollider.parent = transform;
        this.MeleeAttackColliderTransform = MeleeAttackCollider;
    }

    //Physics
    public Vector2 Force {
        get { return rb.velocity; }
        set { rb.velocity = value; }
    }    

    public float Drag {
        get { return rb.drag; }
        set { rb.drag = value; }
    }

    public bool HasForce {
        get { return rb.velocity.magnitude >= 0.1f; }        
    }

    //public void NormalizeMass() {
    //    rb.mass = 1;        
    //}

    public void ZerolizeForce() {
        if (rb.bodyType == RigidbodyType2D.Static)
            return;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = 0;
    }

    public void ZerolizeDrag() {
        Drag = 0;
    }

    public void NormalizeDrag() {
        Drag = 10;
    }

    public void NormalizePhysics() {
        ZerolizeForce();
        NormalizeDrag();
        //NormalizeMass();
    }

    public void AddForce(Vector2 Force,GreedyForceMode Mode) {                
        switch (Mode) {
            case GreedyForceMode.Addictive:
                this.Force += Force;
                break;   
            case GreedyForceMode.Override:
                this.Force = Force;
                break;
        }     
    }


    //Particle VFX
    public float GetVFXScale() {
        return VFXTransform.GetComponent<VFXScaler>().scale;
    }

    public void ClearVFX() {
        foreach (Transform VFX in VFXTransform)
            Destroy(VFX.gameObject);
    }
    public void ActiveOutsideVFXPartical(GameObject VFX) {        
        string cached_name = VFX.name;
        float scale = VFXTransform.GetComponent<VFXScaler>().scale;
        VFX = Instantiate(VFX, new Vector3(Position.x,Position.y, Layer.Effect_Z), transform.rotation) as GameObject;
        VFX.transform.GetComponent<ParticleSystem>().startSize *= scale;
        VFX.name = cached_name;
        Destroy(VFX, VFX.transform.GetComponent<ParticleSystem>().duration);
    }
    public void ActiveVFXParticalWithStayTime(GameObject VFX, float StayTime) {
        string cached_name = VFX.name;
        float scale = VFXTransform.GetComponent<VFXScaler>().scale;
        VFX = Instantiate(VFX, VFXTransform) as GameObject;
        VFX.transform.position = VFXTransform.position + VFX.transform.position * scale;
        VFX.transform.GetComponent<ParticleSystem>().startSize *= scale;
        VFX.name = cached_name;
        Destroy(VFX, StayTime);
    }
    public ParticleSystem ActiveVFXParticle(GameObject VFX) {
        string cached_name = VFX.name;
        float scale = VFXTransform.GetComponent<VFXScaler>().scale;
        VFX = Instantiate(VFX, VFXTransform) as GameObject;
        VFX.transform.position = VFXTransform.position + VFX.transform.position * scale;
        VFX.transform.GetComponent<ParticleSystem>().startSize *= scale;
        VFX.name = cached_name;
        return VFX.GetComponent<ParticleSystem>();
    }
    public void DeactiveVFXParticle(GameObject VFX) {
        Destroy(VFXTransform.Find(VFX.name).gameObject);
    }
    public void FadeVFXParticle(GameObject VFX) {
        ParticleSystem ps = VFXTransform.Find(VFX.name).gameObject.GetComponent<ParticleSystem>();
        ps.transform.parent = null;
        StartCoroutine(DelayFade(ps, 1f));
    }IEnumerator DelayFade(ParticleSystem ps,float delay) {
        yield return new WaitForSeconds(delay);
        ps.Simulate(ps.duration, true);
        ps.loop = false;
        ps.Play(true);
        ps.gameObject.GetComponent<LoopSFX>().Fade();
        if(ps.gameObject.GetComponent<LoopSFX>().Duration>ps.duration)
            Destroy(ps.gameObject, ps.gameObject.GetComponent<LoopSFX>().Duration);
        else
            Destroy(ps.gameObject, ps.duration);
    }

    public void ActiveOneShotVFXParticle(GameObject VFX) {
        string cached_name = VFX.name;
        float scale = VFXTransform.GetComponent<VFXScaler>().scale;
        VFX = Instantiate(VFX,VFXTransform) as GameObject;
        VFX.transform.position = VFXTransform.position + VFX.transform.position * scale;
        VFX.transform.GetComponent<ParticleSystem>().startSize *= scale;
        VFX.name = cached_name;
        float length = VFX.transform.GetComponent<ParticleSystem>().duration;
        Destroy(VFX, length);
    }        

    protected void ApplyOnGoingEffects() {
        if (Data.OnGoingEffects == null)
            return;


        foreach (var effect in Data.OnGoingEffects) {
            effect.Duration -= 1f;
            if(effect.Duration>0)
                CacheManager.ApplyEffect(effect);
        }
        Data.OnGoingEffects.Clear();
    }

    public virtual void HealHP(Heal heal_hp) {
        if (GetCurrStats(STATSTYPE.HEALTH) < GetMaxStats(STATSTYPE.HEALTH) && GetCurrStats(STATSTYPE.HEALTH) + heal_hp.Amount <= GetMaxStats(STATSTYPE.HEALTH)) {
            AddCurrStats(STATSTYPE.HEALTH, heal_hp.Amount);
            IC.PopUpText(heal_hp);
        } else if (GetCurrStats(STATSTYPE.HEALTH) < GetMaxStats(STATSTYPE.HEALTH) && GetCurrStats(STATSTYPE.HEALTH) + heal_hp.Amount > GetMaxStats(STATSTYPE.HEALTH)) {
            heal_hp.Amount = GetMaxStats(STATSTYPE.HEALTH) - GetCurrStats(STATSTYPE.HEALTH);
            AddCurrStats(STATSTYPE.HEALTH,heal_hp.Amount);
            IC.PopUpText(heal_hp);
        }
    }
    public void GainEssense(EssenseGain essense_gain) {//No need to override because it doesnt talk across the network
        if (GetCurrStats(STATSTYPE.ESSENCE) < GetMaxStats(STATSTYPE.ESSENCE) && GetCurrStats(STATSTYPE.ESSENCE) + essense_gain.Amount <= GetMaxStats(STATSTYPE.ESSENCE)) {
            AddCurrStats(STATSTYPE.ESSENCE, essense_gain.Amount);
        } else if (GetCurrStats(STATSTYPE.ESSENCE) < GetMaxStats(STATSTYPE.ESSENCE) && GetCurrStats(STATSTYPE.ESSENCE) + essense_gain.Amount > GetMaxStats(STATSTYPE.ESSENCE)) {
            essense_gain.Amount = GetMaxStats(STATSTYPE.ESSENCE) - GetCurrStats(STATSTYPE.ESSENCE);
            AddCurrStats(STATSTYPE.ESSENCE, essense_gain.Amount);
        }
    }

    abstract public void DeductHealth(Damage dmg);

    public virtual void Die(Damage dmg) {
        ObjectCollider.enabled = false;
        CurrHealth = 0;
        Alive = false;
        Casting = false;        
        VisualHolder.gameObject.SetActive(false);
        ClearEffects();       
        IC.DisableHealthBar();
        //Contacts.Clear();
        if(DieSFX!=null)
            AudioSource.PlayClipAtPoint(DieSFX, transform.position, GameManager.SFX_Volume);                
    }

    public void DeductEssense(EssenseLoss essense_cost) {//No need to override because it doesnt talk across the network     
        if (GetCurrStats(STATSTYPE.ESSENCE) - essense_cost.Amount >= 0)//Double check
            DecCurrStats(STATSTYPE.ESSENCE,essense_cost.Amount);
    }
    
    public bool CurrentlyHasEffect {
        get { return Effects.Count > 0; }
    }

    public void ClearEffects() {
        while (Effects.Count > 0) {
            Effects.First().Value.Expire();
        }
    }

    public bool HasEffect(CR Type) {
        return Effects.ContainsKey(Type);
    }

    public Effect GetEffect(CR Type) {        
        return Effects[Type];
    }    

    public void AddEffect(CR Type,Effect Effect) {//Override duplicated
        if (HasEffect(Type))
            GetEffect(Type).Expire();
        Effects[Type] = Effect;
    }

    public void RemoveEffect(CR Type) {
        Effects.Remove(Type);
    }


    //Animation
    public float GetMovementAnimSpeed() {
        return (CurrMoveSpeed / 100) / (movement_animation_interval);
    }
    public float GetAttackAnimSpeed() {
        return (CurrAttackSpeed / 100) / (attack_animation_interval);
    }

    //General Stats handling
    public float GetMaxStats(STATSTYPE type) {
        return MaxStats.Get(type);
    }

    public void SetMaxStats(STATSTYPE type, float value) {
        MaxStats.Set(type, value);
    }

    public void AddMaxStats(STATSTYPE type, float value) {
        MaxStats.Add(type, value);
    }

    public void DecMaxStats(STATSTYPE type, float value) {
        MaxStats.Dec(type, value);
    }

    public float GetCurrStats(STATSTYPE type) {
        return CurrStats.Get(type);
    }

    public void SetCurrStats(STATSTYPE type, float value) {
        CurrStats.Set(type, value);
    }

    public void AddCurrStats(STATSTYPE type, float value) {
        CurrStats.Add(type, value);
    }

    public void DecCurrStats(STATSTYPE type, float value) {
        CurrStats.Dec(type, value);
    }

    //Specific Stats Handling
    public float CurrHealth {
        get { return CurrStats.Get(STATSTYPE.HEALTH); }
        set { CurrStats.Set(STATSTYPE.HEALTH, value); }            
    }
    public float CurrEssense {
        get { return CurrStats.Get(STATSTYPE.ESSENCE); }
        set { CurrStats.Set(STATSTYPE.ESSENCE, value); }
    }
    public float CurrDamage {
        get { return CurrStats.Get(STATSTYPE.DAMAGE); }
        set { CurrStats.Set(STATSTYPE.DAMAGE, value); }
    }
    public float CurrAttackSpeed {
        get { return CurrStats.Get(STATSTYPE.ATTACK_SPEED); }
        set { CurrStats.Set(STATSTYPE.ATTACK_SPEED, value); }
    }
    public float CurrMoveSpeed {
        get { return CurrStats.Get(STATSTYPE.MOVE_SPEED); }
        set { CurrStats.Set(STATSTYPE.MOVE_SPEED, value); }
    }
    public float CurrDefense {
        get { return CurrStats.Get(STATSTYPE.DEFENSE); }
        set { CurrStats.Set(STATSTYPE.DEFENSE, value); }
    }
    public float CurrPenetration {
        get { return CurrStats.Get(STATSTYPE.PENETRATION); }
        set { CurrStats.Set(STATSTYPE.PENETRATION, value); }
    }
    public float CurrCritChance {
        get { return CurrStats.Get(STATSTYPE.CRIT_CHANCE); }
        set { CurrStats.Set(STATSTYPE.CRIT_CHANCE, value); }
    }
    public float CurrCritDmg {
        get { return CurrStats.Get(STATSTYPE.CRIT_DMG); }
        set { CurrStats.Set(STATSTYPE.CRIT_DMG, value); }
    }
    public float CurrLPH {
        get { return CurrStats.Get(STATSTYPE.LPH); }
        set { CurrStats.Set(STATSTYPE.LPH, value); }
    }
    public float CurrHaste {
        get { return CurrStats.Get(STATSTYPE.HASTE); }
        set { CurrStats.Set(STATSTYPE.HASTE, value); }
    }
    public float CurrHealthRegen {
        get { return CurrStats.Get(STATSTYPE.HEALTH_REGEN); }
        set { CurrStats.Set(STATSTYPE.HEALTH_REGEN, value); }
    }
    public float CurrEssenseRegen {
        get { return CurrStats.Get(STATSTYPE.ESSENCE_REGEN); }
        set { CurrStats.Set(STATSTYPE.ESSENCE_REGEN, value); }
    }

    public float MaxHealth {
        get { return MaxStats.Get(STATSTYPE.HEALTH); }
        set { MaxStats.Set(STATSTYPE.HEALTH, value); }
    }
    public float MaxEssense {
        get { return MaxStats.Get(STATSTYPE.ESSENCE); }
        set { MaxStats.Set(STATSTYPE.ESSENCE, value); }
    }
    public float MaxDamage {
        get { return MaxStats.Get(STATSTYPE.DAMAGE); }
        set { MaxStats.Set(STATSTYPE.DAMAGE, value); }
    }
    public float MaxAttackSpeed {
        get { return MaxStats.Get(STATSTYPE.ATTACK_SPEED); }
        set { MaxStats.Set(STATSTYPE.ATTACK_SPEED, value); }
    }
    public float MaxMoveSpeed {
        get { return MaxStats.Get(STATSTYPE.MOVE_SPEED); }
        set { MaxStats.Set(STATSTYPE.MOVE_SPEED, value); }
    }
    public float MaxDefense {
        get { return MaxStats.Get(STATSTYPE.DEFENSE); }
        set { MaxStats.Set(STATSTYPE.DEFENSE, value); }
    }
    public float MaxPenetration {
        get { return MaxStats.Get(STATSTYPE.PENETRATION); }
        set { MaxStats.Set(STATSTYPE.PENETRATION, value); }
    }
    public float MaxCritChance {
        get { return MaxStats.Get(STATSTYPE.CRIT_CHANCE); }
        set { MaxStats.Set(STATSTYPE.CRIT_CHANCE, value); }
    }
    public float MaxCritDmg {
        get { return MaxStats.Get(STATSTYPE.CRIT_DMG); }
        set { MaxStats.Set(STATSTYPE.CRIT_DMG, value); }
    }
    public float MaxLPH {
        get { return MaxStats.Get(STATSTYPE.LPH); }
        set { MaxStats.Set(STATSTYPE.LPH, value); }
    }
    public float MaxHaste {
        get { return MaxStats.Get(STATSTYPE.HASTE); }
        set { MaxStats.Set(STATSTYPE.HASTE, value); }
    }
    public float MaxHealthRegen {
        get { return MaxStats.Get(STATSTYPE.HEALTH_REGEN); }
        set { MaxStats.Set(STATSTYPE.HEALTH_REGEN, value); }
    }
    public float MaxEssenseRegen {
        get { return MaxStats.Get(STATSTYPE.ESSENCE_REGEN); }
        set { MaxStats.Set(STATSTYPE.ESSENCE_REGEN, value); }
    }

    public abstract string GetName();

    public Vector2 Position {
        get { return rb.position; }
        set { rb.position = value; }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GreedyNameSpace;

using Networking.Data;

public abstract class Player : ObjectController {
    [HideInInspector]
    public bool Infusing = false;
    [HideInInspector]
    public Portal InfusedPortal = null;
    [HideInInspector]
    public int NextLevelExp = -999;

    public AudioClip LevelUpSFX;

    public ReviveInteraction RI;

    protected List<GameObject> EquipPrefabs;

    protected GameObject BaseModel;    

    List<Bounus> Bounuses = new List<Bounus>();

    protected override void Awake() {
        base.Awake();
        MaxStats = new Stats();        
    }

    protected override void Start() {
        base.Start();
        InitPlayer();
        StartCoroutine(ActiveTargetForAI());
    }private IEnumerator ActiveTargetForAI() {
        yield return new WaitForSeconds(1f);
        AITargetable = true;
    }

    // Update is called once per frame
    protected override void Update() {
        base.Update();
        ControlUpdate();        
        EquiPrefabsUpdate();
        BaseModelUpdate();
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();
    }

    protected abstract void ControlUpdate();

    public static int AvgLvl {
        get {            
            int totoal = 0;
            Player[] Players = FindObjectsOfType<Player>();
            foreach (var p in Players)
                totoal += p.lvl;
            return totoal / Players.Length;
        }
    }
    public static int MinLvl {
        get {
            Player[] Players = FindObjectsOfType<Player>();
            int[] Lvls = new int[Players.Length];
            for(int i = 0; i < Lvls.Length; i++) {
                Lvls[i] = Players[i].lvl;
            }
            return Mathf.Min(Lvls);
        }
    }

    //----------public
    public WeaponController GetWC() {
        if (((PlayerData)Data).Equipments[(int)EQUIPTYPE.Weapon].isNull) {
            return null;
        }
        return EquipPrefabs[(int)EQUIPTYPE.Weapon].GetComponent<WeaponController>();
    }

    public override void Die(Damage dmg) {
        base.Die(dmg);
        ActiveOutsideVFXPartical(DieVFX);
        RI.Enable();        
}

    //-------private

    protected void BaseModelUpdate() {
        if (!Alive)
            return;
        Animator BaseModelAnim = BaseModel.GetComponent<Animator>();
        BaseModelAnim.SetInteger("Direction", Direction);
        BaseModelAnim.speed = GetMovementAnimSpeed();
    }

    protected void EquiPrefabsUpdate() {
        if (!Alive)
            return;
        foreach (var e_prefab in EquipPrefabs) {
            if (e_prefab != null)
                e_prefab.GetComponent<EquipmentController>().EquipUpdate(this);
        }
    }

    protected void SetUpForDead() {
        ObjectCollider.enabled = false;
        VisualHolder.gameObject.SetActive(false);
        ClearEffects();
        IC.DisableHealthBar();
        if (this is OtherPlayer && OID == OID.FriendlyPlayer)
            ((OtherPlayer)this).RI.Enable();
            
    }

    protected void InstantiateVisualModels() {
        BaseModel = Instantiate(Resources.Load("BaseModelPrefabs/BaseModel"), OZC.transform) as GameObject;
        BaseModel.name = "BaseModel";        
        BaseModel.GetComponent<SpriteRenderer>().color = new Color(((PlayerData)Data).SkinColor.R, ((PlayerData)Data).SkinColor.G, ((PlayerData)Data).SkinColor.B);
        //BaseModel.GetComponent<SpriteRenderer>().sortingLayerName = SortingLayer.Object;
        BaseModel.transform.localPosition = Vector3.zero;
        EquipPrefabs = new List<GameObject>();
        foreach (var e in ((PlayerData)Data).Equipments) {
            if (e.isNull) {
                EquipPrefabs.Add(null);
            } else {
                GameObject equipPrefab = EquipmentController.ObtainPrefab(e, this);
                EquipPrefabs.Add(equipPrefab);
            }
        }
    }





    public CLASS Class {
        get { return ((PlayerData)Data).Class; }
    }
    //public int lvl {        
    //    get { return ((PlayerData)Data).lvl; }
    //    set { ((PlayerData)Data).lvl = value; }
    //}
    public int exp{
        get { return ((PlayerData)Data).exp; }
        set { ((PlayerData)Data).exp = value; }
    }

    public override string GetName() {
        return ((PlayerData)Data).Name;
    }

    //PlayerUnique methods

    //Skills Handling
    public int GetAvailableSkillPoints() {
        return ((PlayerData)Data).SkillPoints;
    }

    public ActiveSkill GetActiveSlotSkill(int Slot) {
        if (((PlayerData)Data).ActiveSlotData[Slot] == CR.None) {           
            return null;
        }
        return (ActiveSkill)Skills[((PlayerData)Data).ActiveSlotData[Slot]];
    }

    public Skill GetRawSkill(int SkillIndex) {
        return CachedTreeSkills[SkillIndex];
    }

    public Skill GetActualSkill(int SkillIndex) {
        return Skills[GetRawSkill(SkillIndex).Type];
    }

    public int GetSkilllvlByIndex(int SkillIndex) {
        return ((PlayerData)Data).SkillTreelvls[SkillIndex];
    }

    public virtual void LvlUpSkill(int SkillIndex) {
        ((PlayerData)Data).SkillTreelvls[SkillIndex]++;
        ((PlayerData)Data).SkillPoints--;
        UpdateSkillTreeState(SkillIndex);
        UpdateStats();
    }

    public virtual void Respec() {
        ((PlayerData)Data).ResetActionSlotData();
        ((PlayerData)Data).ResetSkillTreeData();        
        ((PlayerData)Data).SkillPoints = lvl;
        for (int i = 0; i < ((PlayerData)Data).SkillTreelvls.Count; i++)
            UpdateSkillTreeState(i);
        UpdateStats();                
    }

    //Equipment/Inventory Handling
    public bool InventoryIsFull() {
        return FirstAvailbleInventorySlot == ((PlayerData)Data).Inventory.Count;
    }

    public Equipment GetEquippedItem(EQUIPTYPE Slot) {
        return ((PlayerData)Data).Equipments[(int)Slot];
    }
    public Equipment GetInventoryItem(int Slot) {
        return ((PlayerData)Data).Inventory[Slot];
    }    

    public bool Compatible(Equipment E) {
        if (E == null)
            return false;
        if (E.Class == CLASS.All)//Trinket
            return ((PlayerData)Data).lvl >= E.LvlReq;
        return (((PlayerData)Data).lvl >= E.LvlReq && ((PlayerData)Data).Class == E.Class);
    }

    public int FirstAvailbleInventorySlot {
        get {
            for (int i = 0; i < ((PlayerData)Data).Inventory.Count; i++) {
                if (((PlayerData)Data).Inventory[i].isNull)
                    return i;
            }
            return ((PlayerData)Data).Inventory.Count;
        }
    }    

    public virtual void Equip(Equipment E) {
        ((PlayerData)Data).Equipments[(int)E.EquipType] = E;
        GameObject equipPrefab = EquipmentController.ObtainPrefab(E, this);
        EquipPrefabs[(int)E.EquipType] = equipPrefab;
        UpdateStats();        
    }

    public virtual void UnEquip(EQUIPTYPE Slot) {
        Destroy(EquipPrefabs[(int)Slot]);
        ((PlayerData)Data).Equipments[(int)Slot] = new Equipment();
        UpdateStats();        
    }

    public virtual void AddToInventory(int Slot, Equipment E) {
        ((PlayerData)Data).Inventory[Slot] = E;        
    }

    public virtual void RemoveFromInventory(int Slot) {
        ((PlayerData)Data).Inventory[Slot] = new Equipment();                
    }

    public void LevelUp() {
        ((PlayerData)Data).lvl++;
        ((PlayerData)Data).exp = 0;
        ((PlayerData)Data).SkillPoints++;
        ((PlayerData)Data).BaseStats.Grow(Patch.GetGrowth(Class));
        MaxStats.Grow(Patch.GetGrowth(Class));
        CurrStats.Grow(Patch.GetGrowth(Class));        
        //UpdateStats();
        //CurrStats.Set(STATSTYPE.HEALTH, MaxStats.Get(STATSTYPE.HEALTH));
        //CurrStats.Set(STATSTYPE.ESSENSE, MaxStats.Get(STATSTYPE.ESSENSE));
        NextLevelExp = Patch.GetRequiredExp(((PlayerData)Data).lvl + 1);
        AudioSource.PlayClipAtPoint(LevelUpSFX, transform.position, GameManager.SFX_Volume);        
    }

    //EXP handling
    void CheckLevelUp() {
        if (((PlayerData)Data).lvl >= Patch.LvlCap)
            return;
        if (((PlayerData)Data).exp >= NextLevelExp) {
            ((PlayerData)Data).lvl++;
            ((PlayerData)Data).exp = 0;
            CurrStats.Set(STATSTYPE.HEALTH, MaxStats.Get(STATSTYPE.HEALTH));
            CurrStats.Set(STATSTYPE.ESSENCE, MaxStats.Get(STATSTYPE.ESSENCE));
            NextLevelExp = Patch.GetRequiredExp(((PlayerData)Data).lvl + 1);
            AudioSource.PlayClipAtPoint(LevelUpSFX, transform.position, GameManager.SFX_Volume);
            ((PlayerData)Data).SkillPoints++;
        }
    }

    protected void InitPlayer() {
        //if (Alive || Reset)//For now
        //    InstantiateVisualModels();
        InstantiateVisualModels();
        if(!Alive && !Reset)
            SetUpForDead();
        if (((PlayerData)Data).lvl < Patch.LvlCap)
            NextLevelExp = Patch.GetRequiredExp(((PlayerData)Data).lvl + 1);
        InitMaxStats();
        ApplyEquipmentsUtilities();
        InitSkillTree();
        InitOnCallEvent();
        ApplyBounuses();
        InitPassives();
        if (!Alive && Reset)
            ReviveUponReset();
        else
            InitCurrStats();
        ApplyMapEvents();
        ApplyOnGoingEffects();
        if (Alive)
            LetsRoll();
        InstantiatedCheck();
    }

    protected void InitPassives() {
        foreach (Skill passive in Skills.Values) {
            if(passive is PassiveSkill)
                ((PassiveSkill)passive).ApplyPassive();
        }
    }

    protected void InitSkillTree() {
        GameObject SkillTreeOJ = null;
        if (((PlayerData)Data).Class == CLASS.Warrior) {
            SkillTreeOJ = Resources.Load("SkillTreePrefabs/WarriorSkillTree") as GameObject;
        } else if (((PlayerData)Data).Class == CLASS.Mage) {
            SkillTreeOJ = Resources.Load("SkillTreePrefabs/MageSkillTree") as GameObject;
        } else if (((PlayerData)Data).Class == CLASS.Rogue) {
            SkillTreeOJ = Resources.Load("SkillTreePrefabs/RogueSkillTree") as GameObject;
        }        
        CachedTreeSkills = SkillTreeOJ.GetComponent<SkillTreeController>().SkillTree;
        for (int i = 0; i < Patch.SkillTreeSize; i++) {            
            CachedTreeSkills[i].GenerateDescription();
            if (CachedTreeSkills[i] != null && ((PlayerData)Data).SkillTreelvls[i] != 0) {//Does lvl+skill check here
                Skill s = CachedTreeSkills[i].Instantiate();
                s.InitSkill(this, ((PlayerData)Data).SkillTreelvls[i]);
                Skills[s.Type] = s;
            }
        }
    }

    protected void InitMaxStats() {        
        for (int i = 0; i < Patch.NumOfStatsFields; i++) {
            MaxStats.Set(i, ((PlayerData)Data).BaseStats.Get(i));
        }
    }

    protected void ResetBounuses() {
        if (Bounuses.Count > 0) {
            foreach (Bounus b in Bounuses)
                b.RemoveBounus();
        }
        Bounuses = new List<Bounus>();
    }

    protected void ApplyEquipmentsUtilities() {
        Dictionary<EQUIPSET, int> Sets = new Dictionary<EQUIPSET, int>();
        foreach (var e in ((PlayerData)Data).Equipments) {
            if (e != null) {
                for (int i = 0; i < Patch.NumOfStatsFields; i++) {
                    MaxStats.Add(i, e.Stats.Get(i));
                }
                if (e.Set != EQUIPSET.None) {
                    if (!Sets.ContainsKey(e.Set))
                        Sets.Add(e.Set, 1);
                    else
                        Sets[e.Set]++;
                }
            }
        }
        foreach (var set in Sets) {
            Set s = ((GameObject)Resources.Load("SetPrefabs/" + set.Key.ToString())).GetComponent<Set>();
            foreach (Bounus b in s.Bounuses) {
                if (set.Value >= b.condiction) {
                    Bounuses.Add(new Bounus(b));
                }
            }
        }
    }

    protected void ApplyBounuses() {
        foreach (Bounus b in Bounuses)
            b.ApplyBounus(this);
    }

    protected void ReviveUponReset() {//Passed the arena check because arena's death never synch with world state
        //Revive Animation goes into here        
        CurrStats = CombatGenerator.GenerateCurrStatsUponReviveReset(MaxStats);
        Alive = true;        
        if (this is MainPlayer) {
            Client.Send(Protocols.UpdateToAliveState, new UpdateToAliveStateData(NetworkID, CurrStats, SC), Client.TCP);
        }
    }

    public void ReviveUponHelp() {//Most likely been happened during multiplayer combat
        if (Alive)//Networking, it could always happen
            return;        

        if (OID == OID.EnemyPlayer)
            CacheManager.Map.OpponentsAlives++;
        else
            CacheManager.Map.OursAlives++;

        CurrStats = CombatGenerator.GenerateCurrStatsUponReviveHelp(MaxStats);

        Alive = true;        
        ObjectCollider.enabled = true;        
        VisualHolder.gameObject.SetActive(true);

        if (this is MainPlayer) {
            if(!SceneChecker.TargetInArenas(Scene.Current_SID))
                Client.Send(Protocols.UpdateToAliveState, new UpdateToAliveStateData(NetworkID, CurrStats, SC), Client.TCP);                        
            ControllerManager.SyncActions = true;
            //StartCoroutine(ReviveLateSync());
        }
        else if (this is OtherPlayer) {
            IC.EnableHealthBar();            
        }
        RI.Disable();
        IC.DisableReviveBar();
        IC.SentRevive = false;
    }private IEnumerator ReviveLateSync() {//No State Ultering, for edged joined client ***Might not be needed now b/c of late actions support, require testing
        yield return new WaitForSeconds(1f);
        Client.Send(Protocols.ReviveUponHelp, Client.ClientID, Client.TCP);
    }



    protected void InitCurrStats() {
        if (CurrStats.IsNull) {
            CurrStats = new Stats(MaxStats);
            Client.Send(Protocols.UpdateCurrStats, new UpdateCurrStatsData(NetworkID, CurrStats), Client.TCP);
        }
        else {
            bool UlteredInArenas = false;
            for (int i = 0; i < Patch.NumOfStatsFields; i++) {//Extreme case check for unenquipping items in arenas
                if (CurrStats.Get(i) > MaxStats.Get(i)) {                    
                    UlteredInArenas = true;
                    CurrStats.Set(i, MaxStats.Get(i));
                }
                else if (CurrStats.Get(i) < MaxStats.Get(i) && (STATSTYPE)i!=STATSTYPE.HEALTH) {                    
                    UlteredInArenas = true;
                    CurrStats.Set(i, MaxStats.Get(i));
                }
            }
            if (UlteredInArenas && OID == OID.Main && Client.Connected)
                Client.Send(Protocols.UpdateCurrStats, new UpdateCurrStatsData(NetworkID,CurrStats), Client.TCP);
        }
    }

    //-------helper
    protected virtual void UpdateStats() {
        ResetBounuses();
        InitMaxStats();
        ApplyEquipmentsUtilities();
        ReloadWeaponWC();
        InitOnCallEvent();
        ApplyBounuses();
        InitPassives();
        for (int i = 0; i < Patch.NumOfStatsFields; i++) {
            if ((int)STATSTYPE.HEALTH == i) {
                if (CurrStats.Get(i) > MaxStats.Get(i))
                    CurrStats.Set(i, MaxStats.Get(i));
            } else if ((int)STATSTYPE.ESSENCE == i) {
                if (CurrStats.Get(i) > MaxStats.Get(i))
                    CurrStats.Set(i, MaxStats.Get(i));
            } else {
                CurrStats.Set(i, MaxStats.Get(i));
            }
        }
        ApplyMapEvents();
    }

    protected void UpdateSkillTreeState(int SkillIndex) {
        Skill RawSkillToCheck = GetRawSkill(SkillIndex);
        if (Skills.ContainsKey(RawSkillToCheck.Type) && ((PlayerData)Data).SkillTreelvls[SkillIndex] != 0) {//LvlUp
            Skills[RawSkillToCheck.Type].InitSkill(this, ((PlayerData)Data).SkillTreelvls[SkillIndex]);
        }else if(Skills.ContainsKey(RawSkillToCheck.Type) && ((PlayerData)Data).SkillTreelvls[SkillIndex] == 0) {//Forget NOTE: Shouldnt be here, Reset() should handle this kind of situaion but not yet implemented
            Skills[RawSkillToCheck.Type].Delete();
            Skills.Remove(RawSkillToCheck.Type);
        }else if(!Skills.ContainsKey(RawSkillToCheck.Type) && ((PlayerData)Data).SkillTreelvls[SkillIndex] != 0) {//Learn
            RawSkillToCheck = RawSkillToCheck.Instantiate();
            RawSkillToCheck.InitSkill(this, ((PlayerData)Data).SkillTreelvls[SkillIndex]);
            Skills[RawSkillToCheck.Type] = RawSkillToCheck;
        }
    }

    private ProjectileData Default_PD_Caller(ProjectileData pd) {
        return pd;
    }
    protected void InitOnCallEvent() {
        ON_GENERATE_PD = Default_PD_Caller;
        ON_DMG_DEAL = null;
        ON_HEALTH_LOSS = null;
        ON_HEALTH_GAIN = null;
        ON_ESSENSE_LOSS = null;
        ON_ESSENSE_GAIN = null;
        ON_DEATH_UPDATE = null;
    }    

    protected void ReloadWeaponWC() {
        if (!((PlayerData)Data).Equipments[(int)EQUIPTYPE.Weapon].isNull) {
            EquipPrefabs[(int)EQUIPTYPE.Weapon].GetComponent<WeaponController>().EssenseCost = ((GameObject)Resources.Load("EquipmentPrefabs/" + ((PlayerData)Data).Equipments[(int)EQUIPTYPE.Weapon].Name)).GetComponent<WeaponController>().EssenseCost;
        }
    }
}

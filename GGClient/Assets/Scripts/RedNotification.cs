﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RedNotification : MonoBehaviour {
    static Text Message;
    
    static Animator Anim;
    static AudioSource AS;
    //public AudioClip NO_MANA;
    //public AudioClip ON_CD;
    //public AudioClip NO_SKILL_POINT;
    //public AudioClip NO_STAT_POINT;
    //public AudioClip MAX_SKILL_LVL;


    public enum Type {
        NO_ESSENCE,
        ON_CD,
        NO_SKILL_POINT,
        MAX_SKILL_LVL,
        STUNNED,
        SKILL_REQUIREMENT_NOT_MET,
        SKILL_NOT_LEARNED,
        INVENTORY_FULL,
        CANT_EQUIP,
        CRAZY_DIGGER,
        DUPLICATED_DIGGER,
        OVER_INFUSION,
        RITUAL_INTERRUPTION,
        CANT_DO_NOW,
        STASH_FULL,
        NOT_ENOUGH_SOULS,
        EDIT_DURING_EFFECTS,
        SUMMON_SPAM,
        NO_QUEUE,
        DUP_QUEUE
    };
    void Awake() {
        Anim = GetComponent<Animator>();
        AS = GetComponent<AudioSource>();
        Message = transform.Find("Message").GetComponent<Text>();
    }

    public static void Push(Type type) {
        string message = "";
        switch (type) {
            case Type.NO_ESSENCE:
                message = "Not enough Essence.";
                break;
            case Type.ON_CD:
                message = "Skill is not ready.";
                break;
            case Type.NO_SKILL_POINT:
                message = "No skill points.";
                break;
            case Type.MAX_SKILL_LVL:
                message = "This skill has been mastered.";
                break;
            case Type.STUNNED:
                message = "Can't do that while stunned.";
                break;
            case Type.SKILL_REQUIREMENT_NOT_MET:
                message = "You do not meet the requirement.";
                break;
            case Type.SKILL_NOT_LEARNED:
                message = "You have not learned this skill yet.";
                break;
            case Type.INVENTORY_FULL:
                message = "Your inventory is full.";
                break;
            case Type.CANT_EQUIP:
                message = "You can't equip this item.";
                break;
            case Type.DUPLICATED_DIGGER:
                message = "You can't dig grave while someone is around.";
                break;
            case Type.CRAZY_DIGGER:
                message = "You can't dig now, please wait a moment.";
                break;
            case Type.OVER_INFUSION:
                message = "Over infusion would cause portal disorder.";
                break;
            case Type.RITUAL_INTERRUPTION:
                message = "Ritual got interrupted.";
                break;
            case Type.CANT_DO_NOW:
                message = "You can't do that right now.";
                break;
            case Type.STASH_FULL:
                message = "This stash is full.";
                break;
            case Type.NOT_ENOUGH_SOULS:
                message = "You don't have enough of souls.";
                break;
            case Type.EDIT_DURING_EFFECTS:
                message = "You can't do that while you are having active effects.";
                break;
            case Type.SUMMON_SPAM:
                message = "You have just summoned this target recently, please try again later.";
                break;
            case Type.NO_QUEUE:
                message = "You are not in queue.";
                break;
            case Type.DUP_QUEUE:
                message = "You are already in queue of this mode.";
                break;
        }        
        if (Anim.GetCurrentAnimatorStateInfo(0).IsName("display")) {
            if (Anim.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.3)
                return;
        }
        Message.text = message;
        Anim.Play("display",0,0);
        AS.volume = GameManager.SFX_Volume;
        AS.Play();
    }


	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GreedyNameSpace;
public class StatsInfo : MonoBehaviour {
    public Animator Anim;
    public Text Content;
	//// Use this for initialization
	//void Start () {
		
	//}
	
	//// Update is called once per frame
	//void Update () {
		
	//}        
            

    public void Show(int Slot) {
        Content.text = StatsType.GetStatsTypeDescription(Slot);
        gameObject.SetActive(true);
        Anim.Play("slide_right");
    }

    public void Hide() {
        gameObject.SetActive(false);
    }
         
}

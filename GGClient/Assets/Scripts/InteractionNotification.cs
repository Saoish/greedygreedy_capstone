﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InteractionNotification : MonoBehaviour {
    static Text message;
    static Image button_image;
    //public static bool Loaded = false;
    
    void Awake() {
        message = GetComponentInChildren<Text>(true);
        button_image = transform.Find("ButtonImage").GetComponent<Image>();                
    }


    public static void SetIndication(string Message, Sprite button_image_sprite) {        
        message.text = Message;
        button_image.sprite = button_image_sprite;
    }

    public static void TurnOn() {
        message.gameObject.SetActive(true);
        if (button_image == null) {
            Debug.Log("here");
            button_image.gameObject.SetActive(false);
        }
        else
            button_image.gameObject.SetActive(true);
    }

    public static void TurnOff() {
        message.gameObject.SetActive(false);
        button_image.gameObject.SetActive(false);
    }

}

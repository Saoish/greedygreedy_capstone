﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;
public static class HandyNetwork {

    public static void SendForce(ObjectController target,Vector2 Force, GreedyForceMode FM) {
        Client.Send(Protocols.ObjectAddForce, new AddForceData(target.NetworkID,target.Position, Force, FM), Client.TCP);
    }

    public static void SendLaunch(ProjectileData PD) {
        Client.Send(Protocols.Launch, PD, Client.UDP);
    }

    public static void SendSkillActivation(ObjectController Caster,CR Type, float[] Parameters) {
        Client.Send(Protocols.ObjectActiveSkill, new SkillActivationData(Caster.NetworkID, Type, Parameters), Client.TCP);
    }
}

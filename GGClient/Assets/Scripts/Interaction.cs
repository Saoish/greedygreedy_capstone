﻿using UnityEngine;
using System.Collections;

public abstract class Interaction : MonoBehaviour {
    [HideInInspector]
    public int Priority = 0;

    protected virtual void Awake() {
        gameObject.layer = CollisionLayer.Interaction;
    }

    protected virtual void Start() {

    }

    public virtual bool AskedForCancel() {
        return ControllerManager.Actions.Cancel.WasPressed;
    }        
    public virtual void Cancel() {//Not all interaction has this kind
    }

    public abstract bool IsTriggered();    
    public abstract void Interact();    
    public virtual void Disengage() {
        CacheManager.MP.MPI.InteractTarget = null;
    }

    public abstract void SetIndication();

    public static void TurnOffAllInteractionWindow() {
        foreach (InteractionContent IC in FindObjectsOfType<InteractionContent>()) {
            IC.TurnOff();
        }
    }    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GreedyNameSpace;
public class QueuingInfo : MonoBehaviour {
    static Animator Anim;
    static Text Message;
    static GameObject Self;
    private void Awake() {
        Self = gameObject;
        Message = GetComponent<Text>();
        Anim = GetComponent<Animator>();
        Self.SetActive(false);
    }

    public static void Push(Queuemode QM) {
        string message = "";
        switch (QM){
            case Queuemode.None:
                Self.SetActive(false);
                return;
            case Queuemode.SoloQueue_1v1:
                message = "Searching for Arena Soloqueue 1v1...";
                break;
            case Queuemode.SoloQueue_2v2:
                message = "Searching for Arena Soloqueue 2v2...";
                break;            
        }        
        Message.text = message;
        Self.SetActive(true);
        Anim.Play("info_pulse",0,0);
    }
}

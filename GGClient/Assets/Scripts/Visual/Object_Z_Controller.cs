﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visual;

namespace Visual {
    public class Object_Z_Controller : Z_Controller {
        private ObjectController OC;
        public static void Assign_Z(Transform EquipTransform, Layer.EquipmentLayerRenderMode ELRM) {
            EquipTransform.localPosition = new Vector3(EquipTransform.localPosition.x, EquipTransform.localPosition.y, Layer.Z_Mapper[ELRM].Front);
        }
        protected override void Awake() {
            base.Awake();
            OC = transform.root.GetComponent<ObjectController>();
        }                    

        protected override void Update() {
            transform.localPosition = new Vector3(0, 0, (OC.Position.y +Offset)/ Layer.Scale);
        }
    }
}

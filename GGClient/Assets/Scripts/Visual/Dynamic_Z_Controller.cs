﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visual {
    public class Dynamic_Z_Controller : Z_Controller {
        public static void Attach(GameObject OJ) {
            OJ.AddComponent<Dynamic_Z_Controller>();
        }
        protected override void Start() {
            base.Start();
            transform.localPosition = new Vector3(transform.position.x, transform.position.y, (transform.position.y + Offset) / Layer.Scale);
        }

        protected override void Update() {
            base.Update();
            transform.localPosition = new Vector3(transform.position.x, transform.position.y, (transform.position.y + Offset) / Layer.Scale);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visual;

namespace Visual {
    public class Static_Z_Controller : Z_Controller {
        protected override void Start() {
            base.Start();
            transform.localPosition = new Vector3(transform.position.x, transform.position.y, (transform.position.y + Offset) / Layer.Scale);
        }

        protected override void Update() {
            base.Update();
            transform.localPosition = new Vector3(transform.position.x, transform.position.y, (transform.position.y + Offset) / Layer.Scale);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visual;

namespace Visual {
    public abstract class Z_Controller : MonoBehaviour {
        public float Offset = 0.0f;
        protected virtual void Start() {

        }
        protected virtual void Awake() {
                        
        }
        protected virtual void Update() {

        }
        protected virtual void FixedUpdate() {

        }
        protected virtual void LateUpdate() {

        }
    }
}
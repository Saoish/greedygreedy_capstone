﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visual {
    public class SemiDynamic_Z_Controller : Z_Controller {
        static SemiDynamic_Z_Controller instance;
        public static void Attach(GameObject OJ,float DisableDelay = 2f) {
            OJ.AddComponent<SemiDynamic_Z_Controller>();
            DestroyObject(OJ.GetComponent<SemiDynamic_Z_Controller>(), DisableDelay);
        }
        
        protected override void Start() {
            base.Start();
            transform.localPosition = new Vector3(transform.position.x, transform.position.y, (transform.position.y + Offset) / Layer.Scale);
        }

        protected override void Update() {
            base.Update();
            transform.localPosition = new Vector3(transform.position.x, transform.position.y, (transform.position.y + Offset) / Layer.Scale);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visual{
    public class SpiritualFollower_Z_Controller : Z_Controller {
        private ObjectController Master;
        private float Z_OffSet;
        public static void Assign(ObjectController Master,GameObject Target, float Z_OffSet) {
            SpiritualFollower_Z_Controller SZC = Target.AddComponent<SpiritualFollower_Z_Controller>();
            SZC.Master = Master;
            SZC.Z_OffSet = Z_OffSet;
        }
        protected override void Update() {
            base.Update();
            if(Master)
                transform.position = new Vector3(transform.position.x, transform.position.y, (Master.Position.y + Offset) / Layer.Scale+ Z_OffSet);
        }
    }
}

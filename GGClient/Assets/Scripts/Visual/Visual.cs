﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visual {
    public static class Layer {
        public static float Scale = 100.0f;
        public static float Effect_Z = -0.06f / Scale;
        public enum EquipmentLayerRenderMode {//Override     
            None,
            Trinket,
            MeleeWeapon,
            MagicOrb,
            Helmet,
            Chest,
            Shackle
        }
        public class Facing {
            public float Front;
            public float Back;
            public Facing(float Front, float Back) {
                this.Front = Front;
                this.Back = Back;
            }
        }
        public static Dictionary<EquipmentLayerRenderMode, Facing> Z_Mapper = new Dictionary<EquipmentLayerRenderMode, Facing> {
                { EquipmentLayerRenderMode.Trinket,new Facing(-0.05f/Scale,-0.05f/Scale ) },
                { EquipmentLayerRenderMode.MeleeWeapon,new Facing(-0.04f/Scale ,0.01f/Scale  ) },
                { EquipmentLayerRenderMode.MagicOrb,new Facing(-0.05f/Scale ,-0.05f/Scale  ) },
                { EquipmentLayerRenderMode.Helmet,new Facing(-0.03f/Scale ,-0.03f/Scale  ) },
                { EquipmentLayerRenderMode.Chest,new Facing(-0.02f/Scale ,-0.02f/Scale  ) },
                { EquipmentLayerRenderMode.Shackle,new Facing(-0.01f/Scale ,-0.01f/Scale  ) }
        };
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopSFX : MonoBehaviour {
    public AudioClip SFX;
    // Use this for initialization
    AudioSource AS;

    private void Awake() {
        AS = gameObject.AddComponent<AudioSource>();
        AS.volume = GameManager.SFX_Volume;
        AS.loop = true;
        AS.clip = SFX;
        AS.Play();
    }

    //void Start () {
    //       AS = gameObject.AddComponent<AudioSource>();
    //       AS.volume = GameManager.SFX_Volume;
    //       AS.loop = true;
    //       AS.clip = SFX;
    //       AS.Play();
    //       Debug.Log("Start");
    //}

    public void Fade() {                
        StartCoroutine(Fading());
    }

    private IEnumerator Fading() {
        AS.loop = false;        
        AS.PlayScheduled(Duration);        
        while (Duration> 0 ) {
            AS.volume = (Duration / AS.clip.length) * GameManager.SFX_Volume;
            yield return null;
        }
    }

    public float Duration {
        get { return AS.clip.length - AS.time; }
    }

}

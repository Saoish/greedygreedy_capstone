﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GreedyNameSpace;

public class StashInteractionContent : InteractionContent {
    public GameObject SelfToolTip;
    public GameObject StashToolTip;

    public Text StashText;
    public GameObject CachedButtonObject;

    public GameObject CachedSelfObject;
    public GameObject CachedStashedObject;
    
    public Transform Self;
    public Transform Stash;

    [HideInInspector]
    public int StashPage = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        StashText.text = "Stash (" + (StashPage+1) + ")";
        if (!SyncActions)
            return;
        if (ControllerManager.Actions.Cancel.WasPressed)
            TurnOff();
        else if (ControllerManager.Actions.Next.WasPressed) {
            if (EventSystem.current.currentSelectedGameObject.transform.IsChildOf(Self)) {
                CachedSelfObject = EventSystem.current.currentSelectedGameObject;
                Self.GetComponent<CanvasGroup>().interactable = false;                
                Stash.GetComponent<CanvasGroup>().interactable = true;
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(CachedStashedObject);                
                StashToolTip.SetActive(true);
                SelfToolTip.SetActive(false);
            }
            else if (EventSystem.current.currentSelectedGameObject.transform.IsChildOf(Stash)) {
                if (StashPage + 1 > Patch.NumberOfStashes-1) {
                    StashPage = 0;
                }
                else {
                    StashPage++;
                }                
                foreach(var SBC in Stash.GetComponentsInChildren<StashButtonController>()) {
                    SBC.UpdateSlotID();
                    SBC.UpdateSlot();                                        
                }
                EventSystem.current.currentSelectedGameObject.GetComponent<StashButtonController>().UpdateInfo();
            }
        }
        else if (ControllerManager.Actions.Prev.WasPressed) {
            if (EventSystem.current.currentSelectedGameObject.transform.IsChildOf(Stash)) {
                CachedStashedObject = EventSystem.current.currentSelectedGameObject;
                Stash.GetComponent<CanvasGroup>().interactable = false;
                Self.GetComponent<CanvasGroup>().interactable = true;
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(CachedSelfObject);
                SelfToolTip.SetActive(true);
                StashToolTip.SetActive(false);
            }
        }
	}

    public override void TurnOn() {
        ControllerManager.SyncActions = false;
        transform.position = CacheManager.MP.Position;
        if (CachedButtonObject.transform.IsChildOf(Self)) {
            Self.GetComponent<CanvasGroup>().interactable = true;
            SelfToolTip.SetActive(true);
            StashToolTip.SetActive(false);
        }
        else if (CachedButtonObject.transform.IsChildOf(Stash)) {
            StashToolTip.SetActive(true);
            SelfToolTip.SetActive(false);
            Stash.GetComponent<CanvasGroup>().interactable = true;
        }
        gameObject.SetActive(true);
        SelectWithDelay(CachedButtonObject);        
    }

    public override void TurnOff() {
        CachedButtonObject = EventSystem.current.currentSelectedGameObject;
        Self.GetComponent<CanvasGroup>().interactable = false;
        Stash.GetComponent<CanvasGroup>().interactable = false;
        gameObject.SetActive(false);
        ControllerManager.SyncActions = true;
    }

    protected override void _Interrupt() {
        CachedButtonObject = EventSystem.current.currentSelectedGameObject;
        SyncActions = false;        
        Self.GetComponent<CanvasGroup>().interactable = false;
        Stash.GetComponent<CanvasGroup>().interactable = false;
    }

    protected override void _Resume() {
        SyncActions = true;        
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(CachedButtonObject);
        if (CachedButtonObject.transform.IsChildOf(Self))
            Self.GetComponent<CanvasGroup>().interactable = true;
        else if (CachedButtonObject.transform.IsChildOf(Stash))
            Stash.GetComponent<CanvasGroup>().interactable = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//public class StatsInfoButton : MonoBehaviour,ISelectHandler {
public class StatsInfoButton : EventTrigger {
    StatsInfo SI;
    private void Awake() {
        SI = (Instantiate(Resources.Load("UIPrefabs/StatsInfo"), transform) as GameObject).GetComponent<StatsInfo>();
        //SI = (Instantiate(SI.gameObject, transform) as GameObject).GetComponent<StatsInfo>();
        SI.transform.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        SI.transform.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
    }

    public override void OnSelect(BaseEventData eventData) {
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position, GameManager.SFX_Volume);
        SI.Show(int.Parse(gameObject.name));
    }

    //public void OnSelect(BaseEventData eventData) {
    //    AudioSource.PlayClipAtPoint(ButtonSFX.EquipmentSelect, transform.position, GameManager.SFX_Volume);
    //    SI.Show(int.Parse(gameObject.name));
    //}


    public override void OnDeselect(BaseEventData data) {        
        SI.Hide();
    }
}

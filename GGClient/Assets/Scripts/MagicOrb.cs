﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visual;
public class MagicOrb : MonoBehaviour {
    public float ChaseSpeed = 1f;        
    public float RollingRadius = 0.1f;
    public float CenterOffSet = 0.1f;
    public float ProjectileTrigger = 0.5f;
    public Animator Anim;

    public Projectile MagicProjectile;

    //bool Idling = true;
    [HideInInspector]
    public Player Master = null;
    //private bool AllowRolling = false;
    float RollingSpeed;
    float time;
    // Use this for initialization    
	void Start () {
        RollingSpeed = Anim.GetCurrentAnimatorStateInfo(0).length / Anim.GetCurrentAnimatorStateInfo(0).speed/0.67f * 10f;
        //Debug.Log(Anim.GetCurrentAnimatorStateInfo(0).length / Anim.GetCurrentAnimatorStateInfo(0).speed);
        StartCoroutine(GetMaster());
    }private IEnumerator GetMaster() {
        yield return new WaitForSeconds(0.1f);
        Master = GetComponent<EquipmentController>().Master;
        if (Master) {
            //Dynamic_Z_Controller.Attach(gameObject);
            SpiritualFollower_Z_Controller.Assign(Master, gameObject, Layer.Z_Mapper[Layer.EquipmentLayerRenderMode.MagicOrb].Front);
        }
        else {
            DestroyObject(this);
        }
    }

    private void Update() {
        BehaviourUpdate();
    }

    //private void FixedUpdate() {
    //    BehaviourFixedUpdate();
    //}

    void BehaviourUpdate() {
        if (!Master)
            return;
        Vector2 TargetPosition;
        if (!Master.Alive) {
            TargetPosition = Master.Position;
            FollowingUpdate(TargetPosition);
        }
        if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_down")) {
            time += Time.deltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
            float x = Mathf.Cos(time) * RollingRadius;
            float y = Mathf.Sin(time) * RollingRadius;
            TargetPosition = Master.Position + new Vector2(x, y) + new Vector2(0, -CenterOffSet);
            RollingUpdate(TargetPosition);
        }
        else if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_left")) {
            time += Time.deltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
            float x = -Mathf.Cos(time) * RollingRadius;
            float y = Mathf.Sin(time) * RollingRadius;
            TargetPosition = Master.Position + new Vector2(x, y) + new Vector2(-CenterOffSet, 0);
            RollingUpdate(TargetPosition);
        }
        else if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_right")) {
            time += Time.deltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
            float x = Mathf.Cos(time) * RollingRadius;
            float y = Mathf.Sin(time) * RollingRadius;
            TargetPosition = Master.Position + new Vector2(x, y) + new Vector2(CenterOffSet, 0);
            RollingUpdate(TargetPosition);
        }
        else if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_up")) {
            time += Time.deltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
            float x = Mathf.Cos(time) * RollingRadius;
            float y = -Mathf.Sin(time) * RollingRadius;
            TargetPosition = Master.Position + new Vector2(x, y) + new Vector2(0, CenterOffSet);
            RollingUpdate(TargetPosition);
        }
        else {
            TargetPosition = Master.Position;
            FollowingUpdate(TargetPosition);
        }
    }

    void BehaviourFixedUpdate() {
        if (!Master)
            return;
        Vector2 TargetPosition;
        if (!Master.Alive) {
            TargetPosition = Master.Position;
            FollowingFixedUpdate(TargetPosition);
        }
        if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_down")) {
            time += Time.fixedDeltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
            float x = Mathf.Cos(time) * RollingRadius;
            float y = Mathf.Sin(time) * RollingRadius;
            TargetPosition = Master.Position + new Vector2(x, y) + new Vector2(0, -CenterOffSet);
            RollingFixedUpdate(TargetPosition);
        }
        else if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_left")) {
            time += Time.fixedDeltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
            float x = -Mathf.Cos(time) * RollingRadius;
            float y = Mathf.Sin(time) * RollingRadius;
            TargetPosition = Master.Position + new Vector2(x, y) + new Vector2(-CenterOffSet, 0);
            RollingFixedUpdate(TargetPosition);
        }
        else if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_right")) {
            time += Time.fixedDeltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
            float x = Mathf.Cos(time) * RollingRadius;
            float y = Mathf.Sin(time) * RollingRadius;
            TargetPosition = Master.Position + new Vector2(x, y) + new Vector2(CenterOffSet, 0);
            RollingFixedUpdate(TargetPosition);
        }
        else if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_up")) {
            time += Time.fixedDeltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
            float x = Mathf.Cos(time) * RollingRadius;
            float y = -Mathf.Sin(time) * RollingRadius;
            TargetPosition = Master.Position + new Vector2(x, y) + new Vector2(0, CenterOffSet);
            RollingFixedUpdate(TargetPosition);
        }
        else {
            TargetPosition = Master.Position;
            RollingFixedUpdate(TargetPosition);
        }
    }

    void FollowingUpdate(Vector2 TargetPosition) {
        transform.position = Vector2.Lerp(transform.position, TargetPosition, Time.deltaTime * ChaseSpeed);
    }

    void RollingUpdate(Vector2 TargetPosition) {
        transform.position = Vector2.Lerp(transform.position, TargetPosition, Time.deltaTime * 7);
    }

    void FollowingFixedUpdate(Vector2 TargetPosition) {        
        transform.position = Vector2.Lerp(transform.position, TargetPosition, Time.fixedDeltaTime * ChaseSpeed);
    }

    void RollingFixedUpdate(Vector2 TargetPosition) {
        transform.position = Vector2.Lerp(transform.position, TargetPosition, Time.fixedDeltaTime * 7);
    }


    //private void FixedUpdate() {
    //    if (!Master)
    //        return;
    //    if (!Master.Alive) {
    //        IdleUpdate();
    //    }
    //    if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_down")) {
    //        AttackDown();
    //    }
    //    else if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_left")) {
    //        AttackLeft();
    //    }
    //    else if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_right")) {
    //        AttackRight();
    //    }
    //    else if (Anim.GetCurrentAnimatorStateInfo(1).IsName("attack_up")) {
    //        AttackUp();
    //    }
    //    else {
    //        IdleUpdate();
    //    }
    //}

    //void IdleUpdate() {
    //    transform.position = Vector2.Lerp(transform.position, Master.Position, Time.fixedDeltaTime * ChaseSpeed);
    //}

    //void AttackDown() {
    //    time += Time.fixedDeltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
    //    float x = Mathf.Cos(time) * RollingRadius;
    //    float y = Mathf.Sin(time) * RollingRadius;
    //    transform.position = Vector2.Lerp(transform.position, Master.Position + new Vector2(x, y) + new Vector2(0, -CenterOffSet), Time.fixedDeltaTime * ChaseSpeed * 7);
    //}

    //void AttackLeft() {
    //    time += Time.fixedDeltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
    //    float x = -Mathf.Cos(time) * RollingRadius;
    //    float y = Mathf.Sin(time) * RollingRadius;
    //    transform.position = Vector2.Lerp(transform.position, Master.Position + new Vector2(x, y) + new Vector2(-CenterOffSet, 0), Time.fixedDeltaTime * ChaseSpeed * 7);
    //}

    //void AttackRight() {
    //    time += Time.fixedDeltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
    //    float x = Mathf.Cos(time) * RollingRadius;
    //    float y = Mathf.Sin(time) * RollingRadius;
    //    transform.position = Vector2.Lerp(transform.position, Master.Position + new Vector2(x, y) + new Vector2(CenterOffSet, 0), Time.fixedDeltaTime * ChaseSpeed * 7);
    //}

    //void AttackUp() {
    //    time += Time.fixedDeltaTime * RollingSpeed * Master.GetAttackAnimSpeed();
    //    float x = Mathf.Cos(time) * RollingRadius;
    //    float y = -Mathf.Sin(time) * RollingRadius;
    //    transform.position = Vector2.Lerp(transform.position, Master.Position + new Vector2(x, y) + new Vector2(0, CenterOffSet), Time.fixedDeltaTime * ChaseSpeed * 7);
    //}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GreedyNameSpace;

public class Path_R : MonoBehaviour {

    MainPlayer MPC;

    public Text PathTitle;

    int[] PathContainSkills = new int[] { 9, 10, 11, 12, 13, 14, 15, 16, 17 };
    // Use this for initialization
    void Start() {
        MPC = transform.parent.GetComponent<Tab_1>().MPC;
        UpdatePathInfo();
        SetPathSpiritColor();
    }

    // Update is called once per frame
    void Update() {
        UpdatePathInfo();
    }

    void SetPathSpiritColor() {
        foreach (var particle in GetComponentsInChildren<ParticleSystem>(true)) {
            particle.startColor = new Color(PathTitle.color.r, PathTitle.color.g, PathTitle.color.b,particle.startColor.a);
        }
    }

    int GetPathTotal() {
        int total = 0;
        foreach (int skillindex in PathContainSkills) {
            total += MPC.GetSkilllvlByIndex(skillindex);
        }
        return total;
    }

    void UpdatePathInfo() {
        if (MPC.Class == CLASS.Warrior) {
            PathTitle.color = MyColor.IronBlue;
            if (GetPathTotal() != 0)
                PathTitle.text = SkillPath.Warlord.ToString() + " (" + GetPathTotal() + ")";
            else
                PathTitle.text = SkillPath.Warlord.ToString();
        }
        else if (MPC.Class == CLASS.Mage) {
            PathTitle.color = MyColor.Purple;
            if (GetPathTotal() != 0)
                PathTitle.text = SkillPath.Destruction.ToString() + " (" + GetPathTotal() + ")";
            else
                PathTitle.text = SkillPath.Destruction.ToString();
        }
    }
}

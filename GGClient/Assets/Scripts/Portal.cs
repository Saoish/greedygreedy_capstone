﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
using System;

public class Portal : Interaction {
    [HideInInspector]
    public int Identity;
    public GameObject InfusionVFX;
    public GameObject ActivationVFX;
    public SceneID SceneToLoad;
    public Vector2 SpawnPostion;
    public int Cap = 4;
    public string ExtraInfo;
    public List<Player> Infused;

    private bool Activating = false;       

    protected override void Start() {
        base.Start();
        //Priority = 1;//For debugging
        Identity = CacheManager.Map.Portals.FindIndex(IsThisPortal);
        Infused = new List<Player>();     
    }
    private bool IsThisPortal(Portal p) {return this == p;}

    public void Infuse(Player Player) {
        Infused.Add(Player);
        Player.Infusing = true;
        Player.ActiveVFXParticle(InfusionVFX);
        Player.InfusedPortal = this;
        ActivationCheck();
    }
    private void ActivationCheck() {
        if(Infused.Count == GameObject.FindGameObjectsWithTag(Tag.FriendlyPlayer).Length) {            
            ActiveVFXParticle(ActivationVFX);
            Activating = true;
            StartCoroutine(Entering());
        }

    }IEnumerator Entering() {
        yield return new WaitForSeconds(5f);
        if(Infused.Count == GameObject.FindGameObjectsWithTag(Tag.FriendlyPlayer).Length) {//Double check
            if (GameObject.FindGameObjectWithTag(Tag.EnemyPlayer) != null) {//A stealthy lie goes into here
                TopNotification.Push("Portal detected disordering fusion power, Raven may be nearby.", MyColor.Red, 3f);
            }
            else {
                foreach (var Player in Infused) {
                    Player.SC = new SceneCoordinate(SceneToLoad, SpawnPostion);
                }
                if(Client.Dominating) {
                    Client.Send(Protocols.UpdateInstanceCurretScene, SceneToLoad, Client.TCP);
                }                
                if(SceneChecker.TargetInWorldStates(SceneToLoad))
                    Client.SendProtocol(Protocols.ReloadInstanceState, Client.TCP);//Incase the fking dominator flee
                GreedyCamera.LerpCamera(transform.position, 0.5f, 0.5f);                
                Scene.Load(SceneToLoad,true,0.5f);
                CacheManager.WipeDymanics();
            }                            
        }

    }

    public void Defuse(Player Player) {
        Infused.Remove(Player);
        Player.Infusing = false;
        Player.InfusedPortal = null;
        Player.FadeVFXParticle(InfusionVFX);
        DeactivationCheck();        
    }private void DeactivationCheck() {
        if (Activating) {
            StopAllCoroutines();
            Activating = false;
            FadeVFXParticle(ActivationVFX);            
        }
    }

    public void Reset() {
        if (Infused.Count == 0)
            return;        
        while (Infused.Count > 0) {                
            Defuse(Infused[0]);            
        }
        RedNotification.Push(RedNotification.Type.RITUAL_INTERRUPTION);
    }

    public override void Cancel() {
        if (CacheManager.MP.Infusing) {
            Client.Send(Protocols.Defusion, new FusionData(Client.ClientID, Identity), Client.TCP);
        }
    }

    public override bool IsTriggered() {
        return ControllerManager.Actions.Submit.WasPressed;
    }    

    public override void Interact() {
        if (!CacheManager.MP.Infusing) {
            if (GameObject.FindGameObjectsWithTag(Tag.FriendlyPlayer).Length > Cap) {
                RedNotification.Push(RedNotification.Type.OVER_INFUSION);
            }
            else if (!CacheManager.MP.MPI.Infusable)
                RedNotification.Push(RedNotification.Type.CANT_DO_NOW);
            else {
                ActiveSkillButtonController.InterruptAll();                
                Client.Send(Protocols.Infusion, new FusionData(Client.ClientID, Identity), Client.TCP);
                MainPlayer.DropQueue();
            }
        }
    }           

    public override void SetIndication() {
        if (!CacheManager.MP.Infusing) {
            if (SceneChecker.TargetInDungeons(SceneToLoad))
                InteractionNotification.SetIndication(MyText.Colofied(Scene.GetName(SceneToLoad), MyText.megenta) + ExtraInfo, ButtonImages.X);
            else if (SceneChecker.TargetInWorldStates(SceneToLoad))
                InteractionNotification.SetIndication(MyText.Colofied(Scene.GetName(SceneToLoad), MyText.orange), ButtonImages.X);
            InteractionNotification.TurnOn();
        }
        else {
            InteractionNotification.SetIndication("Cancel Infusion", ButtonImages.O);
            InteractionNotification.TurnOn();
        }
    }


    private void ActiveVFXParticle(GameObject VFX) {
        string cached_name = VFX.name;        
        VFX = Instantiate(VFX, transform) as GameObject;
        VFX.transform.position = transform.position + VFX.transform.position;        
        VFX.name = cached_name;
    }

    private void FadeVFXParticle(GameObject VFX) {
        ParticleSystem ps = transform.Find(VFX.name).gameObject.GetComponent<ParticleSystem>();
        ps.transform.parent = null;
        StartCoroutine(DelayFade(ps, 1f));
    }private IEnumerator DelayFade(ParticleSystem ps, float delay) {
        yield return new WaitForSeconds(delay);
        ps.Simulate(ps.duration, true);
        ps.loop = false;
        ps.Play(true);
        ps.gameObject.GetComponent<LoopSFX>().Fade();
        if (ps.gameObject.GetComponent<LoopSFX>().Duration > ps.duration)
            Destroy(ps.gameObject, ps.gameObject.GetComponent<LoopSFX>().Duration);
        else
            Destroy(ps.gameObject, ps.duration);
    }
}

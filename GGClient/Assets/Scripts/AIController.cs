﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;    

public class AIController : MonoBehaviour {
    public float DetectionRange = 1;
    public float AttackDistance = 0.3f;

    List<ObjectController> TargetList;    
    [HideInInspector]
    public ObjectController Target;
    [HideInInspector]
    public Vector2 MoveVector;
    [HideInInspector]
    public Vector2 AttackVector;
    [HideInInspector]
    public int Direction;

    private Monster MC;

    public bool Moving {
        get { return MoveVector != Vector2.zero; }
    }

    public bool Attacking {
        get { return AttackVector != Vector2.zero; }
    }

    // Use this for initialization
    void Awake() {
        TargetList = new List<ObjectController>();
    }

    void Start () {
        MC = GetComponent<Monster>();
	}
	
    void FixedUpdate() {
        TargetProcessing();
    }

	// Update is called once per frame
	void Update () {        
        AttackVectorUpdate();
        MoveVectorUpdate();
        DirectionUpdate();
    }

    void TargetProcessing() {
        if (Client.Connected)
            NetworkTargetProcessing();
        else
            LocalTargetProcessing();
    }

    void NetworkTargetProcessing() {
        if (!Client.Dominating) {
            if (Target && !Target.Alive)
                Target = null;
        }
        else {
            SearchTarget();
            //LockOnClosestTarget();
            //DiscardTarget();
        }
    }

    void LocalTargetProcessing() {        
        SearchTarget();
        //LockOnClosestTarget();
        //DiscardTarget();
    }

    //void SearchTarget() {
    //    int layer = (1 << CollisionLayer.KillingGround);
    //    Collider2D[] detacts = Physics2D.OverlapCircleAll(transform.position, DetectionRange, layer);
    //    foreach (var detact in detacts) {
    //        ObjectController target = detact.GetComponent<ObjectController>();
    //        if (target.OID != OID.EnemyMonster && !TargetList.Contains(target) && target.OID != OID.NPC && target.AITargetable) {//Don't attack NPC yet
    //            TargetList.Add(target);
    //        }
    //    }
    //    if (Target && !Target.Alive) {//Other client would notice as well
    //        //if (Client.Connected)
    //        //    Client.Send(Protocols.ObjectUpdateTarget, new UpdateTargetData(MC.NetworkID, 0), Client.TCP); 
    //        if (TargetList.Contains(Target))
    //            TargetList.Remove(Target);
    //        Target = null;
    //    }
    //}

    void SearchTarget() {
        //int layer = (1 << CollisionLayer.KillingGround);
        int layer = (1 << CollisionLayer.Friendly | 1 << CollisionLayer.Enemy);
        Collider2D[] detacts = Physics2D.OverlapCircleAll(transform.position, DetectionRange, layer);
        ObjectController IntendedTarget = null;        
        foreach (var detact in detacts) {
            ObjectController DetectedTarget = detact.GetComponent<ObjectController>();                        
            if (DetectedTarget.Alive && DetectedTarget.OID != OID.EnemyMonster && !TargetList.Contains(DetectedTarget) && DetectedTarget.OID != OID.NPC && DetectedTarget.AITargetable) {//Don't attack NPC yet                
                if (!IntendedTarget) {
                    IntendedTarget = DetectedTarget;
                }
                else if (Vector2.Distance(DetectedTarget.Position, MC.Position) < Vector2.Distance(IntendedTarget.Position, MC.Position)) {
                    IntendedTarget = DetectedTarget;
                }                    
            }
        }
        if (Target != IntendedTarget) {
            if (Client.Connected) {
                if (!IntendedTarget)
                    Client.Send(Protocols.ObjectUpdateTarget, new UpdateTargetData(MC.NetworkID, 0), Client.TCP);
                else
                    Client.Send(Protocols.ObjectUpdateTarget, new UpdateTargetData(MC.NetworkID, IntendedTarget.NetworkID), Client.TCP);
            }
            Target = IntendedTarget;
        }                                      
    }


    //void DiscardTarget() {
    //    if (TargetList.Count == 0)
    //        return;
    //    foreach (var Player in FindObjectsOfType<Player>()) {
    //        if (Vector2.Distance(transform.position, Player.transform.position) > DetectionRange || !Player.Alive) {
    //            TargetList.Remove(Player);
    //            if (Target == Player) {
    //                if (Client.Connected) 
    //                    Client.Send(Protocols.ObjectUpdateTarget, new UpdateTargetData(MC.NetworkID, 0), Client.TCP);                    
    //                Target = null;                                        
    //            }
    //        }
    //    }
    //}

    //void LockOnClosestTarget() {
    //    if (TargetList.Count == 0)
    //        return;
    //    else if (Target == null || !Target.Alive)
    //        TargetList.Remove(Target);
    //    TargetList.Sort(delegate (ObjectController a, ObjectController b) {
    //        if (!a) {
    //            TargetList.Remove(a);
    //            return 999;
    //        }else if (!b) {
    //            TargetList.Remove(b);
    //            return 999;
    //        }
    //        return Vector2.Distance(transform.position, a.transform.position).CompareTo(Vector2.Distance(transform.position, b.transform.position));
    //    });
    //    ObjectController IntendedTarged = TargetList[0];
    //    if (Target!=IntendedTarged) {
    //        if (Client.Connected) {
    //            if(!IntendedTarged)
    //                Client.Send(Protocols.ObjectUpdateTarget, new UpdateTargetData(MC.NetworkID, 0), Client.TCP);
    //            else
    //                Client.Send(Protocols.ObjectUpdateTarget, new UpdateTargetData(MC.NetworkID, IntendedTarged.NetworkID), Client.TCP);
    //        }
    //        Target = IntendedTarged;            
    //    }
    //}

    //-------private
    void MoveVectorUpdate() {
        if (Target != null) {
            float dist = Vector2.Distance(Target.transform.position, transform.position);
            if (dist > AttackDistance) {
                MoveVector = (Vector2)Vector3.Normalize(Target.transform.position - transform.position);
                AttackVector = Vector2.zero;
            } else {
                MoveVector = Vector2.zero;
            }
        } else {
            MoveVector = Vector2.zero;
        }
    }

    void AttackVectorUpdate() {
        if (Target != null) {
            float dist = Vector2.Distance(Target.transform.position, transform.position);
            if (dist <= AttackDistance) {
                AttackVector = (Vector2)Vector3.Normalize(Target.transform.position - transform.position);
                MoveVector = Vector2.zero;
            } else {
                AttackVector = Vector2.zero;
            }
        } else {
            AttackVector = Vector2.zero;
        }
    }

    void DirectionUpdate() {
        if (MoveVector != Vector2.zero && AttackVector == Vector2.zero) {
            if (Vector2.Angle(MoveVector, Vector2.right) <= 45) {//Right
                Direction = 2;
            } else if (Vector2.Angle(MoveVector, Vector2.up) <= 45) {//Up
                Direction = 3;
            } else if (Vector2.Angle(MoveVector, Vector2.left) <= 45) {//Left
                Direction = 1;
            } else if (Vector2.Angle(MoveVector, Vector2.down) <= 45) {//Down
                Direction = 0;
            }
        } else if (AttackVector != Vector2.zero) {
            if (Vector2.Angle(AttackVector, Vector2.right) <= 45) {//Right
                Direction = 2;
            } else if (Vector2.Angle(AttackVector, Vector2.up) <= 45) {//Up
                Direction = 3;
            } else if (Vector2.Angle(AttackVector, Vector2.left) <= 45) {//Left
                Direction = 1;
            } else if (Vector2.Angle(AttackVector, Vector2.down) <= 45) {//Down
                Direction = 0;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;

public class CombatChecker : MonoBehaviour {
    static Dictionary<CR, CR[]> IgnoreEffects = new Dictionary<CR, CR[]>(){
        //Bleeding
        {CR.BattleFury,new CR[] {CR.BleedDebuff }},
        {CR.BleedDebuff,new CR[] {CR.BleedDebuff}},

        //Slowing
        {CR.Chill,new CR[] {CR.RuinDebuff,CR.ChillDebuff}},
        {CR.ChillDebuff, new CR[] {CR.RuinDebuff,CR.ChillDebuff}},
        {CR.Ruin, new CR[] {CR.RuinDebuff,CR.ChillDebuff}},
        {CR.RuinDebuff, new CR[] {CR.RuinDebuff,CR.ChillDebuff}},

        //Damage Nerfing
        {CR.Cripple, new CR[] {CR.CrippleDebuff}},
        {CR.CrippleDebuff, new CR[] {CR.CrippleDebuff}},

        //Defense Netfing
        {CR.Char, new CR[] {CR.CharDebuff}},
        {CR.CharDebuff, new CR[] {CR.CharDebuff}},
    };

    public static bool IgnoreDealingDmg(ObjectController OC,Collider2D collider,Stack<Collider2D> HittedStack) {        
        if (collider.gameObject.layer != CollisionLayer.Enemy && collider.gameObject.layer != CollisionLayer.Friendly)
            return true;
        if (OC.OID == OID.Main || OC.OID == OID.FriendlyPlayer || OC.OID == OID.FriendlyMonster) {//Player  
            if (collider.tag == Tag.FriendlyPlayer || collider.tag == Tag.FriendlyMonster || HittedStack.Count != 0 && HittedStack.Contains(collider))
                return true;
        }
        else if (OC.OID == OID.EnemyPlayer) {
            if (collider.tag == Tag.EnemyPlayer || HittedStack.Count != 0 && HittedStack.Contains(collider))
                return true;
        }
        else {
            if (collider.tag == Tag.EnemyMonster || HittedStack.Count != 0 && HittedStack.Contains(collider))
                return true;
        }
        return false;
    }
    public static bool IgnoreDealingDmd(ObjectController OC,Collider2D collider) {
        if (collider.gameObject.layer != CollisionLayer.Enemy && collider.gameObject.layer != CollisionLayer.Friendly)
            return true;
        if (OC.OID == OID.Main || OC.OID == OID.FriendlyPlayer || OC.OID == OID.FriendlyMonster) {//Player  
            if (collider.tag == Tag.FriendlyPlayer || collider.tag == Tag.FriendlyMonster)
                return true;
        }
        else if (OC.OID == OID.EnemyPlayer) {
            if (collider.tag == Tag.EnemyPlayer)
                return true;
        }
        else {
            if (collider.tag == Tag.EnemyMonster)
                return true;
        }
        return false;
    }
        
    public static bool IgnoreApplyingEffect(int TargetID, CR TypeToCheck) {
        foreach(var Type in IgnoreEffects[TypeToCheck]) {
            if (CacheManager.GetOC(TargetID).HasEffect(Type))
                return true;
        }
        return false;
    }
    public static bool IgnoreApplyingEffect(ObjectController Target, CR TypeToCheck) {
        foreach (var Type in IgnoreEffects[TypeToCheck]) {
            if (Target.HasEffect(Type))
                return true;
        }
        return false;
    }
}

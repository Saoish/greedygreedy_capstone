﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;

public class EquipmentInteraction : Interaction {

    protected override void Start() {
        base.Start();
        Priority = 1;
    }

    public override bool IsTriggered() {
        return ControllerManager.Actions.Submit.WasPressed;
    }

    public override void Interact() {
        if (CacheManager.MP.InventoryIsFull())
            RedNotification.Push(RedNotification.Type.INVENTORY_FULL);
        else {
            CacheManager.MP_Data.InventoryViewSigns[CacheManager.MP.FirstAvailbleInventorySlot] = true;
            CacheManager.MP.AddToInventory(CacheManager.MP.FirstAvailbleInventorySlot,GetComponentInParent<EquipmentController>().E);                        
            CacheManager.MP.MPI.InteractTarget = null;
            Destroy(transform.parent.gameObject);
        }
    }

    public override void SetIndication() {
        InteractionNotification.SetIndication("Pick Up", ButtonImages.X);
        InteractionNotification.TurnOn();        
    }

}

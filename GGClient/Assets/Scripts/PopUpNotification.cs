﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GreedyNameSpace;
using System;
using Networking.Data;
public class PopUpNotification : MonoBehaviour {
    static Vector3 NativeScale = new Vector3(0.001851852f, 0.001851852f, 0.001851852f);

    static Coroutine DecisionCO = null;//Only allow one at a time
       
    public enum Type {
        Pending,
        Confirm,
        Select
    }

    public static bool Decision = false;
    public static bool Decided = false;
    public static bool SyncActions = false;

    static GameObject OK_BtnOJ;
    static GameObject Yes_BtnOJ;
    static GameObject No_BtnOJ;

    static Text msg;
    static Animator anim;
    static GameObject self_oj;
    static PopUpNotification instance;

    static GameObject CachedPointer;

    static DecisionTimer DT;

    //static CanvasGroup[] cgs;   

    private void OnLevelWasLoaded(int level) {
        gameObject.SetActive(false);
        DecisionCO = null;        
        Decision = false;
        Decided = false;
        SyncActions = false;
        CachedPointer = null;
}

    public void SetUp() {
        instance = this;
        msg = transform.Find("Message").GetComponent<Text>();
        anim = GetComponent<Animator>();
        self_oj = gameObject;        
        OK_BtnOJ = transform.Find("Buttons/OK").gameObject;
        Yes_BtnOJ = transform.Find("Buttons/Yes").gameObject;
        No_BtnOJ = transform.Find("Buttons/No").gameObject;
        DT = GetComponentInChildren<DecisionTimer>(true);
        self_oj.SetActive(false);
    }

    void Update() {
        if (SyncActions && ControllerManager.Actions.Cancel.WasPressed)
            Cancel();        
    }

    public static GameObject GameObject{
        get { return self_oj; }
    }

    public static bool IsActive {
        get {
            if (!self_oj)
                return false;
            return self_oj.active;
        }
    }

    public static void AbortWithYes() {
        if (self_oj.active) {
            instance.Yes();
        }
    }

    public static void AbortWithNo() {
        if (self_oj.active) {
            instance.No();
        }
    }

    public static void Close() {
        AbortWithNo();
    }


    public static void Push(string message, Type type = Type.Pending, float TimeOut = 0f) {
        ControllerManager.SyncActions = false;
        if (SceneChecker.TargetInUnPlayables(Scene.Current_SID)) {
            DisableAllCanvasGroup();
        }
        else {
            CheckAndInterrupt();
        }
        if (type == Type.Select && CacheManager.MP && !CacheManager.MP.Alive)        
            return;        
        msg.text = message;
        self_oj.transform.localScale = NativeScale;
        self_oj.SetActive(true);
        anim.Rebind();
        anim.Play("popup", 0, 0f);
        switch (type) {
            case Type.Pending:
                break;
            case Type.Confirm:
                Yes_BtnOJ.SetActive(false);
                No_BtnOJ.SetActive(false);
                OK_BtnOJ.SetActive(true);
                CachedPointer = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;
                UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
                UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(OK_BtnOJ);
                DT.Disable();
                break;
            case Type.Select://Only this case cares about the timer                   
                OK_BtnOJ.SetActive(false);
                Yes_BtnOJ.SetActive(true);
                No_BtnOJ.SetActive(true);
                CachedPointer = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;
                UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
                UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(No_BtnOJ);
                SyncActions = true;
                if (TimeOut > 0)
                    DT.Enable(TimeOut);
                break;
        }
    }

    static private void DisableAllCanvasGroup() {
        foreach (CanvasGroup cg in FindObjectsOfType<CanvasGroup>()) {
            cg.interactable = false;
        }
    }

    static private void EnableAllCanvasGroup() {
        foreach (CanvasGroup cg in FindObjectsOfType<CanvasGroup>()) {
            cg.interactable = true;
        }
    }

    static private void CheckAndInterrupt() {
        if (SceneChecker.TargetInUnPlayables(Scene.Current_SID))
            return;
        ActiveSkillButtonController.InterruptAll();
        if (InteractionContent.HasActive) {
            if (InteractionContent.Current.SyncActions)
                InteractionContent.Interrupt();
        }
    }

    static private void CheckAndRestoreControllerSyncAction() {
        GameManager.instance.StartCoroutine(RunCheckWithDelayInCaseAnotherPopUp());
    }private static IEnumerator RunCheckWithDelayInCaseAnotherPopUp() {
        yield return new WaitForSeconds(0.1f);
        if (!IsActive) {
            if (!InteractionContent.HasActive) {                
                ControllerManager.SyncActions = true;
            }
            else {
                InteractionContent.Resume();
            }
        }
    }

    public void Confirm() {
        OK_BtnOJ.SetActive(false);
        gameObject.SetActive(false);
        if(SceneChecker.TargetInUnPlayables(Scene.Current_SID))
            EnableAllCanvasGroup();
        if (CachedPointer != null) {
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(CachedPointer);
            CachedPointer = null;
        }
        CheckAndRestoreControllerSyncAction();
    }

    public void Yes() {
        SyncActions = false;
        Decided = true;
        Decision = true;
        Yes_BtnOJ.SetActive(false);
        No_BtnOJ.SetActive(false);
        gameObject.SetActive(false);
        DT.Disable();
        if (SceneChecker.TargetInUnPlayables(Scene.Current_SID))
            EnableAllCanvasGroup();
        if (CachedPointer != null) {
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(CachedPointer);
            CachedPointer = null;
        }
        CheckAndRestoreControllerSyncAction();
    }

    public void No() {        
        SyncActions = false;
        Decided = true;
        Decision = false;
        Yes_BtnOJ.SetActive(false);
        No_BtnOJ.SetActive(false);
        gameObject.SetActive(false);
        DT.Disable();
        if (SceneChecker.TargetInUnPlayables(Scene.Current_SID))
            EnableAllCanvasGroup();
        if (CachedPointer != null) {
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(CachedPointer);
            CachedPointer = null;
        }
        CheckAndRestoreControllerSyncAction();
    }    

    public void Cancel() {        
        SyncActions = false;
        Decided = true;
        Decision = false;
        gameObject.SetActive(false);
        DT.Disable();
        if (SceneChecker.TargetInUnPlayables(Scene.Current_SID))
            EnableAllCanvasGroup();
        if (CachedPointer != null) {
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(CachedPointer);
            CachedPointer = null;
        }
        CheckAndRestoreControllerSyncAction();
    }

    public static void HereWeGo(Action ConfirmCall,Action DenyCall = null) {
        if (DecisionCO != null)    
            GameManager.instance.StopCoroutine(DecisionCO);        
        CheckAndInterrupt();            
        DecisionCO = GameManager.instance.StartCoroutine(WaitForDecisionThenPerformAction(ConfirmCall, DenyCall));
    }

    public static void HereWeGo<T>(Action<T> Call, T para) {
        if (DecisionCO != null)
            GameManager.instance.StopCoroutine(DecisionCO);
        CheckAndInterrupt();
        DecisionCO = GameManager.instance.StartCoroutine(WaitForDecisionThenPerformAction(Call, para));
    }

    public static void HereWeGo<T>(Action<T> ConfirmCall, Action<T> DenyCall, T shared_p) {
        if (DecisionCO != null)            
            GameManager.instance.StopCoroutine(DecisionCO);        
        CheckAndInterrupt();
        DecisionCO = GameManager.instance.StartCoroutine(WaitForDecisionThenPerformAction(ConfirmCall, DenyCall, shared_p));
    }


    private static IEnumerator WaitForDecisionThenPerformAction<T>(Action<T> Call,T para) {    
        Decided = false;
        Decision = false;
        while (Decided == false)
            yield return null;
        if(Decision)
            Call(para);       
    }

    private static IEnumerator WaitForDecisionThenPerformAction(Action ConfirmCall,Action DenyCall = null) {        
        Decided = false;
        Decision = false;
        while (Decided == false) {            
            yield return null;
        }
        if (Decision) {            
            ConfirmCall();
        }
        else {
            if (DenyCall != null) {                
                DenyCall();
            }
        }        
    }

    private static IEnumerator WaitForDecisionThenPerformAction<T>(Action<T> ConfirmCall, Action<T> DenyCall,T shared_p) {
        Decided = false;
        Decision = false;
        while (Decided == false)
            yield return null;
        if (Decision)
            ConfirmCall(shared_p);
        else {
            if (DenyCall != null)
                DenyCall(shared_p);
        }        
    }

    private static IEnumerator WaitForDecision() {
        Decided = false;
        Decision = false;
        while (Decided == false)
            yield return null;
    }    
}

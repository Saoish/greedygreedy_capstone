﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;

public class Arena : Map {    
    bool End = false;

    float LeavingCountDown = 5f;
    Coroutine LeavingCO = null;

    protected override void Awake() {
        base.Awake();
        RegisteredPlayers = new List<Player>();
    }

    protected override void Start() {
        base.Start();
    }

    protected override void Update() {
        base.Update();
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();
    }

    public override SceneCoordinate GetCloestestRespawnSC(Vector2 Position) {
        throw new NotImplementedException();
    }

    public override void OnPlayerLeft(Player PC) {
        base.OnPlayerLeft(PC);
        if (End)
            return;
        if (PC.OID == OID.EnemyPlayer)
            OpponentsAlives--;
        else
            OursAlives--;
        if (OursAlives == 0 && OpponentsAlives == 0) {
            End = true;
            Debug.Log("Draw");
            StartLeaving();
            //StartCoroutine(LeavingIn(LeavingCountDown));
        }
        else if (OursAlives == 0) {//Lose
            End = true;
            Debug.Log("Loss");
            foreach (var p in CacheManager.GetPlayers())
                p.ClearEffects();
            StartLeaving();
            //StartCoroutine(LeavingIn(LeavingCountDown));
        }
        else if (OpponentsAlives == 0) {//Win
            End = true;
            Debug.Log("Win");
            foreach (var p in CacheManager.GetPlayers())
                p.ClearEffects();
            StartLeaving();
            //StartCoroutine(LeavingIn(LeavingCountDown));
        }
    }

    public override void ApplyEvents(ObjectController OC) {                
        if (OC is Player) {
            OC.ON_DEATH_UPDATE += OnDeathEvent;
            if (!RegisteredPlayers.Contains((Player)OC)) {
                if (OC.OID == OID.EnemyPlayer)
                    OpponentsAlives++;
                else
                    OursAlives++;
                OC.CurrStats = new Stats(OC.MaxStats);
                RegisteredPlayers.Add((Player)OC);
            }
        }
    }

    void OnDeathEvent(Damage dmg) {
        GreedyCamera.SlowMotionEffect(CacheManager.GetOC(dmg.targetID).Position);
        if (CacheManager.GetOC(dmg.targetID).OID == OID.EnemyPlayer)
            OpponentsAlives--;
        else {            
            OursAlives--;
        }
        if (OursAlives == 0 && OpponentsAlives == 0) {
            End = true;
            Debug.Log("Draw");
            StartLeaving();
            //StartCoroutine(LeavingIn(LeavingCountDown));
        }
        else if (OursAlives == 0) {//Lose
            End = true;
            Debug.Log("Loss");
            foreach (var PC in CacheManager.GetPlayers())
                PC.ClearEffects();
            StartLeaving();
            //StartCoroutine(LeavingIn(LeavingCountDown));
        }
        else if (OpponentsAlives == 0) {//Win
            End = true;
            Debug.Log("Win");
            foreach (var PC in CacheManager.GetPlayers())
                PC.ClearEffects();
            StartLeaving();
            //StartCoroutine(LeavingIn(LeavingCountDown));
        }
    }

    private void StartLeaving() {
        if(LeavingCO==null)       
            LeavingCO = StartCoroutine(LeavingIn(LeavingCountDown));
    }

    IEnumerator LeavingIn(float Countdown) {
        float LastCounter = Countdown;
        TopNotification.Push("Leaving here in " + Countdown.ToString("F0"), MyColor.Yellow, 1f);
        while (Countdown>0) {
            Countdown -= Time.fixedDeltaTime;
            if (LastCounter - Countdown >= 1f) {
                TopNotification.Push("Leaving here in " + Countdown.ToString("F0"), MyColor.Yellow, 1f);
                LastCounter = Countdown;
            }
            yield return new WaitForFixedUpdate();
        }
        CacheManager.WipeOtherPlayers();
        Scene.LoadWithManualActive(CacheManager.PrevState.SC.SID);
        //CacheManager.MP_Data.SC = CacheManager.PrevState.SC;
        CacheManager.ConvertMPToPrevState();
        Client.SendProtocol(Protocols.AbandInstanceAndForwardScene,Client.TCP);        
        //TopNotification.Push("Leaving here in " + CountDown.ToString("F0"), MyColor.Yellow, CountDown);
        
    }
}

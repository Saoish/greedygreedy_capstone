﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public class Tyneham : Map {

    protected override void Start() {//Will be removed
        base.Start();
        StartCoroutine(SetDemoObjectiveWithDelay());
    }private IEnumerator SetDemoObjectiveWithDelay() {
        yield return new WaitForSeconds(0.1f);
        CacheManager.MP.Objective.Set(Portals[0].transform);
    }


    public override void ApplyEvents(ObjectController OC) {
        if(OC is Player) {
            OC.ON_DEATH_UPDATE += OnDeathEvent;            
            if (!RegisteredPlayers.Contains((Player)OC)) {                
                if (!OC.Alive) {
                    RegisteredPlayers.Add((Player)OC);
                    return;
                }                    
                else if (OC.OID == OID.EnemyPlayer)
                    OpponentsAlives++;
                else
                    OursAlives++;
                RegisteredPlayers.Add((Player)OC);
            }
        }       
    }

    public override void OnPlayerLeft(Player PC) {
        base.OnPlayerLeft(PC);
        if (PC.Alive) {
            if (PC.OID == OID.EnemyPlayer)
                OpponentsAlives--;
            else
                OursAlives--;
            if (OursAlives == 0) {
                if (OpponentsAlives != 0) {//Loss the instance                                                        
                    DeathCoroutine = StartCoroutine(Leaving("Your soul is fading to this world..."));
                }
                else {
                    DeathCoroutine = StartCoroutine(Reset("Your soul is fading..."));
                }
            }
        }

    }

    public override SceneCoordinate GetCloestestRespawnSC(Vector2 Position) {
        return new SceneCoordinate(SceneID.Tyneham,Vector2.zero);//For now;
    }

    private void OnDeathEvent(Damage dmg) {
        if (CacheManager.GetOC(dmg.targetID).OID == OID.EnemyPlayer)
            OpponentsAlives--;
        else {            
            OursAlives--;
        }
        if (OursAlives == 0) {
            if (OpponentsAlives != 0) {//Loss the instance                
                DeathCoroutine = StartCoroutine(Leaving("Your soul is fading to this world..."));
            }
            else {
                DeathCoroutine = StartCoroutine(Reset("Your soul is fading..."));
            }
        }        
    }
    

    IEnumerator Reset(string msg) {
        TopNotification.Push(msg, MyColor.Red, 3f);
        yield return new WaitForSeconds(5f);
        if (OursAlives == 0) {            
            Client.SendProtocol(Protocols.ReloadInstanceState, Client.TCP);//Incase the fking dominator flee
            Scene.Load(Scene.Current_SID);            
        }
    }
    
    IEnumerator Leaving(string msg) {
        TopNotification.Push(msg, MyColor.Red, 3f);
        yield return new WaitForSeconds(5f);
        if (OursAlives == 0) {
            CacheManager.WipeOtherPlayers();
            Scene.LoadWithManualActive(Scene.Current_SID);
            Client.SendProtocol(Protocols.ReturnOwnInstance, Client.TCP);
        }
    }

}

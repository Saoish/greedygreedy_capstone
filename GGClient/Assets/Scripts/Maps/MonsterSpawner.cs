﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;

public class MonsterSpawner : MonoBehaviour {
    public List<Monster> SpawnList;
    protected int Identity;
    //protected Vector2[] SpawnOffsets = new Vector2[] {
    //    new Vector2(0,0),
    //    new Vector2(0.1f,0),
    //    new Vector2(0,0.1f),
    //    new Vector2(-0.1f,0),
    //    new Vector2(0,-0.1f),
    //    new Vector2(0.1f,0.1f),
    //    new Vector2(0.1f,-0.1f),
    //    new Vector2(-0.1f,0.1f),
    //    new Vector2(-0.1f,-0.1f)
    //};

    protected bool IsThisSpawner(MonsterSpawner MS) { return this == MS; }	

	protected virtual void Start () {
        Identity = CacheManager.Map.MS.FindIndex(IsThisSpawner);
    }
	
	// Update is called once per frame
	protected virtual void Update () {
	}

    public virtual void Spawn(int DynamicKey,int SpanwIndex) {
        SpawnList[SpanwIndex].Data = CacheManager.GetObjectData(DynamicKey);
        CacheManager.AssignedDynamicOC(DynamicKey, SpawnList[SpanwIndex].Spawn(DynamicKey));
    }

    //public virtual void SpawnByNetwork(int DynamicKey,int SpawnIndex,int SpawnLvl) {
    //    SpawnList[SpawnIndex].Data = new ObjectData(OID.EnemyMonster, new SceneCoordinate(Scene.Current_SID, transform.position), SpawnLvl);
    //    CacheManager.AddDynamicData(DynamicKey, SpawnList[SpawnIndex].Data);
    //    CacheManager.AssignedDynamicOC(DynamicKey, SpawnList[SpawnIndex].Spawn(DynamicKey));        
    //}

    //public virtual void Spawn() {        
    //    if (Client.Connected)
    //        NetworkSpawn();
    //    else
    //        LocalSpawn();        
    //}

    //protected virtual void NetworkSpawn() {
    //    int SpawnIndex;
    //    if (SpawnList.Count > 1) {
    //        SpawnIndex= Random.Range(0, SpawnList.Count);                        
    //    }
    //    else {
    //        SpawnIndex = 0;
    //    }
    //    int DynamicKey = CacheManager.NextMonsterKey--;        
    //    Client.Send(Protocols.SpawnMonster, new SpawnMonsterData(DynamicKey,Identity, SpawnIndex,Player.AvgLvl), Client.TCP);
    //} 

    //protected void LocalSpawn() {        
    //    int SpawnIndex;
    //    if (SpawnList.Count > 1) {
    //        SpawnIndex = Random.Range(0, SpawnList.Count);
    //    }
    //    else {
    //        SpawnIndex = 0;
    //    }
    //    int DynamicKey = CacheManager.NextMonsterKey--;
    //    SpawnList[SpawnIndex].Data = new ObjectData(OID.EnemyMonster, new SceneCoordinate(Scene.Current_SID, transform.position), Player.AvgLvl);
    //    CacheManager.AddDynamicData(DynamicKey,SpawnList[SpawnIndex].Data);
    //    CacheManager.AssignedDynamicOC(DynamicKey, SpawnList[SpawnIndex].Spawn(DynamicKey));        
    //}
}

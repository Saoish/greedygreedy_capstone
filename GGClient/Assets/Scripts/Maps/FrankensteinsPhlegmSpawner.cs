﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public class FrankensteinsPhlegmSpawner : MonsterSpawner {
    public AudioClip Roar; 
    public Animator Anim;

    [HideInInspector]
    public bool SpawnTrigger = false;
	// Use this for initialization
	protected override void  Start () {
        Identity = CacheManager.Map.BS.FindIndex(IsThisSpawner);
	}

    public override void Spawn(int DynamicKey, int SpanwIndex) {
        TriggerEffects();
        StartCoroutine(WaitForEffectsAndSpawn(DynamicKey,SpanwIndex));                        
    }

    private void TriggerEffects() {
        Anim.SetTrigger("Spawn");
        AudioSource.PlayClipAtPoint(Roar, transform.position, GameManager.SFX_Volume);
    }

    private IEnumerator WaitForEffectsAndSpawn(int DynamicKey, int SpanwIndex) {
        while (!SpawnTrigger)
            yield return null;
        SpawnList[SpanwIndex].Data = CacheManager.GetObjectData(DynamicKey);        
        CacheManager.AssignedDynamicOC(DynamicKey, SpawnList[SpanwIndex].Spawn(DynamicKey));        
        CacheManager.MP.Objective.Set(CacheManager.GetOC(DynamicKey).transform);
    }     
}

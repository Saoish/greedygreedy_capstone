﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using System;
using Networking.Data;

public class FrankensteinCastle : Map {    
    public int SlimeAlives = 0;    
    public LootSpawner LootSpawner;
    public Door Door;

    float SpawnTimer = 0f;
    [SerializeField]
    float SpawnInterval = 1f;

    float WaitTimer = 0f;
    float LastWaitTimer = 0f;
    public float WaitInterval = 30f;

    [SerializeField]
    int StartWave = 1;
    int Spawned = 0;

    [SerializeField]
    public int BossWave = 10;
    
    bool Started = false;
    bool Ended = false;

    bool AllowWait = false;
    bool AllowSpawn = false;
    bool BossFighting = false;

    int DynamicSpawnedKey = -1000;

	// Use this for initialization
    protected override void Awake() {
        base.Awake();
    }

    protected override void Start () {
        base.Start();
        StartCoroutine(PreGenerateSpawnPlan());
        StartCoroutine(SetDemoObjectiveWithDelay());
    }private IEnumerator PreGenerateSpawnPlan() {
        yield return new WaitForSeconds(1f);        
        for (int i = StartWave; i < BossWave; i++) {
            for (int j = 0; j < i; j++) {
                for (int k = 0; k < MS.Count; k++) {
                    CacheManager.AddDynamicData(CacheManager.NextMonsterKey--, new ObjectData(OID.EnemyMonster, new SceneCoordinate(Scene.Current_SID, MS[k].transform.position), Player.AvgLvl));
                }
            }
        }
        ObjectData OD;
        int test_key;
        CacheManager.AddDynamicData(test_key=CacheManager.NextMonsterKey--, OD = new ObjectData(OID.EnemyMonster, new SceneCoordinate(Scene.Current_SID, BS[0].transform.position), Player.AvgLvl));        
    }private IEnumerator SetDemoObjectiveWithDelay() {
        yield return new WaitForSeconds(0.1f);
        CacheManager.MP.Objective.Set(BS[0].transform);
    }

    protected override void Update() {
        base.Update();
    }

    protected override void FixedUpdate () {
        base.FixedUpdate();
        if (Door.Triggered && !Started) {
            Started = true;
            AllowWait = true;
            SlimeAlives = StartWave * 3;
            Door.Trigger.Disable();//Just for now
        }
        else if (AllowWait)
            Waiting();
        else if(AllowSpawn)
            Spawining();
    }

    void Spawining() {
        CacheManager.MP.Objective.TurnOff();
        if (SpawnTimer < SpawnInterval) {
            SpawnTimer += Time.fixedDeltaTime;
        } else {
            foreach (MonsterSpawner s in MS) {                
                Spawned++;
                if (Spawned >= StartWave * MS.Count) {
                    AllowSpawn = false;
                    StartWave++;
                    Spawned = 0;
                }                
                s.Spawn(DynamicSpawnedKey--,0);//Index 0 for now
            }
            SpawnTimer = 0;
        }
    }

    void Waiting() {                
        if(WaitTimer == 0) {
            TopNotification.Push((WaitInterval).ToString("F0"), MyColor.Yellow,WaitInterval);
        }
        if (WaitTimer - LastWaitTimer >= 1) {            
            TopNotification.Message = (WaitInterval - WaitTimer).ToString("F0");
            LastWaitTimer = WaitTimer;          
        }
        if (WaitTimer < WaitInterval) {
            WaitTimer += Time.fixedDeltaTime;
        } else {
            TopNotification.Push("Something are coming!", MyColor.Orange, 2f);
            WaitTimer = 0;
            LastWaitTimer = 0;
            AllowWait = false;
            AllowSpawn = true;
            //Door.Close(); //Not for now                    
        }
    }

    public override SceneCoordinate GetCloestestRespawnSC(Vector2 Position) {
        return new SceneCoordinate(SceneID.FrankensteinCastle,new Vector2( 2, -3.7f));
    }

    public override void OnPlayerLeft(Player PC) {//Scale down difficulty
        base.OnPlayerLeft(PC);
        if (PC.Alive) {//No Raven in this scene
            OursAlives--;
        }
        if (OursAlives == 0) {
            DeathCoroutine = StartCoroutine(Reset());
        }
    }

    public override void ApplyEvents(ObjectController OC) {
        if (OC is Player) {
            OC.ON_DEATH_UPDATE += PlayerOnDeathEvent;
            if (!RegisteredPlayers.Contains((Player)OC)) {
                if (OC.Alive)
                    OursAlives++;
                RegisteredPlayers.Add((Player)OC);
            }
        }
        else if (OC.OID == OID.EnemyMonster) {
            if (OC.GetName() == "Frankenstein's Phlegm")//Terminator
                OC.ON_DEATH_UPDATE += BossOnDeathEvent;
            else {
                ((Monster)OC).LootDrop = false;
                OC.ON_DEATH_UPDATE += MobOnDeathEvent;
            }
        }
    }

    void PlayerOnDeathEvent(Damage dmg) {
        OursAlives--;        
        if (OursAlives == 0) {
            DeathCoroutine = StartCoroutine(Reset());
        }
    }

    void BossOnDeathEvent(Damage dmg) {
        GreedyCamera.SlowMotionEffect(CacheManager.GetOC(dmg.targetID).Position);
        Ended = true;
        Portals[0].gameObject.SetActive(true);
        CacheManager.MP.Objective.Set(Portals[0].transform);
    }

    void MobOnDeathEvent(Damage dmg) {
        SlimeAlives--;
        if (SlimeAlives==0 && !BossFighting) {
            if (StartWave == BossWave && !BossFighting) {
                AllowWait = false;
                AllowSpawn = false;
                BossFighting = true;                
                BS[0].Spawn(DynamicSpawnedKey--, 0);                
                HealPlayer();
                CacheManager.MP.Objective.Set(BS[0].transform);
                return;
            }
            AllowWait = true;
            LootSpawner.SpawnLoots(Player.MinLvl);
            StartCoroutine(YieldSetAlives());
            HealPlayer();
            CacheManager.MP.Objective.Set(BS[0].transform);
        }
    }private IEnumerator YieldSetAlives() {
        yield return new WaitForSeconds(WaitInterval/2);
        SlimeAlives = StartWave * 3;
    }

    void HealPlayer() {
        if (Client.Connected) {            
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.HealingBuff,0, Client.ClientID, 5f, CombatGenerator.GenerateEffectParameters(CacheManager.MP.MaxHealth * 0.05f)), Client.TCP);
        }
        else
            CacheManager.Effects[CR.HealingBuff].Apply(null, CacheManager.MP, 5f, CombatGenerator.GenerateEffectParameters(CacheManager.MP.MaxHealth * 0.05f));
    }

    IEnumerator Reset() {
        TopNotification.Push("Your soul is fading...", MyColor.Red, 3f);        
        yield return new WaitForSeconds(5f);
        CacheManager.WipeDymanics();
        Scene.Load(Scene.Current_SID);        
    }
}

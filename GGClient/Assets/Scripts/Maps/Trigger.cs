﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour {

    public bool Triggered = false;

    private void Start() {
        
    }    
    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == Tag.FriendlyPlayer) {
            Triggered = true;            
        }
    }
    public void Disable() {
        gameObject.SetActive(false);
    }
}

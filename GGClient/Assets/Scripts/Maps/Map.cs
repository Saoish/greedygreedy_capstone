﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GreedyNameSpace;
using Networking.Data;

public abstract class Map : MonoBehaviour {
    [HideInInspector]
    public int OursAlives = 0;
    [HideInInspector]
    public int OpponentsAlives = 0;

    public List<Portal> Portals;
    public List<MonsterSpawner> MS;
    public List<MonsterSpawner> BS;
    

    protected Coroutine DeathCoroutine=null;

    protected List<Player> RegisteredPlayers = new List<Player>();

    protected virtual void Awake() {
        CacheManager.Map = this;        
    }

	// Use this for initialization
	protected virtual void Start () {
        
    }

    protected virtual void Update() {

    }
	
	// Update is called once per frame
	protected virtual void FixedUpdate () {
	
	}    

    public virtual void ResetPortals() {        
        foreach (Portal p in Portals)
            p.Reset();
        CacheManager.MP.MPI.InfuseGoInCD();
    }

    public virtual void ResetDeathCoroutine() {
        if (DeathCoroutine != null)
            StopCoroutine(DeathCoroutine);
    }

    public abstract SceneCoordinate GetCloestestRespawnSC(Vector2 Position);

    public virtual void OnPlayerLeft(Player PC) {
        if (PC.InfusedPortal != null)
            PC.InfusedPortal.Defuse(PC);
        RegisteredPlayers.Remove(PC);
    }

    public abstract void ApplyEvents(ObjectController OC);//This function will be called on every instantiated oj, also acts like an on join event   
}

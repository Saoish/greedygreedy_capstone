﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
    public GameObject Locker;
    public Trigger Trigger;
    // Use this for initialization
    void Start() {               
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool Locked {
        get { return Locker.active; }
    }

    public bool Triggered {
        get { return Trigger.Triggered; }
    }

    public void Close(){
        Locker.SetActive(true);
        //((FrankensteinCastle)MC).StartWait();//For now
    }

    public void Open() {
        Locker.SetActive(false);
    }
}

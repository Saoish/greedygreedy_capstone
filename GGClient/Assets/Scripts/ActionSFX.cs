﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ActionSFX{
    public static AudioClip EquipmentSelect {
        get { return Resources.Load<AudioClip>("ActionSFX/Button5"); }
    }

    public static AudioClip SkillLvlUp {
        get { return Resources.Load<AudioClip>("ActionSFX/Ancient_Game_Magic_Poof_Hit"); }
    }

    public static AudioClip MenuSelect {
        get { return Resources.Load<AudioClip>("ActionSFX/Button4"); }
    }
    
    public static AudioClip QueueUp {
        get { return Resources.Load<AudioClip>("ActionSFX/Bravery 3"); }
    }

    public static AudioClip QueuePop {
        get { return Resources.Load<AudioClip>("ActionSFX/Bravery 8"); }
    }

    public static AudioClip Respec {
        get { return Resources.Load<AudioClip>("ActionSFX/Ancient_Game_Black_Magic_Spell_2_with_Whoosh"); }
    }
}

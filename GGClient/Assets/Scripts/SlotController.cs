﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using GreedyNameSpace;
using UnityEngine.EventSystems;

using Networking.Data;
public class SlotController : MonoBehaviour {
    public Transform VisualHolder;

    int Slot;

    Text Name;
    Text LvlClass;

    GameObject PlayButtonObject;
    GameObject DeleteButtonObject;
    GameObject CreateButtonObject;
        
    PlayerData PlayerData;

    private GameObject BaseModel;

    void Awake() {
        Slot = int.Parse(gameObject.name);
        Name = transform.Find("NameText").GetComponent<Text>();
        LvlClass = transform.Find("LvlClassText").GetComponent<Text>();

        PlayButtonObject = transform.Find("PlayButton").gameObject;
        DeleteButtonObject = transform.Find("DeleteButton").gameObject;
        CreateButtonObject = transform.Find("CreateButton").gameObject;

        PlayerData = CacheManager.UserData.PlayerDatas[Slot];
    }
            
    // Use this for initialization
	void Start () {
        LoadSlotData();        
    }
	
	// Update is called once per frame
	void Update () {
    }

    public void PlayButtonOnClick() {
        CacheManager.CPS = Slot;
        PlayerData PD_ToUse = CacheManager.GetLocalPlayerData(CacheManager.CPS);        
        if(SceneChecker.TargetInUnLogables(PD_ToUse.SC.SID)){
            PD_ToUse.SC = new SceneCoordinate(SceneID.Tyneham, Vector2.zero);
        }
        CacheManager.CacheWorldStates();
        CacheManager.AddPlayer(Client.ClientID, PD_ToUse);//Cache Main Player Data        
        PopUpNotification.Push("Waiting for server...");
        Scene.LoadWithManualActive(PD_ToUse.SC.SID);
        Client.Send(Protocols.SubscribeIdentityAndInstance,Slot, Client.TCP);

        TutorialClip.TurnOn();
    }

    public void CreateButtonOnClick() {
        CacheManager.CPS = Slot;
        Scene.Load(SceneID.CharacterCreation);  
    }

    public void DeleteButtonOnClick() {
        PopUpNotification.Push("Are you sure?", PopUpNotification.Type.Select);
        PopUpNotification.HereWeGo(ConfirmDeletetion);
    }

    void LoadSlotData() {
        if(PlayerData.Name == "") {
            Name.text = LvlClass.text = "";
            PlayButtonObject.SetActive(false);
            DeleteButtonObject.SetActive(false);
            CreateButtonObject.SetActive(true);
        } else {
            Name.text = PlayerData.Name;
            LvlClass.text = "lvl " + PlayerData.lvl + " " + PlayerData.Class;
            CreateButtonObject.SetActive(false);
            InstaniateEquipment();
        }     
        
        if(Slot==0) {
            EventSystem.current.SetSelectedGameObject(null);
            if (PlayButtonObject.active) {                
                EventSystem.current.SetSelectedGameObject(PlayButtonObject);
            }else if(CreateButtonObject.active)
                EventSystem.current.SetSelectedGameObject(CreateButtonObject);
        }
    }

    void ConfirmDeletetion() {
        Client.Send(Protocols.DeleteCharacter, Slot, Client.TCP);
        PopUpNotification.Push("Waiting for server...");
    }

    //IEnumerator WaitFroDeleteDecision() {
    //    PopUpNotification.Push("Are you sure?", PopUpNotification.Type.Select);
    //    yield return PopUpNotification.WaitForDecision();
    //    if (PopUpNotification.Decision) {            
    //        Client.Send(Protocols.DeleteCharacter, Slot, Client.TCP);
    //        PopUpNotification.Push("Waiting for server...");
    //    }
    //}






























    void InstaniateEquipment() {
        BaseModel = Instantiate(Resources.Load("BaseModelPrefabs/BaseModel"), VisualHolder) as GameObject;
        BaseModel.name = "BaseModel";
        BaseModel.GetComponent<SpriteRenderer>().color = new Color(PlayerData.SkinColor.R, PlayerData.SkinColor.G, PlayerData.SkinColor.B);
        BaseModel.transform.localPosition = Vector3.zero;
        foreach (var e in PlayerData.Equipments) {
            if (e.Name!= "") {
                GameObject equipPrefab = EquipmentController.ObtainPrefabForCharacterSelection(e, VisualHolder);
            }
        }
    }
}
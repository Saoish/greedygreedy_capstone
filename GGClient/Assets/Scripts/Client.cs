﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using GreedyNameSpace;
using Networking.Data;

public class Client : MonoBehaviour {
    public static bool Dominating = false;

    public static int ping = 0;
    public bool Host = false;
    public enum Mode {
        Local,
        Public
    }
    public static Mode ConnectMode = Mode.Public;

    public class LateAction {
        public Action<Decipher> Protocol;
        public Decipher decipher;
        public LateAction(Action<Decipher> Protocol,Decipher decipher) {
            this.Protocol = Protocol;
            this.decipher = decipher;
        }
    }
    public static List<LateAction> LateActions = new List<LateAction>();//For most updated state changes upon joining state, it's empty in most cases

    Dictionary<string, Action<Decipher>> _Protocols = new Dictionary<string, Action<Decipher>>();
    void SetUp_Protocols() {
        //Unique Secion
        _Protocols[Protocols.Ping] = Ping;
        _Protocols[Protocols.Identify] = Identify;
        _Protocols[Protocols.CreateUsername] = CreateUsername;
        _Protocols[Protocols.PopUpNotify] = PopUpNotify;
        _Protocols[Protocols.TopNotify] = TopNotify;        
        _Protocols[Protocols.DisconnectByServer] = DisconnectByServer;
        _Protocols[Protocols.LoadUserData] = LoadUserData;
        _Protocols[Protocols.LoadGraveState] = LoadGraveState;
        
        _Protocols[Protocols.LoadSceneWithSync] = LoadSceneWithSync;
        _Protocols[Protocols.SyncLoadedScene] = SyncLoadedScene;        

        _Protocols[Protocols.PopQueueEntrance] = PopQueueEntrance;
        _Protocols[Protocols.SummonRequest] = SummonRequest;

        _Protocols[Protocols.LogOffPlayer] = LogOffPlayer;
        _Protocols[Protocols.SetDominateState] = SetDominateState;
        _Protocols[Protocols.PresetMPSCThenUploadStates] = PresetMPSCThenUploadStates;

        _Protocols[Protocols.UploadStates] = UploadStates;
        _Protocols[Protocols.LoadJoiningStates] = LoadJoiningStates;
        _Protocols[Protocols.AddPlayer] = AddPlayer;

        //Duplicated Section        
        _Protocols[Protocols.LevelUp] = LevelUp;

        _Protocols[Protocols.EquipAction] = EquipAction;
        _Protocols[Protocols.UnEquipAction] = UnEquipAction;
        _Protocols[Protocols.LvlUpSkillAction] = LvlUpSkillAction;
        _Protocols[Protocols.Respec] = Respec;

        _Protocols[Protocols.UpdateObjectMoveState] = UpdateObjectMoveState;
        _Protocols[Protocols.UpdateObjectAttackState] = UpdateObjectAttackState;
        _Protocols[Protocols.UpdateObjectDirection] = UpdateObjectDirection;
        _Protocols[Protocols.UpdateObjectPosition] = UpdateObjectPosition;
        _Protocols[Protocols.UpdateObjctDeadPosition] = UpdateObjctDeadPosition;

        _Protocols[Protocols.ObjectOnHealthGain] = ObjectOnHealthGain;
        _Protocols[Protocols.ObjectOnHealthLoss] = ObjectOnHealthLoss;
        _Protocols[Protocols.ObjectOnDeath] = ObjectOnDeath;
        _Protocols[Protocols.ReviveUponHelp] = ReviveUponHelp;

        _Protocols[Protocols.ObjectStartCasting] = ObjectStartCasting;
        _Protocols[Protocols.ObjectInterruptCasting] = ObjectInterruptCasting;

        _Protocols[Protocols.ObjectActiveSkill] = ObjectActiveSkill;
        _Protocols[Protocols.ObjectDeactiveSkill] = ObjectDeactiveSkill;
        _Protocols[Protocols.ObjectAddForce] = ObjectAddForce;        
        _Protocols[Protocols.ObjectAddEffect] = ObjectAddEffect;        
        _Protocols[Protocols.Launch] = Launch;

        _Protocols[Protocols.ObjectUpdateTarget] = ObjectUpdateTarget;

        _Protocols[Protocols.AddGrave] = AddGrave;
        _Protocols[Protocols.RemoveGrave] = RemoveGrave;

        _Protocols[Protocols.Infusion] = Infusion;
        _Protocols[Protocols.Defusion] = Defusion;        

        _Protocols[Protocols.ListeningForPacakage] = ListeningForPacakage;
    }

    private static Package Package;
    public static bool Connected;

    const int ServerID = 0;//Always 0, server's identity    
    public static int ClientID = 0; //Fetch from server, for network identity, 0 is not identified

    private static int connectionID;//Always be 1, and for local identity

    public static int TCP, UDP;
    public static int PackageChannel;
    static int maxConnections = 1;//Always be 1 for client

    static int clientSocket;
    
    static string public_server_ip = "127.0.0.1";

    public static string ip = String.Empty;
    static int port = 34758;

    public static Client instance;

    //---The following part for LAN hole punching
    //int BroadcastKey = 2222;
    //int BroadcastVersion = 22;
    //int BroadcastSubVersion = 33;    
    //void SetupCredentials() {
    //    byte error;
    //    NetworkTransport.SetBroadcastCredentials(clientSocket, BroadcastKey, BroadcastVersion, BroadcastSubVersion, out error);            
    //}
    //void StopBroadCasting() {
    //    NetworkTransport.StopBroadcastDiscovery();
    //}
    //---The following part for LAN hole punching      

    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        } else {
            SetUp_Protocols();            
            ObjectMapper.Load();
            instance = this;
            DontDestroyOnLoad(this);
        }
        UsernameManager.LoadUserName();
    }

    public static void FetchIP(Mode passed_CO) {
        ConnectMode = passed_CO;
        switch (passed_CO) {
            case Mode.Local:
                ip = string.Empty;
                FindObjectOfType<IPFetcher>().UpdateText();
                break;
            //case ConnectOptions.Host:
            //    ip = Network.player.ipAddress;
            //    FindObjectOfType<IPFetcher>().UpdateText();
            //    break;
            case Mode.Public:
                ip = public_server_ip;
                //FindObjectOfType<IPFetcher>().UpdateText();
                break;
        }
    }

    // Use this for initialization
    void Start() {
        NetworkTransport.Init();
        ConnectionConfig config = new ConnectionConfig();
        TCP = config.AddChannel(QosType.Reliable);
        UDP = config.AddChannel(QosType.Unreliable);
        PackageChannel = config.AddChannel(QosType.ReliableSequenced);
        HostTopology topology = new HostTopology(config, maxConnections);
        if (Scene.Current_SID != SceneID.StartMenu) {
            clientSocket = NetworkTransport.AddHost(topology);
        }
        else if (Host) {
            //FindObjectOfType<ConnectOptions>().gameObject.SetActive(false);
            clientSocket = NetworkTransport.AddHost(topology);
            ip = Network.player.ipAddress;
        }
        else {
            clientSocket = NetworkTransport.AddHost(topology, port);
            ip = public_server_ip;
        }

        //SetupCredentials();        
    }

    // Update is called once per frame
    void Update() {
        Process();
    }

    public static void ExecuteLateActions() {//Very edged case, but it happened
        if (LateActions.Count == 0)
            return;        
        foreach(var LA in LateActions) {            
            LA.Protocol(LA.decipher);
        }
        LateActions.Clear();
    }
    public static void WipeLateActions() {
        LateActions.Clear();
    }

    public static void ShutDown() {
        NetworkTransport.Shutdown();
    }

    public static void Connect() {
        if (Connected || ip == string.Empty)
            return;
        //SetUpSocket();
        byte error;
        Debug.Log(ip);
        connectionID = NetworkTransport.Connect(clientSocket, ip, port, 0, out error);
    }

    static void _Send(byte[] binary_data, int channel = 0) {//Default by TCP channel
        byte error;
        if (binary_data.Length > 1024) {
            Debug.Log("Not sent, this package contain size of MTU.");
        }
        NetworkTransport.Send(clientSocket, connectionID, channel, binary_data, binary_data.Length, out error);
    }

    void Process() {
        //int HostID = 0;
        //int ClientID;
        //int rec_hostID = 0;
        int rec_connectionID;
        int rec_channelID;
        byte[] buffer = new byte[1024];
        int buffer_length;
        byte error;
        NetworkEventType networkEvent = NetworkEventType.DataEvent;
        do {
            //networkEvent = NetworkTransport.Receive(out rec_hostID, out rec_connectionID, out rec_channelID, buffer, 1024, out buffer_length, out error); //UDP broadcasting disable
            networkEvent = NetworkTransport.ReceiveFromHost(ServerID, out rec_connectionID, out rec_channelID, buffer, 1024, out buffer_length, out error); //UDP broddcasting capable
            switch (networkEvent) {
                case NetworkEventType.ConnectEvent:// Server received connect event    
                    Connected = true;
                    Debug.Log("Connected.");
                    break;
                case NetworkEventType.DataEvent:
                    if (rec_channelID != PackageChannel) {
                        Decipher d = Serializer.UnSeal(ServerID, buffer);
                        _Protocols[d.protocol](d);
                    }
                    else {
                        AppendBuffer(buffer, buffer_length);
                    }
                    break;
                case NetworkEventType.DisconnectEvent:// Client received disconnect event
                    Connected = false;
                    Dominating = false;
                    if (error == 6) {//Server down or disconnected   
                        if(Scene.Current_SID != SceneID.StartMenu)
                            Scene.LoadWithAction(SceneID.StartMenu, PopUpNotification.Push, "Disconnected from server.", PopUpNotification.Type.Confirm, 0f);
                        //StartCoroutine(Scene.LoadThenExecute(SceneID.StartMenu, PopUpNotification.Push, "Disconnected from server.",PopUpNotification.Type.Confirm, 0f));
                    }
                    Debug.Log("Disconnected.");
                    ClientID = 0;
                    CacheManager.WipeAll();
                    break;
                    //case NetworkEventType.BroadcastEvent://This block should be removed for network version                           
                    //    if (Connected || ip != String.Empty)
                    //        return;
                    //    NetworkTransport.GetBroadcastConnectionMessage(HostID, buffer, 1024, out buffer_length, out error);
                    //    ip = Serializer.DeSerialize<string>(buffer);
                    //    FindObjectOfType<IPFetcher>().UpdateText();
                    //    //RootButtons.cg.interactable = true;
                    //    break;
            }
        } while (networkEvent != NetworkEventType.Nothing);
    }

    private T Get<T>(string content) {
        return JsonUtility.FromJson<T>(content);
    }

    //Pacakage control
    private void AppendBuffer(byte[] buffer, int dataSize) {
        System.Buffer.BlockCopy(buffer, 0, Package.buffer, Package.size, dataSize);
        Package.size += dataSize;
        if (Package.size == Package.length) {
            _Protocols[Package.decipher.protocol](Package.decipher);
        }
    }

    public static void SendProtocol(string Protocol, int Channel) {
        byte[] data = Serializer.Seal(Protocol);
        _Send(data, Channel);
    }

    public static void Send<T>(string Protocol, T instance, int Channel) {
        byte[] data = Serializer.Seal(Protocol, instance);
        _Send(data, Channel);
    }

    public static void SendPacakage<T>(T instance, Decipher decipher) {
        byte[] buffer = Serializer.Serialize(instance);
        byte[] listen_event = Serializer.Seal(Protocols.ListeningForPacakage, new Package(buffer.Length, decipher));
        _Send(listen_event);
        int round = Mathf.FloorToInt(buffer.Length / 1024);
        for (int i = 0; i < round; i++) {
            byte[] transmit = new byte[1024];
            System.Buffer.BlockCopy(buffer, 1024 * i, transmit, 0, 1024);
            _Send(transmit, PackageChannel);
        }
        int remain = buffer.Length - 1024 * round;
        if (remain > 0) {
            byte[] transmit = new byte[remain];
            System.Buffer.BlockCopy(buffer, 1024 * round, transmit, 0, remain);
            _Send(transmit, PackageChannel);
        }
    }

    public static void ManuallyDisconnectSelf() {
        byte error;
        NetworkTransport.Disconnect(ServerID, connectionID, out error);
        ClientID = -1;
    }

    //Client Protocols
    private void Ping(Decipher decipher) {
        byte error;
        int ServerTimeStamp = int.Parse(decipher.content);
        ping = NetworkTransport.GetRemoteDelayTimeMS(ServerID, connectionID, ServerTimeStamp, out error);
        //Debug.Log("ping: " + ping);
    }

    private void Identify(Decipher decipher) {//Any instance based action required Identify self first, and this will be done by server once login
        ClientID = int.Parse(decipher.content);
    }

    private void CreateUsername(Decipher decipher) {
        UsernameManager.SaveUsername(decipher.content);
        PopUpNotification.Push("Successfully registered.", PopUpNotification.Type.Confirm);
    }

    private void PopUpNotify(Decipher decipher) {
        PopUpNotification.Push(decipher.content, PopUpNotification.Type.Confirm);
    }

    private void TopNotify(Decipher decipher) {
        TopNotifyData temp = JsonUtility.FromJson<TopNotifyData>(decipher.content);        
        TopNotification.Push(temp.message, temp.color, temp.period);
    }

    private void DisconnectByServer(Decipher decipher) {
        byte error;
        NetworkTransport.Disconnect(decipher.tail, connectionID, out error);
        ClientID = 0;
        if (decipher.content != string.Empty)
            PopUpNotification.Push(decipher.content, PopUpNotification.Type.Confirm);
    }

    private void LoadSceneWithSync(Decipher decipher) {
        SceneID SID_ToLoad = Get<SceneID>(decipher.content);
        CacheManager.MP_Data.SC.SID = SID_ToLoad;
        Scene.LoadWithManualActive(SID_ToLoad);
    }

    private void SyncLoadedScene(Decipher decipher) {
        Scene.ActiveScene();
    }

    private void PopQueueEntrance(Decipher decipher) {
        PopUpNotification.Push("Your queue is ready, do you want to join?", PopUpNotification.Type.Select, 10f);        
        PopUpNotification.HereWeGo(AcceptEntrance,DenyEntrance);
        AudioSource.PlayClipAtPoint(ActionSFX.QueuePop, MainPlayer.V3_Pos, GameManager.SFX_Volume);
    }private void AcceptEntrance() {
        SendProtocol(Protocols.AcceptMatchMakingEntrance, TCP);
    }private void DenyEntrance() {
        SendProtocol(Protocols.DenyMatchMakingEntrance, TCP);
        MainPlayer.DropQueue();
    }

    private void SummonRequest(Decipher decipher) {
        SummonRequestData temp = Get<SummonRequestData>(decipher.content);
        PopUpNotification.Push("Do you want to accept the summon request from "+ MyText.Colofied(temp.RequesterName, MyText.green) + "?", PopUpNotification.Type.Select, 10f);
        PopUpNotification.HereWeGo(AcceptSummonRequest,temp.LK);
    }private void AcceptSummonRequest(LinkerKey LK) {        
        Send(Protocols.AcceptSummonRequest,LK, TCP);
    }

    private void LoadUserData(Decipher decipher) {
        CacheManager.LoadUserData(Serializer.DeSerialize<UserData>(Package.buffer));
        SendProtocol(Protocols.RequestGraveState, TCP);
    }

    private void LoadGraveState(Decipher decipher) {
        CacheManager.LoadGraveState(Serializer.DeSerialize<Dictionary<int, GraveData>>(Package.buffer));
        Scene.Load(SceneID.CharacterSelection);
    }

    private void LogOffPlayer(Decipher decipher) {
        int TargetNetworkID = int.Parse(decipher.content);
        CacheManager.LogOffPlayer(TargetNetworkID);
    }

    private void SetDominateState(Decipher decipher) {
        Dominating = bool.Parse(decipher.content);
    }

    private void PresetMPSCThenUploadStates(Decipher decipher) {
        Dominating = true;
        CacheManager.MP_Data.SC = Get<SceneCoordinate>(decipher.content);
        SendPacakage(CacheManager.States, new Decipher(Protocols.LoadInstanceState, null, ClientID));
    }

    private void UploadStates(Decipher decipher) {
        Dominating = true;
        SendPacakage(CacheManager.States, new Decipher(Protocols.LoadInstanceState, null, ClientID));        
    }

    private void LoadJoiningStates(Decipher decipher) {        
        CacheManager.LoadJoiningStates(Serializer.DeSerialize<Dictionary<int,ObjectData>>(Package.buffer));
        SendProtocol(Protocols.AddLoadedClient, TCP);
        bool ActiveOnceJoined = bool.Parse(decipher.content);        
        if (ActiveOnceJoined) {
            Scene.ActiveScene(false);            
        }
    }

    private void AddPlayer(Decipher decipher) {        
        CacheManager.AddPlayer(decipher.tail, Serializer.DeSerialize<PlayerData>(Package.buffer));        
        SendProtocol(Protocols.AddLoadedClient, TCP);
    }

    //Duplicated Actions
    private void LevelUp(Decipher decipher) {//*** LateActions Subscribtion required
        int TargetNetworkID = int.Parse(decipher.content);
        if (CacheManager.GetOC(TargetNetworkID)) {
            ((Player)CacheManager.GetOC(TargetNetworkID)).LevelUp();
        }
        else {
            LateActions.Add(new LateAction(LevelUp, decipher));
        }
    }

    private void EquipAction(Decipher decipher) {//*** LateActions Subscribtion required
        EquipActionData temp = JsonUtility.FromJson<EquipActionData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {
            ((Player)CacheManager.GetOC(temp.TargetNetworkID)).Equip(temp.E);
        }
        else {
            LateActions.Add(new LateAction(EquipAction, decipher));
        }
    }

    private void UnEquipAction(Decipher decipher) {//*** LateActions Subscribtion required
        UnEquipActionData temp = JsonUtility.FromJson<UnEquipActionData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID))
            ((Player)CacheManager.GetOC(temp.TargetNetworkID)).UnEquip((EQUIPTYPE)temp.Slot);
        else {
            LateActions.Add(new LateAction(UnEquipAction, decipher));
        }
    }

    private void LvlUpSkillAction(Decipher decipher) {//*** LateActions Subscribtion required
        LvlUpSkillData temp = JsonUtility.FromJson<LvlUpSkillData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID))
            ((Player)CacheManager.GetOC(temp.TargetNetworkID)).LvlUpSkill(temp.SkillIndex);
        else {
            LateActions.Add(new LateAction(LvlUpSkillAction, decipher));
        }
    }    

    private void Respec(Decipher decipher) {//*** LateActions Subscription required
        if (CacheManager.GetOC(decipher.tail))
            ((Player)CacheManager.GetOC(decipher.tail)).Respec();
        else
            LateActions.Add(new LateAction(Respec, decipher));
    }

    private void UpdateObjectMoveState(Decipher decipher) {
        MovementData temp = JsonUtility.FromJson<MovementData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {
            if (CacheManager.GetOC(temp.TargetNetworkID).OID == OID.Main) {//Local authority
                if (!ControllerManager.Moving && temp.Moving)
                    return;
            }            
            CacheManager.GetOC(temp.TargetNetworkID).Moving = temp.Moving;
            CacheManager.GetOC(temp.TargetNetworkID).Direction = temp.Direction;
        }
    }

    private void UpdateObjectAttackState(Decipher decipher) {
        AttackData temp = JsonUtility.FromJson<AttackData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {            
            CacheManager.GetOC(temp.TargetNetworkID).Attacking = temp.Attacking;
            CacheManager.GetOC(temp.TargetNetworkID).Direction = temp.Direction;
        }
    }

    private void UpdateObjectDirection(Decipher decipher) {
        DirectionData temp = JsonUtility.FromJson<DirectionData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {
            CacheManager.GetOC(temp.TargetNetworkID).Direction = temp.Direction;
        }
    }   

    private void UpdateObjectPosition(Decipher decipher) {
        PositionData temp = JsonUtility.FromJson<PositionData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {
            if (!CacheManager.GetOC(temp.TargetNetworkID).HasForce) {
                if (temp.TargetNetworkID > 0) {//Player
                    //((OtherPlayer)CacheManager.GetOC(temp.TargetNetworkID)).Lerp(temp.Position);
                    //((OtherPlayer)CacheManager.GetOC(temp.TargetNetworkID)).NetworkPosition = temp.Position;
                    ((OtherPlayer)CacheManager.GetOC(temp.TargetNetworkID)).AssignNetworkPosition(temp);
                    //GreedyPhysics.StartLerpObjectToPosition(CacheManager.GetOC(temp.TargetNetworkID), temp.Position);
                }
                else {
                    if (Vector2.Distance(CacheManager.GetOC(temp.TargetNetworkID).Position, temp.Position) > 0.3f) {
                        GreedyPhysics.StartLerpObjectToPosition(CacheManager.GetOC(temp.TargetNetworkID), temp.Position);
                    }
                }                    
            }
        }
    }

    private void UpdateObjctDeadPosition(Decipher decipher) {//*** LateActions Subscribtion required        
        PositionData temp = JsonUtility.FromJson<PositionData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {//Hard Sync            
            CacheManager.GetOC(temp.TargetNetworkID).Position = temp.Position;
            CacheManager.GetOC(temp.TargetNetworkID).Direction = temp.Direction;
        }
        else {
            LateActions.Add(new LateAction(UpdateObjctDeadPosition, decipher));
        }
    }

    private void ObjectOnHealthGain(Decipher decipher) {//*** LateActions Subscribtion required
        Heal heal = JsonUtility.FromJson<Heal>(decipher.content);
        if (CacheManager.GetOC(heal.targetID)) {
            CacheManager.GetOC(heal.targetID).ON_HEALTH_GAIN += CacheManager.GetOC(heal.targetID).HealHP;
            CacheManager.GetOC(heal.targetID).ON_HEALTH_GAIN(heal);
            CacheManager.GetOC(heal.targetID).ON_HEALTH_GAIN -= CacheManager.GetOC(heal.targetID).HealHP;
        }
        else {
            LateActions.Add(new LateAction(ObjectOnHealthGain, decipher));
        }
    }

    private void ObjectOnHealthLoss(Decipher decipher) {//*** LateActions Subscribtion required
        Damage dmg = JsonUtility.FromJson<Damage>(decipher.content);
        if (CacheManager.GetOC(dmg.targetID)) {
            CacheManager.GetOC(dmg.targetID).ON_HEALTH_LOSS += CacheManager.GetOC(dmg.targetID).DeductHealth;
            CacheManager.GetOC(dmg.targetID).ON_HEALTH_LOSS(dmg);
            CacheManager.GetOC(dmg.targetID).ON_HEALTH_LOSS -= CacheManager.GetOC(dmg.targetID).DeductHealth;
        }
        else {
            LateActions.Add(new LateAction(ObjectOnHealthLoss, decipher));
        }
    }

    private void ObjectOnDeath(Decipher decipher) {//*** LateActions Subscribtion required
        Damage temp = Get<Damage>(decipher.content);
        if (CacheManager.GetOC(temp.targetID)) {
            if (!CacheManager.GetOC(temp.targetID).Alive)
                return;
            CacheManager.GetOC(temp.targetID).ON_DEATH_UPDATE += CacheManager.GetOC(temp.targetID).Die;
            CacheManager.GetOC(temp.targetID).ON_DEATH_UPDATE(temp);
            CacheManager.GetOC(temp.targetID).ON_DEATH_UPDATE -= CacheManager.GetOC(temp.targetID).Die;
        }
        else {
            LateActions.Add(new LateAction(ObjectOnDeath, decipher));
        }
    }

    private void ReviveUponHelp(Decipher decipher) {//*** LateActions Subscribtion required
        int TargetNetworkID = int.Parse(decipher.content);
        if (CacheManager.GetPC(TargetNetworkID)) {
            if (CacheManager.GetPC(TargetNetworkID).Alive)
                return;
            CacheManager.GetPC(TargetNetworkID).ReviveUponHelp();
        }
        else {
            LateActions.Add(new LateAction(ReviveUponHelp, decipher));
        }
    }

    private void ObjectStartCasting(Decipher decipher) {
        StartCastingData temp = JsonUtility.FromJson<StartCastingData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {
            if (temp.TargetNetworkID > 0) {
                ((OtherPlayer)CacheManager.GetOC(temp.TargetNetworkID)).AssignNetworkPosition(temp.TargetPosition);
            }
            else {
                GreedyPhysics.StartLerpObjectToPosition(CacheManager.GetOC(temp.TargetNetworkID), temp.TargetPosition);
            }
            CacheManager.GetOC(temp.TargetNetworkID).Casting = true;
            CacheManager.GetOC(temp.TargetNetworkID).ActiveOutsideVFXPartical(Resources.Load<GameObject>("VFXPrefabs/StartCasting"));
        }
    }

    private void ObjectInterruptCasting(Decipher decipher) {
        int TargetNetworkID = int.Parse(decipher.content);
        if (CacheManager.GetOC(TargetNetworkID)) {
            CacheManager.GetOC(TargetNetworkID).Casting = false;
        }
    }

    private void ObjectActiveSkill(Decipher decipher) {
        SkillActivationData temp = JsonUtility.FromJson<SkillActivationData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {
            CacheManager.GetOC(temp.TargetNetworkID).Skills[temp.Type].TriggerByNetwork(temp.Paramaters);
        }
    }

    private void ObjectDeactiveSkill(Decipher decipher) {
        SkillDeactivationData temp = Get<SkillDeactivationData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {
            CacheManager.GetOC(temp.TargetNetworkID).Skills[temp.Type].DeactiveByNetwork(temp.TimeOut);
        }
    }

    private void ObjectAddForce(Decipher decipher) {//*** LateActions Subscribtion required
        AddForceData temp = JsonUtility.FromJson<AddForceData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {
            CacheManager.GetOC(temp.TargetNetworkID).Position = temp.StartPosition;
            CacheManager.GetOC(temp.TargetNetworkID).AddForce(temp.Force, temp.ForceMode);
        }
        else {
            LateActions.Add(new LateAction(ObjectAddForce, decipher));
        }
    }

    private void ObjectAddEffect(Decipher decipher) {//*** LateActions Subscribtion required
        AddEffectData temp = JsonUtility.FromJson<AddEffectData>(decipher.content);
        if (CacheManager.GetOC(temp.TargetNetworkID)) {//Both exist
            if(CacheManager.GetOC(temp.TargetNetworkID).Alive)
                CacheManager.ApplyEffect(temp);
        }
        else {
            LateActions.Add(new LateAction(ObjectAddEffect, decipher));
        }
    }

    private void Launch(Decipher decipher) {
        ProjectileData pd = Get<ProjectileData>(decipher.content);
        CacheManager.Launch(pd);
    }

    private void ObjectUpdateTarget(Decipher decipher) {//*** LateActions Subscribtion required
        UpdateTargetData temp = Get<UpdateTargetData>(decipher.content);        
        CacheManager.GetObjectData(temp.TargetNetworkID).Target = temp.SetTargetNetworkID;
        Monster MC = (Monster)CacheManager.GetOC(temp.TargetNetworkID);
        ObjectController TC = CacheManager.GetOC(temp.SetTargetNetworkID);
        if (MC) {
            MC.AI.Target = CacheManager.GetOC(temp.SetTargetNetworkID);
        }
        else {
            LateActions.Add(new LateAction(ObjectUpdateTarget, decipher));
        }
    }

    private void AddGrave(Decipher decipher) {
        CacheManager.AddGrave(Get<GraveData>(decipher.content));
    }

    private void RemoveGrave(Decipher decipher) {
        int OwnerNetworkID = int.Parse(decipher.content);
        CacheManager.RemoveGrave(OwnerNetworkID);
    }

    private void Infusion(Decipher decipher) {
        FusionData temp = Get<FusionData>(decipher.content);                       
        CacheManager.Map.Portals[temp.PortalIdentity].Infuse(((Player)CacheManager.GetOC(temp.PlayerNetworkID)));
    }

    private void Defusion(Decipher decipher) {
        FusionData temp = Get<FusionData>(decipher.content);
        CacheManager.Map.Portals[temp.PortalIdentity].Defuse(((Player)CacheManager.GetOC(temp.PlayerNetworkID)));
    }

    //private void SpawnMonster(Decipher decipher) {
    //    SpawnMonsterData temp = Get<SpawnMonsterData>(decipher.content);        
    //    if (CacheManager.HasObjectKey(temp.AttemptKey))
    //        return;
    //    CacheManager.Map.MS[temp.Identity].SpawnByNetwork(temp.AttemptKey,temp.SpawnIndex,temp.SpawnLvl);
    //}

    //private void SpawnBoss(Decipher decipher) {
    //    SpawnMonsterData temp = Get<SpawnMonsterData>(decipher.content);
    //    if (CacheManager.HasObjectKey(temp.AttemptKey))
    //        return;
    //    CacheManager.Map.BS[temp.Identity].SpawnByNetwork(temp.AttemptKey, temp.SpawnIndex, temp.SpawnLvl);
    //}
























    private void ListeningForPacakage(Decipher decipher) {
        Package = JsonUtility.FromJson<Package>(decipher.content);
        Package.Initialize();
    }


}



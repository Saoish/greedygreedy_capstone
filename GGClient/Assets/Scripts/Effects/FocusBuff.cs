﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using System;

public class FocusBuff : Buff {
    public override CR Type { get { return CR.FocusBuff; } }

    public GameObject VFX;
    float ModAmount;
    float Haste_INC_Percentage;

    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        if (!target.HasEffect(CR.FocusBuff)) {
            GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
            Effect_OJ.name = Type.ToString();
            Effect_OJ.GetComponent <FocusBuff> ().Haste_INC_Percentage = float.Parse(ExtraParamaters[0]);
            Effect_OJ.GetComponent<FocusBuff>()._Apply(applyer, target, Duration);
        }
    }

    protected override void _Apply(ObjectController applyer, ObjectController target, float Duration) {
        base._Apply(applyer, target, Duration);
        ModAmount = Haste_INC_Percentage;//Straight add up
        //ModAmount = (float)System.Math.Round(target.GetMaxStats(STATSTYPE.HASTE) * (Haste_INC_Percentage / 100), 1);
        target.AddCurrStats(STATSTYPE.HASTE, ModAmount);
        target.ActiveVFXParticle(VFX);
    }

    public override void Expire() {
        target.DecCurrStats(STATSTYPE.HASTE, ModAmount);
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }
}

﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
public abstract class Effect : MonoBehaviour {
    public abstract CR Type { get; }
    [HideInInspector]
    public float Duration = 0;    
    protected ObjectController target;
    protected ObjectController applyer;

    protected virtual void FixedUpdate() {
        if (target == null)
            return;
        else {
            if (Duration > 0)
                Duration -= Time.fixedDeltaTime;
            else if (Duration <= 0)
                Expire();
        }
    }

    protected void Awake() {
        if (GetType().ToString() != Type.ToString()) {
            Debug.Log(GetType().ToString() + ": has non-matched CR.Type(" + Type.ToString() + ")");
        }
    }

    public abstract void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters);

    protected virtual void _Apply(ObjectController applyer, ObjectController target,float Duration) {//Any extra paramaters will be taken as string
        this.target = target;
        this.applyer = applyer;
        this.Duration = Duration;
        target.AddEffect(Type, this);
        gameObject.transform.SetParent(target.Effects_T());
        gameObject.transform.localPosition = Vector3.zero;
    }

    public virtual void Expire() {
        target.RemoveEffect(Type);
        DestroyObject(gameObject);
    }

    protected bool Responsible {
        get { return target.OID == OID.Main || (target is Monster && applyer is MainPlayer); }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;

public class RageBuff : Buff {
    public override CR Type { get { return CR.RageBuff; } }

    public GameObject VFX;

    float ModAmount;
    float Damage_INC_Percentage;

    void Awake() {

    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	protected override void FixedUpdate () {
        base.FixedUpdate();
	}

    //public static RageBuff Generate(float AD_INC_Percentage, float Duration) {
    //    GameObject RB_OJ = Instantiate(Resources.Load("BuffPrefabs/RageBuff")) as GameObject;
    //    RB_OJ.name = "RageBuff";
    //    RB_OJ.GetComponent<RageBuff>().Duration = Duration;
    //    RB_OJ.GetComponent<RageBuff>().Damage_INC_Percentage = AD_INC_Percentage;
    //    return RB_OJ.GetComponent<RageBuff>();
    //}

    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        if (!target.HasEffect(CR.RageBuff)) {
            GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
            Effect_OJ.name = Type.ToString();
            Effect_OJ.GetComponent<RageBuff>().Damage_INC_Percentage = float.Parse(ExtraParamaters[0]);
            Effect_OJ.GetComponent<RageBuff>()._Apply(applyer, target,Duration);
        }
    }

    protected override void _Apply(ObjectController applyer, ObjectController target,float Duration) {
        base._Apply(applyer, target, Duration);
        ModAmount = (float)System.Math.Round(target.GetMaxStats(STATSTYPE.DAMAGE) * (Damage_INC_Percentage / 100),0);
        target.AddCurrStats(STATSTYPE.DAMAGE,ModAmount);
        target.ActiveVFXParticle(VFX);
    }

    public override void Expire() {
        target.DecCurrStats(STATSTYPE.DAMAGE, ModAmount);
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }
}

﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;

public class BloodForBloodBuff : Buff {
    public override CR Type { get { return CR.BloodForBloodBuff; } }
    public GameObject VFX;

    float ModAmount;
    float LPH_INC_Percentage;

    //public static BloodForBloodBuff Generate(float LPH_INC_Percentage,float Duration) {
    //    GameObject BFBB_OJ = Instantiate(Resources.Load("BuffPrefabs/BloodForBloodBuff")) as GameObject;
    //    BFBB_OJ.name = "BloodForBloodBuff";
    //    BFBB_OJ.GetComponent<BloodForBloodBuff>().Duration = Duration;
    //    BFBB_OJ.GetComponent<BloodForBloodBuff>().LPH_INC_Percentage = LPH_INC_Percentage;
    //    return BFBB_OJ.GetComponent<BloodForBloodBuff>();
    //}

    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        if (!target.HasEffect(CR.BloodForBloodBuff)) {
            GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
            Effect_OJ.name = Type.ToString();
            Effect_OJ.GetComponent<BloodForBloodBuff>().LPH_INC_Percentage = float.Parse(ExtraParamaters[0]);
            Effect_OJ.GetComponent<BloodForBloodBuff>()._Apply(applyer, target, Duration);
        }
    }


    protected override void _Apply(ObjectController applyer, ObjectController target,float Duration) {
        base._Apply(applyer, target, Duration);
        //ModAmount = (float)System.Math.Round(target.GetMaxStats(STATSTYPE.LPH) * (LPH_INC_Percentage / 100),1);
        ModAmount = LPH_INC_Percentage;
        target.AddCurrStats(STATSTYPE.LPH, ModAmount);
        target.ActiveVFXParticle(VFX);
    }

    public override void Expire() {
        target.DecCurrStats(STATSTYPE.LPH, ModAmount);
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }

}

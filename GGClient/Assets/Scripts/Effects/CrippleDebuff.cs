﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GreedyNameSpace;

public class CrippleDebuff : Debuff {
    public override CR Type { get { return CR.CrippleDebuff; } }
    public GameObject VFX;
    public AudioClip SFX;
    private float ModAmount;    
    float DMG_DEC_Percentage;
    
    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        if (CombatChecker.IgnoreApplyingEffect(target, Type))
            return;
        GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
        Effect_OJ.name = Type.ToString();
        Effect_OJ.GetComponent<CrippleDebuff>().DMG_DEC_Percentage = float.Parse(ExtraParamaters[0]);
        Effect_OJ.GetComponent<CrippleDebuff>()._Apply(applyer, target, Duration);
    }

    protected override void _Apply(ObjectController applyer, ObjectController target,float Duration) {
        base._Apply(applyer, target, Duration);
        ModAmount = (float)System.Math.Round(target.GetMaxStats(STATSTYPE.DAMAGE) * (DMG_DEC_Percentage / 100),1);
        target.DecCurrStats(STATSTYPE.DAMAGE,ModAmount);
        target.ActiveVFXParticle(VFX);
        AudioSource.PlayClipAtPoint(SFX, target.transform.position, GameManager.SFX_Volume);
    }

    public override void Expire() {
        target.AddCurrStats(STATSTYPE.DAMAGE, ModAmount);
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }
}

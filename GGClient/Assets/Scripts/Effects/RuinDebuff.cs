﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GreedyNameSpace;

public class RuinDebuff : Debuff {
    public override CR Type { get { return CR.RuinDebuff; } }

    public GameObject VFX;
    public AudioClip SFX;
    float ModAmount;
    float MOVESPD_DEC_Percentage;
    
    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        if (CombatChecker.IgnoreApplyingEffect(target, Type))
            return;
        GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
        Effect_OJ.name = Type.ToString();
        Effect_OJ.GetComponent<RuinDebuff>().MOVESPD_DEC_Percentage = float.Parse(ExtraParamaters[0]);
        Effect_OJ.GetComponent<RuinDebuff>()._Apply(applyer, target, Duration);
    }

    protected override void _Apply(ObjectController applyer, ObjectController target,float Duration) {
        base._Apply(applyer, target,Duration);
        ModAmount = (float)System.Math.Round(target.GetMaxStats(STATSTYPE.MOVE_SPEED) * (MOVESPD_DEC_Percentage / 100),1);
        target.DecCurrStats(STATSTYPE.MOVE_SPEED,ModAmount);
        target.ActiveVFXParticle(VFX);
        AudioSource.PlayClipAtPoint(SFX, target.transform.position, GameManager.SFX_Volume);
    }

    public override void Expire() {       
        target.AddCurrStats(STATSTYPE.MOVE_SPEED, ModAmount);
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GreedyNameSpace;
using Networking.Data;

public class BleedDebuff : Debuff {
    public override CR Type { get { return CR.BleedDebuff; } }
    public GameObject VFX;
    public AudioClip SFX;

    public float BleedingInterval = 1f;

    private float BleedingTimer = 0f;

    float BleedAmount;    
    // Update is called once per frame
    protected override void FixedUpdate() {
        base.FixedUpdate();
        BleedPerSecond();
    }
    
    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        if (CombatChecker.IgnoreApplyingEffect(target, Type))
            return;            
        GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
        Effect_OJ.name = Type.ToString();
        Effect_OJ.GetComponent<BleedDebuff>().BleedAmount = float.Parse(ExtraParamaters[0]);            
        Effect_OJ.GetComponent<BleedDebuff>()._Apply(applyer, target,Duration);
    }

    protected override void _Apply(ObjectController applyer, ObjectController target,float Duration) {
        base._Apply(applyer,target,Duration);
        target.ActiveVFXParticle(VFX);
        DealBleedDmg();
    }

    private void BleedPerSecond() {
        if (BleedingTimer < BleedingInterval) {
            BleedingTimer += Time.fixedDeltaTime;
        }
        else if (BleedingTimer >= BleedingInterval) {
            DealBleedDmg();
            BleedingTimer = 0;
        }
    }


    private void DealBleedDmg() {
        if (Client.Connected)
            NetworkDealBleedDmg();
        else
            LocalDealBleedDmg();
    }

    private void NetworkDealBleedDmg() {
        if (Responsible) {
            Damage dmg = CombatGenerator.GenerateFlatDamage(applyer,target, BleedAmount,false, Type);
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
        }
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    private void LocalDealBleedDmg() {
        Damage dmg = CombatGenerator.GenerateFlatDamage(applyer, target, BleedAmount, false, Type);
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    public override void Expire() {
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }
}

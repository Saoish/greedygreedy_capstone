﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;

public class StunDebuff : Debuff {
    public override CR Type { get { return CR.StunDebuff; } }

    public GameObject VFX;

    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        if (target.HasEffect(CR.StunDebuff)) {
            if (Duration > target.GetEffect(CR.StunDebuff).Duration)
                target.GetEffect(CR.StunDebuff).Duration = Duration;
        } else {
            GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
            Effect_OJ.name = Type.ToString();
            Effect_OJ.GetComponent<StunDebuff>()._Apply(applyer, target, Duration);
        }
    }

    protected override void _Apply(ObjectController applyer, ObjectController target, float Duration) {
        base._Apply(applyer, target,Duration);
        target.Stunned = true;
        //target.MountainlizeRigibody();
        target.ActiveVFXParticle(VFX);
    }

    public override void Expire() {        
        target.Stunned = false;
        //target.NormalizeRigibody();
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }

}

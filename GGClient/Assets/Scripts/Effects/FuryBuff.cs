﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
using System;

public class FuryBuff : Buff {
    public override CR Type { get { return CR.FuryBuff; } }
    public GameObject VFX;
    float ModAmount;
    float AttkSpd_INC_Percentage;

    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        if (!target.HasEffect(CR.FuryBuff)) {
            GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
            Effect_OJ.name = Type.ToString();
            Effect_OJ.GetComponent<FuryBuff>().AttkSpd_INC_Percentage = float.Parse(ExtraParamaters[0]);
            Effect_OJ.GetComponent<FuryBuff>()._Apply(applyer, target, Duration);
        }
    }

    protected override void _Apply(ObjectController applyer, ObjectController target,float Duration) {
        base._Apply(applyer, target,Duration);        
        ModAmount = AttkSpd_INC_Percentage;
        target.AddCurrStats(STATSTYPE.ATTACK_SPEED,ModAmount);
        target.ActiveVFXParticle(VFX);
    }

    public override void Expire() {
        target.DecCurrStats(STATSTYPE.ATTACK_SPEED, ModAmount);
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public class UnbridledWrathDebuff : Debuff {
    public override CR Type { get { return CR.UnbridledWrathDebuff; } }
    public GameObject VFX;
    public AudioClip SFX;
    public UnbridledWrathFireNova UWFN;

    public static float TickInterval = 1f;

    public static float MaxHealthTickPercentage = 3f;

    private float TickTimer = 0f;

    float DamageScale;

    protected override void FixedUpdate() {
        base.FixedUpdate();
        BurnPerSecond();
    }

    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        //if (CombatChecker.IgnoreApplyingEffect(target, Type))
        //    return;
        GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
        Effect_OJ.name = Type.ToString();
        Effect_OJ.GetComponent<UnbridledWrathDebuff>().DamageScale = float.Parse(ExtraParamaters[0]);
        Effect_OJ.GetComponent<UnbridledWrathDebuff>()._Apply(applyer, target, Duration);
    }

    protected override void _Apply(ObjectController applyer, ObjectController target, float Duration) {
        base._Apply(applyer, target, Duration);
        //target.ON_ESSENSE_LOSS += HomingMeteorPassive;                        
        target.ActiveVFXParticle(VFX);
        Burn();
    }

    //private void HomingMeteorPassive(EssenseLoss EL) {
    //    if (Client.Connected)
    //        NetworkHomingMeteorPassive(EL);
    //    else
    //        LocalHomingMeteorPassive(EL);
    //}



    //private void NetworkHomingMeteorPassive(EssenseLoss EL) {
    //    //if (EL.Type != CR.MageMagicOrbConsume && EL.Type != CR.MageStaffConsume) {
    //    ObjectController HomingTarget = CombatGenerator.MP_RaycastHomingTarget();
    //    if (HomingTarget) {
    //        Vector2 Direction = Vector3.Normalize(HomingTarget.Position - target.Position);
    //        Damage dmg = CombatGenerator.GenerateRawDirectDamage(target, DamageScale / 2, Type);
    //        ProjectileData PD = target.ON_GENERATE_PD(CombatGenerator.GenerateProjectileData(Meteor, target, Direction, 1, dmg, HomingTarget));
    //        HandyNetwork.SendLaunch(PD);
    //        //Client.Send(Protocols.Launch, PD, Client.UDP);
    //    }
    //    //}
    //}
    //private void LocalHomingMeteorPassive(EssenseLoss EL) {
    //    //if (EL.Type != CR.MageMagicOrbConsume && EL.Type!=CR.MageStaffConsume) {
    //    ObjectController HomingTarget = CombatGenerator.MP_RaycastHomingTarget();
    //    if (HomingTarget) {
    //        Vector2 Direction = Vector3.Normalize(HomingTarget.Position - target.Position);
    //        Damage dmg = CombatGenerator.GenerateRawDirectDamage(target, DamageScale / 2, Type);
    //        ProjectileData PD = target.ON_GENERATE_PD(CombatGenerator.GenerateProjectileData(Meteor, target, Direction, 1, dmg, HomingTarget));
    //        CacheManager.Launch(PD);
    //    }
    //    //}
    //}

    private void BurnPerSecond() {
        if (TickTimer < TickInterval) {
            TickTimer += Time.fixedDeltaTime;
        }
        else if (TickTimer >= TickInterval) {
            Burn();
            TickTimer = 0;
        }
    }

    private void Burn() {
        if (Client.Connected)
            NetworkBurn();
        else
            LocalBurn();
    }

    private void NetworkBurn() {
        UWFN.Explode(transform.position, target, DamageScale);
        if (Responsible) {
            Damage dmg = CombatGenerator.GenerateFlatDamage(applyer, target, target.MaxHealth * MaxHealthTickPercentage / 100, false, Type);
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);            
        }        
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    private void LocalBurn() {
        UWFN.Explode(transform.position, target, DamageScale);
        Damage dmg = CombatGenerator.GenerateFlatDamage(applyer, target, target.MaxHealth * MaxHealthTickPercentage / 100, false, Type);
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }


    public override void Expire() {
        //target.ON_ESSENSE_LOSS-= HomingMeteorPassive;
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }
}

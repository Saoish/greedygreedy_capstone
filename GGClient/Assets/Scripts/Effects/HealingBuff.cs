﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;
using Networking.Data;

public class HealingBuff : Buff {
    public override CR Type {get { return CR.HealingBuff; }}
    public GameObject VFX;
    public AudioClip SFX;

    public float HealingInterval = 1f;

    private float HealingTimer = 0f;

    float HealAmount;

    float Heal_Percentage;
	protected override void FixedUpdate () {
        base.FixedUpdate();
        HealPerSecond();
	}

    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {//Overriding        
        GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
        Effect_OJ.name = Type.ToString();
        Effect_OJ.GetComponent<HealingBuff>().HealAmount = float.Parse(ExtraParamaters[0]);
        Effect_OJ.GetComponent<HealingBuff>()._Apply(applyer, target, Duration);
    }

    protected override void _Apply(ObjectController applyer, ObjectController target,float Duration) {        
        base._Apply(applyer, target, Duration);
        target.ActiveVFXParticle(VFX);
        Heal();
    }    

    private void Heal() {
        if (Client.Connected)
            NetworkHeal();
        else
            LocalHeal();
    }

    private void NetworkHeal() {
        if(Responsible) {//A monster is not likly be healed by player
            Heal Heal = CombatGenerator.GenerateFlatHeal(applyer,target, HealAmount,false, CR.HealingBuff);
            Client.Send(Protocols.ObjectOnHealthGain, Heal, Client.TCP);
        }
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    private void LocalHeal() {
        Heal Heal = CombatGenerator.GenerateFlatHeal(applyer, target, HealAmount, false, CR.HealingBuff);
        target.ON_HEALTH_GAIN += target.HealHP;
        target.ON_HEALTH_GAIN(Heal);
        target.ON_HEALTH_GAIN -= target.HealHP;
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    private void HealPerSecond() {
        if (HealingTimer < HealingInterval) {
            HealingTimer += Time.fixedDeltaTime;
        }
        else if(HealingTimer >= HealingInterval) {
            Heal();
            HealingTimer = 0;
        }
    }

    public override void Expire() {
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }
}

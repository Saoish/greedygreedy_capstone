﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public class FrozenImmobolizeDebuff : Debuff {//This Debuff will have to check every other Immobolize Debuff before applying, therefore it's purple

    private ParticleSystem FrozenPS;
    
    public override CR Type { get { return CR.FrozenImmobolizeDebuff; } }

    public GameObject VFX;
    public AudioClip SFX;

    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        if (target.HasEffect(CR.FrozenImmobolizeDebuff)) {
            if (Duration > target.GetEffect(CR.FrozenImmobolizeDebuff).Duration) {                
                target.GetEffect(CR.FrozenImmobolizeDebuff).Duration = Duration;
                target.DeactiveVFXParticle(VFX);
                ((FrozenImmobolizeDebuff)target.GetEffect(CR.FrozenImmobolizeDebuff)).FrozenPS = target.ActiveVFXParticle(VFX);
                ((FrozenImmobolizeDebuff)target.GetEffect(CR.FrozenImmobolizeDebuff)).FrozenPS.startLifetime = Duration;
            }
        }
        else {
            GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
            Effect_OJ.name = Type.ToString();
            Effect_OJ.GetComponent<FrozenImmobolizeDebuff>()._Apply(applyer, target, Duration);
        }
    }

    protected override void _Apply(ObjectController applyer, ObjectController target, float Duration) {
        base._Apply(applyer, target, Duration);
        target.Immobilized = true;
        FrozenPS = target.ActiveVFXParticle(VFX);
        FrozenPS.startLifetime = Duration;        
        AudioSource.PlayClipAtPoint(SFX, target.transform.position, GameManager.SFX_Volume);
    }

    public override void Expire() {
        target.Immobilized = false;        
        target.DeactiveVFXParticle(VFX);
        base.Expire();
    }
}

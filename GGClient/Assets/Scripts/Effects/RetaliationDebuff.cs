﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
using System;

public class RetaliationDebuff : Debuff {
    public override CR Type { get { return CR.RetaliationDebuff; } }

    public GameObject VFX;
    public AudioClip SFX;

    float ReflectedAmount;
    bool Crit;
    // Update is called once per frame
    protected override void FixedUpdate() {
        base.FixedUpdate();
    }


    //public static RetaliationDebuff Generate(float ReflectedAmount,bool Crit) {
    //    GameObject RD_OJ = Instantiate(Resources.Load("DebuffPrefabs/RetaliationDebuff")) as GameObject;
    //    RD_OJ.name = "RetaliationDebuff";
    //    RD_OJ.GetComponent<RetaliationDebuff>().ReflectedAmount = ReflectedAmount;
    //    RD_OJ.GetComponent<RetaliationDebuff>().Crit = Crit;
    //    return RD_OJ.GetComponent<RetaliationDebuff>();
    //}

    public override void Apply(ObjectController applyer, ObjectController target, float Duration, string[] ExtraParamaters) {
        GameObject Effect_OJ = Instantiate(Resources.Load("EffectPrefabs/" + Type.ToString())) as GameObject;
        Effect_OJ.name = Type.ToString();
        Effect_OJ.GetComponent<RetaliationDebuff>().ReflectedAmount = float.Parse(ExtraParamaters[0]);
        Effect_OJ.GetComponent<RetaliationDebuff>().Crit = bool.Parse(ExtraParamaters[1]);
        Effect_OJ.GetComponent<RetaliationDebuff>()._Apply(applyer, target,Duration);
    }

    protected override void _Apply(ObjectController applyer, ObjectController target,float Duration) {
        base._Apply(applyer, target,Duration);
        Damage damage = CombatGenerator.GenerateFlatDamage(applyer, target, ReflectedAmount, Crit,CR.RetaliationDebuff);
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(damage);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
        target.ActiveOneShotVFXParticle(VFX);
        AudioSource.PlayClipAtPoint(SFX, target.transform.position, GameManager.SFX_Volume);
    }
}



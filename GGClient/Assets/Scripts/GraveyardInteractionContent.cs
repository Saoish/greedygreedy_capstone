﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Networking.Data;
using GreedyNameSpace;

public class GraveyardInteractionContent : InteractionContent {
    public Button Prev;
    public Button Next;
    public InputField Message;
    public Button Dig;   
    public Image GraveModelImage;    
    public Text ModelName;
    public Text PlayerName;

    private GraveStoneID CachedModleID = GraveStoneID.Crosswood;

    private GameObject CachedButtonOJ;

    public override void TurnOn() {        
        ControllerManager.SyncActions = false;
        transform.position = CacheManager.MP.Position;
        PlayerName.text = CacheManager.MP.GetName();
        GraveModelImage.sprite = GraveStoneImages.Get(CachedModleID);
        ModelName.text = CachedModleID.ToString();
        GetComponent<CanvasGroup>().interactable = true;
        gameObject.SetActive(true);                
        SelectWithDelay(Dig.gameObject);
        SyncActions = true;
    }

    public override void TurnOff() {
        Message.text = "";
        GetComponent<CanvasGroup>().interactable = false;
        gameObject.SetActive(false);
        ControllerManager.SyncActions = true;
    }


    protected override void _Interrupt() {        
        CachedButtonOJ = EventSystem.current.currentSelectedGameObject;
        SyncActions = false;
        GetComponent<CanvasGroup>().interactable = false;
    }

    protected override void _Resume() {
        SyncActions = true;
        GetComponent<CanvasGroup>().interactable = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(CachedButtonOJ);
    }

    private void ConfirmDig() {        
        Client.Send(Protocols.AddGrave, new GraveData(CacheManager.MP.NetworkID, CachedModleID, CacheManager.MP_Data.Name, CacheManager.MP_Data.Class, CacheManager.MP_Data.lvl, Message.text, new SceneCoordinate(Scene.Current_SID, CacheManager.MP.Position)), Client.TCP);
        CacheManager.MP.MPI.DigGoInCD();
        MainPlayer.DropQueue();
        TurnOff();
    }


    public void OnClickDig() {
        if (MainPlayer.InQueue) {
            PopUpNotification.Push("Your current queue will be dropped, do you want to procceed?", PopUpNotification.Type.Select);
            PopUpNotification.HereWeGo(ConfirmDig);
        }
        else if (MainPlayer.HasGravestone) {
            PopUpNotification.Push("Your previous Gravestone will be discarded, do you want to dig a new one?", PopUpNotification.Type.Select);
            PopUpNotification.HereWeGo(ConfirmDig);
        }
        else
            ConfirmDig();
    }
    
    public void OnClickCancel() {
        if (gameObject.active)
            TurnOff();
    }

    public void OnClickNext() {
        List <GraveStoneID> UnlockModels = CacheManager.UserData.GraveStones;        
        int LastIndex = UnlockModels.Count - 1;        
        if (UnlockModels[LastIndex] == CachedModleID)
            CachedModleID = UnlockModels[0];
        else
            CachedModleID = UnlockModels[UnlockModels.IndexOf(CachedModleID)+1];
        GraveModelImage.sprite = GraveStoneImages.Get(CachedModleID);
        ModelName.text = CachedModleID.ToString();
    }

    public void OnClickPrev() {
        List<GraveStoneID> UnlockModels = CacheManager.UserData.GraveStones;
        int LastIndex = UnlockModels.Count - 1;
        if (UnlockModels[0] == CachedModleID)
            CachedModleID = UnlockModels[LastIndex];
        else
            CachedModleID = UnlockModels[UnlockModels.IndexOf(CachedModleID) - 1];
        GraveModelImage.sprite = GraveStoneImages.Get(CachedModleID);
        ModelName.text = CachedModleID.ToString();
    }

    // Update is called once per frame
    void Update() {
        //if (EventSystem.current.currentSelectedGameObject == Message.gameObject) {
        //    Message.caretPosition = Message.text.Length;
        //    //if (ControllerManager.Actions.Enter.WasPressed) {                
        //    //    EventSystem.current.SetSelectedGameObject(null);
        //    //    EventSystem.current.SetSelectedGameObject(Dig.gameObject);
        //    //}
        //    //else if (ControllerManager.Actions.Cancel.WasPressed) {
        //    //    EventSystem.current.SetSelectedGameObject(null);
        //    //    EventSystem.current.SetSelectedGameObject(Dig.gameObject);
        //    //}
        //}
        //else {
            if (SyncActions && ControllerManager.Actions.Cancel.WasPressed) {
                if (gameObject.active)
                    TurnOff();
            }
            else if (SyncActions && ControllerManager.Actions.Prev.WasPressed) {
                ExecuteEvents.Execute(Prev.gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.submitHandler);
            }
            else if (SyncActions && ControllerManager.Actions.Next.WasPressed)
                ExecuteEvents.Execute(Next.gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.submitHandler);

        //}
    }
}

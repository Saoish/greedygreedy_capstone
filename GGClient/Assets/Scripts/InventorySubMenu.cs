﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GreedyNameSpace;
public class InventorySubMenu : InteractionContent {
    public GameObject EquipBtnOJ;
    public GameObject DumpBtnOJ;
    InventoryButtonController IBC = null;
    public AudioClip selected;

    private GameObject CachedButtonOJ;

    public void OnSelect() {
        AudioSource.PlayClipAtPoint(selected, transform.position, GameManager.SFX_Volume);        
    }

    public override void TurnOn() {
        if(!IBC) {
            IBC = transform.GetComponentInParent<InventoryButtonController>();            
        }
        //IBC.MPC.MPUI.SyncActions = false;
        transform.GetComponentInParent<CharacterSheetController>().UnSyncCSCActions();
        //transform.parent.parent.parent.GetComponent<CanvasGroup>().interactable = false;
        
        gameObject.SetActive(true);
        //Debug.Log(transform.parent.parent.Find("EquippedSlotButtons"));
        Button EQ_Btn = transform.parent.parent.parent.Find("EquippedSlotButtons/" + (int)CacheManager.MP.GetInventoryItem(IBC.Slot).EquipType).GetComponent<Button>();
        ColorBlock cb = EQ_Btn.colors;
        cb.disabledColor = MyColor.Orange;
        EQ_Btn.colors = cb;

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(EquipBtnOJ);
    }

    public override void TurnOff() {
        Button[] EQ_Btns = transform.parent.parent.parent.Find("EquippedSlotButtons").GetComponentsInChildren<Button>(true);
        foreach(var EQ_Btn in EQ_Btns) {
            ColorBlock cb = EQ_Btn.colors;
            cb.disabledColor = new Color(73 / 255f, 73 / 255f, 73 / 255f, 1);
            EQ_Btn.colors = cb;
        }                
        GetComponent<CanvasGroup>().interactable = true;
        gameObject.SetActive(false);
        GameManager.instance.StartCoroutine(RecoverInventoryControl());
    }
    private IEnumerator RecoverInventoryControl() {                
        yield return new WaitForSeconds(0.3f);     //Just for now   
        transform.GetComponentInParent<CharacterSheetController>().ResumeCSCSyncActions();
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(IBC.gameObject);
    }


    protected override void _Interrupt() {
        CachedButtonOJ = EventSystem.current.currentSelectedGameObject;
        SyncActions = false;
        GetComponent<CanvasGroup>().interactable = false;
    }

    protected override void _Resume() {        
        SyncActions = true;
        GetComponent<CanvasGroup>().interactable = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(CachedButtonOJ);
    }

    public void OnClickCancel() {        
        PopUpNotification.Push("Are you sure?", PopUpNotification.Type.Select);
        PopUpNotification.HereWeGo(ConfirmDump, CancelDump);
    }
    void ConfirmDump() {        
        CacheManager.MP.RemoveFromInventory(IBC.Slot);
        IBC.UpdateSlot();
        IBC.EI.Show(IBC.E, EquipmentInfo.Mode.Inventory);
        TurnOff();
    }
    void CancelDump() {
        TurnOff();
    }

    private void Update() {
        if (SyncActions && ControllerManager.Actions.Cancel.WasPressed)
            TurnOff();
    }

    public void OnClickEquip() {
        if (!CacheManager.MP.GetInventoryItem(IBC.Slot).isNull) {
            if (CacheManager.MP.CurrentlyHasEffect) {
                RedNotification.Push(RedNotification.Type.EDIT_DURING_EFFECTS);
                return;
            }
            if (CacheManager.MP.Compatible(IBC.E)) {
                if (!CacheManager.MP.GetEquippedItem(IBC.E.EquipType).isNull) {//Has Equipped Item
                    Equipment TakeOff = CacheManager.MP.GetEquippedItem(IBC.E.EquipType);
                    CacheManager.MP.UnEquip(TakeOff.EquipType);
                    CacheManager.MP.Equip(IBC.E);
                    transform.parent.parent.parent.Find("EquippedSlotButtons/" + (int)IBC.E.EquipType).GetComponent<EquippedButtonController>().UpdateSlot();
                    CacheManager.MP.RemoveFromInventory(IBC.Slot);
                    CacheManager.MP.AddToInventory(IBC.Slot, TakeOff);
                    IBC.UpdateSlot();
                    IBC.EI.Show(IBC.E, EquipmentInfo.Mode.Inventory);                    
                }
                else {//No Equipped Item
                    CacheManager.MP.Equip(IBC.E);
                    transform.parent.parent.parent.Find("EquippedSlotButtons/" + (int)IBC.E.EquipType).GetComponent<EquippedButtonController>().UpdateSlot();
                    CacheManager.MP.RemoveFromInventory(IBC.Slot);
                    IBC.UpdateSlot();
                    IBC.EI.Show(IBC.E, EquipmentInfo.Mode.Inventory);
                }
                TurnOff();
            }            
            else {
                RedNotification.Push(RedNotification.Type.CANT_EQUIP);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using GreedyNameSpace;
using UnityEngine.UI;
using System.Collections.Generic;
using Networking.Data;

public class MenuController : InteractionContent{
    public GameObject Options;
    public GameObject Graphic;
    public GameObject Characters;
    public GameObject Quit;    

    public GameObject OptionsContent;
    public GameObject OptionsContentFirstSelect;
    public GameObject GraphicSetting;
    public GameObject GraphicSettingFirstSelect;
    public ButtonImage[] ControlAxiesButtonImages;
    public GameObject ShowName_Check;
    public GameObject ShowToolTips_Check;
    
    public Text QualityText;
    public Button[] Res_Buttons;
    public Text ResolutionText;
    public GameObject FullScreen_Check;
    public GameObject VSync_Check;

    private GameObject CachedButtonOJ;    

    public void OnSelect() {
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position);
    }

    private void Awake() {
        
    }

    private void Start() {
        
    }

    public void OnClickGraphic() {        
        Options.SetActive(false);
        Graphic.SetActive(false);
        Characters.SetActive(false);
        Quit.SetActive(false);

        UpdateGraphicSettings();

        GraphicSetting.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(GraphicSettingFirstSelect);        
    }
    
    public void OnClickQualityPrev() {
        QualitySettings.DecreaseLevel();
        StartCoroutine(YieldFramesAndUpdateGraphicSettings());
    }
    public void OnClickQualityNext() {
        QualitySettings.IncreaseLevel();
        StartCoroutine(YieldFramesAndUpdateGraphicSettings());
    }
    public void OnClickResolutionPrev() {
        int curr_res_index = 0;
        for(int i = 0; i< Screen.resolutions.Length; i++){
            if(Screen.resolutions[i].width == Screen.currentResolution.width && Screen.resolutions[i].height == Screen.currentResolution.height) {
                curr_res_index = i;
                break;
            }
        }
        if (curr_res_index == 0)
            return;
        Screen.SetResolution(Screen.resolutions[curr_res_index - 1].width, Screen.resolutions[curr_res_index - 1].height,true);
        StartCoroutine(YieldFramesAndUpdateGraphicSettings());
    }
    public void OnClickResolutionNext() {
        int curr_res_index = 0;
        for (int i = 0; i < Screen.resolutions.Length; i++) {
            if (Screen.resolutions[i].width == Screen.currentResolution.width && Screen.resolutions[i].height == Screen.currentResolution.height) {
                curr_res_index = i;
                break;
            }
        }
        if (curr_res_index == Screen.resolutions.Length-1)
            return;
        Screen.SetResolution(Screen.resolutions[curr_res_index + 1].width, Screen.resolutions[curr_res_index + 1].height, true);
        
        StartCoroutine(YieldFramesAndUpdateGraphicSettings());        
    }
    public void OnClickFullScreen() {
        Screen.fullScreen = !Screen.fullScreen;
        StartCoroutine(YieldFramesAndUpdateGraphicSettings());
    }
    public void OnClickVSync() {        
        if (QualitySettings.vSyncCount == 0)
            QualitySettings.vSyncCount = 1;
        else
            QualitySettings.vSyncCount = 0;
        StartCoroutine(YieldFramesAndUpdateGraphicSettings());
    }
    IEnumerator YieldFramesAndUpdateGraphicSettings() {
        yield return null;
        yield return null;
        yield return null;
        yield return null;
        UpdateGraphicSettings();
    }
    private void UpdateGraphicSettings() {       
        QualityText.text = QualitySettings.names[QualitySettings.GetQualityLevel()];        
        ResolutionText.text = Screen.currentResolution.width+" x " + Screen.currentResolution.height;        
        if (Screen.fullScreen) {
            foreach (var button in Res_Buttons) {
                button.interactable = true;
            }
            ResolutionText.color = MyColor.White;
            FullScreen_Check.SetActive(true);
        }
        else {
            foreach (var button in Res_Buttons) {
                button.interactable = false;
            }
            ResolutionText.color = MyColor.Grey;
            FullScreen_Check.SetActive(false);
        }
        if (QualitySettings.vSyncCount == 0)
            VSync_Check.SetActive(false);
        else
            VSync_Check.SetActive(true);
    }

    

    public void OnClickOptions() {
        Options.SetActive(false);
        Graphic.SetActive(false);
        Characters.SetActive(false);
        Quit.SetActive(false);
        OptionsContent.SetActive(true);        
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(OptionsContentFirstSelect);

        UpdateShowName();
        UpdateShowToolTips();
        UpdateAxieImages();        
    }

    public void OnClickToggleShowNames() {
        GameManager.ShowNames = !GameManager.ShowNames;
        UpdateShowName();
    }
    void UpdateShowName() {
        if (GameManager.ShowNames)
            ShowName_Check.SetActive(true);
        else
            ShowName_Check.SetActive(false);
    }

    public void OnClickwToggleShowToolTips() {
        GameManager.ShowFloatingGuide = !GameManager.ShowFloatingGuide;
        if (GameManager.ShowFloatingGuide)
            ToolTipShower.TurnAllOn();
        else
            ToolTipShower.TurnAllOff();
        UpdateShowToolTips();
    }
    void UpdateShowToolTips() {
        if (GameManager.ShowFloatingGuide)
            ShowToolTips_Check.SetActive(true);
        else
            ShowToolTips_Check.SetActive(false);
    }    

    void UpdateAxieImages() {        
        for(int i = 0; i < Patch.SkillSlots; i++) {            
            if (CacheManager.UserData.AxisMappings[CacheManager.CPS].Get(i) == CastSyncAxis.Left)
                ControlAxiesButtonImages[i].FetchButtonImage(ButtonImage.Type.Move);                
            else
                ControlAxiesButtonImages[i].FetchButtonImage(ButtonImage.Type.Attack);
        }
    }

    public void OnClickCharacters() {
        if (SceneChecker.TargetInDungeons(Scene.Current_SID)) {                        
            Client.Send(Protocols.SaveLogOffState, new State(CacheManager.MP_Data.CurrStats, CacheManager.MP_Data.Alive, CacheManager.PrevState.SC, CacheManager.MP_Data.InventoryViewSigns), Client.TCP);
            CacheManager.MP_Data.SC = CacheManager.PrevState.SC;
        }
        else if (!SceneChecker.TargetInUnLogables(Scene.Current_SID)) {
            Client.Send(Protocols.SaveLogOffState, new State(CacheManager.MP_Data.CurrStats, CacheManager.MP_Data.Alive, CacheManager.MP_Data.SC, CacheManager.MP_Data.InventoryViewSigns), Client.TCP);            
        }
        else {
            if (CacheManager.PrevState != null) {
                Client.Send(Protocols.SaveLogOffState, CacheManager.PrevState, Client.TCP);
                CacheManager.MP_Data.CurrStats = CacheManager.PrevState.CurrStats;
                CacheManager.MP_Data.SC = CacheManager.PrevState.SC;
                CacheManager.MP_Data.Alive = CacheManager.PrevState.Alive;
            }                        
        }
        CacheManager.WipeObjects();
        Scene.Load(SceneID.CharacterSelection);
    }

    public void OnClickSwitchAxis() {       
        int Slot = int.Parse(EventSystem.current.currentSelectedGameObject.name);
        CacheManager.UserData.AxisMappings[CacheManager.CPS].Switch(Slot);
        if(CacheManager.UserData.AxisMappings[CacheManager.CPS].Get(Slot) == CastSyncAxis.Left) {
            EventSystem.current.currentSelectedGameObject.transform.Find("Image").GetComponent<ButtonImage>().FetchButtonImage(ButtonImage.Type.Move);
        }
        else {
            EventSystem.current.currentSelectedGameObject.transform.Find("Image").GetComponent<ButtonImage>().FetchButtonImage(ButtonImage.Type.Attack);
        }
        if (Client.Connected)
            Client.Send(Protocols.SwitchControlAnalog, Slot, Client.TCP);
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position);
    }

    public void OnClickBackFromGraphic() {
        Options.SetActive(true);
        Graphic.SetActive(true);
        Characters.SetActive(true);
        Quit.SetActive(true);
        GraphicSetting.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(Graphic);
        
    }

    public void OnClickBackFromOptions() {
        Options.SetActive(true);
        Graphic.SetActive(true);
        Characters.SetActive(true);
        Quit.SetActive(true);
        OptionsContent.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(Options);
    }

    public void OnClickQuit() {
        GameManager.Quit();
    }


    protected override void _Interrupt() {
        CachedButtonOJ = EventSystem.current.currentSelectedGameObject;
        SyncActions = false;
        GetComponent<CanvasGroup>().interactable = false;
    }

    protected override void _Resume() {
        SyncActions = true;
        GetComponent<CanvasGroup>().interactable = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(CachedButtonOJ);
    }

    public override void TurnOn() {
        ControllerManager.SyncActions = false;
        Options.SetActive(true);
        Graphic.SetActive(true);
        Characters.SetActive(true);
        Quit.SetActive(true);
        gameObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(Options);
    }

    public override void TurnOff() {
        OptionsContent.SetActive(false);
        GraphicSetting.SetActive(false);
        gameObject.SetActive(false);
        GameManager.instance.StartCoroutine(Restore());        
    }IEnumerator Restore() {//Need test
        yield return new WaitForSeconds(0.1f);
        if(CacheManager.MP.Alive)
            ControllerManager.SyncActions = true;
    }

    private void Update() {
        if (!SyncActions)
            return;
        //else if(ControllerManager.Actions.Cancel.WasPressed || ControllerManager.Actions.Menu.WasPressed) {            
        //    TurnOff();
        //}
        else if (ControllerManager.Actions.Menu.WasPressed) {
            TurnOff();
        }
        else if (ControllerManager.Actions.Cancel.WasPressed) {            
            if (GraphicSetting.active)
                OnClickBackFromGraphic();
            else if(OptionsContent.active)
                OnClickBackFromOptions();
            else
                TurnOff();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DecisionTimer : MonoBehaviour {
    public PopUpNotification PUN;
    public Image TimeBarImage;
    public Animator Anim;
    float Counter = 0f;
    float Goal = 0f;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Goal <= 0)
            return;
        if (Counter < Goal) {
            Counter += Time.deltaTime;
        }
        else {
            PopUpNotification.Decided = true;
            PUN.Cancel();
            Goal = Counter;
        }
        TimeBarImage.fillAmount = Counter/Goal;
        if (Counter / Goal >= 0.7f&& !Anim.GetBool("panic")) {
            Anim.SetBool("panic", true);
        }
    }

    public void Enable(float Goal) {
        this.Goal = Goal;
        Counter = 0;
        TimeBarImage.fillAmount = 0;
        Anim.SetBool("panic", false);
        gameObject.SetActive(true);
    }
    
    public void Disable() {
        gameObject.SetActive(false);
    }

    

}

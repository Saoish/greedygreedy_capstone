﻿using UnityEngine;
using System.Collections;

public class CollisionLayer : MonoBehaviour {
    public static int Default = LayerMask.NameToLayer("Default");

    public static int Loot = LayerMask.NameToLayer("Loot");
    public static int Interaction = LayerMask.NameToLayer("Interaction");
    public static int Skill = LayerMask.NameToLayer("Skill");
    public static int Melee = LayerMask.NameToLayer("Melee");
    public static int Projectile = LayerMask.NameToLayer("Projectile");        
    public static int Indication = LayerMask.NameToLayer("Indication");
    public static int Friendly = LayerMask.NameToLayer("Friendly");
    public static int Enemy = LayerMask.NameToLayer("Enemy");
    public static int Rune = LayerMask.NameToLayer("Rune");

    public static void SetUpCollisionLayers() {
        Physics2D.IgnoreLayerCollision(Loot, Interaction);
        Physics2D.IgnoreLayerCollision(Loot, Skill);
        Physics2D.IgnoreLayerCollision(Loot, Melee);
        Physics2D.IgnoreLayerCollision(Loot, Projectile);        
        Physics2D.IgnoreLayerCollision(Loot, Indication);
        Physics2D.IgnoreLayerCollision(Loot, Friendly);
        Physics2D.IgnoreLayerCollision(Loot, Enemy);
        Physics2D.IgnoreLayerCollision(Loot, Rune);

        Physics2D.IgnoreLayerCollision(Interaction, Loot);
        Physics2D.IgnoreLayerCollision(Interaction, Skill);
        Physics2D.IgnoreLayerCollision(Interaction, Melee);
        Physics2D.IgnoreLayerCollision(Interaction, Projectile);        
        Physics2D.IgnoreLayerCollision(Interaction, Indication);
        Physics2D.IgnoreLayerCollision(Interaction, Rune);

        Physics2D.IgnoreLayerCollision(Skill, Loot);
        Physics2D.IgnoreLayerCollision(Skill, Interaction);
        Physics2D.IgnoreLayerCollision(Skill, Indication);
        Physics2D.IgnoreLayerCollision(Skill, Rune);

        Physics2D.IgnoreLayerCollision(Melee, Loot);
        Physics2D.IgnoreLayerCollision(Melee, Melee);
        Physics2D.IgnoreLayerCollision(Melee, Projectile);     
        Physics2D.IgnoreLayerCollision(Melee, Interaction);
        Physics2D.IgnoreLayerCollision(Melee, Indication);
        Physics2D.IgnoreLayerCollision(Melee, Rune);
        
        Physics2D.IgnoreLayerCollision(Projectile, Loot);        
        Physics2D.IgnoreLayerCollision(Projectile, Interaction);
        Physics2D.IgnoreLayerCollision(Projectile, Melee);
        Physics2D.IgnoreLayerCollision(Projectile, Projectile);
        Physics2D.IgnoreLayerCollision(Projectile, Indication);
        Physics2D.IgnoreLayerCollision(Projectile, Rune);
        
        Physics2D.IgnoreLayerCollision(Indication, Loot);
        Physics2D.IgnoreLayerCollision(Indication, Interaction);
        Physics2D.IgnoreLayerCollision(Indication, Skill);
        Physics2D.IgnoreLayerCollision(Indication, Melee);
        Physics2D.IgnoreLayerCollision(Indication, Projectile);
        Physics2D.IgnoreLayerCollision(Indication, Indication);
        Physics2D.IgnoreLayerCollision(Indication, Friendly);
        Physics2D.IgnoreLayerCollision(Indication, Enemy);
        Physics2D.IgnoreLayerCollision(Indication, Rune);

        Physics2D.IgnoreLayerCollision(Friendly, Loot);
        Physics2D.IgnoreLayerCollision(Friendly, Indication);
        Physics2D.IgnoreLayerCollision(Enemy, Loot);
        Physics2D.IgnoreLayerCollision(Enemy, Indication);

        Physics2D.IgnoreLayerCollision(Rune, Loot);
        Physics2D.IgnoreLayerCollision(Rune, Interaction);
        Physics2D.IgnoreLayerCollision(Rune, Skill);
        Physics2D.IgnoreLayerCollision(Rune, Melee);
        Physics2D.IgnoreLayerCollision(Rune, Projectile);
        Physics2D.IgnoreLayerCollision(Rune, Indication);
        Physics2D.IgnoreLayerCollision(Rune, Rune);
    }
         
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraveStoneInteraction : Interaction {
    public GraveStoneInteractionContent GI;
    protected override void Start() {
        base.Start();
        Priority = 1;
        GI = Instantiate(GI.gameObject, transform).GetComponent<GraveStoneInteractionContent>();
    }


    public override bool IsTriggered() {
        return ControllerManager.Actions.Submit.WasPressed;
    }

    public override void Interact() {
        GI.TurnOn();
    }

    public override void SetIndication() {
        InteractionNotification.SetIndication("View Gravestone...", ButtonImages.X);
        InteractionNotification.TurnOn();        
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GreedyPhysics {
    public static void StartLerpObjectToPosition(ObjectController OC, Vector2 TargetPosition, float LerpTime = 0.1f) {
        GameManager.instance.StartCoroutine(LerpObjectPosition(OC,TargetPosition,LerpTime));
    }

    static IEnumerator LerpObjectPosition(ObjectController OC, Vector2 TargetPosition, float LerpTime) {                   
        float Counter = 0;        
        while (OC && Counter < LerpTime && OC.Alive && !OC.HasForce) {
            Counter += Time.fixedDeltaTime;
            float timeProgressed = Counter / LerpTime;
            OC.Position = Vector2.Lerp(OC.Position, TargetPosition, timeProgressed);
            yield return new WaitForFixedUpdate();
        }
    }
}

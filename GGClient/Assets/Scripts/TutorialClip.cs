﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialClip : MonoBehaviour {    
    public static void TurnOn() {
        GameManager.instance.StartCoroutine(TurnOnWithDelay());
    }static IEnumerator TurnOnWithDelay() {
        while (Scene.Current_SID != GreedyNameSpace.SceneID.Tyneham)
            yield return null;        
        GreedyCamera.instance.GetComponentInChildren<TutorialClip>(true).gameObject.SetActive(true);        
    }

    public void OnLevelWasLoaded(int level) {
        TurnOff();
    }

    // Use this for initialization
    void Start () {
                	    	
	}
	
	// Update is called once per frame
	void Update () {
        if(gameObject.active)
            ControllerManager.SyncActions = false;
        if (ControllerManager.Actions.Submit.WasPressed) {
            TurnOff();
        }
    }

    void TurnOff() {
        ControllerManager.SyncActions = true;
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective : MonoBehaviour {
    [HideInInspector]
    public float AngleZ;
    [HideInInspector]
    public Transform target = null;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdatePointer();
    }

    void UpdatePointer() {
        if (target) {
            Vector2 Direction = Vector3.Normalize(target.position - transform.position);
            AngleZ = Mathf.Atan2(Direction.y, Direction.x) * Mathf.Rad2Deg;
            transform.localEulerAngles = new Vector3(0, 0, AngleZ);
        }
        else {
            TurnOff();
        }
    }
    
    public void Set(Transform target) {
        this.target = target;
        gameObject.SetActive(true);
    }   
    
    public void TurnOff() {
        target = null;
        gameObject.SetActive(false);
    } 
}

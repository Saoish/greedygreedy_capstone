﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SelectionUI : MonoBehaviour {    
    public CanvasGroup CG;
    public Button BackButton;
	// Update is called once per frame
	void Update () {
        if (ControllerManager.Actions.Cancel.WasPressed && CG.interactable) {
            BackButton.onClick.Invoke();
        }
    }
}

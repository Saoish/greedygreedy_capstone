﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Networking.Data;
using GreedyNameSpace;
using System;
using System.IO;

public class GravePointer {
    public GraveData GD;
    public GraveStoneController GC = null;
    public GravePointer(GraveData GD) {
        this.GD = GD;
    }
}

public class ObjectPointer {
    public ObjectData OD;
    public ObjectController OC;

    public ObjectPointer(ObjectController OC,ObjectData OD) {        
        this.OC = OC;
        this.OD = OD;
    }

    public ObjectPointer(ObjectData OD) {
        this.OD = OD;
    }
}

public class CacheManager : MonoBehaviour {//This class use to cach every during scene transition
    public static int Instantiatings;
    public static int Instantiateds;


    public static UserData UserData;
    public static int CPS;//Current player slot
    public static MainPlayer MP;
    public static Dictionary<CR, Effect> Effects = new Dictionary<CR, Effect>();


    public static int NextMonsterKey = NetworkIDChecker.Dynamic_Start;

    public static State Cached_PrevState = null;
    
    public static Map Map = null;

    public static CacheManager instance;

    //Need go into wipe
    public static Dictionary<int, GravePointer> Gravestones = new Dictionary<int, GravePointer>();
    public static Dictionary<int, ObjectPointer> Objects = new Dictionary<int, ObjectPointer>();    
    //Need go into wipe

    void Awake() {        
        if (instance == null) {
            DontDestroyOnLoad(this);
            instance = this;
            PreloadEffects();            
        } else {
            Destroy(gameObject);
        }
    }            

    public static SceneCoordinate DiggedGraveSC {
        get { return Gravestones[Client.ClientID].GD.SC; }
    }

    public static PlayerData MP_Data {
        get { return (PlayerData)Objects[Client.ClientID].OD; }
    }

    public static ObjectData GetObjectData(int NetworkID) {
        return Objects[NetworkID].OD;
    }

    public static PlayerData GetLocalPlayerData(int SlotIndex) {
        return UserData.PlayerDatas[SlotIndex];
    }

    public static void LoadUserData(UserData userdata) {
        UserData = userdata;
    }

    public static Dictionary<int,ObjectData> States {//Read only
        get {
            Dictionary<int, ObjectData> _States = new Dictionary<int, ObjectData>();
            foreach(var s in Objects) {      
                if(!NetworkIDChecker.IsDynamic(s.Key))//Don't give a fk to dynamic
                    _States[s.Key] = s.Value.OD;
            }
            return _States;
        }
    }

    public static void CacheWorldStates() {
        foreach(var s in UserData.WorldStates) {            
            Objects[(int)s.SNID] = new ObjectPointer(s);
            Objects[(int)s.SNID].OD.Target = 0;
            Objects[(int)s.SNID].OD.Alive = true;
            Objects[(int)s.SNID].OD.SC = new SceneCoordinate(Objects[(int)s.SNID].OD.RespawnSC);
            if(Objects[(int)s.SNID].OD.OID == OID.NPC)
                Objects[(int)s.SNID].OD.CurrStats = new Stats(ObjectMapper.GetStaticOC(s.SNID).MaxStats);//Loaded full stats just for now
            else
                Objects[(int)s.SNID].OD.CurrStats = ((Monster)ObjectMapper.GetStaticOC(s.SNID)).GetScaledMaxStats(Objects[(int)s.SNID].OD.lvl);//Loaded full stats just for now
        }
    }

    public static void LoadGraveState(Dictionary<int,GraveData> GraveState) {
        Gravestones.Clear();
        foreach(var gs in GraveState) {            
            Gravestones[gs.Key] = new GravePointer(gs.Value);
        }
    }

    public static void LoadJoiningStates(Dictionary<int,ObjectData> JoiningStates) {
        Objects.Clear();
        foreach(var state in JoiningStates) {            
            Objects[state.Key] = new ObjectPointer(state.Value);
        }        
    }

    public static void AddPlayer(int NetworkID,PlayerData PD) {        
        Objects[NetworkID] = new ObjectPointer(PD);
        if (PD.SC.SID == Scene.Current_SID) {
            ObjectController OC = ObjectMapper.GetPlayerOC(PD.OID);
            OC.Data = PD;
            Map.ResetPortals();
            if (PD.OID == OID.FriendlyPlayer) {
                Map.ResetDeathCoroutine();
            }
            Objects[NetworkID].OC = OC.Instantiate(NetworkID, false);
            //instance.StartCoroutine(InstantiateWithDelay(NetworkID, OC));//Beautiful but, very dangerous
        }
    }static IEnumerator InstantiateWithDelay(int NetworkID,ObjectController OC) {
        yield return new WaitForSeconds(1f);
        Objects[NetworkID].OC = OC.Instantiate(NetworkID, false);
    }

    public static void AddNPC(SNID SNID, StaticObjectData SOD) {
        if (Objects.ContainsKey((int)SOD.SNID)) {
            Debug.Log("NPC: " + SNID + "'s Network D is already in the cache.");
            return;
        }
        else if ((int)SOD.SNID <= -1000) {
            Debug.Log("NPC: " + SNID + "'s Network ID is invalid: " + (int)SOD.SNID);
            return;
        }
        Objects[(int)SNID] = new ObjectPointer(SOD);
        //Mising Instantiation        
    }

    public static void AddDynamicData(int DynamicKey, ObjectData OD) {
        Objects[DynamicKey] = new ObjectPointer(OD);
    }

    public static void AssignedDynamicOC(int DynamicKey, ObjectController OC) {
        Objects[DynamicKey].OC = OC;
    }

    public static bool HasObjectKey(int Key) {
        return Objects.ContainsKey(Key);
    }

    public static PlayerData GetPlayerData(int NetworkID) {
        return (PlayerData)Objects[NetworkID].OD;
    }

    public static ObjectController GetOC(int NetworkID) {
        if (!Objects.ContainsKey(NetworkID)) {            
            return null;
        }
        return Objects[NetworkID].OC;
    }

    public static Player GetPC(int NetworkID) {
        if (!Objects.ContainsKey(NetworkID) || NetworkID <= 0) {
            return null;
        }
        return (Player)Objects[NetworkID].OC;
    }

    public static List<Player> GetPlayers() {
        List<Player> PList = new List<Player>();
        foreach(var Object in Objects) {
            if ((Object.Key>0))
                PList.Add((Player)Object.Value.OC);
        }
        return PList;
    }

    public static List<OtherPlayer> GetOtherPlayers() {
        List<OtherPlayer> PList = new List<OtherPlayer>();
        foreach(var Object in Objects) {
            if (Object.Key > 0 && Object.Value.OD.OID !=OID.Main)
                PList.Add((OtherPlayer)Object.Value.OC);
        }
        return PList;
    }


    public static void InstantiateGraves() {
        if (!SceneChecker.TargetInUnPlayables(Scene.Current_SID)) {
            foreach (var g in Gravestones) {                
                if (g.Value.GD.SC.SID == Scene.Current_SID) {                    
                    g.Value.GC =  GraveStoneController.Instantiate(g.Value.GD);
                }
            }
        }
    }

    public static void InstantiateOCs(bool Reset) {
        if (!SceneChecker.TargetInUnPlayables(Scene.Current_SID)) {
            List<int> Keys = new List<int>(Objects.Keys);
            Instantiatings = 0;
            Instantiateds = 0;
            foreach (var v in Keys) {
                if (Objects[v].OD.SC.SID == Scene.Current_SID /*&& Objects[v].OD.Alive*/) {
                    Instantiatings++;
                    if (v < 0) {//Static Monster          
                        ObjectController OC;
                        OC = ObjectMapper.GetStaticOC((SNID)v);
                        OC.Data = Objects[v].OD;                        
                        Objects[v].OC = OC.Instantiate(v,Reset);
                    }
                    else {//Player
                        ObjectController OC;
                        OC = ObjectMapper.GetPlayerOC(Objects[v].OD.OID);
                        OC.Data = Objects[v].OD;
                        Objects[v].OC = OC.Instantiate(v,Reset);                        
                    }
                }
            }
            if (!Reset) {
                instance.StartCoroutine(WaitAndExecuteLateActions());
            }
            else {
                Instantiatings = 0;
                Instantiateds = 0;
                Client.WipeLateActions();                
            }
        }                
    }static IEnumerator WaitAndExecuteLateActions() {//For now
        while(Instantiateds< Instantiatings) {
            yield return null;
        }
        Client.ExecuteLateActions();
        Instantiatings = 0;
        Instantiateds = 0;
    }

    public static void ApplyEffect(AddEffectData AD) {
        if (AD.Type == CR.None) {
            Debug.Log("Some effects are not been registered for combat.");
        } else {
            Effects[AD.Type].Apply(GetOC(AD.ApplyerNetworkID), GetOC(AD.TargetNetworkID), AD.Duration, AD.ExtraParamaters);
        }
    }

    public static void UnSubscribeCachedObject(int NetworkID) {
        Objects.Remove(NetworkID);
    }  

    public static void LogOffPlayer(int NetworkID) {
        if (GetOC(NetworkID)) {
            if (Map)
                Map.OnPlayerLeft((Player)GetOC(NetworkID));
            Destroy(GetOC(NetworkID).gameObject);
        }
        Objects.Remove(NetworkID);
    }
    
    public static void WipeAll() {
        Gravestones.Clear();
        Objects.Clear();        
    }    

    public static void WipeGraves() {
        Gravestones.Clear();
    }

    public static void WipeObjects() {
        Objects.Clear();
    }

    public static void WipeOtherPlayers() {
        foreach (var p in GetOtherPlayers()) {            
            LogOffPlayer(p.NetworkID);
        }
    }

    public static void WipeDymanics() {
        for(int i = -1000; i > Mathf.NegativeInfinity; i--) {
            if (!Objects.ContainsKey(i))
                break;
            else
                Objects.Remove(i);
        }
        NextMonsterKey = NetworkIDChecker.Dynamic_Start;
    }

    private static void PreloadEffects() {
        Effect[] PreloadedEffects = Resources.LoadAll<Effect>("EffectPrefabs");
        foreach(Effect e in PreloadedEffects) {
            Effects[e.Type] = e;
        }
    }

    public static void ConvertMPToPrevState() {
        MP_Data.CurrStats = PrevState.CurrStats;
        MP_Data.Alive = PrevState.Alive;
        MP_Data.SC = PrevState.SC;
    }

    public static void CachePrevSceneCoordinate() {
        if (Objects.Count == 0)
            return;
        if (!SceneChecker.TargetInUnLogables(Scene.Current_SID)) {
            SceneCoordinate LogableSC = new SceneCoordinate(Scene.Current_SID, MP.Position);
            Client.Send(Protocols.UpdateLastLogableSC, LogableSC,Client.TCP);
            Cached_PrevState = new State(MP.CurrStats, MP.Alive, LogableSC, MP_Data.InventoryViewSigns);
        }
    }

    public static State PrevState {
        get { return Cached_PrevState; }
    }

    public static void AddGrave(GraveData GD) {
        Gravestones[GD.OwnerNetworkID] = new GravePointer(GD);        
        if(GD.SC.SID == Scene.Current_SID) {
            Gravestones[GD.OwnerNetworkID].GC = GraveStoneController.Instantiate(GD);
        }
    }

    public static void RemoveGrave(int OwnerNetworkID) {
        if (Gravestones.ContainsKey(OwnerNetworkID)) {
            if (Gravestones[OwnerNetworkID].GC)
                Destroy(Gravestones[OwnerNetworkID].GC.gameObject);
            Gravestones.Remove(OwnerNetworkID);
        }            
    }

    public static void Launch(ProjectileData pd) {
        GameObject P_OJ = Resources.Load("ProjectilePrefabs/" + pd.Name) as GameObject;        
        P_OJ = Instantiate(P_OJ, pd.LaunchPosition, Quaternion.identity);
        Projectile P = P_OJ.GetComponent<Projectile>();
        P.Launch(pd);        
    }
    
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsernameFectcher : MonoBehaviour {
    public InputField Username_Input;

	void Start () {
        Load();               
	}

    public void Save() {
        UsernameManager.SaveUsername(Username_Input.text);
    }
    
    public void Load() {
        if (UsernameManager.Username != null)
            Username_Input.text = UsernameManager.Username;
    }
}

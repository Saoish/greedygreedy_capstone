﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IPFetcher : MonoBehaviour {
    public void UpdateIP() {
        Client.ip = GetComponent<InputField>().text;        
    }

    public void UpdateText() {
        GetComponent<InputField>().text = Client.ip;
    }

    public void Enable() {
        gameObject.SetActive(true);
    }    

    public void Disable() {
        gameObject.SetActive(false);
    }
}

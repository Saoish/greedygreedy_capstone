﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
public class Radiation : PassiveSkill {
    public override CR Type { get { return CR.Radiation; } }
    float Penetration_INC_Percentage;
    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Radiationlvl RL = (Radiationlvl)AllLvls[Index];
        return "\nIncrease your "+ MyText.Colofied("Armor Penetration",ScaleHighlight)+  " by " + MyText.Colofied(RL.Penetration_INC_Percentage + "%", ScaleHighlight) + ".";
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Radiationlvl RL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                RL = GetComponent<Radiation1>();
                break;
            case 2:
                RL = GetComponent<Radiation2>();
                break;
            case 3:
                RL = GetComponent<Radiation3>();
                break;
            case 4:
                RL = GetComponent<Radiation4>();
                break;
            case 5:
                RL = GetComponent<Radiation5>();
                break;
        }
        Penetration_INC_Percentage = RL.Penetration_INC_Percentage;
        GenerateDescription();
    }

    public override void ApplyPassive() {
        OC.AddMaxStats(STATSTYPE.PENETRATION, Penetration_INC_Percentage);
    }
}

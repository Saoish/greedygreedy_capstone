﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GreedyNameSpace;
using Networking.Data;

public class IndicationController : MonoBehaviour {
    public GameObject ReviveBar;
    public Image ReviveMask;
    public GameObject HealthBar;
    private ObjectController OC;
    public Transform HealthMask;
    public Image ColorBar;
    public Text Name;


    float ReviveBarCounter;
    [HideInInspector]
    public bool SentRevive = false;

    // Use this for initialization
    void Start() {        
        OC = transform.parent.GetComponent<ObjectController>();
        transform.localPosition = new Vector2(0, OC.IC_yaxis);
        if(OC is MainPlayer) {
            HealthBar.SetActive(false);
        }
        Name.text = OC.GetName();
        UpdateIdentityColor();
        if (GameManager.ShowNames)
            Name.gameObject.SetActive(true);
        else
            Name.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        UpdateHealthBar();
        NameUpdate();
        UpdateIdentityColor();
        UpdateReviveBar();
    }

    void NameUpdate() {      
        ShowNameUpdate();
    }

    void UpdateIdentityColor() {
        if (OC.OID == OID.Main)
            Name.color = MyColor.Orange;
        else if (OC.OID == OID.NPC) {
            Name.color = ColorBar.color = MyColor.Yellow;
        }
        else if (OC.OID == OID.EnemyPlayer || OC.OID == OID.EnemyMonster) {
            Name.color = ColorBar.color = MyColor.Red;
        }
        else {            
            Name.color = ColorBar.color = MyColor.Green;
        }
    }

    //public methods
    public void PopUpText(Value value) {
        if (value.Amount == 0)
            return;
        GameObject PopUpText = Instantiate(Resources.Load("UIPrefabs/PopUpText"),transform) as GameObject;
        PopUpText.transform.localScale = new Vector3(2, 2, 1);
        Text PopText = PopUpText.GetComponent<Text>();
        if (value is Damage) {
            PopUpText.transform.GetComponent<Animator>().SetInteger("Direction", 1);
            if (value.Crit) {
                PopText.color = Color.red;
                PopText.fontSize = 100;
            }

        } else if (value is Heal) {//heal
            PopText.color = Color.cyan;
            PopUpText.transform.GetComponent<Animator>().SetInteger("Direction", 2);
            if (value.Crit) {
                PopText.color = Color.green;
                PopText.fontSize = 100;
            }
        }
        float ExitTime = PopUpText.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
        PopText.text = value.Amount.ToString("F0");
        Destroy(PopUpText, ExitTime);
    }

    public void DisableHealthBar() {
        transform.Find("Health Bar").gameObject.SetActive(false);
    }

    public void EnableHealthBar() {
        transform.Find("Health Bar").gameObject.SetActive(true);
    }

    public void UpdateHealthBar() {
        float CurrHealth = OC.CurrHealth;
        float MaxHealth = OC.MaxHealth;
        if(CurrHealth / MaxHealth>=0)
            HealthMask.transform.localScale = new Vector2(CurrHealth / MaxHealth, 1);//Moving mask
        else
            HealthMask.transform.localScale = new Vector2(0, 1);//Moving mask
    }    

    public void EnableReviveBar() {
        ReviveBarCounter = 0;
        ReviveBar.SetActive(true);
    }

    public void DisableReviveBar() {
        ReviveBar.SetActive(false);
        ReviveBarCounter = 0;
    }
    
    public void UpdateReviveBar() {
        if (ReviveBar.active) {
            if (!SentRevive && ReviveBarCounter >= Patch.ReviveTime) {
                if (OC is MainPlayer)
                    Client.Send(Protocols.ReviveUponHelp, Client.ClientID, Client.TCP);
            }
            else {
                ReviveBarCounter += Time.deltaTime;
                ReviveMask.fillAmount = ReviveBarCounter / Patch.ReviveTime;
            }
        }        
    }           
         

    void ShowNameUpdate() {
        if (GameManager.ShowNames) {
            Name.gameObject.SetActive(true);
            //if (OC is Player)
            //    Name.gameObject.SetActive(true);
            //else if (!OC.Alive)
            //    Name.gameObject.SetActive(false);
        }
        else
            Name.gameObject.SetActive(false);
    }
}

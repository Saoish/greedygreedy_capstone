﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
public static class CombatGenerator {

    public static Damage GenerateRawDirectDamage(ObjectController applyer, float scale_percentage, CR type) {//Must post cal actual amount
        float RawDamage;
        bool Crit;
        if (UnityEngine.Random.value < (applyer.CurrCritChance / 100)) {
            RawDamage = applyer.CurrDamage * (scale_percentage / 100) * (applyer.CurrCritDmg / 100);
            Crit = true;
        }
        else {
            RawDamage = applyer.CurrDamage * (scale_percentage / 100);
            Crit = false;
        }
        return new Damage(RawDamage, applyer.CurrPenetration, Crit, applyer.NetworkID, type, true);
    }

    public static Damage GenerateDirectDamage(ObjectController applyer, ObjectController target, float scale_percentage, CR type) {//Always TraeBack
        float RawDamage;
        bool Crit;
        if (UnityEngine.Random.value < (applyer.CurrCritChance / 100)) {
            RawDamage = applyer.CurrDamage * (scale_percentage / 100) * (applyer.CurrCritDmg / 100);
            Crit = true;
        } else {
            RawDamage = applyer.CurrDamage * (scale_percentage / 100);
            Crit = false;
        }
        return new Damage(RawDamage, target.CurrDefense, applyer.CurrPenetration, Crit, applyer.NetworkID,target.NetworkID,type,true);
    }
    public static Damage GenerateFlatDamage(ObjectController applyer, ObjectController target, float amount, bool Crit, CR type) {//No TraceBack, but applyer can never be null
        return new Damage(amount, 0, 0, Crit, applyer.NetworkID, target.NetworkID, type, false);
    }

    public static Heal GenerateHeal(ObjectController applyer,ObjectController target, float scale_percentage, CR type) {//Always TraceBack
        float RawHeal;
        bool Crit;
        if (UnityEngine.Random.value < (applyer.CurrCritChance / 100)) {
            RawHeal = applyer.CurrDamage * (scale_percentage / 100) * (applyer.CurrCritDmg / 100);
            Crit = true;
        } else {
            RawHeal = applyer.CurrDamage * (scale_percentage / 100);
            Crit = false;
        }
        return new Heal(RawHeal, Crit, applyer.NetworkID,target.NetworkID, type,true);
    }
    public static Heal GenerateFlatHeal(ObjectController applyer,ObjectController target, float amount,bool Crit, CR type) {//No TraceBack
        if (applyer != null)
            return new Heal(amount, Crit, applyer.NetworkID,target.NetworkID, type,false);
        else
            return new Heal(amount, Crit, 0, target.NetworkID, type, false);
    }

    public static EssenseLoss GenerateEssenseCost(ObjectController applyer,ObjectController target, float RawAmount, CR type) {//Non critical but always TraceBack, and applyer is never be null, mostly been use for skill cost
        return new EssenseLoss(RawAmount, false, applyer.NetworkID, target.NetworkID, type, true);
    }
    public static EssenseGain GenerateEssenseGain(ObjectController applyer,ObjectController target, float RawAmount, CR type) {
        return new EssenseGain(RawAmount, false, applyer.NetworkID, target.NetworkID, type, true);
    }

    public static Stats GenerateCurrStatsUponReviveReset(Stats MaxStats) {
        Stats Generated_CurrStats = new Stats(MaxStats);
        Generated_CurrStats.Set(STATSTYPE.HEALTH, Mathf.Ceil(MaxStats.Get(STATSTYPE.HEALTH) * Patch.ReviveUponResetHealthPortion));
        return Generated_CurrStats;       
    }

    public static Stats GenerateCurrStatsUponReviveHelp(Stats MaxStats) {
        Stats Generated_CurrStats = new Stats(MaxStats);
        Generated_CurrStats.Set(STATSTYPE.HEALTH, Mathf.Ceil(MaxStats.Get(STATSTYPE.HEALTH) * Patch.ReviveUponHelp));
        return Generated_CurrStats;
    }

    public static ProjectileData GenerateProjectileData(Projectile P, ObjectController Launcher, Vector2 Direction,float Scale,Damage dmg, ObjectController Homing) {
        Vector2 StartPosition = (P.ScaledColliderRadius(Scale) + Launcher.ObjectCollider.radius) * Direction + Launcher.Position;        
        if (Homing!=null)
            return new ProjectileData(P.gameObject.name, StartPosition, Direction, Scale, dmg, Homing.NetworkID);
        else
            return new ProjectileData(P.gameObject.name, StartPosition, Direction, Scale, dmg, 0);
    }

    //public static ObjectController MP_OverlapCastingHomingTarget() {
    //    float CastRadius = 2f;
    //    int layer = (1 << CollisionLayer.Enemy);
    //    //Collider2D[] Colliders = Physics2D.OverlapCircleAll(CacheManager.MP.Position, CastRadius, layer);//Order is reversed
    //    //if (Colliders.Length == 0)
    //    //    return null;
    //    //else
    //    //    return Colliders[/*Colliders.Length - 1*/0].GetComponent<ObjectController>();//order is reversed

    //    RaycastHit2D[] Hits= Physics2D.CircleCastAll(CacheManager.MP.Position, CastRadius,Vector2.zero,0f, layer);//Order is reversed
    //    if (Hits.Length == 0)
    //        return null;
    //    else
    //        return Hits[Hits.Length - 1].collider.GetComponent<ObjectController>();//order is reversed
    //}

    public static ObjectController MP_RaycastHomingTarget() {//MainPlayer called only, monster homing uses different logic
        Vector2 Direction;
        if (CacheManager.MP.Casting) {
            Direction = ControllerManager.MostUpdatedCastingVector;
        }
        else {
            Direction = ControllerManager.AttackVector;
        }
        float CastRadius = 2f;
        int layer = (1 << CollisionLayer.Enemy);        
        Collider2D[] Colliders = Physics2D.OverlapCircleAll(CacheManager.MP.Position + Direction * (CacheManager.MP.ObjectCollider.radius + CastRadius), CastRadius, layer);//Order is reversed
        if (Colliders.Length == 1) {
            return Colliders[0].GetComponent<ObjectController>();
        }
        else if (Colliders.Length > 1) {//Last two only due to reversed order of Cricle type ray casting rule        
            int Count = Colliders.Length;
            Collider2D BestPick_1 = Colliders[Count - 1];
            Collider2D BestPick_2 = Colliders[Count - 2];
            Ray ray = new Ray(CacheManager.MP.Position, Direction);
            float BestPick_1_Distance = Vector3.Cross(ray.direction, BestPick_1.transform.position - CacheManager.MP.transform.position).magnitude;
            float BestPick_2_Distance = Vector3.Cross(ray.direction, BestPick_2.transform.position - CacheManager.MP.transform.position).magnitude;
            if (BestPick_2_Distance < BestPick_1_Distance) {
                return BestPick_2.GetComponent<ObjectController>();
            }else
                return BestPick_1.GetComponent<ObjectController>();
        }
        return null;
    }

    public static float[] GenerateSkillParamaters(float para) {
        return new float[1] { para };
    }
    public static float[] GenerateSkillParameters(float para, float para1) {
        return new float[2] { para, para1 };
    }
    public static float[] GenerateSkillParameters(float para, float para1, float para2) {
        return new float[3] { para, para1, para2 };
    }

    public static string[] GenerateEffectParameters<T>(T para) {
        return new string[1] { para.ToString() };
    }
    public static string[] GenerateEffectParameters<T, T1>(T para, T1 para1) {
        return new string[2] { para.ToString(), para1.ToString() };
    }
    public static string[] GenerateEffectParameters<T, T1, T2>(T para, T1 para1, T2 para2) {
        return new string[3] { para.ToString(), para1.ToString(), para2.ToString() };
    }
}

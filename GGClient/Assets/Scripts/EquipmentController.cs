﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GreedyNameSpace;
using Visual;

public class EquipmentController : MonoBehaviour {
    public Layer.EquipmentLayerRenderMode ELRM;

    [HideInInspector]
    public Player Master;    

    public List<StatsRangeField> SolidFields;
    public List<StatsRangeField> SpecialFields;    

    [HideInInspector]
    public Equipment E;

    private Animator Anim;
    // Use this for initialization
    void Awake() {       
        Anim = GetComponent<Animator>();
    }

    void Start() {
        if (transform.parent != null) {
            Master = GetComponentInParent<Player>();
        }
        if (GetComponent<WeaponController>()) {
            if (GetComponent<WeaponController>().Type == WEAPONTYPE.MageMagicOrb)
                transform.SetParent(null);
        }
    }

    // Update is called once per frame
    void Update() {
        
    }


    public void EquipUpdate(Player PC) {
        if (E.EquipType == EQUIPTYPE.Trinket) {//No update for trinket for now
            return;
        }
        Anim.SetInteger("Direction", PC.Direction);
        if (PC.InAttackingState) {
            Anim.speed = PC.GetAttackAnimSpeed();
        } else {
            Anim.speed = PC.GetMovementAnimSpeed();
        }
        if (E.EquipType == EQUIPTYPE.Weapon) {//Weapon animation speed is controlled by AttackCollider
            if (PC.Attacking) {
                Anim.SetBool("IsAttacking",true);
                Anim.SetInteger("Direction", PC.Direction);
            } else {
                Anim.SetBool("IsAttacking", false);
            }
            Anim.SetBool("Casting", PC.Casting);
            if (!PC.Casting) {
                Anim.SetBool("Casting", PC.Infusing);
            }
            if (ELRM == Layer.EquipmentLayerRenderMode.MeleeWeapon) {
                if (PC.Direction == 3) {
                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, Layer.Z_Mapper[ELRM].Back);                    
                }
                else {
                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, Layer.Z_Mapper[ELRM].Front);
                }
            }
        } else {//Helmet,Chest,Shackle
            Anim.speed = PC.GetMovementAnimSpeed();
        }
    }

    static public GameObject ObtainPrefabForCharacterSelection(Equipment E,Transform parent) {
        GameObject equipPrefab = Instantiate(Resources.Load("EquipmentPrefabs/" + E.Name)) as GameObject;
        equipPrefab.GetComponent<EquipmentController>().E = E;
        Destroy(equipPrefab.GetComponent<Rigidbody2D>());
        Destroy(equipPrefab.GetComponent<BoxCollider2D>());
        equipPrefab.name = E.Name;        
        Destroy(equipPrefab.transform.Find("LootBox").gameObject);
        Destroy(equipPrefab.transform.Find("Icon").gameObject);
        equipPrefab.transform.parent = parent;
        equipPrefab.transform.localPosition = Vector3.zero;
        equipPrefab.GetComponent<SpriteRenderer>().enabled = true;
        equipPrefab.GetComponent<Animator>().enabled = true;
        equipPrefab.transform.localPosition = new Vector3(0, 0, Layer.Z_Mapper[equipPrefab.GetComponent<EquipmentController>().ELRM].Front);
        return equipPrefab;
    }

    static public GameObject ObtainPrefab(Equipment E,ObjectController OC) {
        GameObject equipPrefab = Instantiate(Resources.Load("EquipmentPrefabs/" + E.Name)) as GameObject;
        equipPrefab.GetComponent<EquipmentController>().E = E;
        Destroy(equipPrefab.GetComponent<Rigidbody2D>());
        Destroy(equipPrefab.GetComponent<BoxCollider2D>());
        equipPrefab.name = E.Name;        
        Destroy(equipPrefab.transform.Find("LootBox").gameObject);
        Destroy(equipPrefab.transform.Find("Icon").gameObject);
        equipPrefab.transform.parent = OC.OZC.transform;
        equipPrefab.transform.localPosition = Vector3.zero;                
        if (E.EquipType == EQUIPTYPE.Weapon) {            
            if (equipPrefab.transform.Find("MeleeAttackCollider") != null) {
                OC.SwapMeleeAttackCollider(equipPrefab.transform.Find("MeleeAttackCollider"));
            }
        }
        equipPrefab.GetComponent<SpriteRenderer>().enabled = true;                
        equipPrefab.GetComponent<Animator>().enabled = true;
        EquipmentController EC = equipPrefab.GetComponent<EquipmentController>();
        //if (EC.ELRM == Layer.EquipmentLayerRenderMode.MagicOrb)
        //    Dynamic_Z_Controller.Attach(equipPrefab);
        //else
        Object_Z_Controller.Assign_Z(equipPrefab.transform, EC.ELRM);       
        return equipPrefab;
    }

    static public GameObject ObtainEquippedIcon(Equipment E, Transform parent) {
        GameObject equipPrefab = Instantiate(Resources.Load("EquipmentPrefabs/" + E.Name)) as GameObject;
        GameObject equipIcon = equipPrefab.transform.Find("Icon").gameObject;        
        equipPrefab.GetComponent<EquipmentController>().E = E;
        equipIcon.SetActive(true);
        equipIcon.name = E.Name;
        equipIcon.transform.position = parent.transform.position + equipIcon.transform.position;
        equipIcon.transform.parent = parent;
        Destroy(equipPrefab);
        return equipIcon;
    }

    static public GameObject ObtainInventoryIcon(Equipment E, Transform parent) {
        GameObject equipPrefab = Instantiate(Resources.Load("EquipmentPrefabs/" + E.Name)) as GameObject;
        GameObject equipIcon = equipPrefab.transform.Find("Icon").gameObject;        
        equipPrefab.GetComponent<EquipmentController>().E = E;
        equipIcon.SetActive(true);
        equipIcon.name = E.Name;
        equipIcon.transform.position = parent.transform.position + equipIcon.transform.position;
        equipIcon.transform.parent = parent;
        equipIcon.transform.localScale = new Vector2(500, 500);
        Destroy(equipPrefab);
        return equipIcon;
    }

    public Equipment GetEquipmentForVendor(Vector2 IlvlRange,float RarityMod) {
        GameObject ToSell = Instantiate(Resources.Load("EquipmentPrefabs/" + E.Name)) as GameObject;
        ToSell.GetComponent<EquipmentController>().RandomlizeEquip(IlvlRange, RarityMod);
        Equipment ToSell_E = new Equipment();
        ToSell_E = ToSell.GetComponent<EquipmentController>().E;
        Destroy(ToSell);
        return ToSell_E;
    }

    public void InstantiateLoot(Vector3 Position, Vector2 ILvlRange, float RarityMod) {
        GameObject Loot = Instantiate(Resources.Load("EquipmentPrefabs/" + E.Name), Position, gameObject.transform.rotation) as GameObject;
        SetUpLootObject(Loot,ILvlRange,RarityMod);
    }    

    void SetUpLootObject(GameObject Loot, Vector2 ILvlRange, float RarityMod) {
        Loot.layer = CollisionLayer.Loot;        
        Loot.name = E.Name;
        Destroy(Loot.transform.Find("Icon").gameObject);
        Loot.GetComponent<EquipmentController>().RandomlizeEquip(ILvlRange, RarityMod);        
        Loot.GetComponent<Collider2D>().enabled = true;
        SemiDynamic_Z_Controller.Attach(Loot);
        Loot.transform.Find("LootBox").gameObject.SetActive(true);
    }    
    

    //Could be public in future
    void RandomlizeEquip(Vector2 ILvlRange, float RarityMod) {
        E.Rarity = GetRandomRarity(RarityMod);
        E.Itemlvl = (int)E.Rarity + GetRandomIlvl(ILvlRange);        
        E.LvlReq = E.Itemlvl - (int)E.Rarity;
        GenerateSolidFields();
        GenerateSpecialFields();
        E.Value = Mathf.FloorToInt(Random.Range(0.8f, 1f)*100 * E.Itemlvl);      
    }

    void GenerateSolidFields() {
        for (int i = 0; i < SolidFields.Count; i++) {
            if (SolidFields[i].stats_range != Vector2.zero) {
                if (Duplicated(i))
                    Debug.Log(E.Name + ": " + SolidFields[i].type + " is duplicated.");
                else if (SolidFields[i].stats_range != Vector2.zero) {
                    float Stats = GenerateStats(E, SolidFields[i]);
                    E.Stats.Add(SolidFields[i].type, Stats);
                }
            }
        }
    }

    void GenerateSpecialFields() {
        if (SpecialFields.Count == 0)
            return;

        int FieldsToAddUp = 0;
        switch (E.Rarity) {
            case RARITY.Common:        
                return;
            case RARITY.Fine:
                FieldsToAddUp = 1;
                break;
            case RARITY.Pristine:
                FieldsToAddUp = 2;
                break;
            case RARITY.Legendary:
                FieldsToAddUp = 3;
                break;
            case RARITY.Mythic:
                FieldsToAddUp = 4;
                break;
        }
        List<int> Available = new List<int>();        
        for (int i = 0; i < SpecialFields.Count; i++) {
            Available.Add((int)SpecialFields[i].type);
        }        
        if (FieldsToAddUp > Available.Count) {            
            Debug.Log("Special Fields Generation Invalid: FieldsToAddUp > Available.");
            return;
        }
        //Debug.Log("------------------------");
        //Debug.Log("***" + E.Name + "***");
        while (FieldsToAddUp > 0) {
            int Rand = Random.Range(0, Available.Count);            
            int AddFieldIndex = Available[Rand];
            //Debug.Log(AddFieldIndex);
            E.Stats.Add(AddFieldIndex, GenerateStats(E, SpecialFields[AddFieldIndex]));
            Available.Remove(AddFieldIndex);
            FieldsToAddUp--;
        }
        //Debug.Log("------------------------");
    }

    int GetRandomIlvl(Vector2 ILvlRange) {
        int Randomlvl = (int)Random.Range(ILvlRange.x, ILvlRange.y + 1);
        return Randomlvl;
    }

    RARITY GetRandomRarity(float RarityMod) {        
        float R_Rate = Random.value;
        if (R_Rate <= (RarityMod / 100) + ((float)RARITYRATE.Mythic / 100)) {
            return RARITY.Mythic;
        } else if (R_Rate <= (RarityMod / 100) + ((float)RARITYRATE.Legendary / 100)) {
            return RARITY.Legendary;
        } else if (R_Rate <= (RarityMod / 100) + ((float)RARITYRATE.Pristine / 100)) {
            return RARITY.Pristine;
        } else if(R_Rate <= (RarityMod / 100) + ((float)RARITYRATE.Fine / 100)) {
            return RARITY.Fine;
        } else {
            return RARITY.Common;
        }
    }

    bool Duplicated(int index) {
        for(int i =0; i < SolidFields.Count; i++) {
            if (i == index)
                continue;
            else
                return SolidFields[i].type == SolidFields[index].type;
        }
        return false;
    }

    float GenerateStats(Equipment _E, StatsRangeField _EF) {        
        float LowerBound = Mathf.Max(_EF.stats_range.y * (float)_E.Rarity / 10, _EF.stats_range.x);        
        float HigherBound = _E.Rarity == RARITY.Mythic ? _EF.stats_range.y : (_EF.stats_range.y - _EF.stats_range.x) * (((float)_E.Rarity + 2) / 2) * 0.2f + _EF.stats_range.x;        
        float Stats = Random.Range(LowerBound, HigherBound);
        //if (_E.Itemlvl < Patch.MaxItemlvl) {
        //    Stats = (Stats / ((Patch.MaxItemlvl+10 - _E.Itemlvl) * Patch.Itemlvl_Scale));            
        //}

        int DownScaleTimes = Patch.LvlCap - _E.LvlReq;
        if(DownScaleTimes>0) {
            Stats = Stats / ((DownScaleTimes+10) * Patch.ItemStats_ScaleDown);
        }

        if ((int)STATSTYPE.DEFENSE <= (int)_EF.type && (int)_EF.type <= (int)STATSTYPE.ESSENCE_REGEN) {
            if(_EF.type == STATSTYPE.HEALTH_REGEN)
                Stats = (float)System.Math.Round(Stats, 0);
            Stats = (float)System.Math.Round(Stats, 1);
        }
        else
            Stats = (float)System.Math.Round(Stats, 0);
        //if(_EF.type == STATSTYPE.DAMAGE)
        //    Debug.Log(_E.Itemlvl + "---" + _EF.type + ": " + LowerBound + ", " + HigherBound + ">>>" + Stats);
        return Stats;
    }
}

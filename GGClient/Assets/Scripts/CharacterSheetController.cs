﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CharacterSheetController : InteractionContent{
    [HideInInspector]
    public Tab_0 Tab_0;
    [HideInInspector]
    public Tab_1 Tab_1;

    [HideInInspector]
    public MainPlayer MPC;

    [HideInInspector]
    public int CachedTabIndex = 0;

    int CurrentTabIndex = 0;

    public EquipmentInfo EI;
    private GameObject CachedButtonOJ;

    void Awake() {
        MPC = transform.parent.GetComponent<MainPlayerUI>().MPC;
        Tab_0 = transform.Find("Tab_0").GetComponent<Tab_0>();
        Tab_1 = transform.Find("Tab_1").GetComponent<Tab_1>();
        EI = (Instantiate(EI.gameObject, Tab_0.transform) as GameObject).GetComponent<EquipmentInfo>();
        EI.transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(-1.009f, -0.1f);
        EI.transform.GetComponent<RectTransform>().localScale = new Vector2(0.001f, 0.001f);
    }

	void Start (){
        TurnOff();
    }

    void Update() {
        TabUpdate();
    }

    protected override void _Interrupt() {        
        CachedButtonOJ = EventSystem.current.currentSelectedGameObject;
        SyncActions = false;
        GetComponent<CanvasGroup>().interactable = false;
    }

    protected override void _Resume() {
        SyncActions = true;
        GetComponent<CanvasGroup>().interactable = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(CachedButtonOJ);
    }

    public void UnSyncCSCActions() {
        SyncActions = false;
        GetComponent<CanvasGroup>().interactable = false;
    }
    public void ResumeCSCSyncActions() {
        SyncActions = true;
        GetComponent<CanvasGroup>().interactable = true;
    }

    public override void TurnOn() {
        ControllerManager.SyncActions = false;
        gameObject.SetActive(true);
        CurrentTabIndex = CachedTabIndex;
        switch (CachedTabIndex) {
            case 0:
                Tab_0.TurnOn();
                break;
            case 1:
                Tab_1.TurnOn();
                break;
        }
    }

    public override void TurnOff() {
        if (!gameObject.active)
            return;        
        CachedTabIndex = CurrentTabIndex;
        switch (CurrentTabIndex) {
            case 0:
                Tab_0.TurnOff();
                break;
            case 1:
                Tab_1.TurnOff();
                break;
        }        
        gameObject.SetActive(false);
        ControllerManager.SyncActions = true;
    }

    public bool IsOn() {
        return gameObject.active;
    }

    private void TabUpdate() {
        if (!SyncActions)
            return;
        //if (ControllerManager.Actions.Flip.WasPressed) {
        //    switch (CurrentTabIndex) {
        //        case 0:
        //            Tab_0.TurnOff();
        //            Tab_1.TurnOn();
        //            CurrentTabIndex = 1;
        //            break;
        //        case 1:
        //            Tab_1.TurnOff();
        //            Tab_0.TurnOn();
        //            CurrentTabIndex = 0;
        //            break;
        //    }
        //}
        else if (ControllerManager.Actions.Prev.WasPressed) {
            switch (CurrentTabIndex) {
                case 0:
                    Tab_0.TurnOff();
                    Tab_1.TurnOn();
                    CurrentTabIndex = 1;
                    break;
                case 1:
                    Tab_1.TurnOff();
                    Tab_0.TurnOn();
                    CurrentTabIndex = 0;
                    break;
            }
        }
        else if (ControllerManager.Actions.Next.WasPressed) {
            switch (CurrentTabIndex) {
                case 0:
                    Tab_0.TurnOff();
                    Tab_1.TurnOn();
                    CurrentTabIndex = 1;
                    break;
                case 1:
                    Tab_1.TurnOff();
                    Tab_0.TurnOn();
                    CurrentTabIndex = 0;
                    break;
            }
        }
        else if (ControllerManager.Actions.Cancel.WasPressed) {
            TurnOff();
        }
        else if (ControllerManager.Actions.Inventory.WasPressed) {
            if (Tab_0.IsOn()) {
                TurnOff();
            }
            else {
                Tab_1.TurnOff();
                Tab_0.TurnOn();
                CurrentTabIndex = 0;
            }
        }
        else if (ControllerManager.Actions.SkillTree.WasPressed) {
            if (Tab_1.IsOn()) {
                TurnOff();
            }
            else {
                Tab_0.TurnOff();
                Tab_1.TurnOn();
                CurrentTabIndex = 1;
            }
        }
    }
}

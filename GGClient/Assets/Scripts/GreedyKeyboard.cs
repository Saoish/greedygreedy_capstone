﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GreedyKeyboard : MonoBehaviour {
    //[HideInInspector]
    //public bool Active = false;

    public static GreedyKeyboard instance;
    public Button CapsLock;
    public Button Backspace;
    public CanvasGroup CG;    
    public InputField InputField;        
    public GameObject FirstSelected;

    public Animator Anim;
    private GreedyInputField CachedGIF;

    private Coroutine TurningOffCO;
    
    public void SetUp() {
        instance = this;
    }        

    public static bool IsActive {
        get {
            return instance.gameObject.active;
        }
    }   

    public static void SlideIn(GreedyInputField GIF) {        
        instance.InputField.text = GIF.text;        
        instance.InputField.characterLimit = GIF.characterLimit;
        instance.CachedGIF = GIF;
        instance.CachedGIF.CG.interactable = false;
        instance.gameObject.SetActive(true);
        instance.CG.interactable = true;        
        instance.Anim.SetBool("In", true);
        instance.Anim.SetBool("Out", false);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(instance.FirstSelected);                
    }
    
    public static void CancelSlideOut() {
        if (!instance.gameObject.active || instance.TurningOffCO != null)
            return;
        instance.CG.interactable = false;
        instance.Anim.SetBool("In", false);
        instance.Anim.SetBool("Out",true);        
        instance.CachedGIF.CG.interactable = true;        
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(instance.CachedGIF.gameObject);
        instance.TurningOffCO = instance.StartCoroutine(instance.TurnOffWithDelay());
    }

    public void OnButtonClickConfirmSlideOut() {        
        if (!instance.gameObject.active || instance.TurningOffCO != null)
            return;
        instance.CG.interactable = false;
        instance.Anim.SetBool("In", false);
        instance.Anim.SetBool("Out", true);
        instance.CachedGIF.text = instance.InputField.text;
        instance.CachedGIF.CG.interactable = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(instance.CachedGIF.gameObject);
        instance.TurningOffCO = instance.StartCoroutine(instance.TurnOffWithDelay());
    }

    public static void ConfirmSlideOut() {        
        if (!instance.gameObject.active || instance.TurningOffCO!=null)
            return;
        instance.CG.interactable = false;
        instance.Anim.SetBool("In", false);
        instance.Anim.SetBool("Out", true);
        instance.CachedGIF.text = instance.InputField.text;
        instance.CachedGIF.CG.interactable = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(instance.CachedGIF.gameObject);
        instance.TurningOffCO = instance.StartCoroutine(instance.TurnOffWithDelay());
    }

    IEnumerator TurnOffWithDelay() {
        yield return new WaitForSeconds(1f);
        TurningOffCO = null;
        gameObject.SetActive(false);        
    }

    void Update() {        
        if (ControllerManager.Actions.Cancel.WasPressed) {
            CancelSlideOut();
        }
        else if (ControllerManager.Actions.Menu.WasPressed) {
            ConfirmSlideOut();
        }
        else if (ControllerManager.Actions.Inventory.WasPressed || ControllerManager.Actions.Inventory.WasRepeated) {
            Backspace.onClick.Invoke();
        }
        else if (ControllerManager.Actions.SkillTree.WasPressed) {
            CapsLock.onClick.Invoke();
        }
    }
}

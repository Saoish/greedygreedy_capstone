﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using System;

public class StaffMastery : PassiveSkill {
    public override CR Type { get { return CR.StaffMastery; } }
    float CritDamage_INC_Percentage;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        StaffMasterylvl SML = (StaffMasterylvl)AllLvls[Index];
        return "\nIncrease your " + MyText.Colofied("Critical Damage", ScaleHighlight) + " by " + MyText.Colofied(SML.CritDamage_INC_Percentage + "%", ScaleHighlight) + " when you have " + MyText.Colofied(WeaponTypeInString.GetString(WEAPONTYPE.MageStaff), ScaleHighlight) + MyText.Colofied(" (Coming Soon)", WarningHighlight) + " equipped.";
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        StaffMasterylvl SML = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                SML = GetComponent<StaffMastery1>();
                break;
            case 2:
                SML = GetComponent<StaffMastery2>();
                break;
            case 3:
                SML = GetComponent<StaffMastery3>();
                break;
            case 4:
                SML = GetComponent<StaffMastery4>();
                break;
            case 5:
                SML = GetComponent<StaffMastery5>();
                break;
        }
        CritDamage_INC_Percentage = SML.CritDamage_INC_Percentage;
        GenerateDescription();
    }

    public override void ApplyPassive() {
        OC.AddMaxStats(STATSTYPE.CRIT_DMG, CritDamage_INC_Percentage);
    }
}

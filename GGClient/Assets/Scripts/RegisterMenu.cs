﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Networking.Data;

public class RegisterMenu : MonoBehaviour {
    public CanvasGroup CG;
    public GameObject RootButtonsOJ;
    public GameObject LoginButtonOJ;
    public GameObject RegisterButtonOJ;
    public GameObject BackButtonOJ;
    public GreedyInputField Username;
    //public InputField Password;    

    public void Register() {//This function will be disable after hook up with Steam login
        if (Username.text == "" /*|| Password.text == ""*/) {
            PopUpNotification.Push("Username is invalid.", PopUpNotification.Type.Confirm);
        }
        else {
            PopUpNotification.Push("Waiting for server...", PopUpNotification.Type.Pending);
            Client.Connect();
            StartCoroutine(CheckConnectionAndSendRequest(0.5f));
        }
    }

    public void EnableForRegisteration() {
        if(Client.ConnectMode == Client.Mode.Local)
            FindObjectOfType<IPFetcher>().UpdateIP();
        if (Client.ip == string.Empty) {
            PopUpNotification.Push("Please enter IP.", PopUpNotification.Type.Confirm);
            return;
        }
        Username.text = "";
        //Password.text = "";
        RootButtonsOJ.SetActive(false);
        LoginButtonOJ.SetActive(false);
        RegisterButtonOJ.SetActive(true);
        gameObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(RegisterButtonOJ);
    }

    //public void EnableForLogin() {
    //    Username.text = "";
    //    //Password.text = "";
    //    RootButtonsOJ.SetActive(false);
    //    LoginButtonOJ.SetActive(true);
    //    RegisterButtonOJ.SetActive(false);
    //    gameObject.SetActive(true);
    //}

    public void BackFromRegistMenu() {
        gameObject.SetActive(false);
        RootButtonsOJ.SetActive(true);                     
    }

    //public void EnableInteraction() {
    //    GetComponent<CanvasGroup>().interactable = true;
    //}
    //public void DisableInteraction() {
    //    GetComponent<CanvasGroup>().interactable = false;
    //}
    

    private IEnumerator CheckConnectionAndSendRequest(float time) {                       
        yield return new WaitForSeconds(time);
        if (!Client.Connected) {
            PopUpNotification.Push("No connection to server.", PopUpNotification.Type.Confirm);
        } else {            
            Client.Send(Protocols.RegisterUser, Username.text, Client.TCP);
        }
    }

    private void Update() {
        if (ControllerManager.Actions.Cancel.WasPressed && CG.interactable) {
            BackFromRegistMenu();
        }
    }
}

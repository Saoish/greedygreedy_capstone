﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
using System;

public class EyeoftheStorm : PassiveSkill {
    public override CR Type { get { return CR.EyeoftheStorm; } }
    private float TriggerChance;
    private float Force;    
    float ICD_Counter = 0f;
    const float ICD = 30f;

    private GameObject VFX;
    public AudioClip SFX;    

    float VFX_StayTime;
    float Root_VFX_DefaultStartSize;
    float Pulse_VFX_DefaultStartSize;
    float SubEmitterBirthVFX_DefaultStartSize;

    CircleCollider2D SelfCollider;
    float RadiusScaleFactor = 0.4f;
    private float ColliderStayTime = 0.1f;

    public Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        EyeoftheStormlvl EOTSL = (EyeoftheStormlvl)AllLvls[Index];
        return "\nUpon taking direct damage, you have " + MyText.Colofied(EOTSL.TriggerChance + "%", ScaleHighlight) + " chance to release Eye of the Storm to push enemies " + MyText.Colofied(EOTSL.Force + " meters", ScaleHighlight) + " away. Eye of the Storm can only be triggered once every "+ MyText.Colofied(ICD, ScaleHighlight) + " secs.";
    }

    protected override void Awake() {
        base.Awake();
        SelfCollider = GetComponent<CircleCollider2D>();
        VFX = transform.Find("Eye Of Storm VFX").gameObject;
        //Root_VFX_DefaultStartSize = transform.Find("Eye Of Storm VFX").GetComponent<ParticleSystem>().startSize;
        //Pulse_VFX_DefaultStartSize = transform.Find("Eye Of Storm VFX/pulse").GetComponent<ParticleSystem>().startSize;
        //SubEmitterBirthVFX_DefaultStartSize = transform.Find("Eye Of Storm VFX/pulse/SubEmitterBirth").GetComponent<ParticleSystem>().startSize;
        VFX_StayTime = transform.Find("Eye Of Storm VFX").GetComponent<ParticleSystem>().duration;
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();
        if (ICD_Counter > 0) {
            ICD_Counter -= Time.fixedDeltaTime;
        }
        else {
            ICD_Counter = 0;
        }
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        EyeoftheStormlvl EOTSL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                EOTSL = GetComponent<EyeoftheStorm1>();
                SelfCollider.radius = 0.4f;
                break;
            case 2:
                EOTSL = GetComponent<EyeoftheStorm2>();
                SelfCollider.radius = 0.5f;
                break;
            case 3:
                EOTSL = GetComponent<EyeoftheStorm3>();
                SelfCollider.radius = 0.6f;
                break;
            case 4:
                EOTSL = GetComponent<EyeoftheStorm4>();
                SelfCollider.radius = 0.7f;
                break;
            case 5:
                EOTSL = GetComponent<EyeoftheStorm5>();
                SelfCollider.radius = 0.8f;
                break;
        }        
        TriggerChance = EOTSL.TriggerChance;
        Force = EOTSL.Force;
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), OC.ObjectCollider);//Ignore self here
        float VFX_Scale = SelfCollider.radius / RadiusScaleFactor;
        transform.Find("Eye Of Storm VFX").GetComponent<ParticleSystem>().startSize *= VFX_Scale;
        transform.Find("Eye Of Storm VFX/pulse").GetComponent<ParticleSystem>().startSize *= VFX_Scale; 
        transform.Find("Eye Of Storm VFX/pulse/SubEmitterBirth").GetComponent<ParticleSystem>().startSize *= VFX_Scale;
        GenerateDescription();
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.transform.GetComponent<ObjectController>();
        Push(target);
        HittedStack.Push(collider);
    }
    void Push(ObjectController target) {
        if (Client.Connected) {
            NetworkPush(target);
        }
        else {
            LocalPush(target);
        }
    }

    void NetworkPush(ObjectController target) {
        if (target is MainPlayer) {
            //ControllerManager.SyncMoveVector = false;
            //ControllerManager.SyncAttackVector = false;
            SendForce(target);
        }
        //else if (OC is MainPlayer && target is Monster) {
        //    SendForce(target);
        //}
        else if (target is Monster) {            
            LocalPush(target);
        }
    }
    void SendForce(ObjectController target) {
        Vector2 BouceOffDirection = Vector3.Normalize(target.transform.position - OC.transform.position);
        target.NormalizeDrag();
        HandyNetwork.SendForce(target, BouceOffDirection * Force, GreedyForceMode.Override);
        //Client.Send(Protocols.ObjectAddForce, new AddForceData(target.NetworkID, BouceOffDirection * Force, GreedyForceMode.Override), Client.UDP);
    }

    void LocalPush(ObjectController target) {
        Vector2 BouceOffDirection = Vector3.Normalize(target.transform.position - OC.transform.position);
        target.NormalizeDrag();
        target.AddForce(BouceOffDirection * Force, GreedyForceMode.Override);
    }

    public override void TriggerByNetwork(float[] Paramaters) {        
        StartCoroutine(ActiveCollider(ColliderStayTime));        
        StartCoroutine(RunVFX(VFX_StayTime));
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    public override void ApplyPassive() {
        if (OC is MainPlayer)
            OC.ON_HEALTH_LOSS += EyeOfStormPassive;
    }

    void EyeOfStormPassive(Damage dmg) {
        if (Client.Connected)
            NetworkPassive(dmg);
        else
            LocalPassive(dmg);
    }

    private void NetworkPassive(Damage dmg) {
        if (dmg.TraceBack) {
            if (dmg.Type == CR.None)
                Debug.Log("Some dmg is not registered");
            else if (UnityEngine.Random.value < (TriggerChance / 100) && ICD_Counter==0) {
                HandyNetwork.SendSkillActivation(OC, Type, null);
                //Client.Send(Protocols.ObjectActiveSkill, new SkillActivationData(OC.NetworkID, Type, null), Client.UDP);
                ICD_Counter = ICD;
            }
        }
    }

    private void LocalPassive(Damage dmg) {
        if (dmg.TraceBack) {
            if (dmg.Type == CR.None)
                Debug.Log("Some dmg is not registered");
            else if (UnityEngine.Random.value < (TriggerChance / 100) && ICD_Counter == 0) {
                StartCoroutine(ActiveCollider(ColliderStayTime));
                StartCoroutine(RunVFX(VFX_StayTime));
                AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
                ICD_Counter = ICD;                
            }
        }
    }

    IEnumerator ActiveCollider(float time) {
        SelfCollider.enabled = true;
        yield return new WaitForSeconds(time);
        SelfCollider.enabled = false;
        HittedStack.Clear();
    }

    IEnumerator RunVFX(float time) {
        VFX.SetActive(true);
        yield return new WaitForSeconds(time);
        VFX.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;

public class Focus : ActiveSkill {
    public override CR Type { get { return CR.Focus; } }

    [HideInInspector]
    public float Duration;
    [HideInInspector]
    public float Haste_INC_Percentage;

    public AudioClip SFX;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Focuslvl FL = (Focuslvl)AllLvls[Index];
        return "\nBoost your "+ MyText.Colofied("Haste",ScaleHighlight)+" by " + MyText.Colofied(FL.Haste_INC_Percentage + "%", ScaleHighlight) + " for " + MyText.Colofied(FL.Duration + " secs", ScaleHighlight) + "\n\nCost: " + MyText.Colofied(FL.EssenceCost + " Essence", ScaleHighlight) + "\nCD: " + MyText.Colofied(FL.CD + " secs", ScaleHighlight);
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Focuslvl FL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                FL = GetComponent<Focus1>();
                break;
            case 2:
                FL = GetComponent<Focus2>();
                break;
            case 3:
                FL = GetComponent<Focus3>();
                break;
            case 4:
                FL = GetComponent<Focus4>();
                break;
            case 5:
                FL = GetComponent<Focus5>();
                break;
        }
        CD = FL.CD;
        EssenceCost = FL.EssenceCost;
        Duration = FL.Duration;
        Haste_INC_Percentage = FL.Haste_INC_Percentage;

        GenerateDescription();
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
        if (OC.OID == OID.Main) {
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.FocusBuff, OC.NetworkID, OC.NetworkID, Duration, CombatGenerator.GenerateEffectParameters(Haste_INC_Percentage)), Client.TCP);
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.Focus));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
            RealTime_CD = CD;
        }
    }

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }

    private void ActiveNetwork() {
        HandyNetwork.SendSkillActivation(OC, Type, null);
        //Client.Send(Protocols.ObjectActiveSkill, new SkillActivationData(OC.NetworkID, Type, null), Client.UDP);
    }

    private void ActiveLocal() {
        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
        CacheManager.Effects[CR.FocusBuff].Apply(OC, OC, Duration, CombatGenerator.GenerateEffectParameters(Haste_INC_Percentage));
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
        RealTime_CD = CD;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
public class WickedIntent : PassiveSkill {
    public override CR Type { get { return CR.WickedIntent; } }
    float CritChance_INC_Percentage;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        WickedIntentlvl WIL = (WickedIntentlvl)AllLvls[Index];
        return "\nIncrease your "+ MyText.Colofied("Critical Chance",ScaleHighlight)+" by " + MyText.Colofied(WIL.CritChance_INC_Percentage + "%", ScaleHighlight) + ".";        
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        WickedIntentlvl WIL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                WIL = GetComponent<WickedIntent1>();
                break;
            case 2:
                WIL = GetComponent<WickedIntent2>();
                break;
            case 3:
                WIL = GetComponent<WickedIntent3>();
                break;
            case 4:
                WIL = GetComponent<WickedIntent4>();
                break;
            case 5:
                WIL = GetComponent<WickedIntent5>();
                break;
        }
        CritChance_INC_Percentage = WIL.CritChance_INC_Percentage;
        GenerateDescription();
    }

    public override void ApplyPassive() {
        OC.AddMaxStats(STATSTYPE.CRIT_CHANCE, CritChance_INC_Percentage);
    }
}

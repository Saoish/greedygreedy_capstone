﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
using System;
using Networking.Data;

public abstract class ActiveSkill : Skill {
    [HideInInspector]
    public bool Holding = false;
    public enum ActiveType {
        Instant,        
        Charge,
        Cast
    }

    [HideInInspector]
    public float CD;
    [HideInInspector]
    public float RealTime_CD = 0;
    [HideInInspector]
    public float EssenceCost = 0;

    private GameObject StartCastingVFX;       
    
    public ActiveType active_type;

    protected override void Awake() {
        base.Awake();
    }


    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);        
    }

    protected override void Start () {
        StartCastingVFX = Resources.Load<GameObject>("VFXPrefabs/StartCasting");
        base.Start();

	}

    protected override void Update () {
        base.Update();
        if (OC != null && OC.Stunned) {            
            Interrupt(false);
        }
        if (RealTime_CD > 0)
            RealTime_CD -= (Time.deltaTime + Time.deltaTime*OC.CurrHaste/100);
        else
            ResetCD();
	}

    public virtual void ResetCD() {
        RealTime_CD = 0;
    }

    public float GetCDPortion() {
        //return RealTime_CD / CombatGenerator.GenerateCD(OC, CD);
        return RealTime_CD / CD;
    }

    public bool Ready() {
        if (OC.Stunned) {
            RedNotification.Push(RedNotification.Type.STUNNED);
            return false;
        } else if (RealTime_CD > 0) {
            RedNotification.Push(RedNotification.Type.ON_CD);
            return false;
        } else if (OC.GetCurrStats(STATSTYPE.ESSENCE) - EssenceCost < 0) {
            RedNotification.Push(RedNotification.Type.NO_ESSENCE);
            return false;
        }
        return true;
    }

    public abstract void Active();

    //Non-Instant skill
    public virtual void StartCasting(int ActionSlot) {
        if (OC.OID == OID.Main && Client.Connected)
            Client.Send(Protocols.ObjectStartCasting, new StartCastingData(OC.NetworkID,OC.Position), Client.TCP);
        transform.localPosition = new Vector3(0, 0, 0);
        OC.Casting = true;
        Holding = true;
        OC.ActiveOutsideVFXPartical(StartCastingVFX);        
    }

    public virtual void Interrupt(bool TellServer) {        
        if (TellServer && OC.OID == OID.Main && Client.Connected) {
            Client.Send(Protocols.ObjectInterruptCasting, OC.NetworkID, Client.TCP);
        }
        OC.Casting = false;
        Holding = false;
    }
}

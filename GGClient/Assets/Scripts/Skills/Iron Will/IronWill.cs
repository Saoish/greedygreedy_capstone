﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;
public class IronWill : PassiveSkill {
    public override CR Type { get { return CR.IronWill; } }
    float DEF_INC_Percentage;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        IronWilllvl IWL = (IronWilllvl)AllLvls[Index];
        return "\nIncrease your "+ MyText.Colofied("Defense",ScaleHighlight) +" by " + MyText.Colofied(IWL.DEF_INC_Percentage+ " % ",ScaleHighlight) + ".";
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        IronWilllvl IWL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                IWL = GetComponent<IronWill1>();
                break;
            case 2:
                IWL = GetComponent<IronWill2>();
                break;
            case 3:
                IWL = GetComponent<IronWill3>();
                break;
            case 4:
                IWL = GetComponent<IronWill4>();
                break;
            case 5:
                IWL = GetComponent<IronWill5>();
                break;
        }
        DEF_INC_Percentage = IWL.DEF_INC_Percentage;

        GenerateDescription();
    }

    public override void ApplyPassive() {
        //OC.AddMaxStats(STATSTYPE.DEFENSE, (float)System.Math.Round(OC.GetMaxStats(STATSTYPE.DEFENSE) * (DEF_INC_Percentage / 100),1));
        OC.AddMaxStats(STATSTYPE.DEFENSE, DEF_INC_Percentage);
    }
}

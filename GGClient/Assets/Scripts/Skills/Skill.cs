﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GreedyNameSpace;
using Networking.Data;

public abstract class Skill : MonoBehaviour {
    //For designing purpose only
    public string Name;
    //[HideInInspector]
    public abstract CR Type { get;}    
    [HideInInspector]
    public string Description;
    [HideInInspector]
    public int lvl;
    [HideInInspector]
    public ObjectController OC;

    protected string ChargeHighlight = "#FF69B4";
    protected string ScaleHighlight = "orange";
    protected string WarningHighlight = "red";

    protected abstract string DescriptionTemplate(Skilllvl[] AllLvls, int Index);

    public virtual void GenerateDescription() {        
        Skilllvl[] AllLvls = GetComponents<Skilllvl>();
        Description = "Level: " + lvl + "/" + Patch.MaxSkilllvl+"\n";
        if (lvl == 0) {
            Description += DescriptionTemplate(AllLvls, 0);
        }
        else {
            Description += DescriptionTemplate(AllLvls, lvl - 1);
            if (lvl == Patch.MaxSkilllvl)
                return;
            Description += "\n\nNext Level:\n";
            Description += DescriptionTemplate(AllLvls, lvl);
        }
    }

    protected virtual void Awake() {
        if (Name == "")
            Debug.Log(gameObject.name + " is not registered.");
        else if (GetType().ToString() != Type.ToString()) {
            Debug.Log(GetType().ToString() + ": has non-matched CR.Type(" + Type.ToString() + ")");
        }
        gameObject.layer = CollisionLayer.Skill;
    }

    public Skill Instantiate() {
        GameObject Skill_OJ = Instantiate(Resources.Load("SkillPrefabs/" +Name)) as GameObject;
        Skill_OJ.name = Name;
        return Skill_OJ.GetComponent<Skill>();
    }

    public void Delete() {
        Destroy(gameObject);
    }

    public virtual void InitSkill(ObjectController OC,int lvl = 0) {//Has to be override
        this.lvl = lvl;
        this.OC = OC;
        transform.position = OC.Skills_T().position;
        transform.SetParent(OC.Skills_T());
    }
    
    protected virtual void Start () {	    
	}
    
    protected virtual void Update () {	
	}

    protected virtual void FixedUpdate() {

    }

    public Sprite GetSkillIcon() {
        return GetComponent<Image>().sprite;
    }

    //Networking purpose functions
    public virtual void TriggerByNetwork(float[] Paramaters) {//Not all children use this function

    }


    //Following functions are used for some very naughty skills, like Phantom Charge etc.
    public void DeactiveByNetwork(float TimeOut) {//Not all children use this function
        StartCoroutine(DeactiveSkillWith(TimeOut));
    }    

    private IEnumerator DeactiveSkillWith(float TimeOut) {//Not all children use this function
        yield return new WaitForSeconds(TimeOut);
        if(StillActive)
            Deactive();
    }
         
    public virtual void Deactive() {//Not all children use this function

    }

    protected virtual bool StillActive {//Anyone who wants to use it must ovveride it with its checking condiction
        get { return false; }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
using System;

public class Char : PassiveSkill {
    public override CR Type { get { return CR.Char; } }
    float TriggerChance;
    float Defense_DEC_Percentage; //Relative Nerf

    public float Duration = 5f;


    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Charlvl CL = (Charlvl)AllLvls[Index];
        return "\nUpon dealing dmg, you have " + MyText.Colofied(CL.TriggerChance + "%", ScaleHighlight) + " chance to lower your target's " + MyText.Colofied("Defense", ScaleHighlight) + " by " + MyText.Colofied(CL.Defense_DEC_Percentage + "%", ScaleHighlight) + " for " + Duration + " secs, Char effect does not stack." /*and can not be triggered again within "+TriggerCD+ " secs."*/;
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Charlvl CL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                CL = GetComponent<Char1>();
                break;
            case 2:
                CL = GetComponent<Char2>();
                break;
            case 3:
                CL = GetComponent<Char3>();
                break;
            case 4:
                CL = GetComponent<Char4>();
                break;
            case 5:
                CL = GetComponent<Char5>();
                break;
        }
        TriggerChance = CL.TriggerChance;
        Defense_DEC_Percentage = CL.Defense_DEC_Percentage;

        GenerateDescription();
    }

    public override void ApplyPassive() {
        if (OC.OID == OID.Main)
            OC.ON_DMG_DEAL += CharPassive;
    }

    void CharPassive(Damage dmg) {
        if (Client.Connected)
            NetworkPassive(dmg);
        else
            LocalPassive(dmg);
    }

    void NetworkPassive(Damage dmg) {
        if (CombatChecker.IgnoreApplyingEffect(dmg.targetID, Type))
            return;
        if (UnityEngine.Random.value < (TriggerChance / 100)) {
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.CharDebuff, OC.NetworkID, dmg.targetID, Duration, CombatGenerator.GenerateEffectParameters(Defense_DEC_Percentage)), Client.TCP);
        }
    }

    void LocalPassive(Damage dmg) {
        if (CombatChecker.IgnoreApplyingEffect(dmg.targetID, Type))
            return;
        if (UnityEngine.Random.value < (TriggerChance / 100)) {
            CacheManager.Effects[CR.CharDebuff].Apply(OC, CacheManager.GetOC(dmg.targetID), Duration, CombatGenerator.GenerateEffectParameters(Defense_DEC_Percentage));
        }
    }
}

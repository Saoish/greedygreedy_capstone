﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
using System;

public class Chill : PassiveSkill {
    public override CR Type { get { return CR.Chill; } }
    [HideInInspector]
    public float TriggerChance;
    [HideInInspector]
    public float MOVESPD_DEC_Percentage;

    public float Duration = 5;
    
    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Chilllvl CL = (Chilllvl)AllLvls[Index];
        return "\nUpon dealing damage, you have " + MyText.Colofied(CL.TriggerChance + "%", ScaleHighlight) + " chance to slow down enemy movement speed by " + MyText.Colofied(CL.MOVESPD_DEC_Percentage + "%", ScaleHighlight) + " for " + Duration + " secs.";
    }


    public override void InitSkill(ObjectController OC, int lvl = 0) {
        base.InitSkill(OC, lvl);
        Chilllvl CL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                CL = GetComponent<Chill1>();
                break;
            case 2:
                CL = GetComponent<Chill2>();
                break;
            case 3:
                CL = GetComponent<Chill3>();
                break;
            case 4:
                CL = GetComponent<Chill4>();
                break;
            case 5:
                CL = GetComponent<Chill5>();
                break;
        }
        TriggerChance = CL.TriggerChance;
        MOVESPD_DEC_Percentage = CL.MOVESPD_DEC_Percentage;
        GenerateDescription();
    }

    protected override void Start() {
        base.Start();
    }

    protected override void Update() {
        base.Update();
    }

    public override void ApplyPassive() {
        if (OC is MainPlayer)
            OC.ON_DMG_DEAL += ChillPassive;
    }

    void ChillPassive(Damage dmg) {
        if (Client.Connected)
            NetworkPassive(dmg);
        else
            LocalPassive(dmg);            
    }

    void NetworkPassive(Damage dmg) {
        if (CombatChecker.IgnoreApplyingEffect(dmg.targetID, Type))
            return;
        if (UnityEngine.Random.value < (TriggerChance / 100)) {
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.ChillDebuff, OC.NetworkID, dmg.targetID, Duration, CombatGenerator.GenerateEffectParameters(MOVESPD_DEC_Percentage)), Client.TCP);
        }
    }

    void LocalPassive(Damage dmg) {
        if (CombatChecker.IgnoreApplyingEffect(dmg.targetID, Type))
            return;
        if (UnityEngine.Random.value < (TriggerChance / 100)) {
            CacheManager.Effects[CR.ChillDebuff].Apply(OC, CacheManager.GetOC(dmg.targetID), Duration, CombatGenerator.GenerateEffectParameters(MOVESPD_DEC_Percentage));
        }
    }

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;

public class Cleave : ActiveSkill {
    public override CR Type { get { return CR.Cleave; } }
    public GameObject HitVFX;    
    float DamageScale;
    [HideInInspector]
    public float ScalingFactor;

    public static float ChargeCap = 3f;

    private Animator Anim;
    private CleaveIndicator Indicator;

    public AudioClip SFX;

    public Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Cleavelvl CL = (Cleavelvl)AllLvls[Index];
        return "\n" + MyText.Colofied("Charge[" + ChargeCap + "]", ChargeHighlight) + " up a demon axe to deal " + MyText.Colofied(CL.DamageScale + "% + charge * " + CL.DamageScale + "% damage", ScaleHighlight) + " to enemies infront of you and knock them away. The demon axe would also refelect any hit hostile projectiles.\n\nCost: " + MyText.Colofied(CL.EssenceCost + " Essence", ScaleHighlight) + "\nCD: " + MyText.Colofied(CL.CD + " secs", ScaleHighlight);
    }

    protected override void Awake() {
        base.Awake();
        Anim = GetComponent<Animator>();
        GetComponent<SpriteRenderer>().sortingLayerName = SortingLayer.Skill;
        Indicator = GetComponentInChildren<CleaveIndicator>(true);
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Cleavelvl CL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                CL = GetComponent<Cleave1>();
                break;
            case 2:
                CL = GetComponent<Cleave2>();
                break;
            case 3:
                CL = GetComponent<Cleave3>();
                break;
            case 4:
                CL = GetComponent<Cleave4>();
                break;
            case 5:
                CL = GetComponent<Cleave5>();
                break;
        }
        CD = CL.CD;
        EssenceCost = CL.EssenceCost;
        DamageScale = CL.DamageScale;        
        Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), OC.ObjectCollider);
        GenerateDescription();
    }

    public override void StartCasting(int ActionSlot) {
        base.StartCasting(ActionSlot);
        Indicator.Active(ActionSlot);
    }

    public override void Interrupt(bool TellServer) {
        base.Interrupt(TellServer);        
        Indicator.Deactive();
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        Interrupt(false);
        ScalingFactor = Paramaters[0];
        transform.localScale = new Vector3(Paramaters[0], Paramaters[0], 0);
        transform.localEulerAngles = new Vector3(0, 0, Paramaters[1]);
        Anim.SetTrigger("Active");
        StartCoroutine(Reset(1));
    }

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }

    public void ActiveNetwork() {//Will only be called by mainplayer
        Indicator.Deactive();
        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);//Local calculated
        float[] Parameters = CombatGenerator.GenerateSkillParameters(ScalingFactor, Indicator.Z_Angle);
        HandyNetwork.SendSkillActivation(OC, Type, Parameters);
        if (OC.OID == OID.Main) {            
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
            RealTime_CD = CD;
        }
    }

    public void ActiveLocal() {
        Interrupt(false);  

        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);//Local calculated

        transform.localScale = new Vector3(ScalingFactor, ScalingFactor, 0);
        transform.localEulerAngles = new Vector3(0, 0, Indicator.Z_Angle);
        
        Anim.SetTrigger("Active");
        StartCoroutine(Reset(1));

        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
        RealTime_CD = CD;
    }    

    void OnTriggerEnter2D(Collider2D collider) {        
        if(collider.gameObject.layer == CollisionLayer.Projectile) {//***More debugging needed. FriendlyMonster's projectile owner will be subscriped with its master
            Projectile P = collider.GetComponent<Projectile>();
            if (OC is MainPlayer && P.Owner.OID == OID.FriendlyPlayer) {
                return;
            }
            else if (OC.OID == OID.FriendlyPlayer && P.Owner is MainPlayer || P.Owner.OID == OID.FriendlyPlayer) {
                return;
            }
            else if (OC.OID == OID.EnemyPlayer && P.Owner.OID == OID.EnemyPlayer)
                return;
            else {
                if (P.HomingTarget)
                    P.HomingTarget = P.Owner;
                P.Owner = OC;
                P.Force *= -1;
                return;
            }
        }
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.transform.GetComponent<ObjectController>();
        Damage dmg = CombatGenerator.GenerateDirectDamage(OC, target, DamageScale + ScalingFactor * DamageScale, Type);
        OC.ON_DMG_DEAL += DealCleaveDmg;
        OC.ON_DMG_DEAL(dmg);
        OC.ON_DMG_DEAL -= DealCleaveDmg;
        HittedStack.Push(collider);
    }

    void DealCleaveDmg(Damage dmg) {
        if (Client.Connected)
            NetworkDealCleaveDmg(dmg);
        else
            LocalDealCleaveDmg(dmg);
    }

    private void NetworkDealCleaveDmg(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        Push(target);
        if (OC.OID == OID.Main) {            
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
        }
        target.ActiveOutsideVFXPartical(HitVFX);
    }

    private void LocalDealCleaveDmg(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        Push(target);        
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;        
        target.ActiveOutsideVFXPartical(HitVFX);
    }

    void Push(ObjectController target) {
        if (Client.Connected) {
            NetworkPush(target);
        }
        else {
            LocalPush(target);
        }
    }

    void NetworkPush(ObjectController target) {
        if (target is MainPlayer) {                
            //ControllerManager.SyncMoveVector = false;
            //ControllerManager.SyncAttackVector = false;
            SendForce(target);
        }
        //else if (OC is MainPlayer && target is Monster) {
        //    SendForce(target);
        //}
        else if (target is Monster) {//Monster been fly off locally
            LocalPush(target);
        }
    }
    void SendForce(ObjectController target) {
        Vector2 BouceOffDirection = Vector3.Normalize(target.Position - OC.Position);
        target.NormalizeDrag();
        HandyNetwork.SendForce(target, BouceOffDirection * (Mathf.Floor(ScalingFactor) * 3 + 1), GreedyForceMode.Override);
        //Client.Send(Protocols.ObjectAddForce, new AddForceData(target.NetworkID, BouceOffDirection * (Mathf.Floor(ScalingFactor) * 3 + 1), GreedyForceMode.Override), Client.UDP);
    }

    void LocalPush(ObjectController target){        
        Vector2 BouceOffDirection = Vector3.Normalize(target.transform.position - OC.transform.position);
        target.NormalizeDrag();
        target.AddForce(BouceOffDirection * (Mathf.Floor(ScalingFactor) * 3 + 1), GreedyForceMode.Override);
    }

    IEnumerator Reset(float time) {
        yield return new WaitForSeconds(time);
        transform.localScale = Vector3.one;
        transform.localEulerAngles = Vector3.zero;
    }
}

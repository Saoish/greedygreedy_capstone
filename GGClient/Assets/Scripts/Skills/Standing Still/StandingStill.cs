﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;
using Networking.Data;
public class StandingStill : ActiveSkill {
    public override CR Type { get { return CR.StandingStill; } }
    public float Duration = 10;
    float Heal_MaxHP_Percentage;
    float DotHeal_MaxHP_Percentage;

    public AudioClip SFX;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        StandingStilllvl SSL = (StandingStilllvl)AllLvls[Index];
        return "\nInstantly gain " + MyText.Colofied(SSL.Heal_MaxHP_Percentage+"%",ScaleHighlight) + " of your MAX HP back and a healing buff to heal " + MyText.Colofied(SSL.DotHeal_MaxHP_Percentage + "%", ScaleHighlight) + " of your MAX HP you every second for " + Duration + " secs.\n\nCost: " + MyText.Colofied(SSL.EssenceCost + " Essence", ScaleHighlight) + "\nCD: " + MyText.Colofied(SSL.CD + " secs", ScaleHighlight);
    }

    //public override void GenerateDescription() {
    //    StandingStilllvl[] AllLvls = GetComponents<StandingStilllvl>();
    //    Description = "Level: " + lvl + "/" + Patch.MaxSkilllvl;
    //    if (lvl == 0) {
    //        Description += DescriptionTemplate(AllLvls, 0);
    //    } else {
    //        Description += DescriptionTemplate(AllLvls, lvl - 1);
    //        if (lvl == Patch.MaxSkilllvl)
    //            return;
    //        Description += "\n\nNext Level:";
    //        Description += DescriptionTemplate(AllLvls, lvl);
    //    }
    //}

    protected override void Start() {
        base.Start();
    }

    protected override void Update() {
        base.Update();
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        StandingStilllvl SSL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                SSL = GetComponent<StandingStill1>();
                break;
            case 2:
                SSL = GetComponent<StandingStill2>();
                break;
            case 3:
                SSL = GetComponent<StandingStill3>();
                break;
            case 4:
                SSL = GetComponent<StandingStill4>();
                break;
            case 5:
                SSL = GetComponent<StandingStill5>();
                break;
        }
        CD = SSL.CD;
        EssenceCost = SSL.EssenceCost;
        Heal_MaxHP_Percentage = SSL.Heal_MaxHP_Percentage;
        DotHeal_MaxHP_Percentage = SSL.DotHeal_MaxHP_Percentage;

        GenerateDescription();
    }    

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }


    public override void TriggerByNetwork(float[] Paramaters) {
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
        if(OC.OID == OID.Main) {
            Heal heal = CombatGenerator.GenerateFlatHeal(OC, OC, OC.MaxHealth * (Heal_MaxHP_Percentage / 100), false, CR.StandingStill);
            Client.Send(Protocols.ObjectOnHealthGain, heal, Client.TCP);
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.HealingBuff, OC.NetworkID, OC.NetworkID, Duration, CombatGenerator.GenerateEffectParameters(OC.MaxHealth * (DotHeal_MaxHP_Percentage / 100))),Client.TCP);
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.StandingStill));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
            RealTime_CD = CD;
        }
    }

    private void ActiveNetwork() {
        HandyNetwork.SendSkillActivation(OC, Type, null);
        //Client.Send(Protocols.ObjectActiveSkill, new SkillActivationData(OC.NetworkID, Type, null),Client.UDP);
    }        

    private void ActiveLocal() {
        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.StandingStill));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;

        Heal heal = CombatGenerator.GenerateFlatHeal(OC, OC, OC.MaxHealth * (Heal_MaxHP_Percentage / 100), false, CR.StandingStill);
        OC.ON_HEALTH_GAIN += OC.HealHP;
        OC.ON_HEALTH_GAIN(heal);
        OC.ON_HEALTH_GAIN -= OC.HealHP;

        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);

        CacheManager.Effects[CR.HealingBuff].Apply(OC, OC, Duration, CombatGenerator.GenerateEffectParameters(OC.MaxHealth * (DotHeal_MaxHP_Percentage / 100)));
        RealTime_CD = CD;
    }
}

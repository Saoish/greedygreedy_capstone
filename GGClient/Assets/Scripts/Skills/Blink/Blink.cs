﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;
using System;

public class Blink : ActiveSkill {
    public override CR Type { get { return CR.Blink; } }
    BlinkIndicator Indicator;
    public AudioClip SFX;
    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Blinklvl BL = (Blinklvl)AllLvls[Index];
        return "\nBlink to a spot you have chosen.\n\n Cost: " + MyText.Colofied(BL.EssenceCost + " Essense", ScaleHighlight) + "\nCD: " + MyText.Colofied(BL.CD + " secs", ScaleHighlight);
    }

    public GameObject BlinkStart;
    public GameObject BlinkEnd;

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Blinklvl BL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                BL = GetComponent<Blink1>();
                break;
            case 2:
                BL = GetComponent<Blink2>();
                break;
            case 3:
                BL = GetComponent<Blink3>();
                break;
            case 4:
                BL = GetComponent<Blink4>();
                break;
            case 5:
                BL = GetComponent<Blink5>();
                break;
        }
        CD = BL.CD;
        EssenceCost = BL.EssenceCost;
        GenerateDescription();
    }

    protected override void Awake() {
        base.Awake();
        Indicator = GetComponentInChildren<BlinkIndicator>(true);
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        Interrupt(false);
        StartCoroutine(Traspassing(new Vector2(Paramaters[0], Paramaters[1])));
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }
    private void ActiveNetwork() {
        Indicator.Deactive();
        float[] Parameters = CombatGenerator.GenerateSkillParameters(Indicator.Spot.position.x,Indicator.Spot.position.y);
        HandyNetwork.SendSkillActivation(OC, Type, Parameters);
        if (OC is MainPlayer) {
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.Blink));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;

            RealTime_CD = CD;
        }
    }
    private void ActiveLocal() {
        Interrupt(false);
        //Traspassing(Indicator.Spot.position);
        StartCoroutine(Traspassing(Indicator.Spot.position));
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);

        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.Blink));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
        
        RealTime_CD = CD;
    }

    public override void StartCasting(int ActionSlot) {
        base.StartCasting(ActionSlot);
        Indicator.Active(ActionSlot);
    }

    public override void Interrupt(bool TellServer) {
        base.Interrupt(TellServer);
        Indicator.Deactive();
    }

   
    private IEnumerator Traspassing(Vector2 TargetPostion) {
        OC.Lerping = true;

        OC.ObjectCollider.enabled = false;
        OC.VisualHolder.gameObject.SetActive(false);        
        GameObject Instantiated_BlinkStart = Instantiate(BlinkStart, OC.Position, Quaternion.identity);
        Destroy(Instantiated_BlinkStart, 0.5f);
        GameObject Instantiated_BlinkEnd = Instantiate(BlinkEnd, TargetPostion, Quaternion.identity);
        Destroy(Instantiated_BlinkEnd, 0.5f);
        OC.NormalizePhysics();

        //if(OC is OtherPlayer) {
        //    ((OtherPlayer)OC).NetworkPosition = ((OtherPlayer)OC).DumpedPosition;
        //    ((OtherPlayer)OC).Position = TargetPostion;
        //    OC.SC.Coordinate.ToVector = OC.Position;
        //}
        //else {
        //    OC.Position = TargetPostion;
        //}
        float Counter = 0;
        float LerpTime = 0.1f;
        while (OC && Counter < LerpTime && !OC.HasForce) {
            Counter += Time.deltaTime;
            float timeProgressed = Counter / LerpTime;
            //OC.Position = Vector2.MoveTowards(OC.Position,TargetPostion,)
            OC.Position = Vector2.Lerp(OC.Position, TargetPostion, timeProgressed);
            yield return new WaitForEndOfFrame();
        }
        OC.Lerping = false;
        if (OC.Alive) {
            //OC.Contacts.Clear();
            OC.ObjectCollider.enabled = true;            
            OC.VisualHolder.gameObject.SetActive(true);
        }
        //GameObject Instantiated_BlinkEnd = Instantiate(BlinkEnd, OC.Position, Quaternion.identity);
        //Destroy(Instantiated_BlinkEnd, 0.5f);
    }
}

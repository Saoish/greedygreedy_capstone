﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using System;

public class MagicOrbMastery : PassiveSkill {
    public override CR Type { get { return CR.MagicOrbMastery; } }
    float Essence_Regen_Amount;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        MagicOrbMasterylvl MOML  = (MagicOrbMasterylvl)AllLvls[Index];        
        return "\nIncrease your " + MyText.Colofied("Essense Regen",ScaleHighlight)+" by " + MyText.Colofied(MOML.Essence_Regen_Amount, ScaleHighlight) + " when you have " + MyText.Colofied(WeaponTypeInString.GetString(WEAPONTYPE.MageMagicOrb), ScaleHighlight) + " equipped.";
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        MagicOrbMasterylvl MOML = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                MOML = GetComponent<MagicOrbMastery1>();
                break;
            case 2:
                MOML = GetComponent<MagicOrbMastery2>();
                break;
            case 3:
                MOML = GetComponent<MagicOrbMastery3>();
                break;
            case 4:
                MOML = GetComponent<MagicOrbMastery4>();
                break;
            case 5:
                MOML = GetComponent<MagicOrbMastery5>();
                break;
        }
        Essence_Regen_Amount = MOML.Essence_Regen_Amount;
        GenerateDescription();
    }

    public override void ApplyPassive() {
        WeaponController WC = ((Player)OC).GetWC();
        if (!WC) {
            return;
        }
        if (WC.Type == WEAPONTYPE.MageMagicOrb) {
            //OC.AddMaxStats(STATSTYPE.ESSENCE_REGEN, (float)System.Math.Round(OC.GetMaxStats(STATSTYPE.ESSENCE_REGEN) * (Essence_Regen_Amount / 100), 1));
            OC.AddMaxStats(STATSTYPE.ESSENCE_REGEN, Essence_Regen_Amount);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;
using System;

public class Electromagnetic : PassiveSkill {
    public override CR Type { get { return CR.Electromagnetic; } }    
    private float TriggerChance;

    BoxCollider2D SelfCollider;

    private float ColliderStayTime = 0.1f;


    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Electromagneticlvl EL = (Electromagneticlvl)AllLvls[Index];
        return "\nYour magic projectile has " + MyText.Colofied(EL.TriggerChance + "%", ScaleHighlight) + " chance to be spiritual, it will target the cloest target on the direction you are aiming.";
    }

    protected override void Awake() {
        base.Awake();
        SelfCollider = GetComponent<BoxCollider2D>();
    }    

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Electromagneticlvl EL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                EL = GetComponent<Electromagnetic1>();
                break;
            case 2:
                EL = GetComponent<Electromagnetic2>();
                break;
            case 3:
                EL = GetComponent<Electromagnetic3>();
                break;
            case 4:
                EL = GetComponent<Electromagnetic4>();
                break;
            case 5:
                EL = GetComponent<Electromagnetic5>();
                break;
        }
        TriggerChance = EL.TriggerChance;        
        GenerateDescription();
    }

    public override void ApplyPassive() {
        if (OC is MainPlayer) {            
            OC.ON_GENERATE_PD += ELPassive;
        }
    }

    ProjectileData ELPassive(ProjectileData PD) {        
        if (UnityEngine.Random.value < (TriggerChance / 100) && PD.HomingTargetID==0) {
            ObjectController HomingTarget = CombatGenerator.MP_RaycastHomingTarget();            
            if (HomingTarget)
                PD.HomingTargetID = HomingTarget.NetworkID;
        }
        return PD;
    }
}

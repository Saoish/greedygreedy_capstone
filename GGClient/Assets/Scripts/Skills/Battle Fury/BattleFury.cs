﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using System;
using Networking.Data;

public class BattleFury : PassiveSkill {
    public override CR Type { get { return CR.BattleFury; } }
    public GameObject HitVFX;
    public AudioClip HitSFX;
    
    float TriggerChance;    
    float Dot_DamageSCale_Percentage;    
    float Sping_DamageScale;

    public float BleedDuration = 10;    
    [HideInInspector]
    public bool Spining = false;
    private Animator SpinAnim;

    public Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        BattleFurylvl BFL = (BattleFurylvl)AllLvls[Index];
        return "\nUpon dealing damage with melee attack, you have " + MyText.Colofied(BFL.TriggerChance + "%", ScaleHighlight) + " chance to summon a weapon dealing " + MyText.Colofied(BFL.Sping_DamageScale + "%", ScaleHighlight) + " damage to enemies around you and apply bleeding on them for " + BleedDuration + " secs to bleed " + MyText.Colofied(BFL.Dot_DamageScale_Percentage + "%", ScaleHighlight) + " damage/sec.";
    }

    protected override void Awake() {
        base.Awake();
        GetComponent<SpriteRenderer>().sortingLayerName = SortingLayer.Skill;
        SpinAnim = GetComponent<Animator>();
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        BattleFurylvl BFL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                BFL = GetComponent<BattleFury1>();
                break;
            case 2:
                BFL = GetComponent<BattleFury2>();
                break;
            case 3:
                BFL = GetComponent<BattleFury3>();
                break;
            case 4:
                BFL = GetComponent<BattleFury4>();
                break;
            case 5:
                BFL = GetComponent<BattleFury5>();
                break;
        }
        TriggerChance = BFL.TriggerChance;
        Sping_DamageScale = BFL.Sping_DamageScale;
        Dot_DamageSCale_Percentage = BFL.Dot_DamageScale_Percentage;
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), OC.ObjectCollider);//Ignore self here
        GenerateDescription();
        
    }

    protected override void Start() {
        base.Start();
    }

    protected override void Update() {
        base.Update();
    }
    


    public override void ApplyPassive() {
        if(OC.OID == OID.Main)
            OC.ON_DMG_DEAL+=ApplyBattlFuryPassive;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.transform.GetComponent<ObjectController>();
        Damage SpinDmg = CombatGenerator.GenerateDirectDamage(OC, target, Sping_DamageScale, Type);
        OC.ON_DMG_DEAL += DealBFSpinDMGAndApplyBleedDebuff;
        OC.ON_DMG_DEAL(SpinDmg);
        OC.ON_DMG_DEAL -= DealBFSpinDMGAndApplyBleedDebuff;
        HittedStack.Push(collider);
    }


    void DealBFSpinDMGAndApplyBleedDebuff(Damage dmg) {
        if (Client.Connected)
            NetworkDealBFSpinDMGAndApplyBleedDebuff(dmg);
        else
            LocalDealBFSpinDMGAndApplyBleedDebuff(dmg);
    }

    void NetworkDealBFSpinDMGAndApplyBleedDebuff(Damage dmg) {        
        if(OC.OID == OID.Main) {            
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
            if(!CombatChecker.IgnoreApplyingEffect(dmg.targetID,Type))
                Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.BleedDebuff, OC.NetworkID, dmg.targetID, BleedDuration, CombatGenerator.GenerateEffectParameters((OC.CurrDamage * (Dot_DamageSCale_Percentage / 100)).ToString())), Client.TCP);
        }        
        CacheManager.GetOC(dmg.targetID).ActiveOneShotVFXParticle(HitVFX);
        //SFX goes into here
    }

    void LocalDealBFSpinDMGAndApplyBleedDebuff(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);

        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;

        if (!CombatChecker.IgnoreApplyingEffect(dmg.targetID, Type))
            CacheManager.Effects[CR.BleedDebuff].Apply(OC, target, BleedDuration, CombatGenerator.GenerateEffectParameters((OC.CurrDamage * (Dot_DamageSCale_Percentage / 100)).ToString()));

        target.ActiveOneShotVFXParticle(HitVFX);
        //SFX goes into here
    }

    void ActiveBattleFury() {        
        if (Client.Connected)
            NetworkActievBattleFury();
        else
            LocalActiveBattleFury();
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        SpinAnim.speed = Paramaters[0];
        SpinAnim.SetTrigger("Active");
    }

    void NetworkActievBattleFury() {
        float[] Paramaters = CombatGenerator.GenerateSkillParamaters(OC.GetAttackAnimSpeed());
        HandyNetwork.SendSkillActivation(OC,Type,Paramaters);
        //Client.Send(Protocols.ObjectActiveSkill, new SkillActivationData(OC.NetworkID, CR.BattleFury, CombatGenerator.GenerateSkillParamaters(OC.GetAttackAnimSpeed())), Client.UDP);
    }

    void LocalActiveBattleFury() {
        SpinAnim.speed = OC.GetAttackAnimSpeed();
        SpinAnim.SetTrigger("Active");
    }

    //Private
    void ApplyBattlFuryPassive(Damage dmg) {
        if (dmg.Type== CR.MeleeAttack && !Spining && UnityEngine.Random.value < (TriggerChance / 100)) {
            ActiveBattleFury();
        }
    }


}

﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
using Networking.Data;
public class Rage : PassiveSkill {
    public override CR Type { get { return CR.Rage; } }
    float Damage_INC_Percentage;
    public float HealthTriggerThreshold = 50;

    float Duration = 10f;
    
    float ICD_Counter = 0f;
    const float ICD =30f;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Ragelvl RL = (Ragelvl)AllLvls[Index];
        return "\nBoost your "+MyText.Colofied("Damage",ScaleHighlight)+" by " + MyText.Colofied(RL.Damage_INC_Percentage + "%",ScaleHighlight) + " for " + Duration + " secs when your health fall below " + HealthTriggerThreshold + "%. Effect can not be triggered again within " + ICD + " secs.";
    }

    protected override void Awake() {
        base.Awake();
    }

    protected override void Start() {
        base.Start();
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Ragelvl RL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                RL = GetComponent<Rage1>();
                break;
            case 2:
                RL = GetComponent<Rage2>();
                break;
            case 3:
                RL = GetComponent<Rage3>();
                break;
            case 4:
                RL = GetComponent<Rage4>();
                break;
            case 5:
                RL = GetComponent<Rage5>();
                break;
        }        
        Damage_INC_Percentage = RL.Damage_INC_Percentage;
        GenerateDescription();        
    }


    protected override void FixedUpdate() {
        base.FixedUpdate();
        if (ICD_Counter > 0) {
            ICD_Counter -= Time.fixedDeltaTime;
        }
        else {
            ICD_Counter = 0;
        }
    }

    public override void ApplyPassive() {
        if(OC.OID == OID.Main)
            OC.ON_HEALTH_LOSS += RagePassive;
    }

    private void RagePassive(Damage damage) {
        if (Client.Connected)
            NetworkRagePassive(damage);
        else
            LocalRagePassive(damage);
    }

    private void NetworkRagePassive(Damage damage) {
        if ((OC.CurrHealth - damage.Amount) / OC.MaxHealth <= HealthTriggerThreshold / 100) {
            if (ICD_Counter == 0) {
                Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.RageBuff, OC.NetworkID, OC.NetworkID, Duration, CombatGenerator.GenerateEffectParameters(Damage_INC_Percentage)), Client.TCP);
                ICD_Counter = ICD;
            }
        }
    }

    private void LocalRagePassive(Damage damage) {
        if ((OC.CurrHealth - damage.Amount) / OC.MaxHealth <= HealthTriggerThreshold / 100) {            
            if (ICD_Counter == 0) {                
                CacheManager.Effects[CR.RageBuff].Apply(OC, OC, Duration, CombatGenerator.GenerateEffectParameters(Damage_INC_Percentage));
                ICD_Counter = ICD;
            }
        }
    }
}

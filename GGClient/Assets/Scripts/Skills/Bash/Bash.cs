﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;
using Networking.Data;

public class Bash : PassiveSkill {
    public override CR Type { get { return CR.Bash; } }
    float TriggerChance;

    public float StunDuration = 1f;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Bashlvl BL = (Bashlvl)AllLvls[Index];
        return "\nUpon dealing damage, you have " + MyText.Colofied(BL.TriggerChance + "%", ScaleHighlight) + " chance to stun your targets for " + StunDuration + " secs." /*effect can't be triggered again within"+ TriggerCD +" secs."*/;
    }


    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Bashlvl BL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                BL = GetComponent<Bash1>();
                break;
            case 2:
                BL = GetComponent<Bash2>();
                break;
            case 3:
                BL = GetComponent<Bash3>();
                break;
            case 4:
                BL = GetComponent<Bash4>();
                break;
            case 5:
                BL = GetComponent<Bash5>();
                break;
        }
        TriggerChance = BL.TriggerChance;
        GenerateDescription();        
    }

    public override void ApplyPassive() {    
        if(OC.OID == OID.Main)
            OC.ON_DMG_DEAL += BashPassive;
    }



    //Private
    void BashPassive(Damage dmg) {
        if (Client.Connected)
            NetworkBashPassive(dmg);
        else
            LocalBashPassive(dmg);
    }

    private void NetworkBashPassive(Damage dmg) {
        if (UnityEngine.Random.value < (TriggerChance / 100)) {
            ObjectController target = CacheManager.GetOC(dmg.targetID);
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.StunDebuff, OC.NetworkID, target.NetworkID, StunDuration, null), Client.TCP);            
        }
    }

    private void LocalBashPassive(Damage dmg) {
        if (UnityEngine.Random.value < (TriggerChance / 100)) {
            ObjectController target = CacheManager.GetOC(dmg.targetID);
            CacheManager.Effects[CR.StunDebuff].Apply(OC, target, StunDuration, null);            
        }
    }
}

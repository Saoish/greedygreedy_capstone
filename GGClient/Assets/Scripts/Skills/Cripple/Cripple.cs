﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
using Networking.Data;
using System.Collections.Generic;
public class Cripple : PassiveSkill {
    public override CR Type { get { return CR.Cripple; } }
    float TriggerChance;
    float DMG_DEC_Percentage;

    public float Duration = 5f;
    

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Cripplelvl CL = (Cripplelvl)AllLvls[Index];
        return "\nUpon taking dmg, you have " + MyText.Colofied(CL.TriggerChance+ "%",ScaleHighlight) + " chance to cripple the attacker which lowers their "+ MyText.Colofied("Damage",ScaleHighlight)+ " by " + MyText.Colofied(CL.DMG_DEC_Percentage + "%", ScaleHighlight) + " for " + Duration + " secs, Cripple effect does not stack." /*and can not be triggered again within "+TriggerCD+ " secs."*/;
    }

    //protected override void Update() {
    //    base.Update();
    //    //if (RealTime_TriggerCD > 0)
    //    //    RealTime_TriggerCD -= Time.deltaTime;
    //    //else
    //    //    RealTime_TriggerCD = 0;
    //}

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Cripplelvl CL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                CL = GetComponent<Cripple1>();
                break;
            case 2:
                CL = GetComponent<Cripple2>();
                break;
            case 3:
                CL = GetComponent<Cripple3>();
                break;
            case 4:
                CL = GetComponent<Cripple4>();
                break;
            case 5:
                CL = GetComponent<Cripple5>();
                break;
        }
        TriggerChance = CL.TriggerChance;
        DMG_DEC_Percentage = CL.DMG_DEC_Percentage;

        GenerateDescription();
    }


    public override void ApplyPassive() {
        if(OC.OID == OID.Main)
            OC.ON_HEALTH_LOSS += CripplePassive;
    }








    //Private
    void CripplePassive(Damage dmg) {
        if (Client.Connected)
            NetworkCripplePasive(dmg);
        else
            LocalCripplePassive(dmg);
    }

    private void NetworkCripplePasive(Damage dmg) {
        if (CombatChecker.IgnoreApplyingEffect(dmg.targetID,Type))
            return;
        else if (dmg.TraceBack) {
            if (dmg.Type == CR.None)
                Debug.Log("Some skill is not register.");
            else if (CacheManager.GetOC(dmg.applyerID)) {//Exist
                if (UnityEngine.Random.value < (TriggerChance / 100) /*&& RealTime_TriggerCD == 0*/) {
                    Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.CrippleDebuff, OC.NetworkID, dmg.applyerID, Duration, CombatGenerator.GenerateEffectParameters(DMG_DEC_Percentage)), Client.TCP);
                }
            }
        }
    }    

    private void LocalCripplePassive(Damage dmg) {
        if (CombatChecker.IgnoreApplyingEffect(dmg.targetID, Type))
            return;
        else if (dmg.TraceBack) {
            if (dmg.Type == CR.None)
                Debug.Log("Some skill is not register.");
            else if (CacheManager.GetOC(dmg.applyerID)) {//Exist
                if (UnityEngine.Random.value < (TriggerChance / 100) /*&& RealTime_TriggerCD == 0*/) {
                    CacheManager.Effects[CR.CrippleDebuff].Apply(OC, CacheManager.GetOC(dmg.applyerID), Duration, CombatGenerator.GenerateEffectParameters(DMG_DEC_Percentage));                    
                }
            }
        }
    }               
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using System;
using Networking.Data;

public class FlashFreeze : PassiveSkill {
    public override CR Type { get { return CR.FlashFreeze; } }
    public float ImmobolizeDuration = 1f;
    float TriggerChance;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        FlashFreezelvl FFL = (FlashFreezelvl)AllLvls[Index];
        return "\nUpon dealing damage, you have " + MyText.Colofied(FFL.TriggerChance + "%", ScaleHighlight) + " chance to immobolize your targets for " + ImmobolizeDuration + " secs." /*effect can't be triggered again within"+ TriggerCD +" secs."*/;
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        FlashFreezelvl FFL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                FFL = GetComponent<FlashFreeze1>();
                break;
            case 2:
                FFL = GetComponent<FlashFreeze2>();
                break;
            case 3:
                FFL = GetComponent<FlashFreeze3>();
                break;
            case 4:
                FFL = GetComponent<FlashFreeze4>();
                break;
            case 5:
                FFL = GetComponent<FlashFreeze5>();
                break;
        }
        TriggerChance = FFL.TriggerChance;
        GenerateDescription();
    }

    public override void ApplyPassive() {
        if (OC is MainPlayer)
            OC.ON_DMG_DEAL += FlashFreezePassive;
    }

    void FlashFreezePassive(Damage dmg) {
        if (Client.Connected)
            NetworkPassive(dmg);
        else
            LocalPassive(dmg);
    }

    void NetworkPassive(Damage dmg) {
        if (UnityEngine.Random.value < (TriggerChance / 100) /*&& RealTime_TriggerCD == 0*/) {
            ObjectController target = CacheManager.GetOC(dmg.targetID);
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.FrozenImmobolizeDebuff, OC.NetworkID, target.NetworkID, ImmobolizeDuration, null), Client.TCP);
            //RealTime_TriggerCD = TriggerCD;
        }
    }

    void LocalPassive(Damage dmg) {
        if (UnityEngine.Random.value < (TriggerChance / 100) /*&& RealTime_TriggerCD == 0*/) {
            ObjectController target = CacheManager.GetOC(dmg.targetID);
            CacheManager.Effects[CR.FrozenImmobolizeDebuff].Apply(OC, target, ImmobolizeDuration, null);
            //RealTime_TriggerCD = TriggerCD;
        }
    }
}

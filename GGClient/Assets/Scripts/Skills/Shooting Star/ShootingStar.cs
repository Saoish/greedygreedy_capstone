﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
using System;

public class ShootingStar : ActiveSkill {
    public override CR Type { get { return CR.ShootingStar; } }
    float DamageScale;
    float ScalingFactor;
    public Projectile ShootingStartProjectile;    
    private ShootingStarIndicator Indicator;

    public static float ChargeCap = 2f;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        ShootingStarlvl SSL = (ShootingStarlvl)AllLvls[Index];
        return "\n" + MyText.Colofied("Charge["+ChargeCap+"]",ChargeHighlight)+" up and launch an " + MyText.Colofied("explosive projectile", ScaleHighlight)+" to the direction you are aiming with, and it deals "+MyText.Colofied("charge * "+ SSL.DamageScale +"% damage",ScaleHighlight)+" to enemies who get hit.\n\nCost: " + MyText.Colofied(SSL.EssenceCost + " Essence", ScaleHighlight) + "\nCD: " + MyText.Colofied(SSL.CD + " secs", ScaleHighlight);
    }

    protected override void Awake() {
        Indicator = GetComponentInChildren<ShootingStarIndicator>(true);
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        ShootingStarlvl SSL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                SSL = GetComponent<ShootingStar1>();
                break;
            case 2:
                SSL = GetComponent<ShootingStar2>();
                break;
            case 3:
                SSL = GetComponent<ShootingStar3>();
                break;
            case 4:
                SSL = GetComponent<ShootingStar4>();
                break;
            case 5:
                SSL = GetComponent<ShootingStar5>();
                break;
        }
        CD = SSL.CD;
        EssenceCost = SSL.EssenceCost;
        DamageScale = SSL.DamageScale;        
        GenerateDescription();
    }


    public override void StartCasting(int ActionSlot) {
        base.StartCasting(ActionSlot);
        Indicator.Active(ActionSlot);
    }

    public override void Interrupt(bool TellServer) {
        base.Interrupt(TellServer);
        Indicator.Deactive();
    }

    //public override void TriggerByNetwork(float[] Paramaters) {//No need
        
    //}

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }

    public void ActiveNetwork() {
        Interrupt(true);
        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);//Local calculated

        Damage dmg = CombatGenerator.GenerateRawDirectDamage(OC, ScalingFactor * DamageScale, Type);
        //Vector2 StartPosition = ShootingStartProjectile.ScaledColliderRadius(ScalingFactor) * Indicator.CastVector + OC.Position;
        ProjectileData PD;
        //if (ScalingFactor == Indicator.ScalingCap) {
        //    ObjectController HomingTarget = CombatGenerator.GetHomingTargetToMPBasedOnAming();            
        //    PD = OC.ON_GENERATE_PD(CombatGenerator.GenerateProjectileData(ShootingStartProjectile, StartPosition, Indicator.CastVector, Indicator.ScalingFactor, dmg, HomingTarget));            
        //}
        //else
        PD = OC.ON_GENERATE_PD(CombatGenerator.GenerateProjectileData(ShootingStartProjectile, OC, Indicator.CastVector, Indicator.ScalingFactor, dmg, null));
        HandyNetwork.SendLaunch(PD);
        //Client.Send(Protocols.Launch, PD, Client.UDP);

        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;

        RealTime_CD = CD;
    }

    public void ActiveLocal() {
        Interrupt(false);
        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);//Local calculated

        Damage dmg = CombatGenerator.GenerateRawDirectDamage(OC, ScalingFactor * DamageScale, Type);
        //Vector2 StartPosition = ShootingStartProjectile.ScaledColliderRadius(ScalingFactor) * Indicator.CastVector + OC.Position;
        ProjectileData PD;
        //if (ScalingFactor == ChargeCap) {
        //    ObjectController HomingTarget = CombatGenerator.MP_RaycastHomingTarget();
        //    PD = OC.ON_GENERATE_PD(CombatGenerator.GenerateProjectileData(ShootingStartProjectile, StartPosition, Indicator.CastVector, Indicator.ScalingFactor, dmg, HomingTarget));
        //}
        //else
        PD = OC.ON_GENERATE_PD(CombatGenerator.GenerateProjectileData(ShootingStartProjectile, OC, Indicator.CastVector, Indicator.ScalingFactor, dmg, null));
        CacheManager.Launch(PD);

        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;

        RealTime_CD = CD;
    }



    
}

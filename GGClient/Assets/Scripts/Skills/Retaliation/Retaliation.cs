﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
using Networking.Data;

public class Retaliation : PassiveSkill {
    public override CR Type { get { return CR.Retaliation; } }
    float TriggerChance;
    float Reflected_DMG_Percentage;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Retaliationlvl RL = (Retaliationlvl)AllLvls[Index];
        return "\nUpon taking damage, you have " + MyText.Colofied(RL.TriggerChance +"%",ScaleHighlight) + " chance to reflect " + MyText.Colofied(RL.Reflected_DMG_Percentage + "%", ScaleHighlight) + " of taken raw damge to attacker as true damage.";
    }

    //public override void GenerateDescription() {
    //    Retaliationlvl[] AllLvls = GetComponents<Retaliationlvl>();
    //    Description = "Level: " + lvl + "/" + Patch.MaxSkilllvl;
    //    if (lvl == 0) {
    //        Description += DescriptionTemplate(AllLvls, 0);
    //    } else {
    //        Description += DescriptionTemplate(AllLvls, lvl - 1);
    //        if (lvl == Patch.MaxSkilllvl)
    //            return;
    //        Description += "\n\nNext Level:";
    //        Description += DescriptionTemplate(AllLvls, lvl);
    //    }
    //}

    protected override void Awake() {
        base.Awake();
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Retaliationlvl RL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                RL = GetComponent<Retaliation1>();
                break;
            case 2:
                RL = GetComponent<Retaliation2>();
                break;
            case 3:
                RL = GetComponent<Retaliation3>();
                break;
            case 4:
                RL = GetComponent<Retaliation4>();
                break;
            case 5:
                RL = GetComponent<Retaliation5>();
                break;
        }
        TriggerChance = RL.TriggerChance;
        Reflected_DMG_Percentage = RL.Reflected_DMG_Percentage;

        GenerateDescription();
    }

    protected override void Start() {
        base.Start();
    }

    protected override void Update() {
        base.Update();
    }

    public override void ApplyPassive() {
        if(OC.OID == OID.Main)
            OC.ON_HEALTH_LOSS += RetaliationPassive;
    }

    private void RetaliationPassive(Damage dmg) {
        if (Client.Connected)
            NetworkRetaliationPassive(dmg);
        else
            LocalRetaliationPassive(dmg);
    }

    private void NetworkRetaliationPassive(Damage dmg) {
        if (dmg.TraceBack) {
            if (dmg.Type == CR.None)
                Debug.Log("Some skill is not register.");
            else if (UnityEngine.Random.value < (TriggerChance / 100) && CacheManager.GetOC(dmg.applyerID)) {                
                float raw_dmg_amount =CacheManager.GetOC(dmg.applyerID).CurrDamage;
                float reflected_dmg_amount = raw_dmg_amount * (Reflected_DMG_Percentage / 100);                
                Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.RetaliationDebuff,OC.NetworkID,dmg.applyerID,0, CombatGenerator.GenerateEffectParameters(reflected_dmg_amount, dmg.Crit)), Client.TCP);
            }
        }
    }

    private void LocalRetaliationPassive(Damage dmg) {
        if (dmg.TraceBack) {
            if (dmg.Type == CR.None)
                Debug.Log("Some skill is not register.");
            else if (UnityEngine.Random.value < (TriggerChance / 100) && CacheManager.GetOC(dmg.applyerID)) {
                float raw_dmg_amount = CacheManager.GetOC(dmg.applyerID).CurrDamage;
                float reflected_dmg_amount = raw_dmg_amount * (Reflected_DMG_Percentage / 100);
                CacheManager.Effects[CR.RetaliationDebuff].Apply(OC, CacheManager.GetOC(dmg.applyerID), 0, CombatGenerator.GenerateEffectParameters(reflected_dmg_amount, dmg.Crit));
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GreedyNameSpace;
using Networking.Data;
public class BloodyHand : ActiveSkill {
    public override CR Type { get { return CR.BloodyHand; } }
    float Min_TriggerDistance = 0.3f;
    float DamageScale;
    [HideInInspector]
    public float RangeScale = 1;

    BloodyHandIndicator Indicator;

    Animator Anim;
    public AudioClip SFX;

    public Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        BloodyHandlvl BHL = (BloodyHandlvl)AllLvls[Index];
        return "\nSummon a " + MyText.Colofied("size "+BHL.RangeScale, ScaleHighlight) + " bloody hand to deal " + MyText.Colofied(BHL.DamageScale + "%", ScaleHighlight) + " damage and grab enemies to you.\n\nCost: " + MyText.Colofied(BHL.EssenceCost + " Essence", ScaleHighlight) + "\nCD: " + MyText.Colofied(BHL.CD + " secs", ScaleHighlight);
    }


    protected override void Awake() {
        base.Awake();
        Anim = GetComponent<Animator>();
        GetComponent<SpriteRenderer>().sortingLayerName = SortingLayer.Skill;
        Indicator = GetComponentInChildren<BloodyHandIndicator>(true);
    }

    protected override void Start() {
        base.Start();
    }


    protected override void Update() {
        base.Update();

    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        BloodyHandlvl BHL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                BHL = GetComponent<BloodyHand1>();
                break;
            case 2:
                BHL = GetComponent<BloodyHand2>();
                break;
            case 3:
                BHL = GetComponent<BloodyHand3>();
                break;
            case 4:
                BHL = GetComponent<BloodyHand4>();
                break;
            case 5:
                BHL = GetComponent<BloodyHand5>();
                break;
        }
        CD = BHL.CD;
        EssenceCost = BHL.EssenceCost;
        DamageScale = BHL.DamageScale;
        RangeScale = BHL.RangeScale;
        transform.localScale = new Vector2(RangeScale, RangeScale);
        Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), OC.ObjectCollider);
        GenerateDescription();
    }

    public override void StartCasting(int ActionSlot) {
        base.StartCasting(ActionSlot);
        Indicator.Active(ActionSlot);                
    }

    public override void Interrupt(bool TellServer) {
        base.Interrupt(TellServer);
        Indicator.Deactive();                
    }

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        Interrupt(false);
        transform.localEulerAngles = new Vector3(0, 0, Paramaters[0]);
        Anim.SetTrigger("Active");
        StartCoroutine(Reset(1));
    }

    private void ActiveNetwork() {
        Indicator.Deactive();
        float[] Parameters = CombatGenerator.GenerateSkillParamaters(Indicator.Z_Angle);
        HandyNetwork.SendSkillActivation(OC, Type, Parameters);
        if (OC.OID == OID.Main) {            
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;

            RealTime_CD = CD;
        }
    }

    private void ActiveLocal() {
        Interrupt(false);        
        transform.localEulerAngles = new Vector3(0, 0, Indicator.Z_Angle);
        Anim.SetTrigger("Active");

        StartCoroutine(Reset(1));
        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
        RealTime_CD = CD;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.transform.GetComponent<ObjectController>();        
        Damage Dmg = CombatGenerator.GenerateDirectDamage(OC, target, DamageScale, Type);
        OC.ON_DMG_DEAL += DealBHDmg;
        OC.ON_DMG_DEAL(Dmg);
        OC.ON_DMG_DEAL -= DealBHDmg;
        HittedStack.Push(collider);
    }


    void DealBHDmg(Damage dmg) {
        if (Client.Connected)
            NetworkDealBHDmg(dmg);
        else
            LocalDealBHDmg(dmg);
    }

    private void NetworkDealBHDmg(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        Pull(target);
        if(OC.OID == OID.Main) {
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
        }        
    }

    private void LocalDealBHDmg(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        Pull(target);
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
    }

    void Pull(ObjectController target) {
        if (Client.Connected) {
            NetworkPull(target);            
        }
        else {
            LocalPull(target);
        }
    }

    void NetworkPull(ObjectController target) {
        if (target is MainPlayer) {
            //ControllerManager.SyncMoveVector = false;
            //ControllerManager.SyncAttackVector = false;
            SendForce(target);
        }
        //else if (OC is MainPlayer && target is Monster) {
        //    SendForce(target);
        //}
        else if (target is Monster) {//Local
            LocalPull(target);
        }
    }
    void SendForce(ObjectController target) {
        float Force = Vector2.Distance(OC.Position, target.Position) - (OC.ObjectCollider.radius + target.ObjectCollider.radius);
        if (Force <= Min_TriggerDistance)            
            return;
        else
            Force *= 10;
        Vector2 PullDirection = Vector3.Normalize(OC.Position - target.Position);
        target.NormalizeDrag();
        HandyNetwork.SendForce(target, PullDirection * Force, GreedyForceMode.Override);
        //Client.Send(Protocols.ObjectAddForce, new AddForceData(target.NetworkID, PullDirection * Force, GreedyForceMode.Override), Client.TCP);
    }

    void LocalPull(ObjectController target) {
        float Force = Vector2.Distance(OC.Position, target.Position) - (OC.ObjectCollider.radius + target.ObjectCollider.radius);        
        if (Force <= Min_TriggerDistance)
            Force = 0f;
        else
            Force *= 10;        
        Vector2 PullDirection = Vector3.Normalize(OC.Position - target.Position);
        target.NormalizeDrag();
        target.AddForce(PullDirection * Force, GreedyForceMode.Override);
    }

    IEnumerator Reset(float time) {
        yield return new WaitForSeconds(time);
        transform.localEulerAngles = Vector3.zero;
    }

}
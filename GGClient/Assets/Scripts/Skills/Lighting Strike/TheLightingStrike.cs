﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public class TheLightingStrike : MonoBehaviour {    
    public CircleCollider2D SelfCollider;

    private float ColliderStayTime = 0.1f;
    public float StrikeDelay = 0.5f;

    ObjectController OC;
    float DamageScale;
    float StunDuration;

    public GameObject Electric;
    public GameObject Strike;    

    private Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    public void Call(Vector2 Position,ObjectController OC, float DamageScale, float StunDuration) {
        TheLightingStrike TLS = Instantiate((Resources.Load("SkillPrefabs/Tools/" + gameObject.name) as GameObject),Position,Quaternion.identity).GetComponent<TheLightingStrike>();
        TLS.OC = OC;
        TLS.DamageScale = DamageScale;
        TLS.StunDuration = StunDuration;                
    }

    void Start() {
        StartCoroutine(RunLightingStrike());
    }

    IEnumerator RunLightingStrike() {                
        Electric.SetActive(true);
        yield return new WaitForSeconds(StrikeDelay);        
        Strike.SetActive(true);
        StartCoroutine(RunCollider());
        Destroy(gameObject, 3f);
    }


    IEnumerator RunCollider() {        
        SelfCollider.enabled = true;        
        yield return new WaitForSeconds(ColliderStayTime);
        SelfCollider.enabled = false;        
    }

    private void OnTriggerEnter2D(Collider2D collider) {        
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.transform.GetComponent<ObjectController>();
        Damage dmg = CombatGenerator.GenerateDirectDamage(OC, target, DamageScale, CR.LightingStrike);        
        OC.ON_DMG_DEAL += DealDamageAndStunEnemy;
        OC.ON_DMG_DEAL(dmg);
        OC.ON_DMG_DEAL -= DealDamageAndStunEnemy;
        HittedStack.Push(collider);
    }

    private void DealDamageAndStunEnemy(Damage dmg) {       
        if (Client.Connected)
            NetworkDealAndStunEnemy(dmg);
        else
            LocalDealAndStunEnemy(dmg);
    }

    private void NetworkDealAndStunEnemy(Damage dmg) {        
        if (OC is MainPlayer) {
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.StunDebuff, OC.NetworkID, dmg.targetID, StunDuration, null), Client.TCP);
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
        }
    }

    private void LocalDealAndStunEnemy(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        CacheManager.Effects[CR.StunDebuff].Apply(OC, target, StunDuration, null);
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
    }
}

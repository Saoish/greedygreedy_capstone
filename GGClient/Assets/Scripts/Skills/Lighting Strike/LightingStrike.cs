﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
public class LightingStrike : PassiveSkill {
    public override CR Type { get { return CR.LightingStrike; } }

    private float DamageScale;
    private float TriggerChance;

    public TheLightingStrike TLS;
    public float StunDuration = 1f;    

    private float ICD_Counter = 0f;
    private float ICD;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        LightingStrikelvl LSL = (LightingStrikelvl)AllLvls[Index];
        return "\nUpon dealing direct damage, you have " + MyText.Colofied(LSL.TriggerChance + "%", ScaleHighlight) + " chance to call up a lighting to strike at your target area dealing " + MyText.Colofied(LSL.DamageScale + "% damage and Stun striked enemies for " + StunDuration + " secs.", ScaleHighlight) + " Lighting Strike can only be triggered once every " + MyText.Colofied(LSL.ICD, ScaleHighlight) + " secs.";
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();
        if (ICD_Counter > 0) {
            ICD_Counter -= Time.fixedDeltaTime;
        }
        else {
            ICD_Counter = 0;
        }
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        LightingStrikelvl LSL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                LSL = GetComponent<LightingStrike1>();                
                break;
            case 2:
                LSL = GetComponent<LightingStrike2>();                
                break;
            case 3:
                LSL = GetComponent<LightingStrike3>();                
                break;
            case 4:
                LSL = GetComponent<LightingStrike4>();                
                break;
            case 5:
                LSL = GetComponent<LightingStrike5>();                
                break;
        }
        DamageScale = LSL.DamageScale;
        TriggerChance = LSL.TriggerChance;
        ICD = LSL.ICD;
        GenerateDescription();
    }

    public override void ApplyPassive() {
        if (OC is MainPlayer)
            OC.ON_DMG_DEAL += LightingStrikePassive;
    }

    private void LightingStrikePassive(Damage dmg) {
        if (Client.Connected)
            NetworkPassive(dmg);
        else
            LocalPassive(dmg);
    }

    public override void TriggerByNetwork(float[] Parameters) {
        TLS.Call(new Vector2(Parameters[0], Parameters[1]), OC, DamageScale, StunDuration);                
    }            

    private void NetworkPassive(Damage dmg) {
        if (UnityEngine.Random.value < (TriggerChance / 100) && ICD_Counter == 0) {
            Vector2 Position = CacheManager.GetOC(dmg.targetID).Position;
            float[] Parameters = CombatGenerator.GenerateSkillParameters(Position.x, Position.y);
            HandyNetwork.SendSkillActivation(OC, Type, Parameters);
            //Client.Send(Protocols.ObjectActiveSkill, new SkillActivationData(OC.NetworkID, Type, Parameters), Client.UDP);
            ICD_Counter = ICD;
        }        
    }
    private void LocalPassive(Damage dmg) {
        if (UnityEngine.Random.value < (TriggerChance / 100) && ICD_Counter==0) {
            TLS.Call(CacheManager.GetOC(dmg.targetID).Position, OC, DamageScale, StunDuration);
            ICD_Counter = ICD;
        }
    }
}

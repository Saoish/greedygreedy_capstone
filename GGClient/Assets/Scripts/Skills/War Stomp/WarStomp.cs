﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;
public class WarStomp : ActiveSkill {
    public override CR Type { get { return CR.WarStomp; } }
    float DamageScale;
    float ScalingFactor;

    public static float ChargeCap = 2f;

    public Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    GameObject VFX;
    public AudioClip SFX;

    float VFX_StayTime;
    float Root_VFX_DefaultStartSize;
    float Pulse_VFX_DefaultStartSize;
    float SubEmitterBirthVFX_DefaultStartSize;

    CircleCollider2D SelfCollider;
    float RadiusScaleFactor = 0.4f;
    private float ColliderStayTime = 0.1f;

    WarStompIndicator Indicator;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        WarStomplvl WSL = (WarStomplvl)AllLvls[Index];
        return "\n" + MyText.Colofied("Charge["+ChargeCap+"]",ChargeHighlight)+" up and heavily stomp the ground, dealing " + MyText.Colofied("charge * " +WSL.DamageScale+ "% damage",ScaleHighlight) + " to nearby foes and "+MyText.Colofied("stun them for 1-3 secs according to the charge",ScaleHighlight)+".\n\nCost: " + MyText.Colofied(WSL.EssenceCost + " Essence", ScaleHighlight) + "\nCD: " + MyText.Colofied(WSL.CD + " secs", ScaleHighlight);
    }

    protected override void Awake() {
        base.Awake();
        SelfCollider = GetComponent<CircleCollider2D>();
        VFX = transform.Find("War Stomp VFX").gameObject;
        Root_VFX_DefaultStartSize = transform.Find("War Stomp VFX").GetComponent<ParticleSystem>().startSize;
        Pulse_VFX_DefaultStartSize = transform.Find("War Stomp VFX/pulse").GetComponent<ParticleSystem>().startSize;
        SubEmitterBirthVFX_DefaultStartSize = transform.Find("War Stomp VFX/pulse/SubEmitterBirth").GetComponent<ParticleSystem>().startSize;
        VFX_StayTime = transform.Find("War Stomp VFX").GetComponent<ParticleSystem>().duration;
        Indicator = GetComponentInChildren<WarStompIndicator>(true);
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        WarStomplvl WSL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                WSL = GetComponent<WarStomp1>();
                break;
            case 2:
                WSL = GetComponent<WarStomp2>();
                break;
            case 3:
                WSL = GetComponent<WarStomp3>();
                break;
            case 4:
                WSL = GetComponent<WarStomp4>();
                break;
            case 5:
                WSL = GetComponent<WarStomp5>();
                break;
        }
        CD = WSL.CD;
        EssenceCost = WSL.EssenceCost;
        DamageScale = WSL.DamageScale;                
        Physics2D.IgnoreCollision(SelfCollider, OC.ObjectCollider);//Ignore self here
        GenerateDescription();
    }

    public override void StartCasting(int ActionSlot) {
        base.StartCasting(ActionSlot);
        Indicator.Active(ActionSlot);                
    }

    public override void Interrupt(bool TellServer) {
        base.Interrupt(TellServer);
        Indicator.Deactive();                
    }

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        Interrupt(false);
        ScalingFactor = Paramaters[0];
        SelfCollider.radius = ScalingFactor * RadiusScaleFactor;
        transform.Find("War Stomp VFX").GetComponent<ParticleSystem>().startSize = Root_VFX_DefaultStartSize * ScalingFactor;
        transform.Find("War Stomp VFX/pulse").GetComponent<ParticleSystem>().startSize = Pulse_VFX_DefaultStartSize * ScalingFactor;
        transform.Find("War Stomp VFX/pulse/SubEmitterBirth").GetComponent<ParticleSystem>().startSize = SubEmitterBirthVFX_DefaultStartSize * ScalingFactor;
        StartCoroutine(ActiveStompCollider(ColliderStayTime));
        StartCoroutine(RunStompVFX(VFX_StayTime));
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    private void ActiveNetwork() {
        Indicator.Deactive();
        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);
        float[] Parameters = CombatGenerator.GenerateSkillParamaters(ScalingFactor);
        HandyNetwork.SendSkillActivation(OC, Type, Parameters);
        if (OC.OID == OID.Main) {
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.WarStomp));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
            RealTime_CD = CD;
        }
    }

    private void ActiveLocal() {
        Interrupt(false);

        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.WarStomp));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);        
        SelfCollider.radius = ScalingFactor * RadiusScaleFactor;
        transform.Find("War Stomp VFX").GetComponent<ParticleSystem>().startSize = Root_VFX_DefaultStartSize * ScalingFactor;
        transform.Find("War Stomp VFX/pulse").GetComponent<ParticleSystem>().startSize = Pulse_VFX_DefaultStartSize * ScalingFactor;
        transform.Find("War Stomp VFX/pulse/SubEmitterBirth").GetComponent<ParticleSystem>().startSize = SubEmitterBirthVFX_DefaultStartSize * ScalingFactor;
        
        StartCoroutine(ActiveStompCollider(ColliderStayTime));        
        StartCoroutine(RunStompVFX(VFX_StayTime));
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
        RealTime_CD = CD;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.transform.GetComponent<ObjectController>();
        Damage dmg = CombatGenerator.GenerateDirectDamage(OC, target, DamageScale * ScalingFactor, CR.WarStomp);
        OC.ON_DMG_DEAL += StunAndDealStompDmg;
        OC.ON_DMG_DEAL(dmg);
        OC.ON_DMG_DEAL -= StunAndDealStompDmg;
        HittedStack.Push(collider);
    }

    private void StunAndDealStompDmg(Damage dmg) {
        if (Client.Connected)
            NetworkStunAndDealStompDmg(dmg);
        else
            LocalStunAndDealStompDmg(dmg);
    }

    private void NetworkStunAndDealStompDmg(Damage dmg) {        
        if (OC.OID == OID.Main) {
            float StunDuration;
            if (ScalingFactor < 1.5f)
                StunDuration = 1f;
            else if (ScalingFactor < 2)
                StunDuration = 2f;
            else {//Double it
                StunDuration = 3;
            }
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.StunDebuff, OC.NetworkID, dmg.targetID, StunDuration, null), Client.TCP);
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
        }        
    }

    private void LocalStunAndDealStompDmg(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        float StunDuration;
        if (ScalingFactor < 1.5f)
            StunDuration = 1f;
        else if (ScalingFactor < 2)
            StunDuration = 2f;
        else {//Double it
            StunDuration = 3;
        }                
        CacheManager.Effects[CR.StunDebuff].Apply(OC, target, StunDuration, null);

        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
    }

    IEnumerator ActiveStompCollider(float time) {
        SelfCollider.enabled = true;
        yield return new WaitForSeconds(time);
        SelfCollider.enabled = false;
        HittedStack.Clear();
    }

    IEnumerator RunStompVFX(float time) {
        VFX.SetActive(true);
        yield return new WaitForSeconds(time);
        VFX.SetActive(false);
    }

}

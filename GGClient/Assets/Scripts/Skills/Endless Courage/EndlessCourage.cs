﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;

public class EndlessCourage : PassiveSkill {
    public override CR Type { get { return CR.EndlessCourage; } }
    public float EPH = 1;
    protected override string DescriptionTemplate(Skilllvl[] AllSkills,int Index) {
        return null;
    }

    public override void GenerateDescription() {
        Description = "Generate " + EPH + " Essense upon dealing damage with auto attack.";
    }

    public override void ApplyPassive() {
        if(OC.OID == OID.Main)
            OC.ON_DMG_DEAL += EndlessCouragePassive;
    }

    private void EndlessCouragePassive(Damage dmg) {
        if(dmg.Type == CR.MeleeAttack) {
            OC.ON_ESSENSE_GAIN += OC.GainEssense;
            OC.ON_ESSENSE_GAIN(CombatGenerator.GenerateEssenseGain(OC, OC, EPH, Type));
            OC.ON_ESSENSE_GAIN -= OC.GainEssense;
        }
    }
}

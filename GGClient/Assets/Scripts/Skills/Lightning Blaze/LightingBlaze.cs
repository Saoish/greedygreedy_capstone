﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public class LightingBlaze : ActiveSkill {
    public override CR Type { get { return CR.LightingBlaze; } }
    float Recoil = 1f;//should include indication time
    float Force = 5f;
    public static float ChargeCap = 5f;    
    float DamageScale;

    LightingBlazeIndicator Indicator;
    float ScalingFactor;    

    public GameObject LightingBlazePrewarmVFX;
    public GameObject LightingBlazeBlastVFX;

    private PolygonCollider2D LB_Collider;

    public Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        LightingBlazelvl LBL = (LightingBlazelvl)AllLvls[Index];
        return "\n" + MyText.Colofied("Charge[" + ChargeCap + "]", ChargeHighlight) + " up a powerful piercing blast to deal " + MyText.Colofied(LBL.DamageScale + "% + charge * " + LBL.DamageScale + "% damage", ScaleHighlight) + " to enemies along the path and push them away.\n\nCost: " + MyText.Colofied(LBL.EssenceCost + " Essence", ScaleHighlight) + "\nCD: " + MyText.Colofied(LBL.CD + " secs", ScaleHighlight);
    }

    protected override void Awake() {
        base.Awake();           
        Indicator = GetComponentInChildren<LightingBlazeIndicator>(true);
        LB_Collider = GetComponent<PolygonCollider2D>();

        //DefaultPrewarmStartSize = PrewarmVFX.startSize;        
        //DefaultPrewarmVelocity_X = PrewarmVFX.velocityOverLifetime.x.constantMax;        

        //DefaultBlazeStartSize = BlazeVFX.startSize;
        //DefaultBlazeRadius = BlazeVFX.shape.radius;
        //DefaultBlazePrewarmVelocity_x = BlazeVFX.velocityOverLifetime.x.constantMax;
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        LightingBlazelvl LBL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                LBL = GetComponent<LightingBlaze1>();
                break;
            case 2:
                LBL = GetComponent<LightingBlaze2>();
                break;
            case 3:
                LBL = GetComponent<LightingBlaze3>();
                break;
            case 4:
                LBL = GetComponent<LightingBlaze4>();
                break;
            case 5:
                LBL = GetComponent<LightingBlaze5>();
                break;
        }
        CD = LBL.CD;
        EssenceCost = LBL.EssenceCost;
        DamageScale = LBL.DamageScale;
        Physics2D.IgnoreCollision(GetComponent<PolygonCollider2D>(), OC.ObjectCollider);
        GenerateDescription();
    }

    public override void StartCasting(int ActionSlot) {
        base.StartCasting(ActionSlot);
        Indicator.Active(ActionSlot);
    }

    public override void Interrupt(bool TellServer) {
        base.Interrupt(TellServer);
        Indicator.Deactive();
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.transform.GetComponent<ObjectController>();
        Damage dmg = CombatGenerator.GenerateDirectDamage(OC, target, DamageScale + ScalingFactor * DamageScale, Type);
        Push(target);
        OC.ON_DMG_DEAL += DealDamage;
        OC.ON_DMG_DEAL(dmg);
        OC.ON_DMG_DEAL -= DealDamage;
        HittedStack.Push(collider);
    }

    private void DealDamage(Damage dmg) {
        if (Client.Connected)
            NetworkDealDamage(dmg);
        else
            LocalDealDamage(dmg);
    }
    private void NetworkDealDamage(Damage dmg) {
        //ObjectController target = CacheManager.GetOC(dmg.targetID);
        if(OC is MainPlayer) {
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
        }
    }
    private void LocalDealDamage(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;                                                           
    }

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        Interrupt(false);
        StartCoroutine(RunLightingBlaze(Paramaters[0], Paramaters[1]));       
    }

    void ActiveNetwork() {
        Indicator.Deactive();
        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);//Local calculated    
        float[] Parameters = CombatGenerator.GenerateSkillParameters(ScalingFactor, Indicator.Z_Angle);
        HandyNetwork.SendSkillActivation(OC, Type, Parameters);
        if (OC is MainPlayer) {
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
            RealTime_CD = CD;
        }
    }

    void ActiveLocal() {
        Interrupt(false);
        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);//Local calculated                 
        StartCoroutine(RunLightingBlaze(ScalingFactor,Indicator.Z_Angle));

        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
        RealTime_CD = CD;
    }    

    void Push(ObjectController target) {
        if (Client.Connected)
            NetworkPush(target);
        else
            LocalPush(target);
    }
    void NetworkPush(ObjectController target) {
        if(target is MainPlayer) {
            //ControllerManager.SyncMoveVector = false;
            //ControllerManager.SyncAttackVector = false;
            SendForce(target);
        }
        else if(target is Monster) {
            LocalPush(target);
        }
    }
    void LocalPush(ObjectController target) {        
        Vector2 BouceOffDirection = Vector3.Normalize(target.transform.position - OC.transform.position);
        target.NormalizeDrag();
        target.AddForce(BouceOffDirection * Force, GreedyForceMode.Override);
    }
    void SendForce(ObjectController target) {        
        Vector2 BouceOffDirection = Vector3.Normalize(target.transform.position - OC.transform.position);
        target.NormalizeDrag();
        HandyNetwork.SendForce(target, BouceOffDirection * Force, GreedyForceMode.Override);
        //Client.Send(Protocols.ObjectAddForce, new AddForceData(target.NetworkID, BouceOffDirection * Force, GreedyForceMode.Override), Client.UDP);
    }

    private IEnumerator RunLightingBlaze(float Scaling,float Z_Angle) {        
        if(OC is MainPlayer) {
            StartCoroutine(RunRecoil());
        }
        RunPrewarmVFX(Scaling,Z_Angle);
        yield return new WaitForSeconds(0.5f);
        RunBlazeVFX(Scaling, Z_Angle);        
        StartCoroutine(RunCollider(Scaling,Z_Angle));              
    }

    IEnumerator RunRecoil() {
        ControllerManager.SyncActions = false;
        yield return new WaitForSeconds(Recoil);
        if (OC.Alive)
            ControllerManager.SyncActions = true;
    }

    IEnumerator RunCollider(float Scaling, float Z_Angle) {
        transform.localScale = new Vector2(Scaling, Scaling);
        transform.localEulerAngles = new Vector3(0, 0,Z_Angle);
        LB_Collider.enabled = true;
        yield return new WaitForSeconds(0.1f);
        LB_Collider.enabled = false;
        transform.localScale = new Vector2(1, 1);
        transform.localEulerAngles = new Vector3(0, 0, 0);
        HittedStack.Clear();
    }

    void RunPrewarmVFX(float Scaling, float Z_Angle) {
        GameObject LBP_OJ = Instantiate(LightingBlazePrewarmVFX, OC.Position, Quaternion.Euler(0, 0, Z_Angle));
        LBP_OJ.transform.localEulerAngles = new Vector3(0, 0, Z_Angle);
        ParticleSystem Prewarm_PS = LBP_OJ.GetComponent<ParticleSystem>();
        Prewarm_PS.startSize *= Scaling;        

        ParticleSystem.VelocityOverLifetimeModule tempVOLT = Prewarm_PS.velocityOverLifetime;
        ParticleSystem.MinMaxCurve rate = tempVOLT.x;
        rate.constantMax*= Scaling;
        tempVOLT.x = rate;

        Prewarm_PS.gameObject.SetActive(true);
        Destroy(LBP_OJ, 3f);
    }

    void RunBlazeVFX(float Scaling, float Z_Angle) {
        GameObject LBB_OJ = Instantiate(LightingBlazeBlastVFX, OC.Position, Quaternion.Euler(0, 0, Z_Angle));        
        ParticleSystem Blast_PS = LBB_OJ.GetComponent<ParticleSystem>();
        
        Blast_PS.startSize *= Scaling;

        var tempShape = Blast_PS.shape;
        tempShape.radius *= Scaling;

        ParticleSystem.VelocityOverLifetimeModule tempVOLT = Blast_PS.velocityOverLifetime;
        ParticleSystem.MinMaxCurve rate = tempVOLT.x;
        rate.constantMax *= Scaling;
        tempVOLT.x = rate;

        Blast_PS.gameObject.SetActive(true);
        Destroy(LBB_OJ, 3f);
    }
}

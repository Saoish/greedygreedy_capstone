﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
public class SpiritofShootingStar : PassiveSkill {
    public override CR Type { get { return CR.SpiritofShootingStar; } }
    protected override string DescriptionTemplate(Skilllvl[] AllSkills, int Index) {
        return null;
    }
    public override void GenerateDescription() {
        Description = "Your fully charged Shooting Star will be homing to the cloest target along your aiming path.";
    }
    public override void ApplyPassive() {
        if (OC.OID == OID.Main)
            OC.ON_GENERATE_PD += SpiritofShootingStarPassive;
    }

    ProjectileData SpiritofShootingStarPassive(ProjectileData PD) {
        if(PD.HomingTargetID==0 && PD.dmg.Type == CR.ShootingStar && PD.Size == ShootingStar.ChargeCap) {
            ObjectController HomingTarget = CombatGenerator.MP_RaycastHomingTarget();
            if(HomingTarget)
                PD.HomingTargetID = HomingTarget.NetworkID;
        }
        return PD;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;
public class PhantomCharge : ActiveSkill {
    public override CR Type { get { return CR.PhantomCharge; } }
    public GameObject EndSmoke;
    public AudioClip SFX;
    public AudioClip HitSFX;
    public float SmokeStayTime;
    public float EndStayTime;
    public float StunDuration = 1f;

    [HideInInspector]
    public float DamageScale;
    [HideInInspector]
    public float Force;

    ParticleSystem SmokePS;

    Collider2D ChargeCollider;
    
    PhantomChargeIndicator Indicator;
    //public Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        PhantomChargelvl PCL = (PhantomChargelvl)AllLvls[Index];
        return "\nTurn into a black mist and charge forward with speed of " + MyText.Colofied(PCL.Force, ScaleHighlight) + ", dealing " + MyText.Colofied(PCL.DamageScale + "%", ScaleHighlight) + " damage to collided enemies and stun them for " + StunDuration + " secs.\n\n Cost: " + MyText.Colofied(PCL.EssenceCost + " Essense", ScaleHighlight) + "\nCD: " + MyText.Colofied(PCL.CD + "secs", ScaleHighlight);
    }

    protected override void Awake() {
        base.Awake();
        SmokePS = transform.GetComponent<ParticleSystem>();
        SmokePS.GetComponent<Renderer>().sortingLayerName = SortingLayer.Skill;
        ChargeCollider = GetComponent<Collider2D>();
        Indicator = GetComponentInChildren<PhantomChargeIndicator>(true);
    }
    // Use this for initialization
    protected override void Start() {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update() {
        base.Update();
        if (!ChargeCollider.enabled && OC.Drag <= 0) {
            OC.NormalizeDrag();
        }
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        PhantomChargelvl PCL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                PCL = GetComponent<PhantomCharge1>();
                break;
            case 2:
                PCL = GetComponent<PhantomCharge2>();
                break;
            case 3:
                PCL = GetComponent<PhantomCharge3>();
                break;
            case 4:
                PCL = GetComponent<PhantomCharge4>();
                break;
            case 5:
                PCL = GetComponent<PhantomCharge5>();
                break;
        }
        CD = PCL.CD;
        EssenceCost = PCL.EssenceCost;
        DamageScale = PCL.DamageScale;
        Force = PCL.Force;
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), OC.ObjectCollider);//Ignore self here
        SmokePS.startSize *= OC.GetVFXScale();

        GenerateDescription();
    }

    public override void StartCasting(int ActionSlot) {
        base.StartCasting(ActionSlot);
        Indicator.Active(ActionSlot);                
    }

    public override void Interrupt(bool TellServer) {
        base.Interrupt(TellServer);
        Indicator.Deactive();        
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        Interrupt(false);
        SmokePS.enableEmission = true;
        ChargeCollider.enabled = true;
        OC.ZerolizeDrag();
        if (OC.OID == OID.Main) {
            Vector2 Force = new Vector2(Paramaters[0], Paramaters[1]) * this.Force;
            HandyNetwork.SendForce(OC, Force, GreedyForceMode.Override);
        }
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    public override void Active() {
        if (Client.Connected) {
            ActiveNetwork();
        }
        else {
            ActiveLocal();
        }
    }

    public void ActiveNetwork() {
        Indicator.Deactive();
        float[] Parameters = CombatGenerator.GenerateSkillParameters(Indicator.CastVector.x, Indicator.CastVector.y);
        HandyNetwork.SendSkillActivation(OC, Type, Parameters);
        if (OC.OID == OID.Main) {
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.PhantomCharge));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
            RealTime_CD = CD;
        }
    }

    public void ActiveLocal() {
        Interrupt(false);
        SmokePS.enableEmission = true;        
        ChargeCollider.enabled = true;
        OC.ZerolizeDrag();
        OC.AddForce(Indicator.CastVector * Force, GreedyForceMode.Override);
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);

        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.PhantomCharge));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;

        RealTime_CD = CD;
    }

    void StunAndDealChargeDmg(Damage dmg) {
        if (Client.Connected)
            NetworkStunAndDealChargeDmg(dmg);
        else
            LocalStunAndDealChargeDmg(dmg);
    }

    void NetworkStunAndDealChargeDmg(Damage dmg) {
        if(OC.OID == OID.Main) {
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.StunDebuff, OC.NetworkID, dmg.targetID, StunDuration, null), Client.TCP);            
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
        }
        AudioSource.PlayClipAtPoint(HitSFX, transform.position, GameManager.SFX_Volume);
    }

    void LocalStunAndDealChargeDmg(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        CacheManager.Effects[CR.StunDebuff].Apply(OC, target, StunDuration, null);        

        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
        AudioSource.PlayClipAtPoint(HitSFX, transform.position, GameManager.SFX_Volume);
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.layer == CollisionLayer.Melee) {
            if (collider != OC.Melee_AC.MeleeCollder)
                return;
            else
                Deactive();
        }
        else if(collider.gameObject.layer == CollisionLayer.Projectile) {
            return;
        }
        else if (collider.tag == Tag.PhantomCharge) {
            return;
        }
        else if (!CombatChecker.IgnoreDealingDmd(OC, collider)) {
            ObjectController target = collider.transform.GetComponent<ObjectController>();
            Damage dmg = CombatGenerator.GenerateDirectDamage(OC, target, DamageScale, CR.PhantomCharge);
            OC.ON_DMG_DEAL += StunAndDealChargeDmg;
            OC.ON_DMG_DEAL(dmg);
            OC.ON_DMG_DEAL -= StunAndDealChargeDmg;
            //HittedStack.Push(collider);
            Deactive();
        }
        else {
            Deactive();
        }
    }

    IEnumerator DisableSmokePS(float time) {        
        yield return new WaitForSeconds(time);
        SmokePS.enableEmission = false;
    }    

    public override void Deactive() {        
        if (Client.Connected) {
            NetworkDeactive();
        }
        else {
            LocalDeactive();
        }
    }

    private void NetworkDeactive() {//Local authority
        if (OC.OID == OID.Main) {            
            Client.Send(Protocols.ObjectDeactiveSkill, new SkillDeactivationData(OC.NetworkID, Type, 0f), Client.TCP);
        }
        //OC.NormalizePhysics();
        //HittedStack.Clear();
        OC.NormalizeDrag();
        ChargeCollider.enabled = false;
        OC.ActiveVFXParticalWithStayTime(EndSmoke, EndStayTime);
        StartCoroutine(DisableSmokePS(SmokeStayTime));                
    }

    private void LocalDeactive() {
        //OC.NormalizePhysics();
        //HittedStack.Clear();
        OC.NormalizeDrag();
        ChargeCollider.enabled = false;
        OC.ActiveVFXParticalWithStayTime(EndSmoke, EndStayTime);
        StartCoroutine(DisableSmokePS(SmokeStayTime));
    }

    protected override bool StillActive {
        get {            
            return ChargeCollider.enabled;
        }
    }

}
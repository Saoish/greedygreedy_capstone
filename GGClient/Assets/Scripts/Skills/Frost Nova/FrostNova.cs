﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public class FrostNova : ActiveSkill {
    public override CR Type { get { return CR.FrostNova; } }
    //public GameObject HitVFX;    
    float DamageScale;
    float ScalingFactor;

    public static float ChargeCap = 2f;

    private FrostNovaIndicator Indicator;

    public Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    GameObject VFX;
    public AudioClip SFX;

    float VFX_StayTime;
    float Root_VFX_DefaultStartSize;
    float Pulse_VFX_DefaultStartSize;
    float SubEmitterBirthVFX_DefaultStartSize;

    CircleCollider2D SelfCollider;
    float RadiusScaleFactor = 0.4f;
    private float ColliderStayTime = 0.1f;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        FrostNovalvl FNL = (FrostNovalvl)AllLvls[Index];
        return "\n" + MyText.Colofied("Charge[" + ChargeCap + "]", ChargeHighlight) + " up and release a frost nova, dealing "+ MyText.Colofied("charge * " + FNL.DamageScale + "% damage", ScaleHighlight) + " and "+MyText.Colofied("immobilize enemies that are within the range for 1-3 secs depends on the charge",ScaleHighlight)+".\n\nCost: " + MyText.Colofied(FNL.EssenceCost + " Essence", ScaleHighlight) + "\nCD: " + MyText.Colofied(FNL.CD + " secs", ScaleHighlight);
    }

    protected override void Awake() {
        base.Awake();
        Indicator = GetComponentInChildren<FrostNovaIndicator>(true);
        SelfCollider = GetComponent<CircleCollider2D>();
        VFX = transform.Find("Frost Nova VFX").gameObject;
        Root_VFX_DefaultStartSize = transform.Find("Frost Nova VFX").GetComponent<ParticleSystem>().startSize;
        Pulse_VFX_DefaultStartSize = transform.Find("Frost Nova VFX/pulse").GetComponent<ParticleSystem>().startSize;
        SubEmitterBirthVFX_DefaultStartSize = transform.Find("Frost Nova VFX/pulse/SubEmitterBirth").GetComponent<ParticleSystem>().startSize;
        VFX_StayTime = transform.Find("Frost Nova VFX").GetComponent<ParticleSystem>().duration;
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        FrostNovalvl FNL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                FNL = GetComponent<FrostNova1>();
                break;
            case 2:
                FNL = GetComponent<FrostNova2>();
                break;
            case 3:
                FNL = GetComponent<FrostNova3>();
                break;
            case 4:
                FNL = GetComponent<FrostNova4>();
                break;
            case 5:
                FNL = GetComponent<FrostNova5>();
                break;
        }
        CD = FNL.CD;
        EssenceCost = FNL.EssenceCost;
        DamageScale = FNL.DamageScale;        
        Physics2D.IgnoreCollision(SelfCollider, OC.ObjectCollider);
        GenerateDescription();
    }

    public override void StartCasting(int ActionSlot) {
        base.StartCasting(ActionSlot);
        Indicator.Active(ActionSlot);
    }

    public override void Interrupt(bool TellServer) {
        base.Interrupt(TellServer);
        Indicator.Deactive();
    }

    public override void TriggerByNetwork(float[] Paramaters) {
        Interrupt(false);

        ScalingFactor = Paramaters[0];
        SelfCollider.radius = ScalingFactor * RadiusScaleFactor;

        transform.Find("Frost Nova VFX").GetComponent<ParticleSystem>().startSize = Root_VFX_DefaultStartSize * ScalingFactor;
        transform.Find("Frost Nova VFX/pulse").GetComponent<ParticleSystem>().startSize = Pulse_VFX_DefaultStartSize * ScalingFactor;
        transform.Find("Frost Nova VFX/pulse/SubEmitterBirth").GetComponent<ParticleSystem>().startSize = SubEmitterBirthVFX_DefaultStartSize * ScalingFactor;

        StartCoroutine(ActiveCollider(ColliderStayTime));
        StartCoroutine(RunVFX(VFX_StayTime));
        //SFX here
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
    }

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }

    public void ActiveNetwork() {
        Indicator.Deactive();
        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);
        float[] Parameters = CombatGenerator.GenerateSkillParamaters(ScalingFactor);
        HandyNetwork.SendSkillActivation(OC, Type, Parameters);
        if (OC.OID == OID.Main) {
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
            RealTime_CD = CD;
        }
    }

    public void ActiveLocal() {
        Interrupt(false);
        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
        ScalingFactor = (float)System.Math.Round(Indicator.ScalingFactor, 1);
        SelfCollider.radius = ScalingFactor * RadiusScaleFactor;
        transform.Find("Frost Nova VFX").GetComponent<ParticleSystem>().startSize = Root_VFX_DefaultStartSize* ScalingFactor;
        transform.Find("Frost Nova VFX/pulse").GetComponent<ParticleSystem>().startSize = Pulse_VFX_DefaultStartSize * ScalingFactor;
        transform.Find("Frost Nova VFX/pulse/SubEmitterBirth").GetComponent<ParticleSystem>().startSize = SubEmitterBirthVFX_DefaultStartSize * ScalingFactor;

        StartCoroutine(ActiveCollider(ColliderStayTime));
        StartCoroutine(RunVFX(VFX_StayTime));
                
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
        RealTime_CD = CD;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.transform.GetComponent<ObjectController>();
        Damage dmg = CombatGenerator.GenerateDirectDamage(OC, target, DamageScale * ScalingFactor, Type);
        OC.ON_DMG_DEAL += ImmobolizeAndDealDmg;
        OC.ON_DMG_DEAL(dmg);
        OC.ON_DMG_DEAL -= ImmobolizeAndDealDmg;
        HittedStack.Push(collider);
    }

    private void ImmobolizeAndDealDmg(Damage dmg) {
        if (Client.Connected)
            NetworkImmobolizeAndDealDmg(dmg);
        else
            LocalImmobolizeAndDealDmg(dmg);
    }

    private void NetworkImmobolizeAndDealDmg(Damage dmg) {
        if (OC.OID == OID.Main) {
            float ImmobolizeDuration;
            if (ScalingFactor < 1.5f)
                ImmobolizeDuration = 1f;
            else if (ScalingFactor < 2)
                ImmobolizeDuration = 2f;
            else {//Double it
                ImmobolizeDuration = 3;
            }
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.FrozenImmobolizeDebuff, OC.NetworkID, dmg.targetID, ImmobolizeDuration, null), Client.TCP);
            Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
        }
    }

    private void LocalImmobolizeAndDealDmg(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        float ImmobolizeDuration;
        if (ScalingFactor < 1.5f)
            ImmobolizeDuration = 1f;
        else if (ScalingFactor < 2)
            ImmobolizeDuration = 2f;
        else {//Double it
            ImmobolizeDuration = 3;
        }
        CacheManager.Effects[CR.FrozenImmobolizeDebuff].Apply(OC, target, ImmobolizeDuration,null);

        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;
    }



    IEnumerator ActiveCollider(float time) {
        SelfCollider.enabled = true;
        yield return new WaitForSeconds(time);
        SelfCollider.enabled = false;
        HittedStack.Clear();
    }

    IEnumerator RunVFX(float time) {
        VFX.SetActive(true);
        yield return new WaitForSeconds(time);
        VFX.SetActive(false);
    }
}

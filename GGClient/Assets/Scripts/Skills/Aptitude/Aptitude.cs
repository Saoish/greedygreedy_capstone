﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using System;

public class Aptitude : PassiveSkill {
    public override CR Type { get { return CR.Aptitude; } }
    float Haste_INC_Percentage;    
    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Aptitudelvl AL = (Aptitudelvl)AllLvls[Index];
        return "\nIncrease your " + MyText.Colofied("Haste", ScaleHighlight) + " by" + MyText.Colofied(AL.Haste_INC_Percentage + "%", ScaleHighlight) + ".";
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Aptitudelvl AL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                AL = GetComponent<Aptitude1>();
                break;
            case 2:
                AL = GetComponent<Aptitude2>();
                break;
            case 3:
                AL = GetComponent<Aptitude3>();
                break;
            case 4:
                AL = GetComponent<Aptitude4>();
                break;
            case 5:
                AL = GetComponent<Aptitude5>();
                break;
        }
        Haste_INC_Percentage = AL.Haste_INC_Percentage;

        GenerateDescription();
    }

    public override void ApplyPassive() {
        //OC.AddMaxStats(STATSTYPE.HASTE, (float)System.Math.Round(OC.GetMaxStats(STATSTYPE.HASTE) * (Haste_INC_Percentage / 100), 1));
        OC.AddMaxStats(STATSTYPE.HASTE, Haste_INC_Percentage);
    }
}

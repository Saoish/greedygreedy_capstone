﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;

public class BrutalWeaponMastery : PassiveSkill{
    public override CR Type { get { return CR.BrutalWeaponMastery; } }
    float Damage_INC_Percentage;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        BrutalWeaponMasterylvl BWL = (BrutalWeaponMasterylvl)AllLvls[Index];        
        return "\nIncrease your "+MyText.Colofied("Damage",ScaleHighlight)+" by " + MyText.Colofied(BWL.Damage_INC_Percentage + "%", ScaleHighlight) + " when you have " + MyText.Colofied(WeaponTypeInString.GetString(WEAPONTYPE.WarriorTwoHanded),ScaleHighlight)+" equipped.";
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        BrutalWeaponMasterylvl BL = null;
        switch (this.lvl){
            case 0:
                return;
            case 1:
                BL = GetComponent<BrutalWeaponMastery1>();
                break;
            case 2:
                BL = GetComponent<BrutalWeaponMastery2>();
                break;
            case 3:
                BL = GetComponent<BrutalWeaponMastery3>();
                break;
            case 4:
                BL = GetComponent<BrutalWeaponMastery4>();
                break;
            case 5:
                BL = GetComponent<BrutalWeaponMastery5>();
                break;
        }
        Damage_INC_Percentage = BL.Damage_INC_Percentage;
        GenerateDescription();
    }


    public override void ApplyPassive() {
        WeaponController WC = ((Player)OC).GetWC();
        if (!WC) {
            return;
        }
        if (WC.Type == WEAPONTYPE.WarriorTwoHanded) {
            OC.AddMaxStats(STATSTYPE.DAMAGE, (float)System.Math.Round(OC.GetMaxStats(STATSTYPE.DAMAGE) * (Damage_INC_Percentage / 100), 0));
        }

    }
}
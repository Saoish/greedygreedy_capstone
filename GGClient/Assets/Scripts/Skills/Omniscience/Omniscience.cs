﻿using System;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using UnityEngine;

public class Omniscience : PassiveSkill {
    public override CR Type { get { return CR.Omniscience; } }    
    float TriggerChance;    
    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Omnisciencelvl OL = (Omnisciencelvl)AllLvls[Index];
        return "\nUpon cosuming Essence on active skill, you have " + MyText.Colofied(OL.TriggerChance + "%", ScaleHighlight) + " chance to eliminate the cost.";
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Omnisciencelvl OL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                OL = GetComponent<Omniscience1>();
                break;
            case 2:
                OL = GetComponent<Omniscience2>();
                break;
            case 3:
                OL = GetComponent<Omniscience3>();
                break;
            case 4:
                OL = GetComponent<Omniscience4>();
                break;
            case 5:
                OL = GetComponent<Omniscience5>();
                break;
        }
        TriggerChance = OL.TriggerChance;
        GenerateDescription();
    }

    public override void ApplyPassive() {
        if (OC.OID == OID.Main)
            OC.ON_ESSENSE_LOSS += OmnisciencePassive;
    }

    private void OmnisciencePassive(EssenseLoss el) {        
        if (el.Type != CR.RangedAttack && UnityEngine.Random.value < (TriggerChance / 100)) {
            el.Amount = 0;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;

public abstract class SkillIndicator : MonoBehaviour {
    protected int ActionSlot;
    public ActiveSkill Root; //for absolute fast reason, this goes public

    // Update is called once per frame
    protected abstract void Update();


    public abstract void Active(int ActionSlot);

    public abstract void Deactive();

    protected CastSyncAxis SyncCSA {
        get {
            return CacheManager.UserData.AxisMappings[CacheManager.CPS].Get(ActionSlot);
        }
    }
}

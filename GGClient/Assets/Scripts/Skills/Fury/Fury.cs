﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;
using Networking.Data;
public class Fury : ActiveSkill {
    public override CR Type { get { return CR.Fury; } }
    [HideInInspector]
    public float Duration;
    [HideInInspector]
    public float AttkSpd_INC_Percentage;

    public AudioClip SFX;    

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Furylvl FL = (Furylvl)AllLvls[Index];
        return "\nBoost your "+MyText.Colofied("Attack Speed",ScaleHighlight)+" by " + MyText.Colofied(FL.AttkSpd_INC_Percentage+ "%", ScaleHighlight) + " for " + MyText.Colofied(FL.Duration + " secs", ScaleHighlight) + "\n\nCost: " + MyText.Colofied(FL.EssenceCost+" Essence",ScaleHighlight) + "\nCD: " + MyText.Colofied(FL.CD + " secs",ScaleHighlight);
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Furylvl FL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                FL = GetComponent<Fury1>();
                break;
            case 2:
                FL = GetComponent<Fury2>();
                break;
            case 3:
                FL = GetComponent<Fury3>();
                break;
            case 4:
                FL = GetComponent<Fury4>();
                break;
            case 5:
                FL = GetComponent<Fury5>();
                break;
        }
        CD = FL.CD;
        EssenceCost = FL.EssenceCost;
        Duration = FL.Duration;
        AttkSpd_INC_Percentage = FL.AttkSpd_INC_Percentage;        

        GenerateDescription();
    }

    public override void TriggerByNetwork(float[] Paramaters) {                
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
        if (OC.OID == OID.Main) {
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.FuryBuff, OC.NetworkID, OC.NetworkID, Duration, CombatGenerator.GenerateEffectParameters(AttkSpd_INC_Percentage)),Client.TCP);
            OC.ON_ESSENSE_LOSS += OC.DeductEssense;
            OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, CR.Fury));
            OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
            RealTime_CD = CD;
        }
    }

    public override void Active() {
        if (Client.Connected)
            ActiveNetwork();
        else
            ActiveLocal();
    }

    private void ActiveNetwork() {
        HandyNetwork.SendSkillActivation(OC, Type, null);
        //Client.Send(Protocols.ObjectActiveSkill, new SkillActivationData(OC.NetworkID, Type, null), Client.UDP);
    }

    private void ActiveLocal() {
        OC.ON_ESSENSE_LOSS += OC.DeductEssense;
        OC.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(OC, OC, EssenceCost, Type));
        OC.ON_ESSENSE_LOSS -= OC.DeductEssense;
        CacheManager.Effects[CR.FuryBuff].Apply(OC, OC, Duration, CombatGenerator.GenerateEffectParameters(AttkSpd_INC_Percentage));        
        AudioSource.PlayClipAtPoint(SFX, transform.position, GameManager.SFX_Volume);
        RealTime_CD = CD;
    }
}

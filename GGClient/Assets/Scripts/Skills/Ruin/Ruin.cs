﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;
public class Ruin : PassiveSkill {
    public override CR Type { get { return CR.Ruin; } }
    [HideInInspector]
    public float TriggerChance;
    [HideInInspector]
    public float MOVESPD_DEC_Percentage;

    public float Duration = 5;
    
    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Ruinlvl SL = (Ruinlvl)AllLvls[Index];
        return "\nUpon dealing damage, you have " + MyText.Colofied(SL.TriggerChance+ "%",ScaleHighlight) + " chance to slow down enemy movement speed by " + MyText.Colofied(SL.MOVESPD_DEC_Percentage + "%", ScaleHighlight) + " for "+ Duration+" secs.";
    }

    protected override void Awake() {
        base.Awake();
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Ruinlvl RL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                RL = GetComponent<Ruin1>();
                break;
            case 2:
                RL = GetComponent<Ruin2>();
                break;
            case 3:
                RL = GetComponent<Ruin3>();
                break;
            case 4:
                RL = GetComponent<Ruin4>();
                break;
            case 5:
                RL = GetComponent<Ruin5>();
                break;
        }
        TriggerChance = RL.TriggerChance;
        MOVESPD_DEC_Percentage = RL.MOVESPD_DEC_Percentage;
        GenerateDescription();
    }

    protected override void Start() {
        base.Start();
    }

    protected override void Update() {
        base.Update();
    }

    public override void ApplyPassive() {
        if(OC.OID == OID.Main)
            OC.ON_DMG_DEAL += RuinPassive;
    }












    //Private
    void RuinPassive(Damage dmg) {
        if (Client.Connected)
            NetworkRuinPassive(dmg);
        else
            LocalRuinPassive(dmg);
    }

    private void NetworkRuinPassive(Damage dmg) {
        if (CombatChecker.IgnoreApplyingEffect(dmg.targetID, Type))
            return;
        if (UnityEngine.Random.value < (TriggerChance / 100)) {
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.RuinDebuff, OC.NetworkID, dmg.targetID, Duration, CombatGenerator.GenerateEffectParameters(MOVESPD_DEC_Percentage)), Client.TCP);
        }
    }

    private void LocalRuinPassive(Damage dmg) {
        if (CombatChecker.IgnoreApplyingEffect(dmg.targetID, Type))
            return;
        if (UnityEngine.Random.value < (TriggerChance / 100)) {
            CacheManager.Effects[CR.RuinDebuff].Apply(OC, CacheManager.GetOC(dmg.targetID), Duration, CombatGenerator.GenerateEffectParameters(MOVESPD_DEC_Percentage));
        }
    }
}

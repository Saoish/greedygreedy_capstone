﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
using Networking.Data;
public class BloodForBlood : PassiveSkill {
    public override CR Type { get { return CR.BloodForBlood; } }
    float LPH_INC_Perentage;
    public float HealthTriggerThreshold = 50;    

    float Duration = 10f;    
    float ICD_Counter = 0f;
    float ICD = 30f;    

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        BloodForBloodlvl BFBL = (BloodForBloodlvl)AllLvls[Index];
        return "\nBoost your "+ MyText.Colofied("Life/Hit",ScaleHighlight)+" by " + MyText.Colofied(BFBL.LPH_INC_Perentage+ " %",ScaleHighlight) + " when your health fall below "+ HealthTriggerThreshold+" %.Effect lasts "+ Duration+" secs and can not be triggered again within "+ ICD+" secs.";
    }

    protected override void Awake() {
        base.Awake();
    }

    protected override void Start() {
        base.Start();
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        BloodForBloodlvl BFBL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                BFBL = GetComponent<BloodForBlood1>();
                break;
            case 2:
                BFBL = GetComponent<BloodForBlood2>();
                break;
            case 3:
                BFBL = GetComponent<BloodForBlood3>();
                break;
            case 4:
                BFBL = GetComponent<BloodForBlood4>();
                break;
            case 5:
                BFBL = GetComponent<BloodForBlood5>();
                break;
        }
        LPH_INC_Perentage = BFBL.LPH_INC_Perentage;
        GenerateDescription();
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();
        if (ICD_Counter > 0) {
            ICD_Counter -= Time.fixedDeltaTime;
        }
        else {
            ICD_Counter = 0;
        }
    }

    public override void ApplyPassive() {
        if(OC.OID == OID.Main)
            OC.ON_HEALTH_LOSS += BFBPassive;
    }

    private void BFBPassive(Damage dmg) {
        if (Client.Connected)
            NetworkBFBPassive(dmg);
        else
            LocalBFBPassive(dmg);
    }

    private void NetworkBFBPassive(Damage dmg) {
        if ((OC.GetCurrStats(STATSTYPE.HEALTH) - dmg.Amount) / OC.GetMaxStats(STATSTYPE.HEALTH) <= HealthTriggerThreshold / 100) {
            if (ICD_Counter == 0) {
                Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.BloodForBloodBuff, OC.NetworkID, OC.NetworkID, Duration, CombatGenerator.GenerateEffectParameters(LPH_INC_Perentage)), Client.TCP);
                ICD_Counter = ICD;
            }
        }
    }

    private void LocalBFBPassive(Damage dmg) {
        if ((OC.GetCurrStats(STATSTYPE.HEALTH) - dmg.Amount) / OC.GetMaxStats(STATSTYPE.HEALTH) <= HealthTriggerThreshold / 100) {
            if (ICD_Counter == 0) {
                CacheManager.Effects[CR.BloodForBloodBuff].Apply(OC, OC, Duration, CombatGenerator.GenerateEffectParameters(LPH_INC_Perentage));
                ICD_Counter = ICD;
            }
        }
    }    
}

﻿using UnityEngine;
using System.Collections;

public class Oscillator : MonoBehaviour {
	public float speed = 1f;
	public float radius = 0.16f;
	public Vector3 offSet = new Vector3(-0.1f, -0.1f, 0.0f);//For most of trinkets
	float time = 0;
	Transform target;
	// Use this for initialization
	void Start () {
		if (transform.parent == null)
			return;
		target = transform.root;
	}

	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate() {
		if (target == null)
			return;
		time += speed * Time.fixedDeltaTime;
		float x = Mathf.Cos(time) * radius;
		float y = Mathf.Sin(time) * radius;
        //transform.position = target.transform.position + new Vector3(x, 0, y / 4.0f) + offSet;        
        transform.position = target.transform.position + new Vector3(x, 0, transform.position.z) + offSet;
        //transform.position = target.transform.position + new Vector3(x, y, 0f) + offSet;
    }
}
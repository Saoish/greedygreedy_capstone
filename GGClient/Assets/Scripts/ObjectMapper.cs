﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;

public static class ObjectMapper {
    public static Dictionary<SNID, ObjectController> SOM = new Dictionary<SNID, ObjectController>();

    public static void Load() {
        SOM[SNID.MasterSifu] = Resources.Load<ObjectController>("NPCPrefabs/Master Sifu");
        SOM[SNID.MrBlack] = Resources.Load<ObjectController>("NPCPrefabs/Mr. Black");

        SOM[SNID.TestSlime] = Resources.Load<ObjectController>("MonsterPrefabs/Frankenstein's Phlegm");
    }

    public static ObjectController GetStaticOC(SNID SNID) {             
        return SOM[SNID];
    }

    public static ObjectController GetStaticOC(int INT_SNID) {        
        if (!NetworkIDChecker.IsStatic(INT_SNID)) {
            Debug.Log("ObjectMapper.GetStaticOC: " + INT_SNID + " is invalid.");
            return null;
        }
        return SOM[(SNID)INT_SNID];
    }

    public static Player GetPlayerOC(OID OID) {
        if(OID == OID.Main) {
            return Resources.Load<MainPlayer>("PlayerPrefabs/MainPlayer");
        }
        else if(OID == OID.FriendlyPlayer || OID == OID.EnemyPlayer){
            return Resources.Load<OtherPlayer>("PlayerPrefabs/OtherPlayer");
        }
        else {
            Debug.Log("Invalied use of " + OID);
            return null;
        }
    }
        
}

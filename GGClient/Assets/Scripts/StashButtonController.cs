﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using UnityEngine.EventSystems;
public class StashButtonController : MonoBehaviour, ISelectHandler {
    [HideInInspector]
    public int Slot = -999;
    GameObject EquipmentIcon;
    public EquipmentInfo EI;    
    [HideInInspector]
    public Equipment E = null;

    public void UpdateSlotID() {
        Slot = int.Parse(gameObject.name) + Patch.InventoryCapacity* (GetComponentInParent<StashInteractionContent>().StashPage);        
    }

    public void OnSelect(BaseEventData eventData) {
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position, GameManager.SFX_Volume);
        if (!EI) {
            EI = transform.parent.parent.GetComponentInChildren<EquipmentInfo>(true);
        }
        EI.Show(E, EquipmentInfo.Mode.Inventory);
    }


    void OnEnable() {
        UpdateSlotID();
        //EI = transform.parent.parent.Find("EquipmentInfo").GetComponent<EquipmentInfo>();
        UpdateSlot();

    }

    public void OnClickMoveToInventory() {
        CacheManager.MP.GetStashItem(Slot);
        if (!CacheManager.MP.GetStashItem(Slot).isNull) {            
            if (CacheManager.MP.InventoryIsFull()) {
                RedNotification.Push(RedNotification.Type.INVENTORY_FULL);
                return;
            }
            else {
                int InventorySlot = CacheManager.MP.FirstAvailbleInventorySlot;
                CacheManager.MP.Switch_S2I(InventorySlot, Slot);
                transform.parent.parent.Find("InventoryButtons/" + InventorySlot).GetComponent<InventoryButtonController>().UpdateSlot();
                UpdateSlot();
                EI.Show(E, EquipmentInfo.Mode.Inventory);
            }
        }
    }

    public void UpdateInfo() {
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == gameObject) {
            EI.Show(E, EquipmentInfo.Mode.Inventory);
        }
    }


    public void UpdateSlot() {
        E = CacheManager.MP.GetStashItem(Slot);
        if (!E.isNull) {
            if (EquipmentIcon != null)
                DestroyObject(EquipmentIcon);
            EquipmentIcon = EquipmentController.ObtainInventoryIcon(E, transform);
        }
        else {
            DestroyObject(EquipmentIcon);
            EquipmentIcon = null;
        }
    }
}

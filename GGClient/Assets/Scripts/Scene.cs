﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GreedyNameSpace;


[System.Serializable]
public static class Scene {
    private static bool UnderLoadSceneProgress = false;

    private static float FadeTime = 0.5f;
    private static float LoadIn_Out_Time = 0.5f;

    private static void SetUpForLoading() {//Must call functions for setting up load
        GreedyCamera.FadeIn();        
        foreach(var Player in UnityEngine.Object.FindObjectsOfType<Player>())
            UnityEngine.Object.Destroy(Player.gameObject);        
        CacheManager.CachePrevSceneCoordinate();
        CacheManager.Map = null;        
    }

    private static void SetUpAfterLoaded(bool Reset = true) {//Must call functions for after load
        GreedyCamera.Reset();//Just in case
        GreedyCamera.FadeOut();
        CacheManager.InstantiateOCs(Reset);        
        CacheManager.InstantiateGraves();        
    }    

    public static AsyncOperation async;     

    public static void Load(SceneID ID, bool ResetStatic = true, float Delay = 0f) {        
        GameManager.instance.StartCoroutine(LoadOnceLoaded(ID,ResetStatic,Delay));
    }
    public static void Load(int ID, bool ResetStatic = true, float Delay = 0f) {        
        GameManager.instance.StartCoroutine(LoadOnceLoaded((SceneID)ID, ResetStatic,Delay));
    }             

    public static void LoadWithAction(SceneID ID,Action call, float Delay = 0f) {//Don't use it for gameplay
        GameManager.instance.StartCoroutine(LoadThenExecute(ID, call,Delay));
    }
    public static void LoadWithAction<T1, T2>(SceneID ID, Action<T1, T2> Call, T1 p1, T2 p2, float Delay = 0f) {//Don't use it for gameplay
        GameManager.instance.StartCoroutine(LoadThenExecute(ID, Call, p1, p2, Delay));
    }
    public static void LoadWithAction<T1, T2, T3>(SceneID ID, Action<T1, T2, T3> Call, T1 p1, T2 p2, T3 p3, float Delay = 0f) {//Don't use it for gameplay
        GameManager.instance.StartCoroutine(LoadThenExecute(ID, Call, p1, p2, p3,Delay));
    }

    public static void LoadWithManualActive(SceneID ID, float Delay = 0f) {        
        GameManager.instance.StartCoroutine(LoadWithSync(ID,Delay));
    }
    public static void LoadWithManualActive(int ID, float Delay = 0f) {        
        GameManager.instance.StartCoroutine(LoadWithSync((SceneID)ID,Delay));
    }

    private static IEnumerator LoadLoadingScreen(float Delay) {//Careful passing delay here
        UnderLoadSceneProgress = true;
        SetUpForLoading();
        yield return new WaitForSeconds(Delay);//For Fader Fadein                
        if (GreedyCamera.Dirty) {//Remove jitter transition            
            GreedyCamera.Reset();
        }        
        async = null;
        AsyncOperation loadingscreen_async = Application.LoadLevelAsync((int)SceneID.Loading);
        while (!loadingscreen_async.isDone) {
            yield return null;
        }
        yield return new WaitForSeconds(LoadIn_Out_Time);//For LoadingUI LoadIn
        UnderLoadSceneProgress = false;
    }
    public static void ActiveScene(bool Reset = true, Action call = null) {//Have to have at least some time before calling active
        GameManager.instance.StartCoroutine(ActiveSceneUntilLoaded(Reset, call));
    }private static IEnumerator ActiveSceneUntilLoaded(bool Reset = true, Action call = null) {//Load execute upon above function        
        while (UnderLoadSceneProgress)
            yield return null;        
        while (async.progress < 0.9f)
            yield return null;
        LoadingUI.LoadOut();
        //yield return new WaitForSeconds(LoadIn_Out_Time);//For LoadingUI LoadOut        
        async.allowSceneActivation = true;
        while (!async.isDone) {
            yield return null;
        }
        SetUpAfterLoaded(Reset);
        if (call != null)
            call();
    }

    private static IEnumerator LoadWithSync(SceneID ID,float Delay) {//Require ActiveScene to active the scene after loaded                
        yield return LoadLoadingScreen(Delay);        
        async = Application.LoadLevelAsync((int)ID);        
        async.allowSceneActivation = false;
        while (async.progress < 0.9f) {            
            yield return null;
        }
        while (!async.isDone)
            yield return null;
    }
    private static IEnumerator LoadWithSyncAndExecute(SceneID ID, Action call,float Delay) {//Require ActiveScene to active the scene after loaded, indication function to tell server that progress is ready to proceed        
        yield return LoadLoadingScreen(Delay);
        async = Application.LoadLevelAsync((int)ID);
        async.allowSceneActivation = false;
        while (async.progress < 0.9f) {
            yield return null;
        }
        while (!async.isDone)
            yield return null;
        call();
    }   

    private static IEnumerator LoadOnceLoaded(SceneID ID,bool ResetStatic, float Delay) {
        yield return LoadLoadingScreen(Delay);            
        async = Application.LoadLevelAsync((int)ID);
        async.allowSceneActivation = false;
        while (async.progress<0.9f) {                                
            yield return null;                
        }
        ActiveScene(ResetStatic);        
    }

    private static IEnumerator LoadThenExecute(SceneID ID, Action Call, float Delay) {        
        yield return LoadLoadingScreen(Delay);
        async = Application.LoadLevelAsync((int)ID);
        async.allowSceneActivation = false;
        while (async.progress < 0.9f) {
            yield return null;
        }
        ActiveScene();
        while (!async.isDone)
            yield return null;
        Call();
    }
    private static IEnumerator LoadThenExecute<T>(SceneID ID,Action<T> Call,T para, float Delay) {        
        yield return LoadLoadingScreen(Delay);
        async = Application.LoadLevelAsync((int)ID);
        async.allowSceneActivation = false;
        while (async.progress < 0.9f) {                
            yield return null;
        }
        ActiveScene();
        while (!async.isDone)
            yield return null;
        Call(para);
    }
    private static IEnumerator LoadThenExecute<T1,T2>(SceneID ID, Action<T1,T2> Call, T1 p1,T2 p2, float Delay) {        
        yield return LoadLoadingScreen(Delay);
        async = Application.LoadLevelAsync((int)ID);
        async.allowSceneActivation = false;
        while (async.progress < 0.9f) {                
            yield return null;
        }
        ActiveScene();
        while (!async.isDone)
            yield return null;
        Call(p1,p2);        
    }
    private static IEnumerator LoadThenExecute<T1, T2,T3>(SceneID ID, Action<T1, T2,T3> Call, T1 p1, T2 p2,T3 p3, float Delay) {        
        yield return LoadLoadingScreen(Delay);
        async = Application.LoadLevelAsync((int)ID);
        async.allowSceneActivation = false;
        while (async.progress < 0.9f) {
            yield return null;
        }
        ActiveScene();
        while (!async.isDone)
            yield return null;
        Call(p1, p2,p3);        
    }

    public static SceneID Current_SID {        
        get { return (SceneID)Application.loadedLevel; }
    }

    public static int Current_SInt {
        get { return Application.loadedLevel; }
    }

    public static string Current_SName {
        get {
            return GetName(Current_SID);
        }
    }

    public static string GetName(SceneID SID) {
        if (SID == SceneID.FrankensteinCastle)
            return "Frankenstein's Castle";
        string result = "";
        foreach (char c in SID.ToString()) {
            if (System.Char.IsUpper(c) && result.Length > 0)
                result += " " + c;
            else
                result += c;
        }
        return result;
    }
}

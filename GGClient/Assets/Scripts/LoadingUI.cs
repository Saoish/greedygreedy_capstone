﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingUI : MonoBehaviour {

    public Text _LoadingText;
    public Image _FillBar;
    public Text _Progress;
    public Animator Anim;

    void Awake() {
        Initialize();
    }

    void Update() {
        SyncProgress();
    }

    private void SyncProgress() {
        if (Scene.async == null)
            return;
        FillBar.fillAmount = 0.1f+Scene.async.progress;
        Progress = (10+Scene.async.progress * 100).ToString("F0") + "%";
        if (Scene.async.progress >= 0.89) {
            _LoadingText.text = "Done";
            //_LoadingText.GetComponent<Animator>().Play("LoadingTextLoadOut");
            //Anim.Play("LoadOut");
            //Abort = true;
        }
    }

    public static void LoadOut() {
        FindObjectOfType<LoadingUI>()._LoadingText.GetComponent<Animator>().Play("LoadingTextLoadOut");
        FindObjectOfType<LoadingUI>().Anim.Play("LoadOut");     
    }

    public string LoadingText {
        get { return _LoadingText.text; }
        set { _LoadingText.text = value; }
    }

    public Image FillBar {
        get { return _FillBar; }
        set { _FillBar = value; }
    }

    public string Progress {
        get { return _Progress.text; }
        set { _Progress.text = value; }
    }

    public void Initialize() {        
        _LoadingText.text = "Loading...";
        _FillBar.fillAmount = 0;
        _Progress.text = "0%";
        Anim.Play("LoadIn");
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;

public class MageMagicOrbAttackState : StateMachineBehaviour {
    bool AllowShoot = true;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        MagicOrb MO = animator.GetComponent<MagicOrb>();
        MO.Master.InAttackingState = true;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        MagicOrb MO = animator.GetComponent<MagicOrb>();        
        if (MO.Master.Stunned || !MO.Master.Alive) {
            animator.CrossFade("Entrance", 0);            
        }
        if (MO.Master is MainPlayer) {
            float RelativeTime = Mathf.Repeat(stateInfo.normalizedTime, 1f);
            if (RelativeTime > 0f && RelativeTime < MO.ProjectileTrigger) {
                AllowShoot = true;
            }
            else if (AllowShoot && RelativeTime >= MO.ProjectileTrigger) {
                if (ControllerManager.AttackVector != Vector2.zero) {
                    if (Client.Connected)
                        NetworkLaunch(MO);
                    else
                        LocalLaunch(MO);
                    AllowShoot = false;
                }
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        MagicOrb MO = animator.GetComponent<MagicOrb>();
        MO.Master.InAttackingState = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    private void LocalLaunch(MagicOrb MO) {        
        WeaponController WC = MO.Master.GetWC();
        MO.Master.ON_ESSENSE_LOSS += MO.Master.DeductEssense;
        MO.Master.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(MO.Master, MO.Master, WC.EssenseCost, CR.RangedAttack));
        MO.Master.ON_ESSENSE_LOSS -= MO.Master.DeductEssense;
        Damage dmg = CombatGenerator.GenerateRawDirectDamage(MO.Master,100,CR.RangedAttack);        
        //Vector2 StartPosition = MO.MagicProjectile.UnScaledColliderRadius * ControllerManager.AttackVector + MO.Master.Position;
        ProjectileData pd = MO.Master.ON_GENERATE_PD(CombatGenerator.GenerateProjectileData(MO.MagicProjectile, MO.Master, ControllerManager.AttackVector, 1, dmg, null));
        CacheManager.Launch(pd);
    }

    private void NetworkLaunch(MagicOrb MO) {
        Damage dmg = CombatGenerator.GenerateRawDirectDamage(MO.Master, 100, CR.RangedAttack);
        //Vector2 StartPosition = MO.MagicProjectile.UnScaledColliderRadius * ControllerManager.AttackVector + MO.Master.Position;
        ProjectileData pd = MO.Master.ON_GENERATE_PD(CombatGenerator.GenerateProjectileData(MO.MagicProjectile, MO.Master, ControllerManager.AttackVector, 1, dmg, null));
        HandyNetwork.SendLaunch(pd);
        WeaponController WC = MO.Master.GetWC();
        MO.Master.ON_ESSENSE_LOSS += MO.Master.DeductEssense;
        MO.Master.ON_ESSENSE_LOSS(CombatGenerator.GenerateEssenseCost(MO.Master, MO.Master, WC.EssenseCost, CR.MageMagicOrbConsume));
        MO.Master.ON_ESSENSE_LOSS -= MO.Master.DeductEssense;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class MenuButtonOnSelect : MonoBehaviour, ISelectHandler {
    public void OnSelect(BaseEventData eventData) {
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position, GameManager.SFX_Volume);
    }   
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillageTeleporter : MonoBehaviour {

    public Vector2 TargetPosition;

    void Start () {
        SpriteRenderer portal = transform.GetComponent<SpriteRenderer>();
        Color col = portal.color;
        col.a = 0.85f;
        portal.color = col;
    }
	
	void Update () {
		
	}

    void OnTriggerStay2D(Collider2D collider)
    {
        //Debug.Log("Please add teleport protocols");
        if (collider.tag == Tag.FriendlyMonster || collider.tag == Tag.FriendlyPlayer || collider.tag == Tag.EnemyPlayer)
        {
            collider.transform.position = TargetPosition;
            return;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using GreedyNameSpace;

public class SkillButton : MonoBehaviour, ISelectHandler {
    //public GameObject SkillInfo;
    SkillInfo SI;

    [HideInInspector]
    public int SkillIndex = -999;

    [HideInInspector]
    public Skill Skill;    
    
    public SkillSubMenu SkillSubMenu;

    string PathName;

    int Tier1_Req = Patch.Tier1_Req;
    int Tier2_Req = Patch.Tier2_Req;
    int Tier3_Req = Patch.Tier3_Req;

    List<int> L_Tier1 = new List<int>() { 0, 1, 2 };
    List<int> L_Tier2 = new List<int>() { 3, 4, 5 };
    List<int> L_Tier3 = new List<int>() { 6, 7, 8 };

    List<int> R_Tier1 = new List<int>() { 9, 10, 11 };
    List<int> R_Tier2 = new List<int>() { 12, 13, 14 };
    List<int> R_Tier3 = new List<int>() { 15, 16, 17 };

    enum SkillPosition { LT1, LT2, LT3, RT1, RT2, RT3 };

    SkillPosition SP;

    [HideInInspector]
    public MainPlayerUI MPUI;
    [HideInInspector]
    public CharacterSheetController CSC;

    ActiveSkillButtonController[] ASBCs;

    public void OnSelect(BaseEventData eventData) {        
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position, GameManager.SFX_Volume);        
    }

    void Awake() {
        SkillSubMenu = (Instantiate(SkillSubMenu.gameObject, transform) as GameObject).GetComponent<SkillSubMenu>();
        SkillSubMenu.transform.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        SkillSubMenu.transform.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
    }
            
    // Use this for initialization
    void Start() {
        SkillIndex = int.Parse(gameObject.name);        
        //MPC = transform.parent.parent.parent.GetComponent<Tab_1>().MPC;
        MPUI = MainPlayer.Self.MPUI;
        CSC = MPUI.transform.Find("CharacterSheet").GetComponent<CharacterSheetController>();
        AssignSkillPosition();
        FetchSkill();
        LoadSkillIcon();
        SI = transform.parent.parent.parent.Find("SkillInfo").GetComponent<SkillInfo>();
        //SkillSubMenu = transform.Find("Skill Sub Menu").GetComponent<SkillSubMenu>();
        //AssignSkillPosition();
        FetchPathName();
        ASBCs = MPUI.transform.Find("Action Bar/Active Skill Panel").GetComponentsInChildren<ActiveSkillButtonController>();
    }

    // Update is called once per frame
    void Update() {
        SkillInfoUpdate();
    }

    public void ActiveSubMenu() {
        SkillSubMenu.TurnOn();
    }

    public void LvlUp() {
        if (CacheManager.MP.CurrentlyHasEffect)
            RedNotification.Push(RedNotification.Type.EDIT_DURING_EFFECTS);
        else if (MainPlayer.Self.GetAvailableSkillPoints() <= 0) {
            RedNotification.Push(RedNotification.Type.NO_SKILL_POINT);
        } else if (!MeetRequirement())
            RedNotification.Push(RedNotification.Type.SKILL_REQUIREMENT_NOT_MET);
        else if (Skill!=null && Skill.lvl == 5) {
            RedNotification.Push(RedNotification.Type.MAX_SKILL_LVL);
        } else {
            MainPlayer.Self.LvlUpSkill(SkillIndex);
            FetchSkill();
            FetechActiveSlotSkills();
            UpdateThisPathSkillImageRootColor();
            SkillSubMenu.TurnOff();
            AudioSource.PlayClipAtPoint(ActionSFX.SkillLvlUp, transform.position, GameManager.SFX_Volume);
        }
    }

    public void Assign() {
        if (Skill.lvl<=0)
            RedNotification.Push(RedNotification.Type.SKILL_NOT_LEARNED);
        else
            EnableActiveSlotsAssigning();
    }

    public static void FactoryReset() {
        foreach(var SB in MainPlayer.Self.MPUI.GetComponentsInChildren<SkillButton>(true)) {
            SB.SkillIndex = int.Parse(SB.gameObject.name);
            SB.FetchSkill();
        }
    }

    void FetchSkill() {        
        if (MainPlayer.Self.GetSkilllvlByIndex(SkillIndex) == 0)
            Skill = MainPlayer.Self.GetRawSkill(SkillIndex);
        else {
            Skill = MainPlayer.Self.GetActualSkill(SkillIndex);
        }
        //if (Skill.lvl <= 0)//Not learned
        //    IconImage.color = MyColor.Grey;
        //else {
        //    IconImage.color = MyColor.White;
        //    if (Skill.lvl == 5)
        //        transform.parent.GetComponent<Image>().color = MyColor.Purple;
        //}        

        if (!MeetRequirement()) {
            GetComponent<Image>().color = MyColor.Grey;
        }
        else {
            GetComponent<Image>().color = MyColor.White;
            //if (Skill.lvl == 5)
            //    transform.parent.GetComponent<Image>().color = MyColor.Purple;
        }
    }

    void UpdateThisPathSkillImageRootColor() {
        SkillButton[] SBs = transform.parent.parent.GetComponentsInChildren<SkillButton>(true);
        foreach(var SB in SBs) {
            if (!SB.MeetRequirement()) {
                SB.GetComponent<Image>().color = MyColor.Grey;
            }
            else {
                SB.GetComponent<Image>().color = MyColor.White;
            }
        }
    }

    void LoadSkillIcon() {
        if (!Skill) {            
            Color ImageIconColor = GetComponent<Image>().color;
            ImageIconColor.a = 0;
            GetComponent<Image>().color = ImageIconColor;
        }
        else{
            GetComponent<Image>().sprite = Resources.Load<Sprite>("SkillIcons/" + Skill.Name);
            Color ImageIconColor = GetComponent<Image>().color;
            ImageIconColor.a = 255;
            GetComponent<Image>().color = ImageIconColor;
        }
    }

    void AssignSkillPosition() {
        if (L_Tier1.Contains(SkillIndex))
            SP = SkillPosition.LT1;
        else if (L_Tier2.Contains(SkillIndex))
            SP = SkillPosition.LT2;
        else if (L_Tier3.Contains(SkillIndex))
            SP = SkillPosition.LT3;
        else if (R_Tier1.Contains(SkillIndex))
            SP = SkillPosition.RT1;
        else if (R_Tier2.Contains(SkillIndex))
            SP = SkillPosition.RT2;
        else if (R_Tier3.Contains(SkillIndex))
            SP = SkillPosition.RT3;
    }

    void SkillInfoUpdate() {
        if (Skill != null && UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == gameObject) {
            transform.parent.GetComponent<Animator>().enabled = true;
            transform.parent.GetComponent<Animator>().SetBool("Blinking", true);
            if (SP == SkillPosition.LT1 || SP == SkillPosition.RT1)
                SI.Show(Skill, PathName, MeetRequirement(), Tier1_Req);
            else if (SP == SkillPosition.LT2 || SP == SkillPosition.RT2)
                SI.Show(Skill, PathName, MeetRequirement(), Tier2_Req);
            else if (SP == SkillPosition.LT3 || SP == SkillPosition.RT3)
                SI.Show(Skill, PathName, MeetRequirement(), Tier3_Req);

        } else {
            transform.parent.GetComponent<Animator>().SetBool("Blinking", false);
            transform.parent.GetComponent<Animator>().enabled = false;
            if (transform.parent.GetComponent<Image>().color.a != 0) {
                Color c = transform.parent.GetComponent<Image>().color;
                c.a = 0;
                transform.parent.GetComponent<Image>().color = c;
            }
        }
    }

    public bool MeetRequirement() {
        if (SP == SkillPosition.LT1 || SP == SkillPosition.RT1)
            return true;
        else if (SP == SkillPosition.LT2) {
            return GetTierSkillPoints(L_Tier1) >= Tier2_Req;
        } else if (SP == SkillPosition.RT2) {
            return GetTierSkillPoints(R_Tier1) >= Tier2_Req;
        } else if (SP == SkillPosition.LT3) {
            return GetTierSkillPoints(L_Tier2) + GetTierSkillPoints(L_Tier1) >= Tier3_Req;
        } else if (SP == SkillPosition.RT3) {
            return GetTierSkillPoints(R_Tier2) + GetTierSkillPoints(R_Tier1) >= Tier3_Req;
        }
        return false;
    }

    int GetTierSkillPoints(List<int> Tier) {
        int total = 0;
        foreach (int skillindex in Tier) {
            total += MainPlayer.Self.GetSkilllvlByIndex(skillindex);
        }
        return total;
    }

    void FetchPathName() {
        if (MainPlayer.Self.Class == CLASS.Warrior) {
            if (SP == SkillPosition.LT1 || SP == SkillPosition.LT2 || SP == SkillPosition.LT3)
                PathName = SkillPath.Berserker.ToString();
            else
                PathName = SkillPath.Warlord.ToString();
        }
        else if (MainPlayer.Self.Class == CLASS.Mage) {            
            if (SP == SkillPosition.LT1 || SP == SkillPosition.LT2 || SP == SkillPosition.LT3)
                PathName = SkillPath.Dominance.ToString();
            else
                PathName = SkillPath.Destruction.ToString();
        }
    }

    void EnableActiveSlotsAssigning() {
        MPUI.ActionBar.Find("AssignToolTip").gameObject.SetActive(true);
        foreach (ActiveSkillButtonController ASBC in ASBCs) {
            ASBC.EnableAssigning(GetComponent<SkillButton>());
        }
        SkillSubMenu.DisableSubmenuNavigation();
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(ASBCs[0].gameObject);
    }

    public void DisableActiveSlotsAssigning() {
        MPUI.ActionBar.Find("AssignToolTip").gameObject.SetActive(false);
        foreach (ActiveSkillButtonController ASBC in ASBCs) {
            ASBC.DisableAssigning();
        }
    }

    void FetechActiveSlotSkills() {
        foreach (ActiveSkillButtonController ASBC in ASBCs) {
            ASBC.FetchSkill();
        }
    }

    public void RestoreCSCMPUIControl() {
        StartCoroutine(_RestoreCSCMPUIControl());
        //MPUI.AllowControl = true;
        //CSC.AllowControl = true;
    }

    IEnumerator _RestoreCSCMPUIControl() {
        yield return new WaitForSeconds(0.1f);
        MPUI.SyncActions = true;
        CSC.SyncActions = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
public class TestEquipments : MonoBehaviour {
    public enum SpawnMode {
        Basic,
        Class
    }

    public SpawnMode SP;
    
    public int SpawnLvl;
	// Use this for initialization
	void Start () {
        StartCoroutine(SpawnWithDelay());
	}private IEnumerator SpawnWithDelay() {        
        yield return new WaitForSeconds(1f);
        if (SP == SpawnMode.Basic) {
            if (SpawnLvl != 0)
                GetComponents<LootSpawner>()[0].SpawnLoots(SpawnLvl);
            else
                GetComponents<LootSpawner>()[0].SpawnLoots(CacheManager.MP.lvl);            
        }
        else if (SP == SpawnMode.Class) {
            switch (CacheManager.MP.Class) {
                case CLASS.Warrior:
                    if (SpawnLvl != 0)
                        GetComponents<LootSpawner>()[0].SpawnLoots(SpawnLvl);
                    else
                        GetComponents<LootSpawner>()[0].SpawnLoots(CacheManager.MP.lvl);
                    break;
                case CLASS.Mage:
                    if (SpawnLvl != 0)
                        GetComponents<LootSpawner>()[1].SpawnLoots(SpawnLvl);
                    else
                        GetComponents<LootSpawner>()[1].SpawnLoots(CacheManager.MP.lvl);
                    break;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

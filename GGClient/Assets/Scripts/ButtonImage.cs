﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonImage : MonoBehaviour {    
    public enum Type {//Last 14
        Submit = 0,
        Cancel = 1,
        Menu = 2,  
        Inventory = 13,   
        SkillTree = 14,   
        Move= 3,
        Attack = 4,
        Next = 5,
        Prev = 6,
        ToggleName = 7,
        Enter = 8,
        Skill_0 = 9,
        Skill_1 = 10,
        Skill_2 = 11,
        Skill_3 = 12,      
        
        Unknown = 999// This option is for fetching on run time               
    }

    public Type ButtonType;


	// Use this for initialization
	void Start () {
        //Debug.Log(gameObject);
        FetechButtonImage();            
	}    

    public void FetchButtonImage(Type T) {        
        switch (T) {
            case Type.Submit:
                GetComponent<Image>().sprite = ButtonImages.X;
                break;
            case Type.Cancel:
                GetComponent<Image>().sprite = ButtonImages.O;
                break;
            case Type.Menu:
                GetComponent<Image>().sprite = ButtonImages.Start;
                break;            
            case Type.Inventory:
                GetComponent<Image>().sprite = ButtonImages.Sqauare;
                break;            
            case Type.SkillTree:
                GetComponent<Image>().sprite = ButtonImages.Triangle;
                break;
            case Type.Move:
                GetComponent<Image>().sprite = ButtonImages.L_Analog_Stick;
                break;
            case Type.Attack:
                GetComponent<Image>().sprite = ButtonImages.R_Analog_Stick;
                break;
            case Type.Next:
                GetComponent<Image>().sprite = ButtonImages.R1;
                break;
            case Type.Prev:
                GetComponent<Image>().sprite = ButtonImages.L1;
                break;
            case Type.ToggleName:
                GetComponent<Image>().sprite = ButtonImages.D_Down;
                break;
            case Type.Enter:
                GetComponent<Image>().sprite = ButtonImages.X;
                break;
            case Type.Skill_0:
                GetComponent<Image>().sprite = ButtonImages.L1;
                break;
            case Type.Skill_1:
                GetComponent<Image>().sprite = ButtonImages.R1;
                break;
            case Type.Skill_2:
                GetComponent<Image>().sprite = ButtonImages.L2;
                break;
            case Type.Skill_3:
                GetComponent<Image>().sprite = ButtonImages.R2;
                break;
        }
    }

    void FetechButtonImage() {        
        switch (ButtonType) {
            case Type.Submit:
                GetComponent<Image>().sprite = ButtonImages.X;
                break;
            case Type.Cancel:
                GetComponent<Image>().sprite = ButtonImages.O;
                break;
            case Type.Menu:
                GetComponent<Image>().sprite = ButtonImages.Start;                
                break;
            case Type.Inventory:
                GetComponent<Image>().sprite = ButtonImages.Sqauare;
                break;
            case Type.SkillTree:
                GetComponent<Image>().sprite = ButtonImages.Triangle;
                break;
            case Type.Move:
                GetComponent<Image>().sprite = ButtonImages.L_Analog_Stick;
                break;
            case Type.Attack:
                GetComponent<Image>().sprite = ButtonImages.R_Analog_Stick;
                break;
            case Type.Next:
                GetComponent<Image>().sprite = ButtonImages.R1;
                break;
            case Type.Prev:
                GetComponent<Image>().sprite = ButtonImages.L1;
                break;
            case Type.ToggleName:
                GetComponent<Image>().sprite = ButtonImages.D_Down;
                break;
            case Type.Enter:
                GetComponent<Image>().sprite = ButtonImages.X;
                break;
            case Type.Skill_0:
                GetComponent<Image>().sprite = ButtonImages.L1;
                break;
            case Type.Skill_1:
                GetComponent<Image>().sprite = ButtonImages.R1;
                break;
            case Type.Skill_2:
                GetComponent<Image>().sprite = ButtonImages.L2;
                break;
            case Type.Skill_3:
                GetComponent<Image>().sprite = ButtonImages.R2;
                break;


            case Type.Unknown:                
                break;                
        }
    }
}

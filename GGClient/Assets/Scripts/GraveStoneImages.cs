﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
public static class GraveStoneImages {    
    public static Sprite Get(GraveStoneID ModelID) {
        return Resources.Load<Sprite>("GraveStoneImages/" + ModelID.ToString());
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StashInteraction : NPCInteraction {        
    public EquipmentInfo EI;

    protected override void Start() {
        base.Start();
        EI = (Instantiate(EI.gameObject, IC.transform) as GameObject).GetComponent<EquipmentInfo>();
        EI.transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        EI.transform.GetComponent<RectTransform>().localScale = new Vector2(0.0015f, 0.0015f);
    }

    public override void SetIndication() {
        InteractionNotification.SetIndication("Stash", ButtonImages.X);
        InteractionNotification.TurnOn();
    }
}

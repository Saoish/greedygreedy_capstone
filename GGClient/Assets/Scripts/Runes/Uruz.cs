﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;
public class Uruz : Rune {
    public float HealPercentageTick = 5f;
    public override void Apply(MainPlayer MP) {
        if (Client.Connected) {
            Client.Send(Protocols.ObjectAddEffect, new AddEffectData(CR.HealingBuff, 0, Client.ClientID, 5f, CombatGenerator.GenerateEffectParameters(CacheManager.MP.MaxHealth * (HealPercentageTick/100))), Client.TCP);
        }
        else
            CacheManager.Effects[CR.HealingBuff].Apply(null, CacheManager.MP, 5f, CombatGenerator.GenerateEffectParameters(CacheManager.MP.MaxHealth * (HealPercentageTick / 100)));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Rune : MonoBehaviour {

    public virtual Rune Spawn(Vector2 Position) {
        return (Instantiate(Resources.Load("RunePrefabs/" + gameObject.name), Position, Quaternion.identity) as GameObject).GetComponent<Rune>();
    }

    protected virtual void Awake() {
        gameObject.layer = CollisionLayer.Rune;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.layer == CollisionLayer.Friendly || collider.gameObject.layer == CollisionLayer.Enemy) {
            ObjectController Target = collider.GetComponent<ObjectController>();
            if (Target is Player) {
                if (Target is MainPlayer)
                    Apply((MainPlayer)Target);
                Expire();
            }
        }
    }

    public abstract void Apply(MainPlayer MP);                 

    public virtual void Expire() {
        Destroy(gameObject);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneSpawner : MonoBehaviour {
    Rune Spawned = null;
    public enum _Mode {
        Cycle,
        Manual
    }
    public _Mode Mode;
    public float CycleDuration;
    public bool PreSpawned = false;
    //public bool Random = false;
    public List<Rune> Runes;

    private void Start() {
        if (PreSpawned)
            Spawn();
        if(Mode == _Mode.Cycle) {
            StartCoroutine(Cycling());
        }
    }

    public void Spawn() {//Doesnt support random mode yet
        if (Spawned)
            Spawned.Expire();
        Spawned = Runes[0].Spawn(transform.position);
    }    
    
    IEnumerator Cycling() {
        while (true) {
            yield return new WaitForSeconds(CycleDuration);
            Spawn();
        }
    }
                          
}

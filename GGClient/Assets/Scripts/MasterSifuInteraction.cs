﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterSifuInteraction : NPCInteraction {    
    public override void SetIndication() {
        InteractionNotification.SetIndication("Talk", ButtonImages.X);
        InteractionNotification.TurnOn();
    }
}

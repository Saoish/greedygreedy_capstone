﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class HomingDetector : MonoBehaviour {
//    [HideInInspector]
//    public Collider2D HomingTargetCollider;

//    public Skill S;
//    BoxCollider2D SelfCollider;

//    public void TurnOn() {
//        gameObject.SetActive(true);
//    }

//    private void Start() {
//        gameObject.layer = CollisionLayer.HomingDetection;
//        SelfCollider = GetComponent<BoxCollider2D>();
//        Physics2D.IgnoreCollision(SelfCollider, S.OC.ObjectCollider);//Ignore self here
//    }

//    private void Update() {
//        float Z_Angle;
//        if (S.OC.Casting) {
//            Z_Angle = Mathf.Atan2(ControllerManager.GetCastVector(ControllerManager.CurrentCastSyncAxis).y, ControllerManager.GetCastVector(ControllerManager.CurrentCastSyncAxis).x) * Mathf.Rad2Deg;
//            transform.localEulerAngles = new Vector3(0, 0, Z_Angle);
//        }
//        else {
//            Z_Angle = Mathf.Atan2(ControllerManager.AttackVector.y, ControllerManager.AttackVector.x) * Mathf.Rad2Deg;
//        }
//        transform.localEulerAngles = new Vector3(0, 0, Z_Angle);
//    }

//    private void OnTriggerEnter2D(Collider2D collider) {
//        if (CombatChecker.ShouldIgnore(S.OC, collider))
//            return;
//        if (HomingTargetCollider) {
//            if (Vector2.Distance(collider.transform.position, S.OC.Position) < Vector2.Distance(HomingTargetCollider.transform.position, S.OC.Position))
//                HomingTargetCollider = collider;
//        }
//        else
//            HomingTargetCollider = collider;
//    }

//    private void OnTriggerExit2D(Collider2D collider) {
//        if (HomingTargetCollider && collider == HomingTargetCollider)
//            HomingTargetCollider = null;
//    }


//}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
public class BlinkIndicator : SkillIndicator {    
    public SpotDetector SD;
    public Transform Spot;    
    private float Radius = 1f;
    private Vector2 CastVector;

    protected override void Update() {
        IndicationUpdate();
    }

    private void IndicationUpdate() {
        if (ControllerManager.GetCastVector(SyncCSA) != Vector2.zero)
            CastVector = ControllerManager.GetCastVector(SyncCSA);
        SD.transform.localPosition = Radius * CastVector;
        Debug.DrawLine(transform.position, SD.transform.position, MyColor.Green);
        if (SD.InSolid) {            
            int layer = (1 << CollisionLayer.Default)/* | (1 << CollisionLayer.KillingGround)*/;
            RaycastHit2D RH = Physics2D.Raycast(transform.position, CastVector, Radius, layer);
            if (RH)
                Spot.localPosition = RH.distance * CastVector;
            else
                Spot.localPosition = SD.transform.localPosition;
        }
        else {            
            Spot.localPosition = SD.transform.localPosition;            
        }
    }

    public override void Active(int ActionSlot) {
        this.ActionSlot = ActionSlot;
        if (ControllerManager.GetCastVector(SyncCSA) != Vector2.zero) {
            CastVector = ControllerManager.GetCastVector(SyncCSA);
        }
        else {
            switch (Root.OC.Direction) {
                case 0:
                    CastVector = new Vector2(0, -1);
                    break;
                case 1:
                    CastVector = new Vector2(-1, 0);
                    break;
                case 2:
                    CastVector = new Vector2(1, 0);
                    break;
                case 3:
                    CastVector = new Vector2(0, 1);
                    break;
            }
        }        
        gameObject.SetActive(true);
        StartCoroutine(CheckAndEnableSpot());
    }IEnumerator CheckAndEnableSpot() {
        while (!SD.Checked)
            yield return null;
        Spot.gameObject.SetActive(true);
    }
    
    public override void Deactive() {
        Spot.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}

﻿using UnityEngine;
using System.Collections;

public class PhantomChargeIndicator : SkillIndicator {
    [HideInInspector]
    public Vector2 CastVector;
    [HideInInspector]
    public float Z_Angle;

    protected override void Update() {
        if (ControllerManager.GetCastVector(SyncCSA) != Vector2.zero) {
            CastVector = ControllerManager.GetCastVector(SyncCSA);            
        }
        Z_Angle = Mathf.Atan2(CastVector.y, CastVector.x) * Mathf.Rad2Deg;
        transform.localEulerAngles = new Vector3(0, 0, Z_Angle);
    }

    public override void Active(int ActionSlot) {
        this.ActionSlot = ActionSlot;
        if (ControllerManager.GetCastVector(SyncCSA) != Vector2.zero) {
            CastVector = ControllerManager.GetCastVector(SyncCSA);
        } else {
            switch (Root.OC.Direction) {
                case 0:
                    CastVector = new Vector2(0, -1);
                    break;
                case 1:
                    CastVector = new Vector2(-1, 0);
                    break;
                case 2:
                    CastVector = new Vector2(1, 0);
                    break;
                case 3:
                    CastVector = new Vector2(0, 1);
                    break;
            }
        }
        Z_Angle = Mathf.Atan2(CastVector.y, CastVector.x) * Mathf.Rad2Deg;
        transform.localEulerAngles = new Vector3(0, 0, Z_Angle);
        gameObject.SetActive(true);
    }
    public override void Deactive() {
        gameObject.SetActive(false);
    }
}

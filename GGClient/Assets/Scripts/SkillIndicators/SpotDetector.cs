﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotDetector : MonoBehaviour {
    [HideInInspector]
    public bool InSolid = false;
    public bool Checked = false;
    private void Awake() {
        gameObject.layer = CollisionLayer.Indication;
    }

    private void OnTriggerStay2D(Collider2D collider) {
        InSolid = true;
        Checked = true;
    }
    private void OnTriggerExit2D(Collider2D collision) {        
        InSolid = false;
        Checked = true;
    }

    private void OnEnable() {
        StartCoroutine(TransitToChecked());
    }
    IEnumerator TransitToChecked() {
        yield return new WaitForSeconds(0.06f);
        Checked = true;
    }

    private void OnDisable() {
        InSolid = false;
        Checked = false;
    }
}

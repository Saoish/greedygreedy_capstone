﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingBlazeIndicator : SkillIndicator {
    float GPS = 2f;
    float ScalingCap = LightingBlaze.ChargeCap;
    [HideInInspector]
    public float ScalingFactor = 1f;
    [HideInInspector]
    public float Z_Angle;
    [HideInInspector]
    public Vector2 CastVector;

    protected override void Update() {
        if (ScalingFactor < ScalingCap) {
            ScalingFactor += (GPS + GPS * Root.OC.CurrHaste / 100) * Time.deltaTime;            
        }
        else {
            ScalingFactor = ScalingCap;
        }
        transform.localScale = new Vector3(ScalingFactor, ScalingFactor, 1);
        if (ControllerManager.GetCastVector(SyncCSA) != Vector2.zero) {
            CastVector = ControllerManager.GetCastVector(SyncCSA);
        }
        Z_Angle = Mathf.Atan2(CastVector.y, CastVector.x) * Mathf.Rad2Deg;
        transform.localEulerAngles = new Vector3(0, 0, Z_Angle);
    }

    public override void Active(int ActionSlot) {        
        this.ActionSlot = ActionSlot;        
        ScalingFactor = 1;
        if (ControllerManager.GetCastVector(SyncCSA) != Vector2.zero) {
            CastVector = ControllerManager.GetCastVector(SyncCSA);
        }
        else {
            switch (ControllerManager.Direction) {
                case 0:
                    CastVector = new Vector2(0, -1);
                    break;
                case 1:
                    CastVector = new Vector2(-1, 0);
                    break;
                case 2:
                    CastVector = new Vector2(1, 0);
                    break;
                case 3:
                    CastVector = new Vector2(0, 1);
                    break;
            }
        }
        Z_Angle = Mathf.Atan2(CastVector.y, CastVector.x) * Mathf.Rad2Deg;
        transform.localEulerAngles = new Vector3(0, 0, Z_Angle);
        transform.localScale = Vector3.one;
        gameObject.SetActive(true);        
        GreedyCamera.LerpZoom(1.5f, 1f);
    }

    public override void Deactive() {
        GreedyCamera.LerpZoom(1, 1f);
        transform.localScale = new Vector3(1, 1, 1);
        gameObject.SetActive(false);
    }
}

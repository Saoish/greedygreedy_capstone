﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostNovaIndicator : SkillIndicator {    
    float GPS = 1f;
    float ScalingCap = FrostNova.ChargeCap;
    [HideInInspector]
    public float ScalingFactor = 1;

    protected override void Update() {
        if (ScalingFactor < ScalingCap) {
            ScalingFactor += (GPS + GPS * Root.OC.CurrHaste / 100) * Time.deltaTime;
        }
        else {
            ScalingFactor = ScalingCap;
        }
        transform.localScale = new Vector3(ScalingFactor, ScalingFactor, 1);
    }

    public override void Active(int ActionSlot) {
        this.ActionSlot = ActionSlot;
        ControllerManager.CurrentCastSyncAxis = SyncCSA;
        ScalingFactor = 1f;
        gameObject.SetActive(true);
    }

    public override void Deactive() {
        transform.localScale = new Vector3(1, 1, 1);
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraveStoneController : MonoBehaviour {
    [HideInInspector]
    public GraveData GD;
        
    public static GraveStoneController Instantiate(GraveData GD) {
        GameObject GraveOJ = Resources.Load<GameObject>("GraveStonePrefabs/" + GD.ModelID.ToString());
        GraveOJ.GetComponent<GraveStoneController>().GD = GD;
        GraveOJ = Instantiate(GraveOJ, GD.SC.Coordinate.ToVector, Quaternion.identity);
        GraveOJ.name = GD.ModelID.ToString();        
        return GraveOJ.GetComponent<GraveStoneController>();
    }
}

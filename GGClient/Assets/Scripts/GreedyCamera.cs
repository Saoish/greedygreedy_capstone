﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;

public class GreedyCamera : MonoBehaviour {
    // Use this for initialization    

    public AudioListener AL;
    static Fader fader;
    public static GreedyCamera instance;

    private static Vector3 Position_Identity = new Vector3(0, 0, -10f);
    private static float Zoom_Identity = 1f;

    private static Coroutine CameraDedicatedZoomCO = null;         

    void Awake() {
        if (instance != null && instance != this) {            
            Destroy(this.gameObject);
        }
        else {
            instance = this;
            DontDestroyOnLoad(this);
            GetComponentInChildren<PopUpNotification>(true).SetUp();
            GetComponentInChildren<GreedyKeyboard>(true).SetUp();
        }        
    }

    void Start () {
        Camera.main.aspect = 1920f / 1080f;
        fader = transform.GetComponentInChildren<Fader>();
    }
	
	// Update is called once per frame
	void Update () {               
    }    

    static bool Lerping = false;

    public static bool Dirty {
        get {                            
            if (Position != Position_Identity ||Zoom != Zoom_Identity || Lerping)
                return true;
            return false;
        }
    }

    public static void SlowMotionEffect(Vector2 TargetPosition) {
        if (Lerping) {            
            return;
        }
        instance.StopAllCoroutines();
        instance.StartCoroutine(SlotMotionEffectStart(TargetPosition));
    }

    static IEnumerator SlotMotionEffectStart(Vector2 TargetPosition) {        
        Time.timeScale = 0.3f;
        Time.fixedDeltaTime = 0.3f * 0.02f;
        LerpCamera(TargetPosition, 0.7f, 0.5f);        
        yield return new WaitForSeconds(0.7f);        
        Time.timeScale = 1f;
        SlotMotionReset(0.5f);

    }

    public static Vector3 LocalPosition {
        get { return Camera.main.transform.localPosition; }
        set { Camera.main.transform.localPosition = value; }
    }

    public static Vector3 Position {
        get { return Camera.main.transform.position; }
        set { Camera.main.transform.position = value; }
    }

    public static float Zoom {
        get { return Camera.main.orthographicSize;}
        set { Camera.main.orthographicSize = value;}
    }

    public static void LerpCamera(Vector2 TargetPosition, float TargetZoom, float LerpTime) {        
        instance.StartCoroutine(LerpCameraStart(TargetPosition, TargetZoom, LerpTime));
    }

    public static void SlotMotionReset(float LerpTime = 0f) {        
        instance.StartCoroutine(SlowMotionReset(LerpTime));
    }

    public static void Reset() {
        instance.StopAllCoroutines();
        fader.StopAllCoroutines();        
        Zoom = 1;
        Time.timeScale = 1f;
        Camera.main.transform.position = new Vector3(0, 0, -10f);
        Lerping = false;
    }

    public static void FadeIn() {
        Detatch();
        fader.FadeIn();        
    }

    public static void FadeOut() {        
        fader.FadeOut();
    }

    public static void Detatch() {        
        Camera.main.transform.SetParent(null);        
    }   

    public static void Attatch() {
        Camera.main.transform.parent = CacheManager.MP.transform;
        Camera.main.transform.localPosition = new Vector3(0, 0, -10f);
    }

    public static void LerpZoom(float TargetZoom,float LerpTime) {//Different thread, will be override by main thread
        if (Lerping) {
            return;
        }
        else if (CameraDedicatedZoomCO != null)
            instance.StopCoroutine(CameraDedicatedZoomCO);        
        CameraDedicatedZoomCO =  instance.StartCoroutine(LerpCameraZoom(TargetZoom, LerpTime));
    }

    static IEnumerator LerpCameraZoom(float TargetZoom, float LerpTime) {        
        float Counter = 0f;
        while (Counter < LerpTime) {
            Counter += Time.fixedDeltaTime;
            float timeProgressed = Counter / LerpTime;
            Zoom = Mathf.Lerp(Zoom, TargetZoom, timeProgressed);
            yield return new WaitForFixedUpdate();
        }                
    }

    static IEnumerator LerpCameraStart(Vector2 TargetPosition,float TargetZoom, float LerpTime) {
        Lerping = true;
        Detatch();        
        float Counter = 0f;
        Vector3 WorldTargetPosition = new Vector3(TargetPosition.x, TargetPosition.y, -10);                
        while (Counter < LerpTime) {
            Counter += Time.fixedDeltaTime;
            float timeProgressed = Counter / LerpTime;
            Position = Vector3.Lerp(Position, WorldTargetPosition, timeProgressed);            
            Zoom = Mathf.Lerp(Zoom, TargetZoom, timeProgressed);
            yield return new WaitForFixedUpdate();
        }
    }

    static IEnumerator SlowMotionReset(float LerpTime) {
        Camera.main.transform.parent = CacheManager.MP.transform;
        float Counter = 0f;
        while (Counter < LerpTime) {
            Counter += Time.fixedDeltaTime;
            float timeProgressed = Counter / LerpTime;
            LocalPosition = Vector3.Lerp(LocalPosition, new Vector3(0,0,-10), timeProgressed);
            Zoom = Mathf.Lerp(Zoom, 1, timeProgressed);
            yield return new WaitForFixedUpdate();
        }
        Lerping = false;
    }

}

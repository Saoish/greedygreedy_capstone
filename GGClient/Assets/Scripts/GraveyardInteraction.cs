﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraveyardInteraction : Interaction {

    public InteractionContent IC; 

    public override bool IsTriggered() {
        return ControllerManager.Actions.Submit.WasPressed;
    }

    public override void Interact() {
        if (FindObjectOfType<OtherPlayer>() != null) {
            RedNotification.Push(RedNotification.Type.DUPLICATED_DIGGER);
        }
        else if (Client.Dominating) {
            if (CacheManager.MP.MPI.Diggable) {
                
                IC.TurnOn();
            }
            else {
                RedNotification.Push(RedNotification.Type.CRAZY_DIGGER);
            }
        }
    }

    public override void SetIndication() {
        if (Client.Dominating && FindObjectOfType<OtherPlayer>()==null && CacheManager.MP.MPI.Diggable) {            
            InteractionNotification.SetIndication("Dig your grave...", ButtonImages.X);
            InteractionNotification.TurnOn();
        }
        else
            InteractionNotification.TurnOff();
    }
}

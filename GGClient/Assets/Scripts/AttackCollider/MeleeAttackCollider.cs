﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using Networking.Data;

public class MeleeAttackCollider : AttackCollider {
    public float AttackRange = 0.1f;//Spawn offset: +x = right, -x = left, +y = up, -y = down    
    public float AttackBoxWidth = 0.32f;
    public float AttackBoxHeight = 0.16f;

    public BoxTrigger BoxTrigger;
    

    public GameObject HitVFX;
    public AudioClip HitSFX;
    public List<AudioClip> DamageSFXList;

    BoxCollider2D SelfCollider;

    ObjectController OC;

    [HideInInspector]
    public Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    protected override void Awake() {
        base.Awake();
        gameObject.layer = CollisionLayer.Melee;
        SelfCollider = GetComponent<BoxCollider2D>();
    }

    protected override void Start() {
        base.Start();
        OC = GetComponentInParent<ObjectController>();
        if (OC != null) {
            Physics2D.IgnoreCollision(SelfCollider, OC.ObjectCollider);            
        }
    }

    protected override void Update () {
        base.Update();
	}

    public void Active() {
        SelfCollider.enabled = true;
    }

    public void Deactive() {
        SelfCollider.enabled = false;
        if (HittedStack.Count != 0)
            HittedStack.Clear();
    }
    
    public BoxCollider2D MeleeCollder {
        get { return GetComponent<BoxCollider2D>(); }
    }

    void NetworkedDealMeleeAttackDMG(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        if (OC is MainPlayer || OC is Monster && target is MainPlayer) {            
            Client.Send(Protocols.ObjectOnHealthLoss, dmg,Client.TCP);
            Heal lph = CombatGenerator.GenerateFlatHeal(OC, OC, dmg.Amount * OC.CurrLPH / 100,false,CR.MeleeAttack);
            Client.Send(Protocols.ObjectOnHealthGain, lph, Client.TCP);            
        }
        if (HitVFX != null) {
            //target.ActiveOneShotVFXParticle(HitVFX);
            target.ActiveOutsideVFXPartical(HitVFX);
        }
        if (DamageSFXList.Count > 0) {
            AudioSource.PlayClipAtPoint(DamageSFXList[UnityEngine.Random.Range(0, DamageSFXList.Count)], target.transform.position, GameManager.SFX_Volume);
        } else if (HitSFX != null)
            AudioSource.PlayClipAtPoint(HitSFX, target.transform.position, GameManager.SFX_Volume);
    }

    void LocalDealMeleeAttackDMG(Damage dmg) {        
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        Heal lph = CombatGenerator.GenerateFlatHeal(OC, OC, dmg.Amount * OC.CurrLPH / 100, false, CR.MeleeAttack);
        OC.ON_HEALTH_GAIN += OC.HealHP;
        OC.ON_HEALTH_GAIN(lph);
        OC.ON_HEALTH_GAIN -= OC.HealHP;
        
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;

        if (HitVFX != null)
            //target.ActiveOneShotVFXParticle(HitVFX);
            target.ActiveOutsideVFXPartical(HitVFX);
        if (DamageSFXList.Count > 0) {
            AudioSource.PlayClipAtPoint(DamageSFXList[UnityEngine.Random.Range(0, DamageSFXList.Count)], target.transform.position, GameManager.SFX_Volume);
        } else if (HitSFX != null)
            AudioSource.PlayClipAtPoint(HitSFX, target.transform.position, GameManager.SFX_Volume);
    }

    void DealMeleeAttackDMG(Damage dmg) {
        if (Client.Connected)
            NetworkedDealMeleeAttackDMG(dmg);
        else
            LocalDealMeleeAttackDMG(dmg);
    }

    protected override void OnTriggerEnter2D(Collider2D collider) {        
        if (CombatChecker.IgnoreDealingDmg(OC, collider, HittedStack))
            return;
        ObjectController target = collider.GetComponent<ObjectController>();
        Damage dmg = CombatGenerator.GenerateDirectDamage(OC, target, 100, CR.MeleeAttack);
        OC.ON_DMG_DEAL += DealMeleeAttackDMG;
        OC.ON_DMG_DEAL(dmg);
        OC.ON_DMG_DEAL -= DealMeleeAttackDMG;
        HittedStack.Push(collider);
    }
}

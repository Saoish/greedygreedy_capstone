﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GreedyNameSpace;

public class VendorInteractionContent : InteractionContent {
    public int RarityMod;
    public int Variation;
    public GameObject SelfToolTip;
    public GameObject VendorToolTip;
    public GameObject CachedButtonObject;
    public GameObject CachedSelfObject;
    public GameObject CachedVendorObject;

    public Transform Self;
    public Transform Vendor;

    [HideInInspector]
    public Equipment[] Goods;

    public void PreStock() {
        Goods = new Equipment[Patch.InventoryCapacity];
        for (int i = 0; i < Patch.InventoryCapacity; i++) {
            Goods[i] = new Equipment();
        }
        EquipmentController[] ECs = Resources.LoadAll<EquipmentController>("EquipmentPrefabs");
        int TotalPicks = Random.Range(4, Patch.InventoryCapacity/2+1);
        List<int> Picked = new List<int>();
        for(int i = 0; i < TotalPicks; i++) {
            Picked.Add(Random.Range(0, ECs.Length));                        
        }

        int min = CacheManager.MP.lvl - Variation < 1 ? 1 : CacheManager.MP.lvl - Variation;
        int max = CacheManager.MP.lvl + Variation > Patch.LvlCap ? Patch.LvlCap : CacheManager.MP.lvl + Variation;        
        for (int i = 0; i < Goods.Length; i++) {
            if (Picked.Count > 0) {
                Goods[i] = ECs[Picked[0]].GetEquipmentForVendor(new Vector2(min, max), RarityMod);
                Picked.Remove(Picked[0]);
            }
        }        
    }
    
    public int FirstAvailableVendorSlot {
        get {
            for(int i = 0; i < Patch.InventoryCapacity; i++) {
                if (Goods[i].isNull)
                    return i;
            }            
            return Patch.InventoryCapacity - 1;
        }
    } 

    void Update() {
        if (!SyncActions)
            return;
        if (ControllerManager.Actions.Cancel.WasPressed)
            TurnOff();
        else if (ControllerManager.Actions.Next.WasPressed) {
            if (EventSystem.current.currentSelectedGameObject.transform.IsChildOf(Self)) {
                CachedSelfObject = EventSystem.current.currentSelectedGameObject;
                Self.GetComponent<CanvasGroup>().interactable = false;
                Vendor.GetComponent<CanvasGroup>().interactable = true;
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(CachedVendorObject);
                VendorToolTip.SetActive(true);
                SelfToolTip.SetActive(false);
            }
        }
        else if (ControllerManager.Actions.Prev.WasPressed) {
            if (EventSystem.current.currentSelectedGameObject.transform.IsChildOf(Vendor)) {
                CachedVendorObject = EventSystem.current.currentSelectedGameObject;
                Vendor.GetComponent<CanvasGroup>().interactable = false;
                Self.GetComponent<CanvasGroup>().interactable = true;
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(CachedSelfObject);
                SelfToolTip.SetActive(true);
                VendorToolTip.SetActive(false);
            }
        }
    }

    public override void TurnOn() {
        ControllerManager.SyncActions = false;
        transform.position = CacheManager.MP.Position;
        if (CachedButtonObject.transform.IsChildOf(Self)) {
            Self.GetComponent<CanvasGroup>().interactable = true;
            SelfToolTip.SetActive(true);
            VendorToolTip.SetActive(false);
        }
        else if (CachedButtonObject.transform.IsChildOf(Vendor)) {
            VendorToolTip.SetActive(true);
            SelfToolTip.SetActive(false);
            Vendor.GetComponent<CanvasGroup>().interactable = true;
        }
        gameObject.SetActive(true);
        SelectWithDelay(CachedButtonObject);
    }

    public override void TurnOff() {
        CachedButtonObject = EventSystem.current.currentSelectedGameObject;
        Self.GetComponent<CanvasGroup>().interactable = false;
        Vendor.GetComponent<CanvasGroup>().interactable = false;
        gameObject.SetActive(false);
        ControllerManager.SyncActions = true;
    }

    protected override void _Interrupt() {
        CachedButtonObject = EventSystem.current.currentSelectedGameObject;
        SyncActions = false;
        Self.GetComponent<CanvasGroup>().interactable = false;
        Vendor.GetComponent<CanvasGroup>().interactable = false;
    }

    protected override void _Resume() {
        SyncActions = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(CachedButtonObject);
        if (CachedButtonObject.transform.IsChildOf(Self))
            Self.GetComponent<CanvasGroup>().interactable = true;
        else if (CachedButtonObject.transform.IsChildOf(Vendor))
            Vendor.GetComponent<CanvasGroup>().interactable = true;
    }
}

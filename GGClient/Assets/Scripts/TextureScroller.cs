﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureScroller : MonoBehaviour {
    //"_MainTex" is the main diffuse texture.
    //"_BumpMap" is the normal map.
    //"_Cube" is the reflection cubemap.
    public float ScrollingSpeed = 1f;
    SpriteRenderer[] SPs;

	// Use this for initialization
	void Start () {
        SPs = GetComponentsInChildren<SpriteRenderer>(true);        
	}
	
	// Update is called once per frame
	void Update () {
        float offset = Time.time * ScrollingSpeed;
        foreach(var SP in SPs) {
            SP.material.SetTextureOffset("_BumpMap", new Vector2(0, offset));
        }
    }
}

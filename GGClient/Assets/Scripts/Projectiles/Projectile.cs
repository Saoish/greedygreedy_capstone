﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Networking.Data;
using GreedyNameSpace;

public class Projectile : MonoBehaviour {
    public AudioClip LaunchSFX;
    public AudioClip ExpireSFX;

    public CircleCollider2D ProjectileCollider;
    [HideInInspector]
    public Damage dmg;
    
    public bool Refelectable = true;
    public bool Explosion = true;
    public float AddictiveExplosionRadius = 0.05f;
    public float Speed = 1f;    

    [HideInInspector]
    public ObjectController Owner;
    [HideInInspector]
    public ObjectController HomingTarget = null;//PreCal

    private Rigidbody2D rb;    
    public float HomingSensity = 30f;//Degree

    public ParticleSystem[] OnGoingEffects;
    public ParticleSystem ExpireEffect;

    Stack<Collider2D> HittedStack = new Stack<Collider2D>();

    private bool Expired = false;

    public float UnScaledColliderRadius {
        get { return ProjectileCollider.radius; }
    }

    public float ScaledColliderRadius(float ScalingFactor) {        
        return ProjectileCollider.radius * ScalingFactor;
    }

    protected void Awake() {
        rb = GetComponent<Rigidbody2D>();        
        gameObject.layer = CollisionLayer.Projectile;
    }

    protected void Start () {
    }

    // Update is called once per frame
    protected void Update () {        
	}

    protected void FixedUpdate() {
        if (HomingTarget)
            HomingFixedUpdate();
    }
    void HomingFixedUpdate() {
        if (/*HomingTarget == null || */!HomingTarget.Alive) {
            HomingTarget = null;
            return;
        }
        Vector2 toTarget = (HomingTarget.Position - (Vector2)transform.position).normalized;
        Force = Vector3.RotateTowards(Force, toTarget, HomingSensity * Mathf.Deg2Rad * Time.fixedDeltaTime, 0);//Don't uss Vector2 MoveForward, it has crazy homing sensitivity                
    }

    public void Launch(ProjectileData pd) {   
        this.Owner = CacheManager.GetOC(pd.dmg.applyerID);
        transform.localScale = new Vector3(pd.Size, pd.Size,1);        
        this.dmg = pd.dmg;                
        if (pd.HomingTargetID != 0)
            HomingTarget = CacheManager.GetOC(pd.HomingTargetID);
        this.Force = pd.LaunchDirection * Speed;

        foreach (var effect in OnGoingEffects) {
            effect.startSize*= pd.Size;
        }
        ExpireEffect.startSize *= pd.Size;
        AudioSource.PlayClipAtPoint(LaunchSFX, transform.position, GameManager.SFX_Volume);
    }

    public Vector2 Force {
        get { return rb.velocity; }
        set { rb.velocity = value; }
    }
    public Vector2 Position {
        get { return rb.position; }
        set { rb.position = value; }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        bool IgnoreDealingDmg = CombatChecker.IgnoreDealingDmg(Owner, collider, HittedStack);
        bool InKillingGround = collider.gameObject.layer == CollisionLayer.Friendly || collider.gameObject.layer == CollisionLayer.Enemy;
        if (collider == Owner.ObjectCollider || collider.gameObject.layer == CollisionLayer.Skill|| InKillingGround && IgnoreDealingDmg)
            return;
        else if (!IgnoreDealingDmg) {            
            ObjectController target = collider.transform.GetComponent<ObjectController>();
            if (CacheManager.GetOC(dmg.applyerID).Alive && CacheManager.GetOC(dmg.applyerID) is MainPlayer || CacheManager.GetOC(dmg.applyerID) is Monster && target is MainPlayer) {
                dmg.PostCalculate(target.NetworkID, target.CurrDefense);                
                CacheManager.GetOC(dmg.applyerID).ON_DMG_DEAL += DealProjectileDamage;
                CacheManager.GetOC(dmg.applyerID).ON_DMG_DEAL(dmg);
                CacheManager.GetOC(dmg.applyerID).ON_DMG_DEAL -= DealProjectileDamage;
                HittedStack.Push(collider);
            }
            Expire();
        }
        else {
            Expire();
        }
    }
    void DealProjectileDamage(Damage dmg) {        
        if (Client.Connected)
            NetworkDealProjectileDamage(dmg);
        else
            LocalDealProjectileDamage(dmg);
                                     
    }
    void NetworkDealProjectileDamage(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        if (dmg.Type == CR.RangedAttack) {            
            Heal lph = CombatGenerator.GenerateFlatHeal(Owner, Owner, dmg.Amount * Owner.CurrLPH / 100, false, dmg.Type);
            Client.Send(Protocols.ObjectOnHealthGain, lph, Client.TCP);
        }
        Client.Send(Protocols.ObjectOnHealthLoss, dmg, Client.TCP);
    }
    void LocalDealProjectileDamage(Damage dmg) {
        ObjectController target = CacheManager.GetOC(dmg.targetID);
        if (dmg.Type == CR.RangedAttack) {
            Heal lph = CombatGenerator.GenerateFlatHeal(Owner, Owner, dmg.Amount * Owner.CurrLPH / 100, false, dmg.Type);
            Owner.ON_HEALTH_GAIN += Owner.HealHP;
            Owner.ON_HEALTH_GAIN(lph);
            Owner.ON_HEALTH_GAIN -= Owner.HealHP;
        }
        target.ON_HEALTH_LOSS += target.DeductHealth;
        target.ON_HEALTH_LOSS(dmg);
        target.ON_HEALTH_LOSS -= target.DeductHealth;        
    }
    
    void Expire() {
        if (Expired)
            return;
        Expired = true;
        StartCoroutine(Explode());
        Force = Vector2.zero;        
        StartCoroutine(ExpirationProcess());
        StartCoroutine(Extinguish());
        AudioSource.PlayClipAtPoint(ExpireSFX, transform.position, GameManager.SFX_Volume);
    }
    IEnumerator Explode() {
        if(Explosion)
            ProjectileCollider.radius += AddictiveExplosionRadius;
        yield return new WaitForSeconds(0.1f);
        ProjectileCollider.enabled = false;
    }
    IEnumerator Extinguish() {
        yield return new WaitForSeconds(ExpireEffect.duration * 0.2f);
        foreach (var effect in OnGoingEffects) {
            effect.enableEmission = false;
        }
    }        
    IEnumerator ExpirationProcess() {
        ExpireEffect.gameObject.SetActive(true);
        yield return new WaitForSeconds(ExpireEffect.duration);        
        Destroy(gameObject);
    }        
}

﻿using UnityEngine;
using System.Collections;

public class LightPulse : MonoBehaviour
{

	public float MaxIntensity = 6.0f;
	public float MinIntensity = 4.0f;
	private Light lt;
	private int flip = 0;
	public int frequency = 4;

	// Use this for initialization
	void Start ()
	{
		lt = GetComponent<Light>();
	}

	void Update ()
	{
		//super awesome one-liner
		if(++flip > frequency)
		{
			lt.intensity = Random.Range(MinIntensity, MaxIntensity);
			flip = 0;
		}
	}
}

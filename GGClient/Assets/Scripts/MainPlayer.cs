﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using GreedyNameSpace;

using Networking.Data;



public class MainPlayer : Player {    
    public CLASS Developing;

    public MainPlayerUI MPUI;
    public MainPlayerInteraction MPI;

    public Objective Objective;

    [HideInInspector]
    public bool LastRegisteredMovingState = false;
    [HideInInspector]
    public bool LastRegisteredAttackingState = false;
    [HideInInspector]
    public int LastRegisteredDirection = 0;

    //Netwokr Active Monster Instance State Sync
    //[HideInInspector]
    //public List<Monster> BeenHunteds;
    float SyncMonsterPositionInterval = 1f;
    float SynccMonsterPositionCounter = 0f;

    float EssenceRegenCounter = 0f;
    float EssenceRegenInterval = 0.1f;

    float HealthRegenCounter = 0f;
    float HealthRegenInterval = 5f;

    public static int Level {
        get { return CacheManager.MP_Data.lvl; }
    }

    public static PlayerData MainData {
        get { return CacheManager.MP_Data; }
    }

    public static MainPlayer Self {
        get { return CacheManager.MP; }
    }

    public static Vector3 V3_Pos {
        get { return CacheManager.MP.transform.position; }
    }

    [HideInInspector]
    public Queuemode CurrentQueue = Queuemode.None;
        
    public static bool HasGravestone {
        get { return CacheManager.Gravestones.ContainsKey(Client.ClientID); }
    }

    public static Queuemode CQ {
        get {return CacheManager.MP.CurrentQueue; }
    }
    public static bool InQueue {
        get { return CacheManager.MP.CurrentQueue != Queuemode.None;  }
    }
    public static void DropQueue() {
        CacheManager.MP.CurrentQueue = Queuemode.None;
        QueuingInfo.Push(CacheManager.MP.CurrentQueue);
    }
    public static void Queue(Queuemode QM) {
        CacheManager.MP.CurrentQueue = QM;
        QueuingInfo.Push(CacheManager.MP.CurrentQueue);
        AudioSource.PlayClipAtPoint(ActionSFX.QueueUp, CacheManager.MP.transform.position, GameManager.SFX_Volume);
    }

    public override ObjectController Instantiate(int NetworkID,bool Reset) {        
        if (!Alive && Reset) {
            if (Scene.Current_SID != Data.RespawnSC.SID)
                Data.RespawnSC = CacheManager.Map.GetCloestestRespawnSC(Data.SC.Coordinate.ToVector);
            Data.SC = new SceneCoordinate(Data.RespawnSC);
            Data.OnGoingEffects.Clear();         
        }
        GameObject PlayerOJ = Resources.Load("PlayerPrefabs/MainPlayer") as GameObject;
        PlayerOJ = Instantiate(PlayerOJ, Data.SC.Coordinate.ToVector, Quaternion.identity) as GameObject;
        PlayerOJ.GetComponent<MainPlayer>().Reset = Reset;
        PlayerOJ.GetComponent<MainPlayer>().Data = Data;
        PlayerOJ.name = "MainPlayer";
        CacheManager.MP = PlayerOJ.GetComponent<MainPlayer>();
        GreedyCamera.Attatch();
        PlayerOJ.GetComponent<MainPlayer>().NetworkID = NetworkID;
        return PlayerOJ.GetComponent<MainPlayer>();
    }
    private void Mage() {
        string FilePath = "TestData/Mage.txt";
        if (!File.Exists(FilePath)) {
            Data = new PlayerData();            
            ((PlayerData)Data).Name = "Testing";
            ((PlayerData)Data).Class = CLASS.Mage;            
            ((PlayerData)Data).SkillPoints = 99;
            ((PlayerData)Data).BaseStats.Grow(Patch.GetGrowth(((PlayerData)Data).Class));
            string json = JsonUtility.ToJson(Data);
            StreamWriter SaveStream = new StreamWriter(FilePath);
            SaveStream.Write(json);
            SaveStream.Close();
        }
        else {
            StreamReader LoadStream = new StreamReader(FilePath);
            string json = LoadStream.ReadToEnd();
            Data = JsonUtility.FromJson<PlayerData>(json);
        }
        this.OID = Data.OID;
        CacheManager.AddPlayer(Client.ClientID, (PlayerData)Data);
        CacheManager.Objects[Client.ClientID].OC = this;
        Client.Dominating = true;
        NetworkID = Client.ClientID;
        CacheManager.MP = this;        
        GreedyCamera.Attatch();
    }
    private void Warrior() {
        string FilePath = "TestData/Warrior.txt";
        if (!File.Exists(FilePath)) {
            Data = new PlayerData();
            ((PlayerData)Data).Name = "Testing";
            ((PlayerData)Data).Class = CLASS.Warrior;
            ((PlayerData)Data).SkillPoints = 99;
            ((PlayerData)Data).BaseStats.Grow(Patch.GetGrowth(((PlayerData)Data).Class));
            string json = JsonUtility.ToJson(Data);
            StreamWriter SaveStream = new StreamWriter(FilePath);
            SaveStream.Write(json);
            SaveStream.Close();
        }
        else {
            StreamReader LoadStream = new StreamReader(FilePath);
            string json = LoadStream.ReadToEnd();
            Data = JsonUtility.FromJson<PlayerData>(json);
        }
        this.OID = Data.OID;
        CacheManager.AddPlayer(Client.ClientID, (PlayerData)Data);
        CacheManager.Objects[Client.ClientID].OC = this;
        Client.Dominating = true;
        NetworkID = Client.ClientID;
        CacheManager.MP = this;
        GreedyCamera.Attatch();
    }
    // For developing purpose only
    private void LoadDevelopingPlayerData() {
        switch (Developing) {
            case CLASS.Warrior:
                Warrior();
                break;
            case CLASS.Mage:
                Mage();
                break;
        }
        CacheManager.UserData = new UserData();
        CacheManager.UserData.Souls = 999999999;
    }
    private void SaveDevelopingPlayerData() {
        StreamWriter SaveStream;
        switch (Developing) {
            case CLASS.Warrior:
                SaveStream = new StreamWriter("TestData/Warrior.txt");
                break;
            case CLASS.Mage:
                SaveStream = new StreamWriter("TestData/Mage.txt");
                break;
            default:
                SaveStream = new StreamWriter("Let's crash");
                break;
        }        
        string json = JsonUtility.ToJson(Data);
        SaveStream.Write(json);
        SaveStream.Close();
    }

    protected override void Awake() {        
        //For developing purpose        
        if (!Client.Connected) {
            
            LoadDevelopingPlayerData();
        }
        //For developing purpose
        //Objective.Set(FindObjectOfType<NPC>().transform);
        base.Awake();
    }

    private void SpawnFirstTimeLoginLoot() {
        if (CurrStats.IsNull) {//FirstTime Login
            LootSpawner[] LootSpawners = GetComponents<LootSpawner>();
            switch (Class) {
                case CLASS.Warrior:
                    LootSpawners[0].SpawnLoots(lvl);
                    break;
                case CLASS.Mage:
                    LootSpawners[1].SpawnLoots(lvl);
                    break;
            }
        }
    }
    protected override void Start() {
        SpawnFirstTimeLoginLoot();        
        base.Start();
        TopNotification.Push("~ " + Scene.Current_SName + " ~", MyColor.White, 3f);
        ControllerManager.SyncActions = true;        
    }

    protected override void FixedUpdate() {
        base.FixedUpdate();
        SyncInstanceActiveMonsterCoordinates();        
        EssenceRegen();
        HealthRegen();        
    }
    
    private void SyncInstanceActiveMonsterCoordinates() {
        if (!SceneChecker.TargetInWorldStates(Scene.Current_SID))
            return;
        if (SynccMonsterPositionCounter >= SyncMonsterPositionInterval) {            
            int layer = (1 << CollisionLayer.Enemy);                                                
            Collider2D[] colliders = Physics2D.OverlapCircleAll(Position, 10f, layer);//10 is large                   
            if (colliders.Length == 0)
                return;           
            SyncInstanceActiveMonsterCoordinatesData SIAMCD = new SyncInstanceActiveMonsterCoordinatesData();                        
            foreach (var collider in colliders) {                
                if (collider.tag == Tag.EnemyMonster) {
                    Monster MC = collider.GetComponent<Monster>();
                    if (MC.AI.Target == this) {                        
                        SIAMCD.Add(new ID_Coordinates(MC.NetworkID, MC.Position));
                    }
                }
            }
            if (SIAMCD.Count > 0) {                
                Client.SendPacakage(SIAMCD, new Decipher(Protocols.SyncInstanceActiveMonsterCoordinates, null, Client.ClientID));
            }
            SynccMonsterPositionCounter = 0f;
        }
        else {
            SynccMonsterPositionCounter += Time.fixedDeltaTime;
        }                   
    }

    void EssenceRegen() {
        if (!Alive) {
            EssenceRegenCounter = 0;
            return;
        }
        if (EssenceRegenCounter >= EssenceRegenInterval) {
            if (GetCurrStats(STATSTYPE.ESSENCE_REGEN) > 0f) {
                if (GetCurrStats(STATSTYPE.ESSENCE) + GetCurrStats(STATSTYPE.ESSENCE_REGEN) / 10 > GetMaxStats(STATSTYPE.ESSENCE))
                    SetCurrStats(STATSTYPE.ESSENCE, GetMaxStats(STATSTYPE.ESSENCE));
                else
                    AddCurrStats(STATSTYPE.ESSENCE, GetCurrStats(STATSTYPE.ESSENCE_REGEN) / 10);
            }
            EssenceRegenCounter = 0;
        }
        else {
            EssenceRegenCounter += Time.fixedDeltaTime;
        }
    }

    void HealthRegen() {
        if (!Alive) {
            HealthRegenCounter = 0;
            return;
        }
        if (HealthRegenCounter >= HealthRegenInterval) {
            if (CurrHealthRegen > 0f) {
                Heal Heal = CombatGenerator.GenerateFlatHeal(this, this, CurrHealthRegen, false, CR.HealthRegen);
                if (Client.Connected) {
                    Client.Send(Protocols.ObjectOnHealthGain, Heal, Client.TCP);
                }
                else {
                    ON_HEALTH_GAIN += HealHP;
                    ON_HEALTH_GAIN(Heal);
                    ON_HEALTH_GAIN -= HealHP;
                }                
            }
            HealthRegenCounter = 0;
        }
        else {
            HealthRegenCounter += Time.fixedDeltaTime;
        }
    }

    //void Regen() {//No composition on events, and don't give health regen to monster or npc just yet, so far only works for Player
    //    if (EssenceRegenCounter >= EssenceRegenInterval) {
    //        if (GetCurrStats(STATSTYPE.ESSENCE_REGEN) > 0f) {
    //            if (GetCurrStats(STATSTYPE.ESSENCE) + GetCurrStats(STATSTYPE.ESSENCE_REGEN) / 10 > GetMaxStats(STATSTYPE.ESSENCE))
    //                SetCurrStats(STATSTYPE.ESSENCE, GetMaxStats(STATSTYPE.ESSENCE));
    //            else
    //                AddCurrStats(STATSTYPE.ESSENCE, GetCurrStats(STATSTYPE.ESSENCE_REGEN) / 10);
    //        }
    //        if (GetCurrStats(STATSTYPE.HEALTH_REGEN) > 0f) {
    //            if (GetCurrStats(STATSTYPE.HEALTH) + GetCurrStats(STATSTYPE.HEALTH_REGEN) / 10 > GetMaxStats(STATSTYPE.HEALTH))
    //                CurrStats.Health = MaxStats.Health;
    //            else
    //                CurrStats.Health += CurrStats.Health_Regen / 10;
    //        }
    //        EssenceRegenCounter = 0;
    //    }
    //    else {
    //        EssenceRegenCounter += Time.fixedDeltaTime;
    //    }
    //}

    protected override void MoveFixedUpdate() {
        if (Client.Connected)
            NetworkMoveUpdate();
        else
            LocalMoveUpdate();
    }

    void NetworkMoveUpdate() {
        //if (Moving) {
        //    ProcessContacts();
        //    Vector2 MoveVector = ControllerManager.MoveVector;
        //    foreach (var Contacted_OJ in Contacts) {
        //        Vector2 ContactDirection = Vector3.Normalize(Position - Contacted_OJ.Position);
        //        if (MoveVector.x > 0 && ContactDirection.x < 0 || MoveVector.x < 0 && ContactDirection.x > 0) {
        //            MoveVector.x = 0;
        //        }
        //        if (MoveVector.y > 0 && ContactDirection.y < 0 || MoveVector.y < 0 && ContactDirection.y > 0) {
        //            MoveVector.y = 0;
        //        }
        //        if (MoveVector == Vector2.zero)
        //            break;
        //    }
        //    Position = rb.position + MoveVector * (CurrMoveSpeed / 100) * Time.fixedDeltaTime;
        //    Client.Send(Protocols.UpdateObjectPosition, new PositionData(NetworkID, Position), Client.UDP);
        //    SC.Coordinate.ToVector = Position;
        //}

        if (Moving) {
            Vector2 MoveVector = ControllerManager.MoveVector;
            foreach (var Contacted_OJ in Contacts) {
                Vector2 ContactDirection = Vector3.Normalize(Position - (Vector2)Contacted_OJ.transform.position);
                if (MoveVector.x > 0 && ContactDirection.x < 0 || MoveVector.x < 0 && ContactDirection.x > 0) {
                    MoveVector.x = 0;
                }
                if (MoveVector.y > 0 && ContactDirection.y < 0 || MoveVector.y < 0 && ContactDirection.y > 0) {
                    MoveVector.y = 0;
                }
                if (MoveVector == Vector2.zero)
                    break;
            }
            Position = rb.position + MoveVector * (CurrMoveSpeed / 100) * Time.fixedDeltaTime;
            Client.Send(Protocols.UpdateObjectPosition, new PositionData(NetworkID, Position,Direction), Client.UDP);
            SC.Coordinate.ToVector = Position;
        }
    }
    void LocalMoveUpdate() {
        //if (Moving) {
        //    ProcessContacts();
        //    Vector2 MoveVector = ControllerManager.MoveVector;
        //    foreach (var Contacted_OJ in Contacts) {
        //        Vector2 ContactDirection = Vector3.Normalize(Position - Contacted_OJ.Position);
        //        if (MoveVector.x > 0 && ContactDirection.x < 0 || MoveVector.x < 0 && ContactDirection.x > 0) {
        //            MoveVector.x = 0;
        //        }
        //        if (MoveVector.y > 0 && ContactDirection.y < 0 || MoveVector.y < 0 && ContactDirection.y > 0) {
        //            MoveVector.y = 0;
        //        }
        //        if (MoveVector == Vector2.zero)
        //            break;
        //    }
        //    rb.MovePosition(rb.position + MoveVector * (CurrMoveSpeed / 100) * Time.fixedDeltaTime);
        //    SC.Coordinate.ToVector = Position;
        //}

        if (Moving) {
            Vector2 MoveVector = ControllerManager.MoveVector;
            foreach (var Contacted_OJ in Contacts) {
                Vector2 ContactDirection = Vector3.Normalize(Position - (Vector2)Contacted_OJ.transform.position);
                if (MoveVector.x > 0 && ContactDirection.x < 0 || MoveVector.x < 0 && ContactDirection.x > 0) {
                    MoveVector.x = 0;
                }
                if (MoveVector.y > 0 && ContactDirection.y < 0 || MoveVector.y < 0 && ContactDirection.y > 0) {
                    MoveVector.y = 0;
                }
                if (MoveVector == Vector2.zero)
                    break;
            }
            rb.MovePosition(rb.position + MoveVector * (CurrMoveSpeed / 100) * Time.fixedDeltaTime);
            SC.Coordinate.ToVector = Position;
        }
    }

    //Networked Combat Stats Communication
    public override void HealHP(Heal heal_hp) {
        base.HealHP(heal_hp);
        if (!SceneChecker.TargetInArenas(Scene.Current_SID)) {//Don't alter the world state            
            Client.Send(Protocols.UpdateCurrHealth, new UpdateCurrHealthData(NetworkID, CurrHealth), Client.TCP);
        }
    }
    

    override public void DeductHealth(Damage dmg) {
        if (InfusedPortal)
            InfusedPortal.Defuse(this);
        IC.PopUpText(dmg);
        if (dmg.Crit) {            
            VisualHolderAnim.Play("crit");
        }
        if (CurrStats.Get(STATSTYPE.HEALTH) - dmg.Amount <= 0 && Alive) {
            ON_DEATH_UPDATE += Die;
            ON_DEATH_UPDATE(dmg);
            ON_DEATH_UPDATE -= Die;
        }
        else {
            CurrStats.Dec(STATSTYPE.HEALTH, dmg.Amount);
            if (!SceneChecker.TargetInArenas(Scene.Current_SID)) {//Don't alter the world state            
                Client.Send(Protocols.UpdateCurrHealth, new UpdateCurrHealthData(NetworkID, CurrHealth), Client.TCP);
            }
        }
        PopUpNotification.AbortWithNo();
    }

    public override void Die(Damage dmg) {
        MPUI.Close();
        ActiveSkillButtonController.InterruptAll();
        PopUpNotification.Close();
        InteractionContent.Close();                    
        ControllerManager.SyncActions = false;       
        if (Client.Connected)
            NetworkDie(dmg);
        else
            LocalDie(dmg);
    }

    private void NetworkDie(Damage dmg) {
        Client.Send(Protocols.ObjectOnDeath, dmg, Client.TCP);        
        base.Die(dmg);
        if (!SceneChecker.TargetInArenas(Scene.Current_SID)) {//Don't alter the world state            
            //Client.Send(Protocols.UpdateCurrHealth, new UpdateCurrHealthData(NetworkID, CurrHealth), Client.TCP);
            Client.Send(Protocols.UpdateToDeadState, new UpdateToDeadStateData(NetworkID,CacheManager.Map.GetCloestestRespawnSC(Position)), Client.TCP);
        }
        StartCoroutine(DeathPositionSync());
    }private IEnumerator DeathPositionSync() {//Critical to sync its death position
        yield return new WaitForSeconds(0.5f);
        while (HasForce)
            yield return null;
        Client.Send(Protocols.UpdateObjctDeadPosition, new PositionData(NetworkID, Position, Direction), Client.TCP);                
        SC.Coordinate.ToVector = Position;
    }

    private void LocalDie(Damage dmg) {//Not supported yet
        base.Die(dmg);        
        //TopNotification.Push("Your soul is fading...", MyColor.Red, 3f);
        //Destroy(transform.gameObject, 4f);
        //Scene.Load(SceneID.Developing, 5f);
    }

    protected override void ControlUpdate() {        
        if (Client.Connected) {
            NetworkControlUpdate();
        } else {
            SinglePlayerControlUpdate();
        }
    }
   void NetworkControlUpdate() {
        if(Stunned || !Alive) {
            Attacking = false;            
            Moving = false;
            LastRegisteredAttackingState = false;
            LastRegisteredMovingState = false;
        }
        else if (Casting || Infusing) {
            Attacking = false;
            Moving = false;
            LastRegisteredAttackingState = false;
            LastRegisteredMovingState = false;
            Direction = ControllerManager.Direction;
            if(LastRegisteredDirection != ControllerManager.Direction) {
                Client.Send(Protocols.UpdateObjectDirection, new DirectionData(NetworkID, ControllerManager.Direction), Client.UDP);
                LastRegisteredDirection = ControllerManager.Direction;
            }
        }
        else if (Immobilized) {
            Moving = false;
            LastRegisteredMovingState = false;
            if (LastRegisteredAttackingState != ControllerManager.Attacking) {
                Client.Send(Protocols.UpdateObjectAttackState, new AttackData(NetworkID, ControllerManager.Attacking, ControllerManager.Direction), Client.UDP);
                LastRegisteredAttackingState = ControllerManager.Attacking;
                LastRegisteredDirection = ControllerManager.Direction;
            }
            if (LastRegisteredDirection != ControllerManager.Direction) {
                Client.Send(Protocols.UpdateObjectDirection, new DirectionData(NetworkID, ControllerManager.Direction), Client.UDP);
                LastRegisteredDirection = ControllerManager.Direction;
            }
        }
        else {
            if(GetWC()!=null && CurrEssense - GetWC().EssenseCost < 0) {
                if (Attacking) {
                    Attacking = false;
                    Client.Send(Protocols.UpdateObjectAttackState, new AttackData(NetworkID, Attacking, ControllerManager.Direction),Client.UDP);
                    LastRegisteredAttackingState = Attacking;
                    LastRegisteredDirection = ControllerManager.Direction;
                }
                if (ControllerManager.Attacking)
                    RedNotification.Push(RedNotification.Type.NO_ESSENCE);
            } 
            else {
                if (LastRegisteredAttackingState != ControllerManager.Attacking) {
                    Client.Send(Protocols.UpdateObjectAttackState, new AttackData(NetworkID, ControllerManager.Attacking,ControllerManager.Direction), Client.UDP);
                    LastRegisteredAttackingState = ControllerManager.Attacking;
                    LastRegisteredDirection = ControllerManager.Direction;
                }
                if (Attacking && LastRegisteredDirection != ControllerManager.Direction) {
                    Client.Send(Protocols.UpdateObjectDirection, new DirectionData(NetworkID, ControllerManager.Direction), Client.UDP);
                    LastRegisteredDirection = ControllerManager.Direction;
                }
            }
            if (HasForce) {
                Moving = false;
                LastRegisteredMovingState = false;
            }
            else {
                Moving = ControllerManager.Moving;
                LastRegisteredMovingState = ControllerManager.Moving;
                Direction = ControllerManager.Direction;
                LastRegisteredDirection = ControllerManager.Direction;
            }


            ///This part turns into local, other client will detect it's state by check it's netork position      
            //else {
            //    Moving = ControllerManager.Moving;
            //    LastRegisteredMovingState = ControllerManager.Moving;
            //    Direction = ControllerManager.Direction;
            //    LastRegisteredDirection = ControllerManager.Direction;
            //}
            ///***

            //else {
            //    if (LastRegisteredMovingState != ControllerManager.Moving) {
            //        Client.Send(Protocols.UpdateObjectMoveState, new MovementData(NetworkID, ControllerManager.Moving, ControllerManager.Direction), Client.UDP);
            //        LastRegisteredMovingState = ControllerManager.Moving;
            //        LastRegisteredDirection = ControllerManager.Direction;
            //    }
            //}
            //if (LastRegisteredDirection != ControllerManager.Direction) {
            //    Client.Send(Protocols.UpdateObjectDirection, new DirectionData(NetworkID, ControllerManager.Direction), Client.UDP);
            //    LastRegisteredDirection = ControllerManager.Direction;
            //}
        }
    }
    
    protected void SinglePlayerControlUpdate() {//Single player
        if (Stunned || !Alive) {
            Attacking = false;
            Moving = false;
        } else if (Casting || Infusing) {
            Attacking = false;
            Moving = false;
            Direction = ControllerManager.Direction;
        }else if (Immobilized) {
            Moving = false;
            Attacking = ControllerManager.Attacking;
            Direction = ControllerManager.Direction;                        
        }
        else {
            if (GetWC() != null && CurrEssense - GetWC().EssenseCost < 0) {
                Attacking = false;
                if (ControllerManager.Attacking)
                    RedNotification.Push(RedNotification.Type.NO_ESSENCE);
            } else {
                Attacking = ControllerManager.Attacking;
            }
            if (HasForce) {                
                Moving = false;
            } else {
                Moving = ControllerManager.Moving;
            }
            Direction = ControllerManager.Direction;
        }
    }
    //These two function will be deleted

    //Stats Handling
    public void AddStatPoint(string Stat) {//Will be replaced with lvlup fomular
        //if (Stat == "Health")
        //    PlayerData.BaseHealth += StatModule.Health_Weight;
        //else if (Stat == "Mana")
        //    PlayerData.BaseMana += StatModule.Mana_Weight;
        //else if (Stat == "AD")
        //    PlayerData.BaseAD += StatModule.AD_Weight;
        //else if (Stat == "MD")
        //    PlayerData.BaseMD += StatModule.MD_Weight;
        //else if (Stat == "AttkSpd")
        //    PlayerData.BaseAttkSpd += StatModule.AttkSpd_Weight;
        //else if (Stat == "MoveSpd")
        //    PlayerData.BaseMoveSpd += StatModule.MoveSpd_Weight;
        //else if (Stat == "Defense")
        //    PlayerData.BaseDefense += StatModule.Defense_Weight;
        //else if (Stat == "CritChance")
        //    PlayerData.BaseCritChance += StatModule.CritChance_Weight;
        //else if (Stat == "CritDmgBounus")
        //    PlayerData.BaseCritDmgBounus += StatModule.CritDmgBounus_Weight;
        //else if (Stat == "LPH")
        //    PlayerData.BaseLPH += StatModule.LPH_Weight;
        //else if (Stat == "ManaRegen")
        //    PlayerData.BaseManaRegen += StatModule.ManaRegen_Weight;
        //PlayerData.StatPoints--;
        //SaveLoadManager.SaveCurrentPlayerInfo();
        //UpdateStats();
    }

    //Netorked related functions
    protected override void UpdateStats() {
        base.UpdateStats();
        if (!SceneChecker.TargetInArenas(Scene.Current_SID)) {//Don't alter the world state            
            Client.Send(Protocols.UpdateCurrStats, new UpdateCurrStatsData(NetworkID, CurrStats), Client.TCP);
        }
    }

    public void AddEXP(int exp) {
        if (lvl < Patch.LvlCap) {
            if (this.exp + exp >= NextLevelExp) {
                if (Client.Connected)
                    Client.SendProtocol(Protocols.LevelUp, Client.TCP);
                LevelUp();                
            }
            else {
                if(Client.Connected)
                    Client.Send(Protocols.AddExp, exp, Client.TCP);
                this.exp += exp;
            }
            //CheckLevelUp();
        }
    }


    public void SetActiveSkillAt(int Slot, Skill ActiveSkill) {//This methods is for main player only, enemy/friendly player would not update their active slots        
        if (ActiveSkill == null)
            ((PlayerData)Data).ActiveSlotData[Slot] = CR.None;
        else
            ((PlayerData)Data).ActiveSlotData[Slot] = ActiveSkill.Type;
        if (Client.Connected) {
            Client.Send(Protocols.SetActiveSkillAction, new SetActiveSkillActionData(Slot, ((PlayerData)Data).ActiveSlotData[Slot]), Client.TCP);
        } else {
            SaveDevelopingPlayerData();
        }
    }

    public override void LvlUpSkill(int SkillIndex) {
        if (Client.Connected) {
            Client.Send(Protocols.LvlUpSkillAction, new LvlUpSkillData(NetworkID, SkillIndex), Client.TCP);
            base.LvlUpSkill(SkillIndex);
        } else {
            base.LvlUpSkill(SkillIndex);
            SaveDevelopingPlayerData();
        }
    }

    public override void Respec() {        
        if (Client.Connected) {
            Client.SendProtocol(Protocols.Respec,Client.TCP);
            CacheManager.UserData.Souls -= lvl * Patch.RespecCostIncRate;
            base.Respec();            
        }
        else {
            CacheManager.UserData.Souls -= lvl * Patch.RespecCostIncRate;
            base.Respec();            
        }
        ActiveSkillButtonController.FactoryReset();
        SkillButton.FactoryReset();
    }

    public override void Equip(Equipment E) {
        if (Client.Connected) {
            Client.Send(Protocols.EquipAction, new EquipActionData(NetworkID, E), Client.TCP);
            base.Equip(E);
        } else {
            base.Equip(E);
            SaveDevelopingPlayerData();
        }
    }

    public override void UnEquip(EQUIPTYPE Slot) {
        if (Client.Connected) {
            Client.Send(Protocols.UnEquipAction, new UnEquipActionData(NetworkID, (int)Slot), Client.TCP);
            base.UnEquip(Slot);
        } else {
            base.UnEquip(Slot);
            SaveDevelopingPlayerData();
        }
    }    

    public override void AddToInventory(int Slot, Equipment E) {
        if (Client.Connected) {
            Client.Send(Protocols.AddtoInventoryAction, new AddToInventoryData(Slot, E), Client.TCP);
            base.AddToInventory(Slot, E);
        } else {
            base.AddToInventory(Slot, E);
            SaveDevelopingPlayerData();
        }
    }

    public override void RemoveFromInventory(int Slot) {
        if (Client.Connected) {
            Client.Send(Protocols.RemoveFromInventoryAction, new RemoveFromInventoryData(Slot), Client.TCP);
            base.RemoveFromInventory(Slot);
        } else {
            base.RemoveFromInventory(Slot);
            SaveDevelopingPlayerData();
        }
    }

    
    public Equipment GetStashItem(int Slot) {
        return (CacheManager.UserData).Stash[Slot];
    }

    public int GetFirstAvailableStashSlot(int Page) {
        for(int i = 0 + Patch.InventoryCapacity*Page; i < (Page + 1) * Patch.InventoryCapacity; i++) {
            if (CacheManager.UserData.Stash[i].isNull)
                return i;
        }
        return (Page + 1) * Patch.InventoryCapacity;
    }

    public void Switch_I2S(int InventorySlot,int StashSlot) {//This feature only works for network        
        Client.Send(Protocols.Switch_I2S_Action, new InventoryStashSwitchingData(InventorySlot, StashSlot),Client.TCP);
        CacheManager.UserData.Stash[StashSlot] = GetInventoryItem(InventorySlot);
        ((PlayerData)Data).Inventory[InventorySlot] = new Equipment();
    }

    public void Switch_S2I(int InventorySlot, int StashSlot) {//This feature only works for network        
        Client.Send(Protocols.Switch_S2I_Action, new InventoryStashSwitchingData(InventorySlot, StashSlot), Client.TCP);
        ((PlayerData)Data).Inventory[InventorySlot] = GetStashItem(StashSlot);
        CacheManager.UserData.Stash[StashSlot] = new Equipment();        
    }

    public void Sell(int InventorySlot) {//This feature only works for network  
        Client.Send(Protocols.SellAction, InventorySlot, Client.TCP);
        CacheManager.UserData.Souls += ((PlayerData)Data).Inventory[InventorySlot].Value;
        ((PlayerData)Data).Inventory[InventorySlot] = new Equipment();
    }

    public void Buy(int InventorySlot, Equipment E) {//This feature only works for network  
        Client.Send(Protocols.BuyAction, E, Client.TCP);
        CacheManager.UserData.Souls -= E.Value;
        E.Value /= Patch.HeartlessRate;
        ((PlayerData)Data).Inventory[InventorySlot] = E;                
    }
}

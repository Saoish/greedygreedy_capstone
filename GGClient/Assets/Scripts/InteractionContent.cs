﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public abstract class InteractionContent : MonoBehaviour {
    public bool SyncActions = true;    

    public abstract void TurnOn();        
    public abstract void TurnOff();
    protected abstract void _Interrupt();
    protected abstract void _Resume();
    
    static InteractionContent CachedIC = null;

    private void OnEnable() {
        //if (CacheManager.MP.Casting) {
                                    
        //}
    }

    public static void Close() {
        if (Current)
            Current.TurnOff();
    }

    public static InteractionContent Current {
        get { return FindObjectOfType<InteractionContent>(); }
    }
 
    public static bool HasActive {        
        get { return Current != null; }
    }

    public static void Interrupt() {
        //Current._Interrupt();            
        foreach (var IC in FindObjectsOfType<InteractionContent>()) {
            if (IC.GetComponent<CanvasGroup>().interactable) {
                CachedIC = IC;
                IC._Interrupt();                
            }
        }
    }

    public static void Resume() {        
        GameManager.instance.StartCoroutine(ResumeWithDelay());
    }
    static private IEnumerator ResumeWithDelay() {        
        yield return new WaitForSeconds(0.1f);
        if (PopUpNotification.IsActive)
            yield break;
        else
            CachedIC._Resume();        
        //foreach (var IC in FindObjectsOfType<InteractionContent>()) {
        //    if (IC.GetComponent<CanvasGroup>().interactable)
        //        IC._Resume();
        //}
    }

    protected void SelectWithDelay(GameObject GO) {
        StartCoroutine(StartSelectProcess(GO));
    }
    
    protected IEnumerator StartSelectProcess(GameObject GO) {
        EventSystem.current.SetSelectedGameObject(null);
        yield return new WaitForSeconds(0.1f);
        EventSystem.current.SetSelectedGameObject(GO);
    }
}

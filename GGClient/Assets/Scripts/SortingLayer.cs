﻿using UnityEngine;
using System.Collections;

public static class SortingLayer {
    public static string Dynamic = "Dynamic";

    //public static string Map = "Map"; 

    //public static string Ground = "Ground";

    //public static string Environment = "Environment";

    //public static string Loot = "Loot";

    //public static string Object = "Object";

    //public static string Equip = "Equip";

    public static string Skill = "Skill";

    public static string Indication = "Indication";

    public static string MPUI = "MPUI";
}

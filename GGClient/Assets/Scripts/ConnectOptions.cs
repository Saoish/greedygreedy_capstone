﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectOptions : MonoBehaviour {
    public IPFetcher ip_fetecher;
    public GameObject Local;
    public GameObject Host;
    public GameObject Public;

    void Start() {
        Public.GetComponentInChildren<Button>().Select();
    }

    public void OnLocalButtonClick() {
        ip_fetecher.Enable();
        Client.FetchIP(Client.Mode.Local);
        Local.GetComponent<Image>().enabled = true;
        Host.GetComponent<Image>().enabled = false;
        Public.GetComponent<Image>().enabled = false;
    }

    public void OnHostButtonClick() {
        //Client.FetchIP(Client.ConnectOptions.Host);
        Local.GetComponent<Image>().enabled = false;
        Host.GetComponent<Image>().enabled = true;
        Public.GetComponent<Image>().enabled = false;
    }

    public void OnPublicButtonClick() {
        ip_fetecher.Disable();
        Client.FetchIP(Client.Mode.Public);
        Local.GetComponent<Image>().enabled = false;
        Host.GetComponent<Image>().enabled = false;
        Public.GetComponent<Image>().enabled = true;
    }
}

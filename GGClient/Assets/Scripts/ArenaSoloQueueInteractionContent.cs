﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Networking.Data;

using GreedyNameSpace;
using System;
using UnityEngine.UI;
public class ArenaSoloQueueInteractionContent : InteractionContent {    

    public GameObject CachedButtonOJ;

    public void OnButtonSelect() {
        AudioSource.PlayClipAtPoint(ActionSFX.EquipmentSelect, transform.position, GameManager.SFX_Volume);
    }
         

    void Update() {
        if (SyncActions && ControllerManager.Actions.Cancel.WasPressed) {
            if (gameObject.active) {
                TurnOff();       
            }
        }
    }

    protected override void _Interrupt() {
        CachedButtonOJ = EventSystem.current.currentSelectedGameObject;
        SyncActions = false;
        GetComponent<CanvasGroup>().interactable = false;
    }

    protected override void _Resume() {
        SyncActions = true;
        GetComponent<CanvasGroup>().interactable = true;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(CachedButtonOJ);
    }

    public override void TurnOn() {
        ControllerManager.SyncActions = false;
        gameObject.SetActive(true);
        SelectWithDelay(CachedButtonOJ);
    }

    public override void TurnOff() {
        if (EventSystem.current)
            CachedButtonOJ = EventSystem.current.currentSelectedGameObject;
        gameObject.SetActive(false);
        ControllerManager.SyncActions = true;
    }

    public void OneVOneOnClick() {
        if (MainPlayer.CQ != Queuemode.SoloQueue_1v1) {
            if (MainPlayer.InQueue) {
                PopUpNotification.Push("Your current queue will be dropped, do you want to procceed?", PopUpNotification.Type.Select);
                PopUpNotification.HereWeGo(QueueUpSoloQueue_1v1);
            }
            else if (MainPlayer.HasGravestone) {
                PopUpNotification.Push("Your Gravestone will be discarded, do you want to procceed?", PopUpNotification.Type.Select);
                PopUpNotification.HereWeGo(QueueUpSoloQueue_1v1);
            }
            else
                QueueUpSoloQueue_1v1();
        }
        else {
            RedNotification.Push(RedNotification.Type.DUP_QUEUE);
        }
    }void QueueUpSoloQueue_1v1() {                
        Client.Send(Protocols.Queue,Queuemode.SoloQueue_1v1, Client.TCP);
        MainPlayer.Queue(Queuemode.SoloQueue_1v1);
        
    }

    public void TwoVTwoOnClick() {
        if (MainPlayer.CQ != Queuemode.SoloQueue_2v2) {
            if (MainPlayer.InQueue) {
                PopUpNotification.Push("Your current queue will be dropped, do you want to procceed?", PopUpNotification.Type.Select);
                PopUpNotification.HereWeGo(QueueUpSoloQueue_2v2);
            }
            else if (MainPlayer.HasGravestone) {
                PopUpNotification.Push("Your Gravestone will be discarded, do you want to procceed?", PopUpNotification.Type.Select);
                PopUpNotification.HereWeGo(QueueUpSoloQueue_2v2);
            }
            else
                QueueUpSoloQueue_2v2();
        }
        else {
            RedNotification.Push(RedNotification.Type.DUP_QUEUE);
        }
    }
    void QueueUpSoloQueue_2v2() {
        Client.Send(Protocols.Queue, Queuemode.SoloQueue_2v2, Client.TCP);
        MainPlayer.Queue(Queuemode.SoloQueue_2v2);                
    }


    public void OnClickDropQueue() {
        if (MainPlayer.InQueue) {
            PopUpNotification.Push("Are you sure?", PopUpNotification.Type.Select);
            PopUpNotification.HereWeGo(DropQueue);
        }
        else {
            RedNotification.Push(RedNotification.Type.NO_QUEUE);
        }
    }void DropQueue() {
        Client.SendProtocol(Protocols.Drop,Client.TCP);
        MainPlayer.DropQueue();
    }

}

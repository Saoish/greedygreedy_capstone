﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GreedyNameSpace;
using Networking.Data;
[System.Serializable]
public class GraveData {
    public int OwnerNetworkID;
    public GraveStoneID ModelID;
    public string Name;
    public CLASS Class;
    public int Lvl;
    public string Message;
    public SceneCoordinate SC;

    public GraveData(int OwnerNetworkID,GraveStoneID ModelID, string PlayerName, CLASS PlayerClass, int PlayerLvl, string Message, SceneCoordinate SC) {
        this.OwnerNetworkID = OwnerNetworkID;
        this.ModelID = ModelID;
        this.Name = PlayerName;
        this.Class = PlayerClass;
        this.Lvl = PlayerLvl;
        this.Message = Message;
        this.SC = SC;
    }
}

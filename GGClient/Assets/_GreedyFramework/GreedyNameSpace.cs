﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace GreedyNameSpace {
    public static class Patch {
        //public static float Version = 1.46f;        
        public static int UsernameCharLimit = 16;
        public static int CharacterNameCharLimit = 12;
        public static int GraveNoteCharLimit = 258;     

        public static int NumOfStatsFields = 13;
        public static int LvlCap = 50;
        public static float ItemStats_ScaleDown = 0.1f;
        public static int InventoryCapacity = 40;
        public static int StashCapacity = 240;
        public static int SkillTreeSize = 18;
        public static int SkillSlots = 4;
        public static int CharacterSlots = 6;
        public static int MaxItemlvl = LvlCap + (int)RARITY.Mythic;
        public static int MaxSkilllvl = 5;

        public static int Tier1_Req = 0;
        public static int Tier2_Req = 10;
        public static int Tier3_Req = 25;

        public static int NumberOfStashes = 6;

        public static int HeartlessRate = 2;

        public static int RespecCostIncRate = 1000;

        public static float ReviveUponResetHealthPortion = 1f;//Just for now
        public static float ReviveUponHelp = 0.3f;
        public static float ReviveTime = 5f;
        
        static public int GetRequiredExp(int NextLvl) {
            float e = 0;
            for (int i = 1; i < NextLvl; i++)
                e += Mathf.Floor(i + 300 * Mathf.Pow(2, (i / 7)));
            return (int)e;
        }

        public static float[] GetGrowth(CLASS C) {
            switch (C) {
                case CLASS.Warrior:
                    return new float[] {
                        20, //HEALTH
                        0, //ESSENCE
                        1, //DAMAGE
                        0.1f, //DEFENSE
                        0, //PENETRATION
                        0, //ATTACK_SPEED
                        0, //MOVE_SPEED
                        0, //CRIT_CHANCE
                        0, //CRIT_DMG
                        0, //LPH
                        0, //HASTE
                        0, //HEALTH_REGEN
                        0 }; //ESSENCE_REGEN
                case CLASS.Mage:
                    return new float[] {
                        15, //HEALTH
                        0.1f, //ESSENCE
                        1, //DAMAGE
                        0f, //DEFENSE
                        0, //PENETRATION
                        0, //ATTACK_SPEED
                        0, //MOVE_SPEED
                        0, //CRIT_CHANCE
                        0, //CRIT_DMG
                        0, //LPH
                        0, //HASTE
                        0, //HEALTH_REGEN
                        0 }; //ESSENCE_REGEN
                case CLASS.Rogue:
                    return new float[] { 0, 0, 0 };//Not yet implemented
                default:
                    Debug.Log("Invalid Base Growth");
                    return null;
            }
        }
    }


    public enum Side {
        Blue = 0,
        Red = 1000
    }

    public static class SceneChecker {
        public static List<SceneID> UnLogables = new List<SceneID>() {//Non-WorldStates
            SceneID.StartMenu,
            SceneID.CharacterCreation,
            SceneID.CharacterSelection,
            SceneID.Developing,
            SceneID.RootOfEvil,
            SceneID.MysteriousWell,
            SceneID.FrankensteinCastle,
            SceneID.Loading
        };

        public static List<SceneID> UnPlayables = new List<SceneID>() {
            SceneID.StartMenu,
            SceneID.CharacterCreation,
            SceneID.CharacterSelection,
            SceneID.Loading
        };

        public static List<SceneID> Arenas = new List<SceneID>() {
            SceneID.RootOfEvil,
            SceneID.MysteriousWell
        };

        public static List<SceneID> Dungeons = new List<SceneID>() {
            SceneID.FrankensteinCastle
        };

        public static List<SceneID> WordStates = new List<SceneID>() {
            SceneID.Tyneham
        };

        public static bool TargetInWorldStates(SceneID SID) {
            return WordStates.Contains(SID);
        }

        public static bool TargetInUnLogables(SceneID SID) {
            return UnLogables.Contains(SID);
        }

        public static bool TargetInUnPlayables(SceneID SID) {
            return UnPlayables.Contains(SID);
        }

        public static bool TargetInDungeons(SceneID SID) {
            return Dungeons.Contains(SID);
        }

        public static bool TargetInArenas(SceneID SID) {
            return Arenas.Contains(SID);
        }
    }
    public enum SceneID {        
        StartMenu,
        CharacterSelection,
        CharacterCreation,
        Tyneham,
        RootOfEvil,
        MysteriousWell,
        FrankensteinCastle,
        Developing,
        Loading
    }

    public enum CLASS {
        Warrior,
        Mage,
        Rogue,
        All
    };

    public enum SkillPath {
        Berserker, Warlord,
        Dominance, Destruction
    }
    public enum STATSTYPE {
        HEALTH,
        ESSENCE,
        DAMAGE,
        DEFENSE,
        PENETRATION,
        ATTACK_SPEED,
        MOVE_SPEED,
        CRIT_CHANCE,
        CRIT_DMG,
        LPH,
        HASTE,
        HEALTH_REGEN,
        ESSENCE_REGEN
    }
    public static class StatsType {
        public static StringPair GetStatsTypeString(STATSTYPE type) {
            return GetStatsTypeString((int)type);
        }
        public static string GetStatsTypeDescription(int Slot) {
            STATSTYPE _type = (STATSTYPE)Slot;
            switch (_type) {
                case STATSTYPE.HEALTH:
                    return "Your hit points, you will die once it reaches 0.";
                case STATSTYPE.ESSENCE:
                    return "Your resource to use active skills.";
                case STATSTYPE.DAMAGE:
                    return "Direct damage you could do.";
                case STATSTYPE.ATTACK_SPEED:
                    return "Determine how fast you can auto attack.";
                case STATSTYPE.MOVE_SPEED:
                    return "Determine how fast you can move.";
                case STATSTYPE.DEFENSE:
                    return "Mitigate incoming direct damage.";
                case STATSTYPE.PENETRATION:
                    return "Ignore your target's defense while dealing direct damage.";
                case STATSTYPE.CRIT_CHANCE:
                    return "Gives you chance to heavily damage your foes while dealing direct damage.";
                case STATSTYPE.CRIT_DMG:
                    return "Determine how heavily you will damage your foes when your direct damage is critical.";
                case STATSTYPE.LPH:
                    return "Generate health from dealing direct daamge using auto attack.";
                case STATSTYPE.HASTE:
                    return "An ability to increase your skills' charging speed and mitigate your skills' cooldowns.";
                case STATSTYPE.HEALTH_REGEN:
                    return "An ability to gain health every 5 seconds.";
                case STATSTYPE.ESSENCE_REGEN:
                    return "An ability to gain essence every second.";
            }
            return "";
        }

        public static StringPair GetStatsTypeString(int type) {
            STATSTYPE _type = (STATSTYPE)type;
            switch (_type) {
                case STATSTYPE.HEALTH:
                    return new StringPair("Health", "");
                case STATSTYPE.ESSENCE:
                    return new StringPair("Essence", "");
                case STATSTYPE.DAMAGE:
                    return new StringPair("Damage", "");
                case STATSTYPE.ATTACK_SPEED:
                    return new StringPair("Attack Speed", "%");
                case STATSTYPE.MOVE_SPEED:
                    return new StringPair("Move Speed", "%");
                case STATSTYPE.DEFENSE:
                    return new StringPair("Defense", "%");
                case STATSTYPE.PENETRATION:
                    return new StringPair("Penetration", "%");
                case STATSTYPE.CRIT_CHANCE:
                    return new StringPair("Critical Chance", "%");
                case STATSTYPE.CRIT_DMG:
                    return new StringPair("Critical Damaga", "%");
                case STATSTYPE.LPH:
                    return new StringPair("Life/Hit", "%");
                case STATSTYPE.HASTE:
                    return new StringPair("Haste", "%");
                case STATSTYPE.HEALTH_REGEN:
                    return new StringPair("Health Regen", "/5s");
                case STATSTYPE.ESSENCE_REGEN:
                    return new StringPair("Essence Regen", "/s");
            }
            return new StringPair("", "");
        }
    }

    public enum GraveStoneID {
        Crosswood = 0,
        Root = 1,
        Residue = 2
    }

    public enum EQUIPTYPE {
        Helmet,
        Chest,
        Shackle,
        Weapon,
        Trinket
    };
    public enum WEAPONTYPE {        
        WarriorTwoHanded,
        WarriorOneHandedShield,

        MageMagicOrb,
        MageStaff,

        RogueOneHanded,
        RogueTwoHanded
    }
    public static class WeaponTypeInString {
        public static string GetString(WEAPONTYPE WT) {
            switch (WT) {
                case WEAPONTYPE.WarriorTwoHanded:
                    return "Two-Handed Weapon";
                case WEAPONTYPE.WarriorOneHandedShield:
                    return "One-Handed Weapon & Shield";
                case WEAPONTYPE.MageMagicOrb:
                    return "Magic Orb";
                case WEAPONTYPE.MageStaff:
                    return "Staff";
                case WEAPONTYPE.RogueOneHanded:
                    return "One-Handed Weapon";
                case WEAPONTYPE.RogueTwoHanded:
                    return "Two-Handed Weapon";
                default:
                    return "Invalid Weapon Type";
            }
        }
    }
    public enum RARITY {
        Common = 0,
        Fine = 2,
        Pristine = 4,
        Legendary = 6,
        Mythic = 8
    }
    public enum RARITYRATE {
        Common = 100,
        Fine = 20,
        Pristine = 10,
        Legendary = 5,
        Mythic = 1
    }
    public enum EQUIPSET {
        None = 0,
        //Warriors
        Skeleton = 1,
        //Mages
        Conjurer
    }


    //--------------
    public static class DamageCalculation {
        public static float GetNotTrueDamage(float RawDamage, float TargetDefense, float SelfPenetration) {
            float Diff = TargetDefense - SelfPenetration;
            if (Diff < 0) {
                Diff = 0;
            }
            float reduced_dmg = RawDamage * Diff / 100;
            return RawDamage - reduced_dmg >= 1 ? Mathf.Ceil(RawDamage - reduced_dmg) : 1;
        }
    }

    public static class MyString {
        public static string ParseWithoutSpace(string Str) {
            return Str.Replace(" ", string.Empty);
        }
    }

    public static class MyText {
        public static string black = "#000000ff", blue = "#0000ffff", brown = "#a52a2aff", cyan = "#00ffffff", darkblue = "#0000a0ff", megenta = "#ff00ffff", green = "#008000ff", grey = "#808080ff", lightblue = "#add8e6ff", lime = "#00ff00ff", maroon = "#800000ff", navy = "#000080ff", olive = "#808000ff", orange = "#ffa500ff", purple = "#800080ff", red = "#ff0000ff", silve = "#c0c0c0ff", teal = "#008080ff", white = "#ffffffff", yellow = "#ffff00ff";        
        static public string Colofied<T>(T content, string color) {
            return "<color=" + color + ">" + content.ToString() + "</color>";
        }
    }

    public static class MyColor {
        static public Color LightGrey = new Color(0.76f, 0.76f, 0.76f, 1f);
        static public Color White = Color.white;
        static public Color Cyan = Color.cyan;
        static public Color Yellow = Color.yellow;
        static public Color Orange = new Color(1f, 0.65f, 0f, 1f);
        static public Color Green = Color.green;
        static public Color Red = Color.red;
        static public Color Blue = Color.blue;
        static public Color IronBlue = new Color(0.38f, 0.53f, 0.71f, 1f);
        static public Color Grey = Color.grey;
        static public Color Purple = new Color(1f, 0, 1f, 1f);
        static public Color Pink = new Color(1f, 0.4f, 0.7f, 1f);

        static public Color Common = Color.white;
        static public Color Fine = Color.cyan;
        static public Color Pristine = Color.yellow;
        static public Color Legendary = MyColor.Orange;
        static public Color Mythic = MyColor.Purple;
    }
        
    public enum OID { EnemyMonster,FriendlyMonster, Main, FriendlyPlayer, EnemyPlayer,NPC };
    public enum GreedyForceMode { Addictive, Override };

    public enum Queuemode {       
        None, 
        SoloQueue_1v1,
        SoloQueue_2v2
    }

    public enum CR {//Combat Register, sequnce the the enum before release
        None,

        Rune,

        //Specal Tyeps 
        MeleeAttack,
        RangedAttack,
        HealthRegen,
        
        WarriorTwoHandedWeaponConsume,
        WarriorOneHandedShieldConsume,
        MageMagicOrbConsume,
        MageStaffConsume,
        //Specal Tyeps

        FuryBuff,
        HealingBuff,
        RageBuff,
        BloodForBloodBuff,
        FocusBuff,

        BleedDebuff,        
        CrippleDebuff,
        RetaliationDebuff,
        RuinDebuff,
        StunDebuff,
        ChillDebuff,
        FrozenImmobolizeDebuff,
        CharDebuff,
        UnbridledWrathDebuff,        

        BloodyHand,
        BattleFury,
        Cleave,
        Fury,
        PhantomCharge,
        StandingStill,
        WarStomp,
        Bash,
        BloodForBlood,
        BrutalWeaponMastery,
        Cripple,
        Grit,
        IronWeaponMastery,
        IronWill,
        Rage,
        Retaliation,
        Ruin,
        Vigor,
        EndlessCourage,

        Chill,
        Blink,
        Aptitude,
        Omniscience,
        FrostNova,
        EyeoftheStorm,
        MagicOrbMastery,
        Focus,
        FlashFreeze,
        WickedIntent,
        ShootingStar,
        Radiation,
        Char,
        LightingBlaze,
        Electromagnetic,
        StaffMastery,
        UnbridledWrath,
        LightingStrike,

        SpiritofShootingStar
    }

    public enum SNID {        
        MasterSifu = -1,
        MrBlack = -2,

        TestSlime = -3
    }

    public enum CastSyncAxis {//For controller only
        Left,
        Right
    }
}

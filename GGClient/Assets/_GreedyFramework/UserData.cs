﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;
using System;
using Networking.Data;

[System.Serializable]
public class AxisMapping {//Each player based
    public List<CastSyncAxis> Maps;
    public AxisMapping() {
        Maps = new List<CastSyncAxis> { CastSyncAxis.Left, CastSyncAxis.Left, CastSyncAxis.Left, CastSyncAxis.Left };        
    }
    public void Switch(int Slot) {
        if (Maps[Slot] == CastSyncAxis.Left)
            Maps[Slot] = CastSyncAxis.Right;
        else
            Maps[Slot] = CastSyncAxis.Left;
    }
    public CastSyncAxis Get(int Slot) {
        return Maps[Slot];
    }
}

[System.Serializable]
public class UserData{
    public int Souls = 0;
    public int Paragon = 0;
    public List<PlayerData> PlayerDatas;
    public List<Equipment> Stash;
    public List<StaticObjectData> WorldStates;

    //Achievement
    public List<GraveStoneID> GraveStones;

    //Settings    
    public List<AxisMapping> AxisMappings;    

    public UserData(int pre_lvl = 1) {        
        PlayerDatas = new List<PlayerData>();
        for(int i = 0; i < Patch.CharacterSlots; i++) {            
            PlayerDatas.Add(new PlayerData(pre_lvl));
        }
        Stash = new List<Equipment>();
        for(int i = 0; i< Patch.StashCapacity; i++) {
            Stash.Add(new Equipment());
        }
        GenerateDefaultWorldStates();

        //Achievement                
        GraveStones = new List<GraveStoneID> { GraveStoneID.Crosswood, GraveStoneID.Root, GraveStoneID.Residue };

        //Settings
        AxisMappings = new List<AxisMapping>();
        for (int i = 0; i<Patch.CharacterSlots;i++) {
            AxisMappings.Add(new AxisMapping());
        }        
    }

    public void ResetToDefaultWorldStates() {
        GenerateDefaultWorldStates();
    }

    private void GenerateDefaultWorldStates() {
        WorldStates = new List<StaticObjectData>() {
            new StaticObjectData(SNID.MasterSifu, 10f, new SceneCoordinate(SceneID.Tyneham, new Vector2(-1.035f, 4.434f))),
            new StaticObjectData(SNID.MrBlack,10f,new SceneCoordinate(SceneID.Tyneham,new Vector2(1.782f,3.694f)))
            //new StaticObjectData(SNID.TestSlime,10f,new SceneCoordinate(SceneID.Tyneham,new Vector2(1.44f,1.78f)),10)
        };
    }
    
}

﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
[System.Serializable]
public class EssenseLoss : Value {
    public EssenseLoss(float RawEssenseLoss, bool Crit,int applyerID,int targetID, CR Type,bool TraceBack) {
        this.Amount = RawEssenseLoss;
        this.Crit = Crit;
        this.applyerID = applyerID;
        this.targetID = targetID;
        this.Type = Type;
        this.TraceBack = TraceBack;
    }

    private float EssenseLossCalculation(float RawEssenseLoss) {
        return Mathf.Ceil(RawEssenseLoss);
    }
}
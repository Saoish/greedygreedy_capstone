﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GreedyNameSpace;

[System.Serializable]
public struct BoxTrigger {
    public float Start;
    public float End;
}

[System.Serializable]
public struct StatsRangeField {
    public STATSTYPE type;
    public Vector2 stats_range;
}

[System.Serializable]
public struct SetStatsField {
    public enum ValueType {
        Raw,
        Percentage
    }
    public STATSTYPE stats_type;
    public ValueType value_type;
    public float value;
}

[System.Serializable]
public struct Loot {
    public float Rate;
    public GameObject Item;
}

[System.Serializable]
public struct StringPair {
    public string F;
    public string S;
    public StringPair(string F, string S) {
        this.F = F;
        this.S = S;
    }
}
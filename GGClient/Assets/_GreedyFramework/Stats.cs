﻿using UnityEngine;
using System.Collections;
using GreedyNameSpace;
using System.Collections.Generic;

[System.Serializable]
public class Stats {

    //public static int Size = 13;

    public float[] stats;


    public enum InitStatsType {
        OBJECT,
        EQUIP
    }

    public int Size {
        get { return stats.Length; }
    }

    public bool IsNull {
        get { return this==null ||stats == null || stats.Length < Patch.NumOfStatsFields; }
    }        

    public void Grow(float[] stats) {
        if (stats.Length != Patch.NumOfStatsFields) {
            Debug.Log("Stats.Grow: stats given has invalid number of fields.");
            return;
        }
        for(int i = 0; i < stats.Length; i++) {
            this.stats[i] += stats[i];
        }            
    }

    public Stats(Stats copy) {
        this.stats = new float[Patch.NumOfStatsFields];
        for (int i = 0; i < Patch.NumOfStatsFields; i++) {
            this.Set(i, copy.Get(i));
        }
    }

    public Stats(InitStatsType type = InitStatsType.OBJECT) {
        switch (type) {
            case InitStatsType.OBJECT:
                stats = new float[] {
                100, //HEALTH
                100, //ESSENCE
                0,   //DAMAGE
                0,   //DEFENSE
                0,   //PENETRATION
                100, //ATTACK_SPEED
                100, //MOVE_SPEED
                0,   //CRIT_CHANCE
                200, //CRIT_DMG
                0,   //LPH
                0,   //HASTE
                0,   //HEALTH_REGEN
                10   //ESSENCE_REGEN
                };
                break;
            case InitStatsType.EQUIP:
                stats = new float[Patch.NumOfStatsFields];
                break;
        }
    }    
    public float Health {
        get { return Get(STATSTYPE.HEALTH); }
        set { Set(STATSTYPE.HEALTH, value); }
    }
    public float Essense {
        get { return Get(STATSTYPE.ESSENCE); }
        set { Set(STATSTYPE.ESSENCE, value); }
    }
    public float Damage {
        get { return Get(STATSTYPE.DAMAGE); }
        set { Set(STATSTYPE.DAMAGE, value); }
    }
    public float Defense {
        get { return Get(STATSTYPE.DEFENSE); }
        set { Set(STATSTYPE.DEFENSE, value); }
    }
    public float Penetration {
        get { return Get(STATSTYPE.PENETRATION); }
        set { Set(STATSTYPE.PENETRATION, value); }
    }
    public float Attack_Speed {
        get { return Get(STATSTYPE.ATTACK_SPEED); }
        set { Set(STATSTYPE.ATTACK_SPEED, value); }
    }
    public float Move_Speed {
        get { return Get(STATSTYPE.MOVE_SPEED); }
        set { Set(STATSTYPE.MOVE_SPEED, value); }
    }
    public float Crit_Chance {
        get { return Get(STATSTYPE.CRIT_CHANCE); }
        set { Set(STATSTYPE.CRIT_CHANCE, value); }
    }
    public float Crit_Dmg {
        get { return Get(STATSTYPE.CRIT_DMG); }
        set { Set(STATSTYPE.CRIT_DMG, value); }
    }
    public float LPH {
        get { return Get(STATSTYPE.LPH); }
        set { Set(STATSTYPE.LPH, value); }
    }
    public float Haste {
        get { return Get(STATSTYPE.HASTE); }
        set { Set(STATSTYPE.HASTE, value); }
    }
    public float Health_Regen {
        get { return Get(STATSTYPE.HEALTH_REGEN); }
        set { Set(STATSTYPE.HEALTH_REGEN, value); }
    }
    public float Essense_Regen {
        get { return Get(STATSTYPE.ESSENCE_REGEN); }
        set { Set(STATSTYPE.ESSENCE_REGEN, value); }
    }


    public void Set(STATSTYPE type, float value) {
        stats[(int)type] = value;
    }

    public void Set(int type, float value) {
        stats[type] = value;
    }

    public float Get(STATSTYPE type) {
        return stats[(int)type];
    }

    public float Get(int type) {
        return stats[type];
    }

    public void Add(STATSTYPE type, float value) {
        stats[(int)type] += value;
    }

    public void Add(int type, float value) {
        stats[type] += value;
    }

    public void Dec(STATSTYPE type, float value) {
        if (stats[(int)type] - value >= 0)
            stats[(int)type] -= value;
        else
            stats[(int)type] = 0;
    }

    public void Dec(int type, float value) {
        if (stats[type] - value >= 0)
            stats[type] -= value;
        else
            stats[type] = 0;
    }

}

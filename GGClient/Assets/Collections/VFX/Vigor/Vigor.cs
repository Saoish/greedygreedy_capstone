﻿using UnityEngine;
using System.Collections;
using System;
using GreedyNameSpace;

public class Vigor : PassiveSkill {
    public override CR Type { get { return CR.Vigor; } }
    float Damage_INC_Percentage;

    protected override string DescriptionTemplate(Skilllvl[] AllLvls, int Index) {
        Vigorlvl VL = (Vigorlvl)AllLvls[Index];
        return "\nIncrease your "+ MyText.Colofied("Damage",ScaleHighlight)+" by " + MyText.Colofied(VL.Damage_INC_Percentage+"%",ScaleHighlight) + ".";
    }

    public override void InitSkill(ObjectController OC, int lvl) {
        base.InitSkill(OC, lvl);
        Vigorlvl VL = null;
        switch (this.lvl) {
            case 0:
                return;
            case 1:
                VL = GetComponent<Vigor1>();
                break;
            case 2:
                VL = GetComponent<Vigor2>();
                break;
            case 3:
                VL = GetComponent<Vigor3>();
                break;
            case 4:
                VL = GetComponent<Vigor4>();
                break;
            case 5:
                VL = GetComponent<Vigor5>();
                break;
        }
        Damage_INC_Percentage = VL.Damage_INC_Percentage;

        GenerateDescription();
    }

    public override void ApplyPassive() {        
        float damage_inc_value =(float)System.Math.Round(OC.GetMaxStats(STATSTYPE.DAMAGE) * (Damage_INC_Percentage / 100),0);
        OC.AddMaxStats(STATSTYPE.DAMAGE, damage_inc_value);        
    }
}
